﻿using UnityEditor;

namespace IngameDebugConsole
{
    [CustomEditor(typeof(DebugLogManager))]
    public class DebugLogManagerEditor : Editor
    {
        private SerializedProperty singleton;
        private SerializedProperty minimumHeight;
        private SerializedProperty enableHorizontalResizing;
        private SerializedProperty resizeFromRight;
        private SerializedProperty minimumWidth;
        private SerializedProperty enablePopup;
        private SerializedProperty startInPopupMode;
        private SerializedProperty startMinimized;
        private SerializedProperty toggleWithKey;
        private SerializedProperty toggleKey;
        private SerializedProperty enableSearchbar;
        private SerializedProperty topSearchbarMinWidth;
        private SerializedProperty captureLogTimestamps;
        private SerializedProperty alwaysDisplayTimestamps;
        private SerializedProperty clearCommandAfterExecution;
        private SerializedProperty commandHistorySize;
        private SerializedProperty showCommandSuggestions;
        private SerializedProperty receiveLogcatLogsInAndroid;
        private SerializedProperty logcatArguments;

        private void OnEnable()
        {
            this.singleton = this.serializedObject.FindProperty("singleton");
            this.minimumHeight = this.serializedObject.FindProperty("minimumHeight");
            this.enableHorizontalResizing = this.serializedObject.FindProperty("enableHorizontalResizing");
            this.resizeFromRight = this.serializedObject.FindProperty("resizeFromRight");
            this.minimumWidth = this.serializedObject.FindProperty("minimumWidth");
            this.enablePopup = this.serializedObject.FindProperty("enablePopup");
            this.startInPopupMode = this.serializedObject.FindProperty("startInPopupMode");
            this.startMinimized = this.serializedObject.FindProperty("startMinimized");
            this.toggleWithKey = this.serializedObject.FindProperty("toggleWithKey");
#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
            this.toggleKey = this.serializedObject.FindProperty("toggleBinding");
#else
			toggleKey = serializedObject.FindProperty( "toggleKey" );
#endif
            this.enableSearchbar = this.serializedObject.FindProperty("enableSearchbar");
            this.topSearchbarMinWidth = this.serializedObject.FindProperty("topSearchbarMinWidth");
            this.captureLogTimestamps = this.serializedObject.FindProperty("captureLogTimestamps");
            this.alwaysDisplayTimestamps = this.serializedObject.FindProperty("alwaysDisplayTimestamps");
            this.clearCommandAfterExecution = this.serializedObject.FindProperty("clearCommandAfterExecution");
            this.commandHistorySize = this.serializedObject.FindProperty("commandHistorySize");
            this.showCommandSuggestions = this.serializedObject.FindProperty("showCommandSuggestions");
            this.receiveLogcatLogsInAndroid = this.serializedObject.FindProperty("receiveLogcatLogsInAndroid");
            this.logcatArguments = this.serializedObject.FindProperty("logcatArguments");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.singleton);
            EditorGUILayout.PropertyField(this.minimumHeight);

            EditorGUILayout.PropertyField(this.enableHorizontalResizing);
            if (this.enableHorizontalResizing.boolValue)
            {
                DrawSubProperty(this.resizeFromRight);
                DrawSubProperty(this.minimumWidth);
            }

            EditorGUILayout.PropertyField(this.enablePopup);
            if (this.enablePopup.boolValue)
            {
                DrawSubProperty(this.startInPopupMode);
            }
            else
            {
                DrawSubProperty(this.startMinimized);
            }

            EditorGUILayout.PropertyField(this.toggleWithKey);
            if (this.toggleWithKey.boolValue)
            {
                DrawSubProperty(this.toggleKey);
            }

            EditorGUILayout.PropertyField(this.enableSearchbar);
            if (this.enableSearchbar.boolValue)
            {
                DrawSubProperty(this.topSearchbarMinWidth);
            }

            EditorGUILayout.PropertyField(this.captureLogTimestamps);
            if (this.captureLogTimestamps.boolValue)
            {
                DrawSubProperty(this.alwaysDisplayTimestamps);
            }

            EditorGUILayout.PropertyField(this.clearCommandAfterExecution);
            EditorGUILayout.PropertyField(this.commandHistorySize);
            EditorGUILayout.PropertyField(this.showCommandSuggestions);

            EditorGUILayout.PropertyField(this.receiveLogcatLogsInAndroid);
            if (this.receiveLogcatLogsInAndroid.boolValue)
            {
                DrawSubProperty(this.logcatArguments);
            }

            DrawPropertiesExcluding(this.serializedObject, "m_Script");
            this.serializedObject.ApplyModifiedProperties();
        }

        private void DrawSubProperty(SerializedProperty property)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(property);
            EditorGUI.indentLevel--;
        }
    }
}