﻿//#define IDG_OMIT_ELAPSED_TIME
//#define IDG_OMIT_FRAMECOUNT

using System.Text;
using UnityEngine;

// Container for a simple debug entry
namespace IngameDebugConsole
{
    public class DebugLogEntry : System.IEquatable<DebugLogEntry>
    {
        private const int HASH_NOT_CALCULATED = -623218;

        public string logString;
        public string stackTrace;

        private string completeLog;

        // Sprite to show with this entry
        public Sprite logTypeSpriteRepresentation;

        // Collapsed count
        public int count;

        private int hashValue;

        public void Initialize(string logString, string stackTrace)
        {
            this.logString = logString;
            this.stackTrace = stackTrace;

            this.completeLog = null;
            this.count = 1;
            this.hashValue = HASH_NOT_CALCULATED;
        }

        // Check if two entries have the same origin
        public bool Equals(DebugLogEntry other)
        {
            return this.logString == other.logString && this.stackTrace == other.stackTrace;
        }

        // Checks if logString or stackTrace contains the search term
        public bool MatchesSearchTerm(string searchTerm)
        {
            return (this.logString != null && this.logString.IndexOf(searchTerm, System.StringComparison.OrdinalIgnoreCase) >= 0) ||
                (this.stackTrace != null && this.stackTrace.IndexOf(searchTerm, System.StringComparison.OrdinalIgnoreCase) >= 0);
        }

        // Return a string containing complete information about this debug entry
        public override string ToString()
        {
            if (this.completeLog == null)
            {
                this.completeLog = string.Concat(this.logString, "\n", this.stackTrace);
            }

            return this.completeLog;
        }

        // Credit: https://stackoverflow.com/a/19250516/2373034
        public override int GetHashCode()
        {
            if (this.hashValue == HASH_NOT_CALCULATED)
            {
                unchecked
                {
                    this.hashValue = 17;
                    this.hashValue = this.hashValue * 23 + (this.logString == null ? 0 : this.logString.GetHashCode());
                    this.hashValue = this.hashValue * 23 + (this.stackTrace == null ? 0 : this.stackTrace.GetHashCode());
                }
            }

            return this.hashValue;
        }
    }

    public struct QueuedDebugLogEntry
    {
        public readonly string logString;
        public readonly string stackTrace;
        public readonly LogType logType;

        public QueuedDebugLogEntry(string logString, string stackTrace, LogType logType)
        {
            this.logString = logString;
            this.stackTrace = stackTrace;
            this.logType = logType;
        }

        // Checks if logString or stackTrace contains the search term
        public bool MatchesSearchTerm(string searchTerm)
        {
            return (this.logString != null && this.logString.IndexOf(searchTerm, System.StringComparison.OrdinalIgnoreCase) >= 0) ||
                (this.stackTrace != null && this.stackTrace.IndexOf(searchTerm, System.StringComparison.OrdinalIgnoreCase) >= 0);
        }
    }

    public struct DebugLogEntryTimestamp
    {
        public readonly System.DateTime dateTime;
#if !IDG_OMIT_ELAPSED_TIME
        public readonly float elapsedSeconds;
#endif
#if !IDG_OMIT_FRAMECOUNT
        public readonly int frameCount;
#endif

        public DebugLogEntryTimestamp(System.TimeSpan localTimeUtcOffset)
        {
            // It is 10 times faster to cache local time's offset from UtcNow and add it to UtcNow to get local time at any time
            this.dateTime = System.DateTime.UtcNow + localTimeUtcOffset;
#if !IDG_OMIT_ELAPSED_TIME
            this.elapsedSeconds = Time.realtimeSinceStartup;
#endif
#if !IDG_OMIT_FRAMECOUNT
            this.frameCount = Time.frameCount;
#endif
        }

        public void AppendTime(StringBuilder sb)
        {
            // Add DateTime in format: [HH:mm:ss]
            sb.Append("[");

            int hour = this.dateTime.Hour;
            if (hour >= 10)
            {
                sb.Append(hour);
            }
            else
            {
                sb.Append("0").Append(hour);
            }

            sb.Append(":");

            int minute = this.dateTime.Minute;
            if (minute >= 10)
            {
                sb.Append(minute);
            }
            else
            {
                sb.Append("0").Append(minute);
            }

            sb.Append(":");

            int second = this.dateTime.Second;
            if (second >= 10)
            {
                sb.Append(second);
            }
            else
            {
                sb.Append("0").Append(second);
            }

            sb.Append("]");
        }

        public void AppendFullTimestamp(StringBuilder sb)
        {
            AppendTime(sb);

#if !IDG_OMIT_ELAPSED_TIME && !IDG_OMIT_FRAMECOUNT
            // Append elapsed seconds and frame count in format: [1.0s at #Frame]
            sb.Append("[").Append(this.elapsedSeconds.ToString("F1")).Append("s at ").Append("#").Append(this.frameCount).Append("]");
#elif !IDG_OMIT_ELAPSED_TIME
			// Append elapsed seconds in format: [1.0s]
			sb.Append( "[" ).Append( elapsedSeconds.ToString( "F1" ) ).Append( "s]" );
#elif !IDG_OMIT_FRAMECOUNT
			// Append frame count in format: [#Frame]
			sb.Append( "[#" ).Append( frameCount ).Append( "]" );
#endif
        }
    }
}