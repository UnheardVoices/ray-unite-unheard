﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Manager class for the debug popup
namespace IngameDebugConsole
{
    public class DebugLogPopup : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private RectTransform popupTransform;

        // Dimensions of the popup divided by 2
        private Vector2 halfSize;

        // Background image that will change color to indicate an alert
        private Image backgroundImage;

        // Canvas group to modify visibility of the popup
        private CanvasGroup canvasGroup;

#pragma warning disable 0649
        [SerializeField]
        private DebugLogManager debugManager;

        [SerializeField]
        private Text newInfoCountText;
        [SerializeField]
        private Text newWarningCountText;
        [SerializeField]
        private Text newErrorCountText;

        [SerializeField]
        private Color alertColorInfo;
        [SerializeField]
        private Color alertColorWarning;
        [SerializeField]
        private Color alertColorError;
#pragma warning restore 0649

        // Number of new debug entries since the log window has been closed
        private int newInfoCount = 0, newWarningCount = 0, newErrorCount = 0;

        private Color normalColor;

        private bool isPopupBeingDragged = false;
        private Vector2 normalizedPosition;

        // Coroutines for simple code-based animations
        private IEnumerator moveToPosCoroutine = null;

        private void Awake()
        {
            this.popupTransform = (RectTransform)this.transform;
            this.backgroundImage = GetComponent<Image>();
            this.canvasGroup = GetComponent<CanvasGroup>();

            this.normalColor = this.backgroundImage.color;

            this.halfSize = this.popupTransform.sizeDelta * 0.5f;

            Vector2 pos = this.popupTransform.anchoredPosition;
            if (pos.x != 0f || pos.y != 0f)
            {
                this.normalizedPosition = pos.normalized; // Respect the initial popup position set in the prefab
            }
            else
            {
                this.normalizedPosition = new Vector2(0.5f, 0f); // Right edge by default
            }
        }

        public void NewLogsArrived(int newInfo, int newWarning, int newError)
        {
            if (newInfo > 0)
            {
                this.newInfoCount += newInfo;
                this.newInfoCountText.text = this.newInfoCount.ToString();
            }

            if (newWarning > 0)
            {
                this.newWarningCount += newWarning;
                this.newWarningCountText.text = this.newWarningCount.ToString();
            }

            if (newError > 0)
            {
                this.newErrorCount += newError;
                this.newErrorCountText.text = this.newErrorCount.ToString();
            }

            if (this.newErrorCount > 0)
            {
                this.backgroundImage.color = this.alertColorError;
            }
            else if (this.newWarningCount > 0)
            {
                this.backgroundImage.color = this.alertColorWarning;
            }
            else
            {
                this.backgroundImage.color = this.alertColorInfo;
            }
        }

        private void Reset()
        {
            this.newInfoCount = 0;
            this.newWarningCount = 0;
            this.newErrorCount = 0;

            this.newInfoCountText.text = "0";
            this.newWarningCountText.text = "0";
            this.newErrorCountText.text = "0";

            this.backgroundImage.color = this.normalColor;
        }

        // A simple smooth movement animation
        private IEnumerator MoveToPosAnimation(Vector2 targetPos)
        {
            float modifier = 0f;
            Vector2 initialPos = this.popupTransform.anchoredPosition;

            while (modifier < 1f)
            {
                modifier += 4f * Time.unscaledDeltaTime;
                this.popupTransform.anchoredPosition = Vector2.Lerp(initialPos, targetPos, modifier);

                yield return null;
            }
        }

        // Popup is clicked
        public void OnPointerClick(PointerEventData data)
        {
            // Hide the popup and show the log window
            if (!this.isPopupBeingDragged)
            {
                this.debugManager.ShowLogWindow();
            }
        }

        // Hides the log window and shows the popup
        public void Show()
        {
            this.canvasGroup.interactable = true;
            this.canvasGroup.blocksRaycasts = true;
            this.canvasGroup.alpha = 1f;

            // Reset the counters
            Reset();

            // Update position in case resolution was changed while the popup was hidden
            UpdatePosition(true);
        }

        // Hide the popup
        public void Hide()
        {
            this.canvasGroup.interactable = false;
            this.canvasGroup.blocksRaycasts = false;
            this.canvasGroup.alpha = 0f;

            this.isPopupBeingDragged = false;
        }

        public void OnBeginDrag(PointerEventData data)
        {
            this.isPopupBeingDragged = true;

            // If a smooth movement animation is in progress, cancel it
            if (this.moveToPosCoroutine != null)
            {
                StopCoroutine(this.moveToPosCoroutine);
                this.moveToPosCoroutine = null;
            }
        }

        // Reposition the popup
        public void OnDrag(PointerEventData data)
        {
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.debugManager.canvasTR, data.position, data.pressEventCamera, out Vector2 localPoint))
            {
                this.popupTransform.anchoredPosition = localPoint;
            }
        }

        // Smoothly translate the popup to the nearest edge
        public void OnEndDrag(PointerEventData data)
        {
            this.isPopupBeingDragged = false;
            UpdatePosition(false);
        }

        public void UpdatePosition(bool immediately)
        {
            Vector2 canvasSize = this.debugManager.canvasTR.rect.size;

            float canvasWidth = canvasSize.x;
            float canvasHeight = canvasSize.y;

            // normalizedPosition allows us to glue the popup to a specific edge of the screen. It becomes useful when
            // the popup is at the right edge and we switch from portrait screen orientation to landscape screen orientation.
            // Without normalizedPosition, popup could jump to bottom or top edges instead of staying at the right edge
            Vector2 pos = immediately ? new Vector2(this.normalizedPosition.x * canvasWidth, this.normalizedPosition.y * canvasHeight) : this.popupTransform.anchoredPosition;

            // Find distances to all four edges
            float distToLeft = canvasWidth * 0.5f + pos.x;
            float distToRight = canvasWidth - distToLeft;

            float distToBottom = canvasHeight * 0.5f + pos.y;
            float distToTop = canvasHeight - distToBottom;

            float horDistance = Mathf.Min(distToLeft, distToRight);
            float vertDistance = Mathf.Min(distToBottom, distToTop);

            // Find the nearest edge's coordinates
            if (horDistance < vertDistance)
            {
                if (distToLeft < distToRight)
                {
                    pos = new Vector2(canvasWidth * -0.5f + this.halfSize.x, pos.y);
                }
                else
                {
                    pos = new Vector2(canvasWidth * 0.5f - this.halfSize.x, pos.y);
                }

                pos.y = Mathf.Clamp(pos.y, canvasHeight * -0.5f + this.halfSize.y, canvasHeight * 0.5f - this.halfSize.y);
            }
            else
            {
                if (distToBottom < distToTop)
                {
                    pos = new Vector2(pos.x, canvasHeight * -0.5f + this.halfSize.y);
                }
                else
                {
                    pos = new Vector2(pos.x, canvasHeight * 0.5f - this.halfSize.y);
                }

                pos.x = Mathf.Clamp(pos.x, canvasWidth * -0.5f + this.halfSize.x, canvasWidth * 0.5f - this.halfSize.x);
            }

            this.normalizedPosition.Set(pos.x / canvasWidth, pos.y / canvasHeight);

            // If another smooth movement animation is in progress, cancel it
            if (this.moveToPosCoroutine != null)
            {
                StopCoroutine(this.moveToPosCoroutine);
                this.moveToPosCoroutine = null;
            }

            if (immediately)
            {
                this.popupTransform.anchoredPosition = pos;
            }
            else
            {
                // Smoothly translate the popup to the specified position
                this.moveToPosCoroutine = MoveToPosAnimation(pos);
                StartCoroutine(this.moveToPosCoroutine);
            }
        }
    }
}