﻿using System;

namespace IngameDebugConsole
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class ConsoleMethodAttribute : Attribute
    {
        private string m_command;
        private string m_description;
        private string[] m_parameterNames;

        public string Command => this.m_command;
        public string Description => this.m_description;
        public string[] ParameterNames => this.m_parameterNames;

        public ConsoleMethodAttribute(string command, string description, params string[] parameterNames)
        {
            this.m_command = command;
            this.m_description = description;
            this.m_parameterNames = parameterNames;
        }
    }
}