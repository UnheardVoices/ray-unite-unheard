﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Listens to scroll events on the scroll rect that debug items are stored
// and decides whether snap to bottom should be true or not
// 
// Procedure: if, after a user input (drag or scroll), scrollbar is at the bottom, then 
// snap to bottom shall be true, otherwise it shall be false
namespace IngameDebugConsole
{
    public class DebugsOnScrollListener : MonoBehaviour, IScrollHandler, IBeginDragHandler, IEndDragHandler
    {
        public ScrollRect debugsScrollRect;
        public DebugLogManager debugLogManager;

        public void OnScroll(PointerEventData data)
        {
            if (IsScrollbarAtBottom())
            {
                this.debugLogManager.SetSnapToBottom(true);
            }
            else
            {
                this.debugLogManager.SetSnapToBottom(false);
            }
        }

        public void OnBeginDrag(PointerEventData data)
        {
            this.debugLogManager.SetSnapToBottom(false);
        }

        public void OnEndDrag(PointerEventData data)
        {
            if (IsScrollbarAtBottom())
            {
                this.debugLogManager.SetSnapToBottom(true);
            }
            else
            {
                this.debugLogManager.SetSnapToBottom(false);
            }
        }

        public void OnScrollbarDragStart(BaseEventData data)
        {
            this.debugLogManager.SetSnapToBottom(false);
        }

        public void OnScrollbarDragEnd(BaseEventData data)
        {
            if (IsScrollbarAtBottom())
            {
                this.debugLogManager.SetSnapToBottom(true);
            }
            else
            {
                this.debugLogManager.SetSnapToBottom(false);
            }
        }

        private bool IsScrollbarAtBottom()
        {
            float scrollbarYPos = this.debugsScrollRect.verticalNormalizedPosition;
            if (scrollbarYPos <= 1E-6f)
            {
                return true;
            }

            return false;
        }
    }
}