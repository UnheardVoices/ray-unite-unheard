﻿// #define RESET_REMOVED_ELEMENTS

namespace IngameDebugConsole
{
    public class CircularBuffer<T>
    {
        private T[] arr;
        private int startIndex;

        public int Count { get; private set; }
        public T this[int index] => this.arr[(this.startIndex + index) % this.arr.Length];

        public CircularBuffer(int capacity)
        {
            this.arr = new T[capacity];
        }

        // Old elements are overwritten when capacity is reached
        public void Add(T value)
        {
            if (this.Count < this.arr.Length)
            {
                this.arr[this.Count++] = value;
            }
            else
            {
                this.arr[this.startIndex] = value;
                if (++this.startIndex >= this.arr.Length)
                {
                    this.startIndex = 0;
                }
            }
        }
    }

    public class DynamicCircularBuffer<T>
    {
        private T[] arr;
        private int startIndex;

        public int Count { get; private set; }
        public T this[int index]
        {
            get => this.arr[(this.startIndex + index) % this.arr.Length];
            set => this.arr[(this.startIndex + index) % this.arr.Length] = value;
        }

        public DynamicCircularBuffer(int initialCapacity = 2)
        {
            this.arr = new T[initialCapacity];
        }

        public void Add(T value)
        {
            if (this.Count >= this.arr.Length)
            {
                int prevSize = this.arr.Length;
                int newSize = prevSize > 0 ? prevSize * 2 : 2; // Size must be doubled (at least), or the shift operation below must consider IndexOutOfRange situations

                System.Array.Resize(ref this.arr, newSize);

                if (this.startIndex > 0)
                {
                    if (this.startIndex <= (prevSize - 1) / 2)
                    {
                        // Move elements [0,startIndex) to the end
                        for (int i = 0; i < this.startIndex; i++)
                        {
                            this.arr[i + prevSize] = this.arr[i];
#if RESET_REMOVED_ELEMENTS
							arr[i] = default( T );
#endif
                        }
                    }
                    else
                    {
                        // Move elements [startIndex,prevSize) to the end
                        int delta = newSize - prevSize;
                        for (int i = prevSize - 1; i >= this.startIndex; i--)
                        {
                            this.arr[i + delta] = this.arr[i];
#if RESET_REMOVED_ELEMENTS
							arr[i] = default( T );
#endif
                        }

                        this.startIndex += delta;
                    }
                }
            }

            this[this.Count++] = value;
        }

        public T RemoveFirst()
        {
            T element = this.arr[this.startIndex];
#if RESET_REMOVED_ELEMENTS
			arr[startIndex] = default( T );
#endif

            if (++this.startIndex >= this.arr.Length)
            {
                this.startIndex = 0;
            }

            this.Count--;
            return element;
        }

        public T RemoveLast()
        {
            T element = this.arr[this.Count - 1];
#if RESET_REMOVED_ELEMENTS
			arr[Count - 1] = default( T );
#endif

            this.Count--;
            return element;
        }
    }
}