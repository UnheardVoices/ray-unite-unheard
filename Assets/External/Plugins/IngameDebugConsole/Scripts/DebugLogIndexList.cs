﻿namespace IngameDebugConsole
{
    public class DebugLogIndexList<T>
    {
        private T[] indices;
        private int size;

        public int Count => this.size;
        public T this[int index]
        {
            get => this.indices[index];
            set => this.indices[index] = value;
        }

        public DebugLogIndexList()
        {
            this.indices = new T[64];
            this.size = 0;
        }

        public void Add(T value)
        {
            if (this.size == this.indices.Length)
            {
                System.Array.Resize(ref this.indices, this.size * 2);
            }

            this.indices[this.size++] = value;
        }

        public void Clear()
        {
            this.size = 0;
        }

        public int IndexOf(T value)
        {
            return System.Array.IndexOf(this.indices, value);
        }
    }
}