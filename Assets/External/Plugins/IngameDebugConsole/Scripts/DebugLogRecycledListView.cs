﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Handles the log items in an optimized way such that existing log items are
// recycled within the list instead of creating a new log item at each chance
namespace IngameDebugConsole
{
    public class DebugLogRecycledListView : MonoBehaviour
    {
#pragma warning disable 0649
        // Cached components
        [SerializeField]
        private RectTransform transformComponent;
        [SerializeField]
        private RectTransform viewportTransform;

        [SerializeField]
        private Color logItemNormalColor1;
        [SerializeField]
        private Color logItemNormalColor2;
        [SerializeField]
        private Color logItemSelectedColor;
#pragma warning restore 0649

        internal DebugLogManager manager;
        private ScrollRect scrollView;

        private float logItemHeight, _1OverLogItemHeight;
        private float viewportHeight;

        // Unique debug entries
        private List<DebugLogEntry> collapsedLogEntries = null;

        // Indices of debug entries to show in collapsedLogEntries
        private DebugLogIndexList<int> indicesOfEntriesToShow = null;
        private DebugLogIndexList<DebugLogEntryTimestamp> timestampsOfEntriesToShow = null;

        private int indexOfSelectedLogEntry = int.MaxValue;
        private float positionOfSelectedLogEntry = float.MaxValue;
        private float heightOfSelectedLogEntry;
        private float deltaHeightOfSelectedLogEntry;

        // Log items used to visualize the debug entries at specified indices
        private readonly Dictionary<int, DebugLogItem> logItemsAtIndices = new Dictionary<int, DebugLogItem>(256);

        private bool isCollapseOn = false;

        // Current indices of debug entries shown on screen
        private int currentTopIndex = -1, currentBottomIndex = -1;

        public float ItemHeight => this.logItemHeight;
        public float SelectedItemHeight => this.heightOfSelectedLogEntry;

        private void Awake()
        {
            this.scrollView = this.viewportTransform.GetComponentInParent<ScrollRect>();
            this.scrollView.onValueChanged.AddListener((pos) => UpdateItemsInTheList(false));

            this.viewportHeight = this.viewportTransform.rect.height;
        }

        public void Initialize(DebugLogManager manager, List<DebugLogEntry> collapsedLogEntries, DebugLogIndexList<int> indicesOfEntriesToShow, DebugLogIndexList<DebugLogEntryTimestamp> timestampsOfEntriesToShow, float logItemHeight)
        {
            this.manager = manager;
            this.collapsedLogEntries = collapsedLogEntries;
            this.indicesOfEntriesToShow = indicesOfEntriesToShow;
            this.timestampsOfEntriesToShow = timestampsOfEntriesToShow;
            this.logItemHeight = logItemHeight;
            this._1OverLogItemHeight = 1f / logItemHeight;
        }

        public void SetCollapseMode(bool collapse)
        {
            this.isCollapseOn = collapse;
        }

        // A log item is clicked, highlight it
        public void OnLogItemClicked(DebugLogItem item)
        {
            OnLogItemClickedInternal(item.Index, item);
        }

        // Force expand the log item at specified index
        public void SelectAndFocusOnLogItemAtIndex(int itemIndex)
        {
            if (this.indexOfSelectedLogEntry != itemIndex) // Make sure that we aren't deselecting the target log item
            {
                OnLogItemClickedInternal(itemIndex);
            }

            float transformComponentCenterYAtTop = this.viewportHeight * 0.5f;
            float transformComponentCenterYAtBottom = this.transformComponent.sizeDelta.y - this.viewportHeight * 0.5f;
            float transformComponentTargetCenterY = itemIndex * this.logItemHeight + this.viewportHeight * 0.5f;
            if (transformComponentCenterYAtTop == transformComponentCenterYAtBottom)
            {
                this.scrollView.verticalNormalizedPosition = 0.5f;
            }
            else
            {
                this.scrollView.verticalNormalizedPosition = Mathf.Clamp01(Mathf.InverseLerp(transformComponentCenterYAtBottom, transformComponentCenterYAtTop, transformComponentTargetCenterY));
            }

            this.manager.SetSnapToBottom(false);
        }

        private void OnLogItemClickedInternal(int itemIndex, DebugLogItem referenceItem = null)
        {
            if (this.indexOfSelectedLogEntry != itemIndex)
            {
                DeselectSelectedLogItem();

                if (!referenceItem)
                {
                    if (this.currentTopIndex == -1)
                    {
                        UpdateItemsInTheList(false); // Try to generate some DebugLogItems, we need one DebugLogItem to calculate the text height
                    }

                    referenceItem = this.logItemsAtIndices[this.currentTopIndex];
                }

                this.indexOfSelectedLogEntry = itemIndex;
                this.positionOfSelectedLogEntry = itemIndex * this.logItemHeight;
                this.heightOfSelectedLogEntry = referenceItem.CalculateExpandedHeight(this.collapsedLogEntries[this.indicesOfEntriesToShow[itemIndex]], (this.timestampsOfEntriesToShow != null) ? this.timestampsOfEntriesToShow[itemIndex] : null);
                this.deltaHeightOfSelectedLogEntry = this.heightOfSelectedLogEntry - this.logItemHeight;

                this.manager.SetSnapToBottom(false);
            }
            else
            {
                DeselectSelectedLogItem();
            }

            if (this.indexOfSelectedLogEntry >= this.currentTopIndex && this.indexOfSelectedLogEntry <= this.currentBottomIndex)
            {
                ColorLogItem(this.logItemsAtIndices[this.indexOfSelectedLogEntry], this.indexOfSelectedLogEntry);
            }

            CalculateContentHeight();

            HardResetItems();
            UpdateItemsInTheList(true);

            this.manager.ValidateScrollPosition();
        }

        // Deselect the currently selected log item
        public void DeselectSelectedLogItem()
        {
            int indexOfPreviouslySelectedLogEntry = this.indexOfSelectedLogEntry;
            this.indexOfSelectedLogEntry = int.MaxValue;

            this.positionOfSelectedLogEntry = float.MaxValue;
            this.heightOfSelectedLogEntry = this.deltaHeightOfSelectedLogEntry = 0f;

            if (indexOfPreviouslySelectedLogEntry >= this.currentTopIndex && indexOfPreviouslySelectedLogEntry <= this.currentBottomIndex)
            {
                ColorLogItem(this.logItemsAtIndices[indexOfPreviouslySelectedLogEntry], indexOfPreviouslySelectedLogEntry);
            }
        }

        // Number of debug entries may be changed, update the list
        public void OnLogEntriesUpdated(bool updateAllVisibleItemContents)
        {
            CalculateContentHeight();
            this.viewportHeight = this.viewportTransform.rect.height;

            if (updateAllVisibleItemContents)
            {
                HardResetItems();
            }

            UpdateItemsInTheList(updateAllVisibleItemContents);
        }

        // A single collapsed log entry at specified index is updated, refresh its item if visible
        public void OnCollapsedLogEntryAtIndexUpdated(int index)
        {
            if (this.logItemsAtIndices.TryGetValue(index, out DebugLogItem logItem))
            {
                logItem.ShowCount();

                if (this.timestampsOfEntriesToShow != null)
                {
                    logItem.UpdateTimestamp(this.timestampsOfEntriesToShow[index]);
                }
            }
        }

        // Log window's width has changed, update the expanded (currently selected) log's height
        public void OnViewportWidthChanged()
        {
            if (this.indexOfSelectedLogEntry >= this.indicesOfEntriesToShow.Count)
            {
                return;
            }

            if (this.currentTopIndex == -1)
            {
                UpdateItemsInTheList(false); // Try to generate some DebugLogItems, we need one DebugLogItem to calculate the text height
                if (this.currentTopIndex == -1) // No DebugLogItems are generated, weird
                {
                    return;
                }
            }

            DebugLogItem referenceItem = this.logItemsAtIndices[this.currentTopIndex];

            this.heightOfSelectedLogEntry = referenceItem.CalculateExpandedHeight(this.collapsedLogEntries[this.indicesOfEntriesToShow[this.indexOfSelectedLogEntry]], (this.timestampsOfEntriesToShow != null) ? this.timestampsOfEntriesToShow[this.indexOfSelectedLogEntry] : null);
            this.deltaHeightOfSelectedLogEntry = this.heightOfSelectedLogEntry - this.logItemHeight;

            CalculateContentHeight();

            HardResetItems();
            UpdateItemsInTheList(true);

            this.manager.ValidateScrollPosition();
        }

        // Log window's height has changed, update the list
        public void OnViewportHeightChanged()
        {
            this.viewportHeight = this.viewportTransform.rect.height;
            UpdateItemsInTheList(false);
        }

        private void HardResetItems()
        {
            if (this.currentTopIndex != -1)
            {
                DestroyLogItemsBetweenIndices(this.currentTopIndex, this.currentBottomIndex);
                this.currentTopIndex = -1;
            }
        }

        private void CalculateContentHeight()
        {
            float newHeight = Mathf.Max(1f, this.indicesOfEntriesToShow.Count * this.logItemHeight + this.deltaHeightOfSelectedLogEntry);
            this.transformComponent.sizeDelta = new Vector2(0f, newHeight);
        }

        // Calculate the indices of log entries to show
        // and handle log items accordingly
        public void UpdateItemsInTheList(bool updateAllVisibleItemContents)
        {
            // If there is at least one log entry to show
            if (this.indicesOfEntriesToShow.Count > 0)
            {
                float contentPosTop = this.transformComponent.anchoredPosition.y - 1f;
                float contentPosBottom = contentPosTop + this.viewportHeight + 2f;

                if (this.positionOfSelectedLogEntry <= contentPosBottom)
                {
                    if (this.positionOfSelectedLogEntry <= contentPosTop)
                    {
                        contentPosTop -= this.deltaHeightOfSelectedLogEntry;
                        contentPosBottom -= this.deltaHeightOfSelectedLogEntry;

                        if (contentPosTop < this.positionOfSelectedLogEntry - 1f)
                        {
                            contentPosTop = this.positionOfSelectedLogEntry - 1f;
                        }

                        if (contentPosBottom < contentPosTop + 2f)
                        {
                            contentPosBottom = contentPosTop + 2f;
                        }
                    }
                    else
                    {
                        contentPosBottom -= this.deltaHeightOfSelectedLogEntry;
                        if (contentPosBottom < this.positionOfSelectedLogEntry + 1f)
                        {
                            contentPosBottom = this.positionOfSelectedLogEntry + 1f;
                        }
                    }
                }

                int newTopIndex = (int)(contentPosTop * this._1OverLogItemHeight);
                int newBottomIndex = (int)(contentPosBottom * this._1OverLogItemHeight);

                if (newTopIndex < 0)
                {
                    newTopIndex = 0;
                }

                if (newBottomIndex > this.indicesOfEntriesToShow.Count - 1)
                {
                    newBottomIndex = this.indicesOfEntriesToShow.Count - 1;
                }

                if (this.currentTopIndex == -1)
                {
                    // There are no log items visible on screen,
                    // just create the new log items
                    updateAllVisibleItemContents = true;

                    this.currentTopIndex = newTopIndex;
                    this.currentBottomIndex = newBottomIndex;

                    CreateLogItemsBetweenIndices(newTopIndex, newBottomIndex);
                }
                else
                {
                    // There are some log items visible on screen

                    if (newBottomIndex < this.currentTopIndex || newTopIndex > this.currentBottomIndex)
                    {
                        // If user scrolled a lot such that, none of the log items are now within
                        // the bounds of the scroll view, pool all the previous log items and create
                        // new log items for the new list of visible debug entries
                        updateAllVisibleItemContents = true;

                        DestroyLogItemsBetweenIndices(this.currentTopIndex, this.currentBottomIndex);
                        CreateLogItemsBetweenIndices(newTopIndex, newBottomIndex);
                    }
                    else
                    {
                        // User did not scroll a lot such that, there are still some log items within
                        // the bounds of the scroll view. Don't destroy them but update their content,
                        // if necessary
                        if (newTopIndex > this.currentTopIndex)
                        {
                            DestroyLogItemsBetweenIndices(this.currentTopIndex, newTopIndex - 1);
                        }

                        if (newBottomIndex < this.currentBottomIndex)
                        {
                            DestroyLogItemsBetweenIndices(newBottomIndex + 1, this.currentBottomIndex);
                        }

                        if (newTopIndex < this.currentTopIndex)
                        {
                            CreateLogItemsBetweenIndices(newTopIndex, this.currentTopIndex - 1);

                            // If it is not necessary to update all the log items,
                            // then just update the newly created log items. Otherwise,
                            // wait for the major update
                            if (!updateAllVisibleItemContents)
                            {
                                UpdateLogItemContentsBetweenIndices(newTopIndex, this.currentTopIndex - 1);
                            }
                        }

                        if (newBottomIndex > this.currentBottomIndex)
                        {
                            CreateLogItemsBetweenIndices(this.currentBottomIndex + 1, newBottomIndex);

                            // If it is not necessary to update all the log items,
                            // then just update the newly created log items. Otherwise,
                            // wait for the major update
                            if (!updateAllVisibleItemContents)
                            {
                                UpdateLogItemContentsBetweenIndices(this.currentBottomIndex + 1, newBottomIndex);
                            }
                        }
                    }

                    this.currentTopIndex = newTopIndex;
                    this.currentBottomIndex = newBottomIndex;
                }

                if (updateAllVisibleItemContents)
                {
                    // Update all the log items
                    UpdateLogItemContentsBetweenIndices(this.currentTopIndex, this.currentBottomIndex);
                }
            }
            else
            {
                HardResetItems();
            }
        }

        private void CreateLogItemsBetweenIndices(int topIndex, int bottomIndex)
        {
            for (int i = topIndex; i <= bottomIndex; i++)
            {
                CreateLogItemAtIndex(i);
            }
        }

        // Create (or unpool) a log item
        private void CreateLogItemAtIndex(int index)
        {
            DebugLogItem logItem = this.manager.PopLogItem();

            // Reposition the log item
            Vector2 anchoredPosition = new Vector2(1f, -index * this.logItemHeight);
            if (index > this.indexOfSelectedLogEntry)
            {
                anchoredPosition.y -= this.deltaHeightOfSelectedLogEntry;
            }

            logItem.Transform.anchoredPosition = anchoredPosition;

            // Color the log item
            ColorLogItem(logItem, index);

            // To access this log item easily in the future, add it to the dictionary
            this.logItemsAtIndices[index] = logItem;
        }

        private void DestroyLogItemsBetweenIndices(int topIndex, int bottomIndex)
        {
            for (int i = topIndex; i <= bottomIndex; i++)
            {
                this.manager.PoolLogItem(this.logItemsAtIndices[i]);
            }
        }

        private void UpdateLogItemContentsBetweenIndices(int topIndex, int bottomIndex)
        {
            DebugLogItem logItem;
            for (int i = topIndex; i <= bottomIndex; i++)
            {
                logItem = this.logItemsAtIndices[i];
                logItem.SetContent(this.collapsedLogEntries[this.indicesOfEntriesToShow[i]], (this.timestampsOfEntriesToShow != null) ? this.timestampsOfEntriesToShow[i] : null, i, i == this.indexOfSelectedLogEntry);

                if (this.isCollapseOn)
                {
                    logItem.ShowCount();
                }
                else
                {
                    logItem.HideCount();
                }
            }
        }

        // Color a log item using its index
        private void ColorLogItem(DebugLogItem logItem, int index)
        {
            if (index == this.indexOfSelectedLogEntry)
            {
                logItem.Image.color = this.logItemSelectedColor;
            }
            else if (index % 2 == 0)
            {
                logItem.Image.color = this.logItemNormalColor1;
            }
            else
            {
                logItem.Image.color = this.logItemNormalColor2;
            }
        }
    }
}