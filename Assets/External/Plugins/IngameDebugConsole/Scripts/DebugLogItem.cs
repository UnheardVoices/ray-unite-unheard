﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
using System.Text.RegularExpressions;
#endif

// A UI element to show information about a debug entry
namespace IngameDebugConsole
{
    public class DebugLogItem : MonoBehaviour, IPointerClickHandler
    {
        #region Platform Specific Elements
#if !UNITY_2018_1_OR_NEWER
#if !UNITY_EDITOR && UNITY_ANDROID
		private static AndroidJavaClass m_ajc = null;
		private static AndroidJavaClass AJC
		{
			get
			{
				if( m_ajc == null )
					m_ajc = new AndroidJavaClass( "com.yasirkula.unity.DebugConsole" );

				return m_ajc;
			}
		}

		private static AndroidJavaObject m_context = null;
		private static AndroidJavaObject Context
		{
			get
			{
				if( m_context == null )
				{
					using( AndroidJavaObject unityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
					{
						m_context = unityClass.GetStatic<AndroidJavaObject>( "currentActivity" );
					}
				}

				return m_context;
			}
		}
#elif !UNITY_EDITOR && UNITY_IOS
		[System.Runtime.InteropServices.DllImport( "__Internal" )]
		private static extern void _DebugConsole_CopyText( string text );
#endif
#endif
        #endregion

#pragma warning disable 0649
        // Cached components
        [SerializeField]
        private RectTransform transformComponent;
        public RectTransform Transform => this.transformComponent;

        [SerializeField]
        private Image imageComponent;
        public Image Image => this.imageComponent;

        [SerializeField]
        private CanvasGroup canvasGroupComponent;
        public CanvasGroup CanvasGroup => this.canvasGroupComponent;

        [SerializeField]
        private Text logText;
        [SerializeField]
        private Image logTypeImage;

        // Objects related to the collapsed count of the debug entry
        [SerializeField]
        private GameObject logCountParent;
        [SerializeField]
        private Text logCountText;

        [SerializeField]
        private RectTransform copyLogButton;
#pragma warning restore 0649

        // Debug entry to show with this log item
        private DebugLogEntry logEntry;
        public DebugLogEntry Entry => this.logEntry;

        private DebugLogEntryTimestamp? logEntryTimestamp;
        public DebugLogEntryTimestamp? Timestamp => this.logEntryTimestamp;

        // Index of the entry in the list of entries
        private int entryIndex;
        public int Index => this.entryIndex;

        private bool isExpanded;
        public bool Expanded => this.isExpanded;

        private Vector2 logTextOriginalPosition;
        private Vector2 logTextOriginalSize;
        private float copyLogButtonHeight;

        private DebugLogRecycledListView listView;

        public void Initialize(DebugLogRecycledListView listView)
        {
            this.listView = listView;

            this.logTextOriginalPosition = this.logText.rectTransform.anchoredPosition;
            this.logTextOriginalSize = this.logText.rectTransform.sizeDelta;
            this.copyLogButtonHeight = this.copyLogButton.anchoredPosition.y + this.copyLogButton.sizeDelta.y + 2f; // 2f: space between text and button

#if !UNITY_EDITOR && UNITY_WEBGL
			copyLogButton.gameObject.AddComponent<DebugLogItemCopyWebGL>().Initialize( this );
#endif
        }

        public void SetContent(DebugLogEntry logEntry, DebugLogEntryTimestamp? logEntryTimestamp, int entryIndex, bool isExpanded)
        {
            this.logEntry = logEntry;
            this.logEntryTimestamp = logEntryTimestamp;
            this.entryIndex = entryIndex;
            this.isExpanded = isExpanded;

            Vector2 size = this.transformComponent.sizeDelta;
            if (isExpanded)
            {
                this.logText.horizontalOverflow = HorizontalWrapMode.Wrap;
                size.y = this.listView.SelectedItemHeight;

                if (!this.copyLogButton.gameObject.activeSelf)
                {
                    this.copyLogButton.gameObject.SetActive(true);

                    this.logText.rectTransform.anchoredPosition = new Vector2(this.logTextOriginalPosition.x, this.logTextOriginalPosition.y + this.copyLogButtonHeight * 0.5f);
                    this.logText.rectTransform.sizeDelta = this.logTextOriginalSize - new Vector2(0f, this.copyLogButtonHeight);
                }
            }
            else
            {
                this.logText.horizontalOverflow = HorizontalWrapMode.Overflow;
                size.y = this.listView.ItemHeight;

                if (this.copyLogButton.gameObject.activeSelf)
                {
                    this.copyLogButton.gameObject.SetActive(false);

                    this.logText.rectTransform.anchoredPosition = this.logTextOriginalPosition;
                    this.logText.rectTransform.sizeDelta = this.logTextOriginalSize;
                }
            }

            this.transformComponent.sizeDelta = size;

            SetText(logEntry, logEntryTimestamp, isExpanded);
            this.logTypeImage.sprite = logEntry.logTypeSpriteRepresentation;
        }

        // Show the collapsed count of the debug entry
        public void ShowCount()
        {
            this.logCountText.text = this.logEntry.count.ToString();

            if (!this.logCountParent.activeSelf)
            {
                this.logCountParent.SetActive(true);
            }
        }

        // Hide the collapsed count of the debug entry
        public void HideCount()
        {
            if (this.logCountParent.activeSelf)
            {
                this.logCountParent.SetActive(false);
            }
        }

        // Update the debug entry's displayed timestamp
        public void UpdateTimestamp(DebugLogEntryTimestamp timestamp)
        {
            this.logEntryTimestamp = timestamp;

            if (this.isExpanded || this.listView.manager.alwaysDisplayTimestamps)
            {
                SetText(this.logEntry, timestamp, this.isExpanded);
            }
        }

        private void SetText(DebugLogEntry logEntry, DebugLogEntryTimestamp? logEntryTimestamp, bool isExpanded)
        {
            if (!logEntryTimestamp.HasValue || (!isExpanded && !this.listView.manager.alwaysDisplayTimestamps))
            {
                this.logText.text = isExpanded ? logEntry.ToString() : logEntry.logString;
            }
            else
            {
                StringBuilder sb = this.listView.manager.sharedStringBuilder;
                sb.Length = 0;

                if (isExpanded)
                {
                    logEntryTimestamp.Value.AppendFullTimestamp(sb);
                    sb.Append(": ").Append(logEntry.ToString());
                }
                else
                {
                    logEntryTimestamp.Value.AppendTime(sb);
                    sb.Append(" ").Append(logEntry.logString);
                }

                this.logText.text = sb.ToString();
            }
        }

        // This log item is clicked, show the debug entry's stack trace
        public void OnPointerClick(PointerEventData eventData)
        {
#if UNITY_EDITOR
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                Match regex = Regex.Match(this.logEntry.stackTrace, @"\(at .*\.cs:[0-9]+\)$", RegexOptions.Multiline);
                if (regex.Success)
                {
                    string line = this.logEntry.stackTrace.Substring(regex.Index + 4, regex.Length - 5);
                    int lineSeparator = line.IndexOf(':');
                    MonoScript script = AssetDatabase.LoadAssetAtPath<MonoScript>(line.Substring(0, lineSeparator));
                    if (script != null)
                    {
                        AssetDatabase.OpenAsset(script, int.Parse(line.Substring(lineSeparator + 1)));
                    }
                }
            }
            else
            {
                this.listView.OnLogItemClicked(this);
            }
#else
			listView.OnLogItemClicked( this );
#endif
        }

        public void CopyLog()
        {
#if UNITY_EDITOR || !UNITY_WEBGL
            string log = GetCopyContent();
            if (string.IsNullOrEmpty(log))
            {
                return;
            }

#if UNITY_EDITOR || UNITY_2018_1_OR_NEWER || (!UNITY_ANDROID && !UNITY_IOS)
            GUIUtility.systemCopyBuffer = log;
#elif UNITY_ANDROID
			AJC.CallStatic( "CopyText", Context, log );
#elif UNITY_IOS
			_DebugConsole_CopyText( log );
#endif
#endif
        }

        internal string GetCopyContent()
        {
            if (!this.logEntryTimestamp.HasValue)
            {
                return this.logEntry.ToString();
            }
            else
            {
                StringBuilder sb = this.listView.manager.sharedStringBuilder;
                sb.Length = 0;

                this.logEntryTimestamp.Value.AppendFullTimestamp(sb);
                sb.Append(": ").Append(this.logEntry.ToString());

                return sb.ToString();
            }
        }

        public float CalculateExpandedHeight(DebugLogEntry logEntry, DebugLogEntryTimestamp? logEntryTimestamp)
        {
            string text = this.logText.text;
            HorizontalWrapMode wrapMode = this.logText.horizontalOverflow;

            SetText(logEntry, logEntryTimestamp, true);
            this.logText.horizontalOverflow = HorizontalWrapMode.Wrap;

            float result = this.logText.preferredHeight + this.copyLogButtonHeight;

            this.logText.text = text;
            this.logText.horizontalOverflow = wrapMode;

            return Mathf.Max(this.listView.ItemHeight, result);
        }

        // Return a string containing complete information about the debug entry
        public override string ToString()
        {
            return this.logEntry.ToString();
        }
    }
}