﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
using UnityEngine.InputSystem;
#endif
#if UNITY_EDITOR && UNITY_2021_1_OR_NEWER
using Screen = UnityEngine.Device.Screen; // To support Device Simulator on Unity 2021.1+
#endif

// Receives debug entries and custom events (e.g. Clear, Collapse, Filter by Type)
// and notifies the recycled list view of changes tto the list of debug entries
// 
// - Vocabulary -
// Debug/Log entry: a Debug.Log/LogError/LogWarning/LogException/LogAssertion request made by
//                   the client and intercepted by this manager object
// Debug/Log item: a visual (uGUI) representation of a debug entry
// 
// There can be a lot of debug entries in the system but there will only be a handful of log items 
// to show their properties on screen (these log items are recycled as the list is scrolled)

// An enum to represent filtered log types
namespace IngameDebugConsole
{
    public enum DebugLogFilter
    {
        None = 0,
        Info = 1,
        Warning = 2,
        Error = 4,
        All = 7
    }

    public class DebugLogManager : MonoBehaviour
    {
        public static DebugLogManager Instance { get; private set; }

#pragma warning disable 0649
        [Header("Properties")]
        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, console window will persist between scenes (i.e. not be destroyed when scene changes)")]
        private bool singleton = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("Minimum height of the console window")]
        private float minimumHeight = 200f;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, console window can be resized horizontally, as well")]
        private bool enableHorizontalResizing = false;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, console window's resize button will be located at bottom-right corner. Otherwise, it will be located at bottom-left corner")]
        private bool resizeFromRight = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("Minimum width of the console window")]
        private float minimumWidth = 240f;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If disabled, no popup will be shown when the console window is hidden")]
        private bool enablePopup = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, console will be initialized as a popup")]
        private bool startInPopupMode = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, console window will initially be invisible")]
        private bool startMinimized = false;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, pressing the Toggle Key will show/hide (i.e. toggle) the console window at runtime")]
        private bool toggleWithKey = false;

#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
        [SerializeField]
        [HideInInspector]
        public InputAction toggleBinding = new InputAction("Toggle Binding", type: InputActionType.Button, binding: "<Keyboard>/backquote", expectedControlType: "Button");
#else
		[SerializeField]
		[HideInInspector]
		private KeyCode toggleKey = KeyCode.BackQuote;
#endif

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, the console window will have a searchbar")]
        private bool enableSearchbar = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("Width of the canvas determines whether the searchbar will be located inside the menu bar or underneath the menu bar. This way, the menu bar doesn't get too crowded on narrow screens. This value determines the minimum width of the canvas for the searchbar to appear inside the menu bar")]
        private float topSearchbarMinWidth = 360f;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, the arrival times of logs will be recorded and displayed when a log is expanded")]
        private bool captureLogTimestamps = false;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, timestamps will be displayed for logs even if they aren't expanded")]
        internal bool alwaysDisplayTimestamps = false;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, the command input field at the bottom of the console window will automatically be cleared after entering a command")]
        private bool clearCommandAfterExecution = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("Console keeps track of the previously entered commands. This value determines the capacity of the command history (you can scroll through the history via up and down arrow keys while the command input field is focused)")]
        private int commandHistorySize = 15;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, while typing a command, all of the matching commands' signatures will be displayed in a popup")]
        private bool showCommandSuggestions = true;

        [SerializeField]
        [HideInInspector]
        [Tooltip("If enabled, on Android platform, logcat entries of the application will also be logged to the console with the prefix \"LOGCAT: \". This may come in handy especially if you want to access the native logs of your Android plugins (like Admob)")]
        private bool receiveLogcatLogsInAndroid = false;

#pragma warning disable 0414
#if UNITY_2018_3_OR_NEWER // On older Unity versions, disabling CS0169 is problematic: "Cannot restore warning 'CS0169' because it was disabled globally"
#pragma warning disable 0169
#endif
        [SerializeField]
        [HideInInspector]
        [Tooltip("Native logs will be filtered using these arguments. If left blank, all native logs of the application will be logged to the console. But if you want to e.g. see Admob's logs only, you can enter \"-s Ads\" (without quotes) here")]
        private string logcatArguments;
#if UNITY_2018_3_OR_NEWER
#pragma warning restore 0169
#endif
#pragma warning restore 0414

        [SerializeField]
        [Tooltip("If enabled, on Android and iOS devices with notch screens, the console window will be repositioned so that the cutout(s) don't obscure it")]
        private bool avoidScreenCutout = true;

        [SerializeField]
        [Tooltip("If a log is longer than this limit, it will be truncated. This helps avoid reaching Unity's 65000 vertex limit for UI canvases")]
        private int maxLogLength = 10000;

#if UNITY_EDITOR || UNITY_STANDALONE
        [SerializeField]
        [Tooltip("If enabled, on standalone platforms, command input field will automatically be focused (start receiving keyboard input) after opening the console window")]
        private bool autoFocusOnCommandInputField = true;
#endif

        [Header("Visuals")]
        [SerializeField]
        private DebugLogItem logItemPrefab;

        [SerializeField]
        private Text commandSuggestionPrefab;

        // Visuals for different log types
        [SerializeField]
        private Sprite infoLog;
        [SerializeField]
        private Sprite warningLog;
        [SerializeField]
        private Sprite errorLog;

        // Visuals for resize button
        [SerializeField]
        private Sprite resizeIconAllDirections;
        [SerializeField]
        private Sprite resizeIconVerticalOnly;

        private Dictionary<LogType, Sprite> logSpriteRepresentations;

        [SerializeField]
        private Color collapseButtonNormalColor;
        [SerializeField]
        private Color collapseButtonSelectedColor;

        [SerializeField]
        private Color filterButtonsNormalColor;
        [SerializeField]
        private Color filterButtonsSelectedColor;

        [SerializeField]
        private string commandSuggestionHighlightStart = "<color=orange>";
        [SerializeField]
        private string commandSuggestionHighlightEnd = "</color>";

        [Header("Internal References")]
        [SerializeField]
        private RectTransform logWindowTR;

        internal RectTransform canvasTR;

        [SerializeField]
        private RectTransform logItemsContainer;

        [SerializeField]
        private RectTransform commandSuggestionsContainer;

        [SerializeField]
        private InputField commandInputField;

        [SerializeField]
        private Button hideButton;

        [SerializeField]
        private Button clearButton;

        [SerializeField]
        private Image collapseButton;

        [SerializeField]
        private Image filterInfoButton;
        [SerializeField]
        private Image filterWarningButton;
        [SerializeField]
        private Image filterErrorButton;

        [SerializeField]
        private Text infoEntryCountText;
        [SerializeField]
        private Text warningEntryCountText;
        [SerializeField]
        private Text errorEntryCountText;

        [SerializeField]
        private RectTransform searchbar;
        [SerializeField]
        private RectTransform searchbarSlotTop;
        [SerializeField]
        private RectTransform searchbarSlotBottom;

        [SerializeField]
        private Image resizeButton;

        [SerializeField]
        private GameObject snapToBottomButton;

        // Canvas group to modify visibility of the log window
        [SerializeField]
        private CanvasGroup logWindowCanvasGroup;

        [SerializeField]
        private DebugLogPopup popupManager;

        [SerializeField]
        private ScrollRect logItemsScrollRect;
        private RectTransform logItemsScrollRectTR;
        private Vector2 logItemsScrollRectOriginalSize;

        // Recycled list view to handle the log items efficiently
        [SerializeField]
        private DebugLogRecycledListView recycledListView;
#pragma warning restore 0649

        private bool isLogWindowVisible = true;
        public bool IsLogWindowVisible => this.isLogWindowVisible;

        public bool PopupEnabled
        {
            get => this.popupManager.gameObject.activeSelf;
            set => this.popupManager.gameObject.SetActive(value);
        }

        private bool screenDimensionsChanged = true;
        private float logWindowPreviousWidth;

        // Number of entries filtered by their types
        private int infoEntryCount = 0, warningEntryCount = 0, errorEntryCount = 0;

        // Number of new entries received this frame
        private int newInfoEntryCount = 0, newWarningEntryCount = 0, newErrorEntryCount = 0;

        // Filters to apply to the list of debug entries to show
        private bool isCollapseOn = false;
        private DebugLogFilter logFilter = DebugLogFilter.All;

        // Search filter
        private string searchTerm;
        private bool isInSearchMode;

        // If the last log item is completely visible (scrollbar is at the bottom),
        // scrollbar will remain at the bottom when new debug entries are received
        private bool snapToBottom = true;

        // List of unique debug entries (duplicates of entries are not kept)
        private List<DebugLogEntry> collapsedLogEntries;
        private List<DebugLogEntryTimestamp> collapsedLogEntriesTimestamps;

        // Dictionary to quickly find if a log already exists in collapsedLogEntries
        private Dictionary<DebugLogEntry, int> collapsedLogEntriesMap;

        // The order the collapsedLogEntries are received 
        // (duplicate entries have the same index (value))
        private DebugLogIndexList<int> uncollapsedLogEntriesIndices;
        private DebugLogIndexList<DebugLogEntryTimestamp> uncollapsedLogEntriesTimestamps;

        // Filtered list of debug entries to show
        private DebugLogIndexList<int> indicesOfListEntriesToShow;
        private DebugLogIndexList<DebugLogEntryTimestamp> timestampsOfListEntriesToShow;

        // The log entry that must be focused this frame
        private int indexOfLogEntryToSelectAndFocus = -1;

        // Whether or not logs list view should be updated this frame
        private bool shouldUpdateRecycledListView = false;

        // Logs that should be registered in Update-loop
        private DynamicCircularBuffer<QueuedDebugLogEntry> queuedLogEntries;
        private object logEntriesLock;
        private int pendingLogToAutoExpand;

        // Command suggestions that match the currently entered command
        private List<Text> commandSuggestionInstances;
        private int visibleCommandSuggestionInstances = 0;
        private List<ConsoleMethodInfo> matchingCommandSuggestions;
        private List<int> commandCaretIndexIncrements;
        private string commandInputFieldPrevCommand;
        private string commandInputFieldPrevCommandName;
        private int commandInputFieldPrevParamCount = -1;
        private int commandInputFieldPrevCaretPos = -1;
        private int commandInputFieldPrevCaretArgumentIndex = -1;

        // Pools for memory efficiency
        private List<DebugLogEntry> pooledLogEntries;
        private List<DebugLogItem> pooledLogItems;

        // History of the previously entered commands
        private CircularBuffer<string> commandHistory;
        private int commandHistoryIndex = -1;
        private string unfinishedCommand;

        // StringBuilder used by various functions
        internal StringBuilder sharedStringBuilder;

        // Offset of DateTime.Now from DateTime.UtcNow
        private System.TimeSpan localTimeUtcOffset;

        // Required in ValidateScrollPosition() function
        private PointerEventData nullPointerEventData;

        // Callbacks for log window show/hide events
        public System.Action OnLogWindowShown, OnLogWindowHidden;

#if UNITY_EDITOR
        private bool isQuittingApplication;
#endif

#if !UNITY_EDITOR && UNITY_ANDROID
		private DebugLogLogcatListener logcatListener;
#endif

        private void Awake()
        {
            // Only one instance of debug console is allowed
            if (!Instance)
            {
                Instance = this;

                // If it is a singleton object, don't destroy it between scene changes
                if (this.singleton)
                {
                    DontDestroyOnLoad(this.gameObject);
                }
            }
            else if (Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            this.pooledLogEntries = new List<DebugLogEntry>(16);
            this.pooledLogItems = new List<DebugLogItem>(16);
            this.commandSuggestionInstances = new List<Text>(8);
            this.matchingCommandSuggestions = new List<ConsoleMethodInfo>(8);
            this.commandCaretIndexIncrements = new List<int>(8);
            this.queuedLogEntries = new DynamicCircularBuffer<QueuedDebugLogEntry>(16);
            this.commandHistory = new CircularBuffer<string>(this.commandHistorySize);

            this.logEntriesLock = new object();
            this.sharedStringBuilder = new StringBuilder(1024);

            this.canvasTR = (RectTransform)this.transform;
            this.logItemsScrollRectTR = (RectTransform)this.logItemsScrollRect.transform;
            this.logItemsScrollRectOriginalSize = this.logItemsScrollRectTR.sizeDelta;

            // Associate sprites with log types
            this.logSpriteRepresentations = new Dictionary<LogType, Sprite>()
            {
                { LogType.Log, this.infoLog },
                { LogType.Warning, this.warningLog },
                { LogType.Error, this.errorLog },
                { LogType.Exception, this.errorLog },
                { LogType.Assert, this.errorLog }
            };

            // Initially, all log types are visible
            this.filterInfoButton.color = this.filterButtonsSelectedColor;
            this.filterWarningButton.color = this.filterButtonsSelectedColor;
            this.filterErrorButton.color = this.filterButtonsSelectedColor;

            this.resizeButton.sprite = this.enableHorizontalResizing ? this.resizeIconAllDirections : this.resizeIconVerticalOnly;

            this.collapsedLogEntries = new List<DebugLogEntry>(128);
            this.collapsedLogEntriesMap = new Dictionary<DebugLogEntry, int>(128);
            this.uncollapsedLogEntriesIndices = new DebugLogIndexList<int>();
            this.indicesOfListEntriesToShow = new DebugLogIndexList<int>();

            if (this.captureLogTimestamps)
            {
                this.collapsedLogEntriesTimestamps = new List<DebugLogEntryTimestamp>(128);
                this.uncollapsedLogEntriesTimestamps = new DebugLogIndexList<DebugLogEntryTimestamp>();
                this.timestampsOfListEntriesToShow = new DebugLogIndexList<DebugLogEntryTimestamp>();
            }

            this.recycledListView.Initialize(this, this.collapsedLogEntries, this.indicesOfListEntriesToShow, this.timestampsOfListEntriesToShow, this.logItemPrefab.Transform.sizeDelta.y);
            this.recycledListView.UpdateItemsInTheList(true);

            if (this.minimumWidth < 100f)
            {
                this.minimumWidth = 100f;
            }

            if (this.minimumHeight < 200f)
            {
                this.minimumHeight = 200f;
            }

            if (!this.resizeFromRight)
            {
                RectTransform resizeButtonTR = (RectTransform)this.resizeButton.GetComponentInParent<DebugLogResizeListener>().transform;
                resizeButtonTR.anchorMin = new Vector2(0f, resizeButtonTR.anchorMin.y);
                resizeButtonTR.anchorMax = new Vector2(0f, resizeButtonTR.anchorMax.y);
                resizeButtonTR.pivot = new Vector2(0f, resizeButtonTR.pivot.y);

                ((RectTransform)this.commandInputField.transform).anchoredPosition += new Vector2(resizeButtonTR.sizeDelta.x, 0f);
            }

            if (this.enableSearchbar)
            {
                this.searchbar.GetComponent<InputField>().onValueChanged.AddListener(SearchTermChanged);
            }
            else
            {
                this.searchbar = null;
                this.searchbarSlotTop.gameObject.SetActive(false);
                this.searchbarSlotBottom.gameObject.SetActive(false);
            }

            if (this.commandSuggestionsContainer.gameObject.activeSelf)
            {
                this.commandSuggestionsContainer.gameObject.SetActive(false);
            }

            // Register to UI events
            this.commandInputField.onValidateInput += OnValidateCommand;
            this.commandInputField.onValueChanged.AddListener(RefreshCommandSuggestions);
            this.commandInputField.onEndEdit.AddListener(OnEndEditCommand);
            this.hideButton.onClick.AddListener(HideLogWindow);
            this.clearButton.onClick.AddListener(ClearLogs);
            this.collapseButton.GetComponent<Button>().onClick.AddListener(CollapseButtonPressed);
            this.filterInfoButton.GetComponent<Button>().onClick.AddListener(FilterLogButtonPressed);
            this.filterWarningButton.GetComponent<Button>().onClick.AddListener(FilterWarningButtonPressed);
            this.filterErrorButton.GetComponent<Button>().onClick.AddListener(FilterErrorButtonPressed);
            this.snapToBottomButton.GetComponent<Button>().onClick.AddListener(() => SetSnapToBottom(true));

            this.localTimeUtcOffset = System.DateTime.Now - System.DateTime.UtcNow;
            this.nullPointerEventData = new PointerEventData(null);

#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
            this.toggleBinding.performed += (context) =>
            {
                if (this.toggleWithKey)
                {
                    if (this.isLogWindowVisible)
                    {
                        HideLogWindow();
                    }
                    else
                    {
                        ShowLogWindow();
                    }
                }
            };

            // On new Input System, scroll sensitivity is much higher than legacy Input system
            this.logItemsScrollRect.scrollSensitivity *= 0.25f;
#endif
        }

        private void OnEnable()
        {
            if (Instance != this)
            {
                return;
            }

            // Intercept debug entries
            Application.logMessageReceivedThreaded -= ReceivedLog;
            Application.logMessageReceivedThreaded += ReceivedLog;

            if (this.receiveLogcatLogsInAndroid)
            {
#if !UNITY_EDITOR && UNITY_ANDROID
				if( logcatListener == null )
					logcatListener = new DebugLogLogcatListener();

				logcatListener.Start( logcatArguments );
#endif
            }

            DebugLogConsole.AddCommand("logs.save", "Saves logs to persistentDataPath", SaveLogsToFile);
            DebugLogConsole.AddCommand<string>("logs.save", "Saves logs to the specified file", SaveLogsToFile);

#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
            if (this.toggleWithKey)
            {
                this.toggleBinding.Enable();
            }
#endif

            //Debug.LogAssertion( "assert" );
            //Debug.LogError( "error" );
            //Debug.LogException( new System.IO.EndOfStreamException() );
            //Debug.LogWarning( "warning" );
            //Debug.Log( "log" );
        }

        private void OnDisable()
        {
            if (Instance != this)
            {
                return;
            }

            // Stop receiving debug entries
            Application.logMessageReceivedThreaded -= ReceivedLog;

#if !UNITY_EDITOR && UNITY_ANDROID
			if( logcatListener != null )
				logcatListener.Stop();
#endif

            DebugLogConsole.RemoveCommand("logs.save");

#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
            if (this.toggleBinding.enabled)
            {
                this.toggleBinding.Disable();
            }
#endif
        }

        private void Start()
        {
            if ((this.enablePopup && this.startInPopupMode) || (!this.enablePopup && this.startMinimized))
            {
                HideLogWindow();
            }
            else
            {
                ShowLogWindow();
            }

            this.PopupEnabled = this.enablePopup;
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (UnityEditor.EditorApplication.isPlaying)
            {
                this.resizeButton.sprite = this.enableHorizontalResizing ? this.resizeIconAllDirections : this.resizeIconVerticalOnly;
            }
        }

        private void OnApplicationQuit()
        {
            this.isQuittingApplication = true;
        }
#endif

        // Window is resized, update the list
        private void OnRectTransformDimensionsChange()
        {
            this.screenDimensionsChanged = true;
        }

#if !ENABLE_INPUT_SYSTEM || ENABLE_LEGACY_INPUT_MANAGER
		private void Update()
		{
			// Toggling the console with toggleKey is handled in Update instead of LateUpdate because
			// when we hide the console, we don't want the commandInputField to capture the toggleKey.
			// InputField captures input in LateUpdate so deactivating it in Update ensures that
			// no further input is captured
			if( toggleWithKey )
			{
				if( Input.GetKeyDown( toggleKey ) )
				{
					if( isLogWindowVisible )
						HideLogWindow();
					else
						ShowLogWindow();
				}
			}
		}
#endif

        private void LateUpdate()
        {
#if UNITY_EDITOR
            if (this.isQuittingApplication)
            {
                return;
            }
#endif

            int queuedLogCount = this.queuedLogEntries.Count;
            if (queuedLogCount > 0)
            {
                for (int i = 0; i < queuedLogCount; i++)
                {
                    QueuedDebugLogEntry logEntry;
                    lock (this.logEntriesLock)
                    {
                        logEntry = this.queuedLogEntries.RemoveFirst();
                    }

                    ProcessLog(logEntry);
                }
            }

            // Update entry count texts in a single batch
            if (this.newInfoEntryCount > 0 || this.newWarningEntryCount > 0 || this.newErrorEntryCount > 0)
            {
                if (this.newInfoEntryCount > 0)
                {
                    this.infoEntryCount += this.newInfoEntryCount;
                    this.infoEntryCountText.text = this.infoEntryCount.ToString();
                }

                if (this.newWarningEntryCount > 0)
                {
                    this.warningEntryCount += this.newWarningEntryCount;
                    this.warningEntryCountText.text = this.warningEntryCount.ToString();
                }

                if (this.newErrorEntryCount > 0)
                {
                    this.errorEntryCount += this.newErrorEntryCount;
                    this.errorEntryCountText.text = this.errorEntryCount.ToString();
                }

                // If debug popup is visible, notify it of the new debug entries
                if (!this.isLogWindowVisible)
                {
                    this.popupManager.NewLogsArrived(this.newInfoEntryCount, this.newWarningEntryCount, this.newErrorEntryCount);
                }

                this.newInfoEntryCount = 0;
                this.newWarningEntryCount = 0;
                this.newErrorEntryCount = 0;
            }

            // Update visible logs if necessary
            if (this.isLogWindowVisible && this.shouldUpdateRecycledListView)
            {
                this.recycledListView.OnLogEntriesUpdated(false);
                this.shouldUpdateRecycledListView = false;
            }

            // Automatically expand the target log (if any)
            if (this.indexOfLogEntryToSelectAndFocus >= 0)
            {
                if (this.indexOfLogEntryToSelectAndFocus < this.indicesOfListEntriesToShow.Count)
                {
                    this.recycledListView.SelectAndFocusOnLogItemAtIndex(this.indexOfLogEntryToSelectAndFocus);
                }

                this.indexOfLogEntryToSelectAndFocus = -1;
            }

            if (this.showCommandSuggestions && this.commandInputField.isFocused && this.commandInputField.caretPosition != this.commandInputFieldPrevCaretPos)
            {
                RefreshCommandSuggestions(this.commandInputField.text);
            }

            if (this.screenDimensionsChanged)
            {
                // Update the recycled list view
                if (this.isLogWindowVisible)
                {
                    this.recycledListView.OnViewportHeightChanged();
                }
                else
                {
                    this.popupManager.UpdatePosition(true);
                }

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
                CheckScreenCutout();
#endif

                this.screenDimensionsChanged = false;
            }

            float logWindowWidth = this.logWindowTR.rect.width;
            if (!Mathf.Approximately(logWindowWidth, this.logWindowPreviousWidth))
            {
                this.logWindowPreviousWidth = logWindowWidth;

                if (this.searchbar)
                {
                    if (logWindowWidth >= this.topSearchbarMinWidth)
                    {
                        if (this.searchbar.parent == this.searchbarSlotBottom)
                        {
                            this.searchbarSlotTop.gameObject.SetActive(true);
                            this.searchbar.SetParent(this.searchbarSlotTop, false);
                            this.searchbarSlotBottom.gameObject.SetActive(false);

                            this.logItemsScrollRectTR.anchoredPosition = Vector2.zero;
                            this.logItemsScrollRectTR.sizeDelta = this.logItemsScrollRectOriginalSize;
                        }
                    }
                    else
                    {
                        if (this.searchbar.parent == this.searchbarSlotTop)
                        {
                            this.searchbarSlotBottom.gameObject.SetActive(true);
                            this.searchbar.SetParent(this.searchbarSlotBottom, false);
                            this.searchbarSlotTop.gameObject.SetActive(false);

                            float searchbarHeight = this.searchbarSlotBottom.sizeDelta.y;
                            this.logItemsScrollRectTR.anchoredPosition = new Vector2(0f, searchbarHeight * -0.5f);
                            this.logItemsScrollRectTR.sizeDelta = this.logItemsScrollRectOriginalSize - new Vector2(0f, searchbarHeight);
                        }
                    }
                }

                if (this.isLogWindowVisible)
                {
                    this.recycledListView.OnViewportWidthChanged();
                }
            }

            // If snapToBottom is enabled, force the scrollbar to the bottom
            if (this.snapToBottom)
            {
                this.logItemsScrollRect.verticalNormalizedPosition = 0f;

                if (this.snapToBottomButton.activeSelf)
                {
                    this.snapToBottomButton.SetActive(false);
                }
            }
            else
            {
                float scrollPos = this.logItemsScrollRect.verticalNormalizedPosition;
                if (this.snapToBottomButton.activeSelf != (scrollPos > 1E-6f && scrollPos < 0.9999f))
                {
                    this.snapToBottomButton.SetActive(!this.snapToBottomButton.activeSelf);
                }
            }

            if (this.isLogWindowVisible && this.commandInputField.isFocused && this.commandHistory.Count > 0)
            {
#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
                if (Keyboard.current != null)
#endif
                {
#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
                    if (Keyboard.current[Key.UpArrow].wasPressedThisFrame)
#else
					if( Input.GetKeyDown( KeyCode.UpArrow ) )
#endif
                    {
                        if (this.commandHistoryIndex == -1)
                        {
                            this.commandHistoryIndex = this.commandHistory.Count - 1;
                            this.unfinishedCommand = this.commandInputField.text;
                        }
                        else if (--this.commandHistoryIndex < 0)
                        {
                            this.commandHistoryIndex = 0;
                        }

                        this.commandInputField.text = this.commandHistory[this.commandHistoryIndex];
                        this.commandInputField.caretPosition = this.commandInputField.text.Length;
                    }
#if ENABLE_INPUT_SYSTEM && !ENABLE_LEGACY_INPUT_MANAGER
                    else if (Keyboard.current[Key.DownArrow].wasPressedThisFrame && this.commandHistoryIndex != -1)
#else
					else if( Input.GetKeyDown( KeyCode.DownArrow ) && commandHistoryIndex != -1 )
#endif
                    {
                        if (++this.commandHistoryIndex < this.commandHistory.Count)
                        {
                            this.commandInputField.text = this.commandHistory[this.commandHistoryIndex];
                        }
                        else
                        {
                            this.commandHistoryIndex = -1;
                            this.commandInputField.text = this.unfinishedCommand ?? string.Empty;
                        }
                    }
                }
            }

#if !UNITY_EDITOR && UNITY_ANDROID
			if( logcatListener != null )
			{
				string log;
				while( ( log = logcatListener.GetLog() ) != null )
					ReceivedLog( "LOGCAT: " + log, string.Empty, LogType.Log );
			}
#endif
        }

        public void ShowLogWindow()
        {
            // Show the log window
            this.logWindowCanvasGroup.interactable = true;
            this.logWindowCanvasGroup.blocksRaycasts = true;
            this.logWindowCanvasGroup.alpha = 1f;

            this.popupManager.Hide();

            // Update the recycled list view 
            // (in case new entries were intercepted while log window was hidden)
            this.recycledListView.OnLogEntriesUpdated(true);

#if UNITY_EDITOR || UNITY_STANDALONE
            // Focus on the command input field on standalone platforms when the console is opened
            if (this.autoFocusOnCommandInputField)
            {
                StartCoroutine(ActivateCommandInputFieldCoroutine());
            }
#endif

            this.isLogWindowVisible = true;

            if (this.OnLogWindowShown != null)
            {
                this.OnLogWindowShown();
            }
        }

        public void HideLogWindow()
        {
            // Hide the log window
            this.logWindowCanvasGroup.interactable = false;
            this.logWindowCanvasGroup.blocksRaycasts = false;
            this.logWindowCanvasGroup.alpha = 0f;

            if (this.commandInputField.isFocused)
            {
                this.commandInputField.DeactivateInputField();
            }

            this.popupManager.Show();

            this.isLogWindowVisible = false;

            if (this.OnLogWindowHidden != null)
            {
                this.OnLogWindowHidden();
            }
        }

        // Command field input is changed, check if command is submitted
        private char OnValidateCommand(string text, int charIndex, char addedChar)
        {
            if (addedChar == '\t') // Autocomplete attempt
            {
                if (!string.IsNullOrEmpty(text))
                {
                    string autoCompletedCommand = DebugLogConsole.GetAutoCompleteCommand(text);
                    if (!string.IsNullOrEmpty(autoCompletedCommand))
                    {
                        this.commandInputField.text = autoCompletedCommand;
                    }
                }

                return '\0';
            }
            else if (addedChar == '\n') // Command is submitted
            {
                // Clear the command field
                if (this.clearCommandAfterExecution)
                {
                    this.commandInputField.text = string.Empty;
                }

                if (text.Length > 0)
                {
                    if (this.commandHistory.Count == 0 || this.commandHistory[this.commandHistory.Count - 1] != text)
                    {
                        this.commandHistory.Add(text);
                    }

                    this.commandHistoryIndex = -1;
                    this.unfinishedCommand = null;

                    // Execute the command
                    DebugLogConsole.ExecuteCommand(text);

                    // Snap to bottom and select the latest entry
                    SetSnapToBottom(true);
                }

                return '\0';
            }

            return addedChar;
        }

        // A debug entry is received
        public void ReceivedLog(string logString, string stackTrace, LogType logType)
        {
#if UNITY_EDITOR
            if (this.isQuittingApplication)
            {
                return;
            }
#endif

            // Truncate the log if it is longer than maxLogLength
            int logLength = logString.Length;
            if (stackTrace == null)
            {
                if (logLength > this.maxLogLength)
                {
                    logString = logString.Substring(0, this.maxLogLength - 11) + "<truncated>";
                }
            }
            else
            {
                logLength += stackTrace.Length;
                if (logLength > this.maxLogLength)
                {
                    // Decide which log component(s) to truncate
                    int halfMaxLogLength = this.maxLogLength / 2;
                    if (logString.Length >= halfMaxLogLength)
                    {
                        if (stackTrace.Length >= halfMaxLogLength)
                        {
                            // Truncate both logString and stackTrace
                            logString = logString.Substring(0, halfMaxLogLength - 11) + "<truncated>";

                            // If stackTrace doesn't end with a blank line, its last line won't be visible in the console for some reason
                            stackTrace = stackTrace.Substring(0, halfMaxLogLength - 12) + "<truncated>\n";
                        }
                        else
                        {
                            // Truncate logString
                            logString = logString.Substring(0, this.maxLogLength - stackTrace.Length - 11) + "<truncated>";
                        }
                    }
                    else
                    {
                        // Truncate stackTrace
                        stackTrace = stackTrace.Substring(0, this.maxLogLength - logString.Length - 12) + "<truncated>\n";
                    }
                }
            }

            QueuedDebugLogEntry queuedLogEntry = new QueuedDebugLogEntry(logString, stackTrace, logType);

            lock (this.logEntriesLock)
            {
                this.queuedLogEntries.Add(queuedLogEntry);
            }
        }

        // Present the log entry in the console
        private void ProcessLog(QueuedDebugLogEntry queuedLogEntry)
        {
            LogType logType = queuedLogEntry.logType;
            DebugLogEntry logEntry;
            if (this.pooledLogEntries.Count > 0)
            {
                logEntry = this.pooledLogEntries[this.pooledLogEntries.Count - 1];
                this.pooledLogEntries.RemoveAt(this.pooledLogEntries.Count - 1);
            }
            else
            {
                logEntry = new DebugLogEntry();
            }

            logEntry.Initialize(queuedLogEntry.logString, queuedLogEntry.stackTrace);

            // Check if this entry is a duplicate (i.e. has been received before)
            bool isEntryInCollapsedEntryList = this.collapsedLogEntriesMap.TryGetValue(logEntry, out int logEntryIndex);
            if (!isEntryInCollapsedEntryList)
            {
                // It is not a duplicate,
                // add it to the list of unique debug entries
                logEntry.logTypeSpriteRepresentation = this.logSpriteRepresentations[logType];

                logEntryIndex = this.collapsedLogEntries.Count;
                this.collapsedLogEntries.Add(logEntry);
                this.collapsedLogEntriesMap[logEntry] = logEntryIndex;

                if (this.collapsedLogEntriesTimestamps != null)
                {
                    this.collapsedLogEntriesTimestamps.Add(new DebugLogEntryTimestamp(this.localTimeUtcOffset));
                }
            }
            else
            {
                // It is a duplicate, pool the duplicate log entry and
                // increment the original debug item's collapsed count
                this.pooledLogEntries.Add(logEntry);

                logEntry = this.collapsedLogEntries[logEntryIndex];
                logEntry.count++;

                if (this.collapsedLogEntriesTimestamps != null)
                {
                    this.collapsedLogEntriesTimestamps[logEntryIndex] = new DebugLogEntryTimestamp(this.localTimeUtcOffset);
                }
            }

            // Add the index of the unique debug entry to the list
            // that stores the order the debug entries are received
            this.uncollapsedLogEntriesIndices.Add(logEntryIndex);

            // Record log's timestamp if desired
            if (this.uncollapsedLogEntriesTimestamps != null)
            {
                this.uncollapsedLogEntriesTimestamps.Add(this.collapsedLogEntriesTimestamps[logEntryIndex]);
            }

            // If this debug entry matches the current filters,
            // add it to the list of debug entries to show
            int logEntryIndexInEntriesToShow = -1;
            Sprite logTypeSpriteRepresentation = logEntry.logTypeSpriteRepresentation;
            if (this.isCollapseOn && isEntryInCollapsedEntryList)
            {
                if (this.isLogWindowVisible)
                {
                    if (!this.isInSearchMode && this.logFilter == DebugLogFilter.All)
                    {
                        logEntryIndexInEntriesToShow = logEntryIndex;
                    }
                    else
                    {
                        logEntryIndexInEntriesToShow = this.indicesOfListEntriesToShow.IndexOf(logEntryIndex);
                    }

                    if (this.timestampsOfListEntriesToShow != null)
                    {
                        this.timestampsOfListEntriesToShow[logEntryIndexInEntriesToShow] = this.collapsedLogEntriesTimestamps[logEntryIndex];
                    }

                    this.recycledListView.OnCollapsedLogEntryAtIndexUpdated(logEntryIndexInEntriesToShow);
                }
            }
            else if ((!this.isInSearchMode || queuedLogEntry.MatchesSearchTerm(this.searchTerm)) && (this.logFilter == DebugLogFilter.All ||
               (logTypeSpriteRepresentation == this.infoLog && ((this.logFilter & DebugLogFilter.Info) == DebugLogFilter.Info)) ||
               (logTypeSpriteRepresentation == this.warningLog && ((this.logFilter & DebugLogFilter.Warning) == DebugLogFilter.Warning)) ||
               (logTypeSpriteRepresentation == this.errorLog && ((this.logFilter & DebugLogFilter.Error) == DebugLogFilter.Error))))
            {
                this.indicesOfListEntriesToShow.Add(logEntryIndex);
                logEntryIndexInEntriesToShow = this.indicesOfListEntriesToShow.Count - 1;

                if (this.timestampsOfListEntriesToShow != null)
                {
                    this.timestampsOfListEntriesToShow.Add(this.collapsedLogEntriesTimestamps[logEntryIndex]);
                }

                if (this.isLogWindowVisible)
                {
                    this.shouldUpdateRecycledListView = true;
                }
            }

            if (logType == LogType.Log)
            {
                this.newInfoEntryCount++;
            }
            else if (logType == LogType.Warning)
            {
                this.newWarningEntryCount++;
            }
            else
            {
                this.newErrorEntryCount++;
            }

            // Automatically expand this log if necessary
            if (this.pendingLogToAutoExpand > 0 && --this.pendingLogToAutoExpand <= 0 && this.isLogWindowVisible && logEntryIndexInEntriesToShow >= 0)
            {
                this.indexOfLogEntryToSelectAndFocus = logEntryIndexInEntriesToShow;
            }
        }

        // Value of snapToBottom is changed (user scrolled the list manually)
        public void SetSnapToBottom(bool snapToBottom)
        {
            this.snapToBottom = snapToBottom;
        }

        // Make sure the scroll bar of the scroll rect is adjusted properly
        internal void ValidateScrollPosition()
        {
            this.logItemsScrollRect.OnScroll(this.nullPointerEventData);
        }

        // Automatically expand the latest log in queuedLogEntries
        public void ExpandLatestPendingLog()
        {
            this.pendingLogToAutoExpand = this.queuedLogEntries.Count;
        }

        // Omits the latest log's stack trace
        public void StripStackTraceFromLatestPendingLog()
        {
            QueuedDebugLogEntry log = this.queuedLogEntries[this.queuedLogEntries.Count - 1];
            this.queuedLogEntries[this.queuedLogEntries.Count - 1] = new QueuedDebugLogEntry(log.logString, string.Empty, log.logType);
        }

        // Clear all the logs
        public void ClearLogs()
        {
            this.snapToBottom = true;

            this.infoEntryCount = 0;
            this.warningEntryCount = 0;
            this.errorEntryCount = 0;

            this.infoEntryCountText.text = "0";
            this.warningEntryCountText.text = "0";
            this.errorEntryCountText.text = "0";

            this.collapsedLogEntries.Clear();
            this.collapsedLogEntriesMap.Clear();
            this.uncollapsedLogEntriesIndices.Clear();
            this.indicesOfListEntriesToShow.Clear();

            if (this.collapsedLogEntriesTimestamps != null)
            {
                this.collapsedLogEntriesTimestamps.Clear();
                this.uncollapsedLogEntriesTimestamps.Clear();
                this.timestampsOfListEntriesToShow.Clear();
            }

            this.recycledListView.DeselectSelectedLogItem();
            this.recycledListView.OnLogEntriesUpdated(true);
        }

        // Collapse button is clicked
        private void CollapseButtonPressed()
        {
            // Swap the value of collapse mode
            this.isCollapseOn = !this.isCollapseOn;

            this.snapToBottom = true;
            this.collapseButton.color = this.isCollapseOn ? this.collapseButtonSelectedColor : this.collapseButtonNormalColor;
            this.recycledListView.SetCollapseMode(this.isCollapseOn);

            // Determine the new list of debug entries to show
            FilterLogs();
        }

        // Filtering mode of info logs has changed
        private void FilterLogButtonPressed()
        {
            this.logFilter = this.logFilter ^ DebugLogFilter.Info;

            if ((this.logFilter & DebugLogFilter.Info) == DebugLogFilter.Info)
            {
                this.filterInfoButton.color = this.filterButtonsSelectedColor;
            }
            else
            {
                this.filterInfoButton.color = this.filterButtonsNormalColor;
            }

            FilterLogs();
        }

        // Filtering mode of warning logs has changed
        private void FilterWarningButtonPressed()
        {
            this.logFilter = this.logFilter ^ DebugLogFilter.Warning;

            if ((this.logFilter & DebugLogFilter.Warning) == DebugLogFilter.Warning)
            {
                this.filterWarningButton.color = this.filterButtonsSelectedColor;
            }
            else
            {
                this.filterWarningButton.color = this.filterButtonsNormalColor;
            }

            FilterLogs();
        }

        // Filtering mode of error logs has changed
        private void FilterErrorButtonPressed()
        {
            this.logFilter = this.logFilter ^ DebugLogFilter.Error;

            if ((this.logFilter & DebugLogFilter.Error) == DebugLogFilter.Error)
            {
                this.filterErrorButton.color = this.filterButtonsSelectedColor;
            }
            else
            {
                this.filterErrorButton.color = this.filterButtonsNormalColor;
            }

            FilterLogs();
        }

        // Search term has changed
        private void SearchTermChanged(string searchTerm)
        {
            if (searchTerm != null)
            {
                searchTerm = searchTerm.Trim();
            }

            this.searchTerm = searchTerm;
            bool isInSearchMode = !string.IsNullOrEmpty(searchTerm);
            if (isInSearchMode || this.isInSearchMode)
            {
                this.isInSearchMode = isInSearchMode;
                FilterLogs();
            }
        }

        // Show suggestions for the currently entered command
        private void RefreshCommandSuggestions(string command)
        {
            if (!this.showCommandSuggestions)
            {
                return;
            }

            this.commandInputFieldPrevCaretPos = this.commandInputField.caretPosition;

            // Don't recalculate the command suggestions if the input command hasn't changed (i.e. only caret's position has changed)
            bool commandChanged = command != this.commandInputFieldPrevCommand;
            bool commandNameOrParametersChanged = false;
            if (commandChanged)
            {
                this.commandInputFieldPrevCommand = command;

                this.matchingCommandSuggestions.Clear();
                this.commandCaretIndexIncrements.Clear();

                string prevCommandName = this.commandInputFieldPrevCommandName;
                DebugLogConsole.GetCommandSuggestions(command, this.matchingCommandSuggestions, this.commandCaretIndexIncrements, ref this.commandInputFieldPrevCommandName, out int numberOfParameters);
                if (prevCommandName != this.commandInputFieldPrevCommandName || numberOfParameters != this.commandInputFieldPrevParamCount)
                {
                    this.commandInputFieldPrevParamCount = numberOfParameters;
                    commandNameOrParametersChanged = true;
                }
            }

            int caretArgumentIndex = 0;
            int caretPos = this.commandInputField.caretPosition;
            for (int i = 0; i < this.commandCaretIndexIncrements.Count && caretPos > this.commandCaretIndexIncrements[i]; i++)
            {
                caretArgumentIndex++;
            }

            if (caretArgumentIndex != this.commandInputFieldPrevCaretArgumentIndex)
            {
                this.commandInputFieldPrevCaretArgumentIndex = caretArgumentIndex;
            }
            else if (!commandChanged || !commandNameOrParametersChanged)
            {
                // Command suggestions don't need to be updated if:
                // a) neither the entered command nor the argument that the caret is hovering has changed
                // b) entered command has changed but command's name hasn't changed, parameter count hasn't changed and the argument
                //    that the caret is hovering hasn't changed (i.e. user has continued typing a parameter's value)
                return;
            }

            if (this.matchingCommandSuggestions.Count == 0)
            {
                OnEndEditCommand(command);
            }
            else
            {
                if (!this.commandSuggestionsContainer.gameObject.activeSelf)
                {
                    this.commandSuggestionsContainer.gameObject.SetActive(true);
                }

                int suggestionInstancesCount = this.commandSuggestionInstances.Count;
                int suggestionsCount = this.matchingCommandSuggestions.Count;

                for (int i = 0; i < suggestionsCount; i++)
                {
                    if (i >= this.visibleCommandSuggestionInstances)
                    {
                        if (i >= suggestionInstancesCount)
                        {
                            this.commandSuggestionInstances.Add(Instantiate(this.commandSuggestionPrefab, this.commandSuggestionsContainer, false));
                        }
                        else
                        {
                            this.commandSuggestionInstances[i].gameObject.SetActive(true);
                        }

                        this.visibleCommandSuggestionInstances++;
                    }

                    ConsoleMethodInfo suggestedCommand = this.matchingCommandSuggestions[i];
                    this.sharedStringBuilder.Length = 0;
                    if (caretArgumentIndex > 0)
                    {
                        this.sharedStringBuilder.Append(suggestedCommand.command);
                    }
                    else
                    {
                        this.sharedStringBuilder.Append(this.commandSuggestionHighlightStart).Append(this.matchingCommandSuggestions[i].command).Append(this.commandSuggestionHighlightEnd);
                    }

                    if (suggestedCommand.parameters.Length > 0)
                    {
                        this.sharedStringBuilder.Append(" ");

                        // If the command name wasn't highlighted, a parameter must always be highlighted
                        int caretParameterIndex = caretArgumentIndex - 1;
                        if (caretParameterIndex >= suggestedCommand.parameters.Length)
                        {
                            caretParameterIndex = suggestedCommand.parameters.Length - 1;
                        }

                        for (int j = 0; j < suggestedCommand.parameters.Length; j++)
                        {
                            if (caretParameterIndex != j)
                            {
                                this.sharedStringBuilder.Append(suggestedCommand.parameters[j]);
                            }
                            else
                            {
                                this.sharedStringBuilder.Append(this.commandSuggestionHighlightStart).Append(suggestedCommand.parameters[j]).Append(this.commandSuggestionHighlightEnd);
                            }
                        }
                    }

                    this.commandSuggestionInstances[i].text = this.sharedStringBuilder.ToString();
                }

                for (int i = this.visibleCommandSuggestionInstances - 1; i >= suggestionsCount; i--)
                {
                    this.commandSuggestionInstances[i].gameObject.SetActive(false);
                }

                this.visibleCommandSuggestionInstances = suggestionsCount;
            }
        }

        // Command input field has lost focus
        private void OnEndEditCommand(string command)
        {
            if (this.commandSuggestionsContainer.gameObject.activeSelf)
            {
                this.commandSuggestionsContainer.gameObject.SetActive(false);
            }
        }

        // Debug window is being resized,
        // Set the sizeDelta property of the window accordingly while
        // preventing window dimensions from going below the minimum dimensions
        internal void Resize(PointerEventData eventData)
        {
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(this.canvasTR, eventData.position, eventData.pressEventCamera, out Vector2 localPoint))
            {
                return;
            }

            // To be able to maximize the log window easily:
            // - When enableHorizontalResizing is true and resizing horizontally, resize button will be grabbed from its left edge (if resizeFromRight is true) or its right edge
            // - While resizing vertically, resize button will be grabbed from its top edge
            const float resizeButtonWidth = 64f;
            const float resizeButtonHeight = 36f;

            Vector2 canvasPivot = this.canvasTR.pivot;
            Vector2 canvasSize = this.canvasTR.rect.size;
            Vector2 anchorMin = this.logWindowTR.anchorMin;

            // Horizontal resizing
            if (this.enableHorizontalResizing)
            {
                if (this.resizeFromRight)
                {
                    localPoint.x += canvasPivot.x * canvasSize.x + resizeButtonWidth;
                    if (localPoint.x < this.minimumWidth)
                    {
                        localPoint.x = this.minimumWidth;
                    }

                    Vector2 anchorMax = this.logWindowTR.anchorMax;
                    anchorMax.x = Mathf.Clamp01(localPoint.x / canvasSize.x);
                    this.logWindowTR.anchorMax = anchorMax;
                }
                else
                {
                    localPoint.x += canvasPivot.x * canvasSize.x - resizeButtonWidth;
                    if (localPoint.x > canvasSize.x - this.minimumWidth)
                    {
                        localPoint.x = canvasSize.x - this.minimumWidth;
                    }

                    anchorMin.x = Mathf.Clamp01(localPoint.x / canvasSize.x);
                }
            }

            // Vertical resizing
            float notchHeight = -this.logWindowTR.sizeDelta.y; // Size of notch screen cutouts at the top of the screen

            localPoint.y += canvasPivot.y * canvasSize.y - resizeButtonHeight;
            if (localPoint.y > canvasSize.y - this.minimumHeight - notchHeight)
            {
                localPoint.y = canvasSize.y - this.minimumHeight - notchHeight;
            }

            anchorMin.y = Mathf.Clamp01(localPoint.y / canvasSize.y);

            this.logWindowTR.anchorMin = anchorMin;

            // Update the recycled list view
            this.recycledListView.OnViewportHeightChanged();
        }

        // Determine the filtered list of debug entries to show on screen
        private void FilterLogs()
        {
            this.indicesOfListEntriesToShow.Clear();

            if (this.timestampsOfListEntriesToShow != null)
            {
                this.timestampsOfListEntriesToShow.Clear();
            }

            if (this.logFilter != DebugLogFilter.None)
            {
                if (this.logFilter == DebugLogFilter.All)
                {
                    if (this.isCollapseOn)
                    {
                        if (!this.isInSearchMode)
                        {
                            // All the unique debug entries will be listed just once.
                            // So, list of debug entries to show is the same as the
                            // order these unique debug entries are added to collapsedLogEntries
                            for (int i = 0, count = this.collapsedLogEntries.Count; i < count; i++)
                            {
                                this.indicesOfListEntriesToShow.Add(i);

                                if (this.timestampsOfListEntriesToShow != null)
                                {
                                    this.timestampsOfListEntriesToShow.Add(this.collapsedLogEntriesTimestamps[i]);
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0, count = this.collapsedLogEntries.Count; i < count; i++)
                            {
                                if (this.collapsedLogEntries[i].MatchesSearchTerm(this.searchTerm))
                                {
                                    this.indicesOfListEntriesToShow.Add(i);

                                    if (this.timestampsOfListEntriesToShow != null)
                                    {
                                        this.timestampsOfListEntriesToShow.Add(this.collapsedLogEntriesTimestamps[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!this.isInSearchMode)
                        {
                            for (int i = 0, count = this.uncollapsedLogEntriesIndices.Count; i < count; i++)
                            {
                                this.indicesOfListEntriesToShow.Add(this.uncollapsedLogEntriesIndices[i]);

                                if (this.timestampsOfListEntriesToShow != null)
                                {
                                    this.timestampsOfListEntriesToShow.Add(this.uncollapsedLogEntriesTimestamps[i]);
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0, count = this.uncollapsedLogEntriesIndices.Count; i < count; i++)
                            {
                                if (this.collapsedLogEntries[this.uncollapsedLogEntriesIndices[i]].MatchesSearchTerm(this.searchTerm))
                                {
                                    this.indicesOfListEntriesToShow.Add(this.uncollapsedLogEntriesIndices[i]);

                                    if (this.timestampsOfListEntriesToShow != null)
                                    {
                                        this.timestampsOfListEntriesToShow.Add(this.uncollapsedLogEntriesTimestamps[i]);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Show only the debug entries that match the current filter
                    bool isInfoEnabled = (this.logFilter & DebugLogFilter.Info) == DebugLogFilter.Info;
                    bool isWarningEnabled = (this.logFilter & DebugLogFilter.Warning) == DebugLogFilter.Warning;
                    bool isErrorEnabled = (this.logFilter & DebugLogFilter.Error) == DebugLogFilter.Error;

                    if (this.isCollapseOn)
                    {
                        for (int i = 0, count = this.collapsedLogEntries.Count; i < count; i++)
                        {
                            DebugLogEntry logEntry = this.collapsedLogEntries[i];

                            if (this.isInSearchMode && !logEntry.MatchesSearchTerm(this.searchTerm))
                            {
                                continue;
                            }

                            bool shouldShowLog = false;
                            if (logEntry.logTypeSpriteRepresentation == this.infoLog)
                            {
                                if (isInfoEnabled)
                                {
                                    shouldShowLog = true;
                                }
                            }
                            else if (logEntry.logTypeSpriteRepresentation == this.warningLog)
                            {
                                if (isWarningEnabled)
                                {
                                    shouldShowLog = true;
                                }
                            }
                            else if (isErrorEnabled)
                            {
                                shouldShowLog = true;
                            }

                            if (shouldShowLog)
                            {
                                this.indicesOfListEntriesToShow.Add(i);

                                if (this.timestampsOfListEntriesToShow != null)
                                {
                                    this.timestampsOfListEntriesToShow.Add(this.collapsedLogEntriesTimestamps[i]);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0, count = this.uncollapsedLogEntriesIndices.Count; i < count; i++)
                        {
                            DebugLogEntry logEntry = this.collapsedLogEntries[this.uncollapsedLogEntriesIndices[i]];

                            if (this.isInSearchMode && !logEntry.MatchesSearchTerm(this.searchTerm))
                            {
                                continue;
                            }

                            bool shouldShowLog = false;
                            if (logEntry.logTypeSpriteRepresentation == this.infoLog)
                            {
                                if (isInfoEnabled)
                                {
                                    shouldShowLog = true;
                                }
                            }
                            else if (logEntry.logTypeSpriteRepresentation == this.warningLog)
                            {
                                if (isWarningEnabled)
                                {
                                    shouldShowLog = true;
                                }
                            }
                            else if (isErrorEnabled)
                            {
                                shouldShowLog = true;
                            }

                            if (shouldShowLog)
                            {
                                this.indicesOfListEntriesToShow.Add(this.uncollapsedLogEntriesIndices[i]);

                                if (this.timestampsOfListEntriesToShow != null)
                                {
                                    this.timestampsOfListEntriesToShow.Add(this.uncollapsedLogEntriesTimestamps[i]);
                                }
                            }
                        }
                    }
                }
            }

            // Update the recycled list view
            this.recycledListView.DeselectSelectedLogItem();
            this.recycledListView.OnLogEntriesUpdated(true);

            ValidateScrollPosition();
        }

        public string GetAllLogs()
        {
            int count = this.uncollapsedLogEntriesIndices.Count;
            int length = 0;
            int newLineLength = System.Environment.NewLine.Length;
            for (int i = 0; i < count; i++)
            {
                DebugLogEntry entry = this.collapsedLogEntries[this.uncollapsedLogEntriesIndices[i]];
                length += entry.logString.Length + entry.stackTrace.Length + newLineLength * 3;
            }

            if (this.uncollapsedLogEntriesTimestamps != null)
            {
                length += count * 12; // Timestamp: "[HH:mm:ss]: "
            }

            length += 100; // Just in case...

            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < count; i++)
            {
                DebugLogEntry entry = this.collapsedLogEntries[this.uncollapsedLogEntriesIndices[i]];

                if (this.uncollapsedLogEntriesTimestamps != null)
                {
                    this.uncollapsedLogEntriesTimestamps[i].AppendTime(sb);
                    sb.Append(": ");
                }

                sb.AppendLine(entry.logString).AppendLine(entry.stackTrace).AppendLine();
            }

            return sb.ToString();
        }

        private void SaveLogsToFile()
        {
            SaveLogsToFile(Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss") + ".txt"));
        }

        private void SaveLogsToFile(string filePath)
        {
            File.WriteAllText(filePath, GetAllLogs());
            Debug.Log("Logs saved to: " + filePath);
        }

        // If a cutout is intersecting with debug window on notch screens, shift the window downwards
        private void CheckScreenCutout()
        {
            if (!this.avoidScreenCutout)
            {
                return;
            }

#if UNITY_2017_2_OR_NEWER && (UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS)
            // Check if there is a cutout at the top of the screen
            int screenHeight = Screen.height;
            float safeYMax = Screen.safeArea.yMax;
            if (safeYMax < screenHeight - 1) // 1: a small threshold
            {
                // There is a cutout, shift the log window downwards
                float cutoutPercentage = (screenHeight - safeYMax) / Screen.height;
                float cutoutLocalSize = cutoutPercentage * this.canvasTR.rect.height;

                this.logWindowTR.anchoredPosition = new Vector2(0f, -cutoutLocalSize);
                this.logWindowTR.sizeDelta = new Vector2(0f, -cutoutLocalSize);
            }
            else
            {
                this.logWindowTR.anchoredPosition = Vector2.zero;
                this.logWindowTR.sizeDelta = Vector2.zero;
            }
#endif
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        private IEnumerator ActivateCommandInputFieldCoroutine()
        {
            // Waiting 1 frame before activating commandInputField ensures that the toggleKey isn't captured by it
            yield return null;
            this.commandInputField.ActivateInputField();

            yield return null;
            this.commandInputField.MoveTextEnd(false);
        }
#endif

        // Pool an unused log item
        internal void PoolLogItem(DebugLogItem logItem)
        {
            logItem.CanvasGroup.alpha = 0f;
            logItem.CanvasGroup.blocksRaycasts = false;

            this.pooledLogItems.Add(logItem);
        }

        // Fetch a log item from the pool
        internal DebugLogItem PopLogItem()
        {
            DebugLogItem newLogItem;

            // If pool is not empty, fetch a log item from the pool,
            // create a new log item otherwise
            if (this.pooledLogItems.Count > 0)
            {
                newLogItem = this.pooledLogItems[this.pooledLogItems.Count - 1];
                this.pooledLogItems.RemoveAt(this.pooledLogItems.Count - 1);

                newLogItem.CanvasGroup.alpha = 1f;
                newLogItem.CanvasGroup.blocksRaycasts = true;
            }
            else
            {
                newLogItem = Instantiate(this.logItemPrefab, this.logItemsContainer, false);
                newLogItem.Initialize(this.recycledListView);
            }

            return newLogItem;
        }
    }
}