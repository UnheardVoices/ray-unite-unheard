using System.Collections;
using UnityEngine;

public class SS_direction : MonoBehaviour
{
    public Material[] materials;
    public Transform theSun;
    public Vector3 theRotation;

    private void Start()
    {
        StartCoroutine("writeToMaterial");
    }

    private IEnumerator writeToMaterial()
    {
        while (true)
        {
            this.theRotation = this.theSun.transform.eulerAngles;
            for (int i = 0; i < this.materials.Length; i++)

            {
                this.materials[i].SetVector("_rotation", this.theRotation);
            }
            yield return null;
        }
    }
}
