using System.Collections;
using UnityEngine;

public class Interactive_grass : MonoBehaviour
{
    public Material[] materials;
    public Transform thePlayer;
    private Vector3 thePosition;

    private void Start()
    {
        StartCoroutine("writeToMaterial");
    }

    private IEnumerator writeToMaterial()
    {
        while (true)
        {
            this.thePosition = this.thePlayer.transform.position;
            for (int i = 0; i < this.materials.Length; i++)

            {
                this.materials[i].SetVector("_position", this.thePosition);
            }
            yield return null;
        }
    }
}