﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Jump
{
    public class JumpControlGamepadSettingControlInfo :
        ControlBindingGamepadControl
    {
        public JumpControlGamepadSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .GetCurrentGamepadJumpBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .SetNewGamepadJumpBinding(newGamepadBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
