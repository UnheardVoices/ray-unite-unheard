﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.AttackSecondary
{
    public class AttackSecondaryControlGamepadSettingControlInfo
        : ControlBindingGamepadControl
    {
        public AttackSecondaryControlGamepadSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .GetCurrentGamepadAttackSecondaryBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .SetNewGamepadAttackSecondaryBinding(newGamepadBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
