﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Roll
{
    internal class GroundRollControlGamepadSettingControlInfo
        : ControlBindingGamepadControl
    {
        public GroundRollControlGamepadSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .GetCurrentGamepadGroundRollBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .SetNewGamepadGroundRollBinding(newGamepadBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
