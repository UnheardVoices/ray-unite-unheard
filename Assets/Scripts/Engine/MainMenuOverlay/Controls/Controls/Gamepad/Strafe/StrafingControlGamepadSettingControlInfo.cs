﻿using Assets.Scripts.Engine.Input.InputSystem;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Strafe
{
    internal class StrafingControlGamepadSettingControlInfo
        : ControlBindingGamepadControl
    {
        public StrafingControlGamepadSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .GetCurrentGamepadStrafingBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newGamepadBindingInfo)
        {
            this.controlBindingInfo =
                GameplayGamepadControlsBindingsHelper
                    .SetNewGamepadStrafingBinding(newGamepadBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
