﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Camera.Sensitivity
{
    public class CameraRotationSensitivitySettingControlInfo
        : InstantIntFromRangeValueControl
    {
        public CameraRotationSensitivitySettingControlInfo(
            string label, string description, string controlId) :
            base(label, description, controlId, 1, 100)
        { }

        public override void InitStateValue()
        {
            this.currentValue = PlayerPrefs.GetInt(
                CameraSettingsUtilsInfo.CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY,
                CameraSettingsUtilsInfo.DEFAULT_CAMERA_ROTATION_SENSITIVITY
                );
            UpdateTextValueToCurrentNumberValue();
        }

        protected override void OnSelectedValueChange()
        {
            PlayerPrefs.SetInt(
                CameraSettingsUtilsInfo.CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY,
                this.currentValue);
        }
    }
}
