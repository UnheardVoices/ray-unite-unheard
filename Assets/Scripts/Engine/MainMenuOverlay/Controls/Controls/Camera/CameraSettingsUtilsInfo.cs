﻿namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera
{
    public static class CameraSettingsUtilsInfo
    {
        public const string CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY =
            "CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY";

        public const string INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY =
            "INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY";
        public const string INVERT_CAMERA_VERTICAL_AXIS_PLAYER_PREFS_KEY =
            "INVERT_CAMERA_VERTICAL_AXIS_PLAYER_PREFS_KEY";

        public const int DEFAULT_CAMERA_ROTATION_SENSITIVITY = 20;
    }
}
