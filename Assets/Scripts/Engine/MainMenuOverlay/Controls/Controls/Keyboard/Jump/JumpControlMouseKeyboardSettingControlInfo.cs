﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Jump
{
    public class JumpControlMouseKeyboardSettingControlInfo
        : ControlBindingMouseKeyboardControl
    {
        public JumpControlMouseKeyboardSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .GetCurrentMouseKeyboardJumpBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .SetNewMouseKeyboardJumpBinding(newMouseKeyboardBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
