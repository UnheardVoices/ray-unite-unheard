﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Walk
{
    public class WalkingControlMouseKeyboardSettingControlInfo
        : ControlBindingMouseKeyboardControl
    {
        public WalkingControlMouseKeyboardSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .GetCurrentMouseKeyboardWalkingBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .SetNewMouseKeyboardWalkingBinding(newMouseKeyboardBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
