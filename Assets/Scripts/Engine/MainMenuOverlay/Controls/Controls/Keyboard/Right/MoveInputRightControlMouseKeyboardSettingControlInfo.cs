﻿using Assets.Scripts.Engine.Input;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Right
{
    public class MoveInputRightControlMouseKeyboardSettingControlInfo
        : ControlBindingMouseKeyboardControl
    {
        public MoveInputRightControlMouseKeyboardSettingControlInfo(
            string label, string controlId, string description = "")
            : base(label, controlId, description)
        {
        }

        public override void InitStateValue()
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .GetCurrentMouseKeyboardRightBinding();
            SetValueText(this.controlBindingInfo.displayName);
        }

        protected override void OnApplyNewControlBinding(
            ControlBindingInfo newMouseKeyboardBindingInfo)
        {
            this.controlBindingInfo =
                GameplayMouseKeyboardControlsBindingsHelper
                    .SetNewMouseKeyboardRightBinding(newMouseKeyboardBindingInfo);
            SetValueText(this.controlBindingInfo.displayName);
        }
    }
}
