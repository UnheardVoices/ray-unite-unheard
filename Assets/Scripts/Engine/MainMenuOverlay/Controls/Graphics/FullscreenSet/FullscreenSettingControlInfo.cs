﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using Assets.Scripts.Settings;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics
{
    public class FullscreenSettingControlInfo : ConfirmableSelectValueControl
    {
        public FullscreenSettingControlInfo(
            string label, string controlId) : base(label, controlId) { }

        protected override Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts()
        {
            return Tuple.Create(
                "You've changed screen mode setting value.\nDo you want to apply it?",
                "Yes", "No");
        }

        protected override void ConfirmedApplySelectedOption()
        {
            FullScreenMode selectedFullScreenMode =
                (FullScreenMode)this.optionValues[this.selectedOptionIndex].additionalData;
            UnityEngine.Resolution currentResolution =
               PlayerPreferences.GetCurrentResolution();
            Screen.SetResolution(
               currentResolution.width,
               currentResolution.height,
               selectedFullScreenMode);
        }

        protected override List<ValueSelectionInfo> GetOptionValues()
        {
            return new List<ValueSelectionInfo>()
            {
                new ValueSelectionInfo("Fullscreen", 0,
                    FullScreenMode.ExclusiveFullScreen),
                new ValueSelectionInfo("Window Fullscreen", 1,
                    FullScreenMode.FullScreenWindow),
                // this mode is not currently supported on Windows,
                // not adding it in order to not disrupt user experience
                // in settings
                //new ValueSelectionInfo("Window Maximized", 2,
                //    FullScreenMode.MaximizedWindow),
                new ValueSelectionInfo("Windowed", 3,
                    FullScreenMode.Windowed)
            };
        }

        public override void InitStateValue()
        {
            FullScreenMode currentFullScreenMode =
                PlayerPreferences.GetFullScreenMode();
            this.selectedOptionIndex = this.optionValues.FindIndex(x =>
            {
                FullScreenMode mode = (FullScreenMode)x.additionalData;
                return mode == currentFullScreenMode;
            });

            this.selectedOptionIndex = this.selectedOptionIndex >= 0 ?
                this.selectedOptionIndex : 0;
            SetValueText(this.optionValues[this.selectedOptionIndex].label);
        }
    }
}
