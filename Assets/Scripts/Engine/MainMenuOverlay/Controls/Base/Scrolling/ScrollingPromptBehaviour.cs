﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public class ScrollingPromptBehaviour : MonoBehaviour
    {
        public Color activeColor;
        public Color inactiveColor;

        public Text text;

        public void ShowAsActive()
        {
            this.text.color = this.activeColor;
        }

        public void ShowAsInactive()
        {
            this.text.color = this.inactiveColor;
        }
    }
}
