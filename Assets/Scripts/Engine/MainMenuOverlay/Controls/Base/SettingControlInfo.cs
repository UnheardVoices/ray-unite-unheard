﻿using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public abstract class SettingControlInfo
    {
        public string label;
        public string description;
        public string controlId;

        public SettingControlBehaviour settingControlBehaviour;

        public SettingControlInfo(
            string label, string controlId,
            string description = "")
        {
            this.label = label;
            this.controlId = controlId;
            this.description = description;
        }

        protected void SetValueText(string valueText)
        {
            this.settingControlBehaviour.valueText.text = valueText;
        }

        protected virtual void PreinitStateHandler() { }

        public abstract void InitStateValue();

        protected void InitStateUi()
        {
            if (this.settingControlBehaviour)
            {
                this.settingControlBehaviour.labelText
                    .rectTransform.localScale = new Vector2(1f, 1f);
            }
        }

        public void InitState()
        {
            PreinitStateHandler();
            InitStateUi();
            InitStateValue();
        }

        public abstract bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour);

        protected abstract void OnSelectedValueChange();

        public abstract bool SelectOptionLeft(bool fromRepeatedInput = false);
        public abstract bool SelectOptionRight(bool fromRepeatedInput = false);
        public abstract void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour);
    }
}
