﻿using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue
{
    public abstract class InstantYesNoValueControl : InstantSelectValueControl
    {
        public InstantYesNoValueControl(
            string label, string controlId,
            string description = "") :
            base(label, controlId, description)
        { }

        protected override List<ValueSelectionInfo> GetOptionValues()
        {
            return new List<ValueSelectionInfo>()
            {
                new ValueSelectionInfo("No", 0, false),
                new ValueSelectionInfo("Yes", 1, true)
            };
        }

        public override void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            UpdateTextValueToCurrentOptionIndex();
            InitStateUi();
        }

        protected override void OnSelectedValueChange()
        {
            UpdateTextValueToCurrentOptionIndex();
            if (this.selectedOptionValue == 1)
            {
                OnYesValue();
            }
            else
            {
                OnNoValue();
            }
        }

        public override void InitStateValue()
        {
            bool value = GetInitialTrueFalseControlValue();
            this.selectedOptionIndex = value ? 1 : 0;
            this.selectedOptionValue = value ? 1 : 0;
            UpdateTextValueToCurrentOptionIndex();
        }

        protected abstract bool GetInitialTrueFalseControlValue();
        protected abstract void OnYesValue();
        protected abstract void OnNoValue();
    }
}
