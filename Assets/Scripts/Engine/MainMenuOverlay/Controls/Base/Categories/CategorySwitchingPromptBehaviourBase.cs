﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories
{
    public static class CategorySwitchPromptParts
    {
        public const string relatedCategoryNameTextPanel =
            "relatedCategoryNameTextPanel";
        public const string keyInputPromptTextPanel =
            "keyInputPromptTextPanel";
    }

    public abstract class CategorySwitchingPromptBehaviourBase : MonoBehaviour
    {
        protected Font font;
        protected int fontSize;
        protected Color textColor;
        protected Color categoryChangePromptKeyTextColor;
        protected int inputPromptFontSize;

        protected Text relatedCategoryNameText;
        protected Text keyInputPromptKeyboardText;
        protected Text keyInputPromptGamepadText;

        protected float desiredPanelWidth;
        protected float desiredPanelHeight;

        protected float optionOscillationSpeed;
        protected float timeProgress = 0f;

        public void InitGuiInfo(
            Font font,
            int fontSize,
            int inputPromptFontSize,
            Color textColor,
            Color categoryChangePromptKeyTextColor,
            float optionOscillationSpeed,
            float timeOffset = 0f)
        {
            this.font = font;
            this.timeProgress = timeOffset;
            this.fontSize = fontSize;
            this.inputPromptFontSize = inputPromptFontSize;
            this.textColor = textColor;
            this.categoryChangePromptKeyTextColor = categoryChangePromptKeyTextColor;
            this.optionOscillationSpeed = optionOscillationSpeed;
        }

        protected abstract Text InputPromptTextUiProvider(
            float desiredPanelWidth, string keyInputPrompt);

        public void RepropagatePanel(
            float desiredPanelWidth,
            float desiredPanelHeight,
            string? relatedCategoryName = null,
            string? keyInputKeyboardPrompt = null,
            string? keyInputGamepadPrompt = null)
        {
            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            this.desiredPanelWidth = desiredPanelWidth;
            this.desiredPanelHeight = desiredPanelHeight;

            if (relatedCategoryName == null)
            {
                relatedCategoryName = GetCategoryNamePromptText();
            }

            if (keyInputKeyboardPrompt == null)
            {
                keyInputKeyboardPrompt = GetInputPromptKeyboardText();
            }

            if (keyInputGamepadPrompt == null)
            {
                keyInputGamepadPrompt = GetInputPromptGamepadText();
            }

            Text relatedCategoryNameText =
                UnityUiTextFactory.GetCenteredTextLabel(
                    CategorySwitchPromptParts.relatedCategoryNameTextPanel,
                    this.font, this.fontSize, relatedCategoryName,
                    this.textColor, desiredPanelWidth);

            relatedCategoryNameText.gameObject.AddComponent<Outline>();
            relatedCategoryNameText.gameObject.AddComponent<Outline>();
            relatedCategoryNameText.gameObject.AddComponent<Outline>();
            Text keyInputPromptKeyboardText = InputPromptTextUiProvider(
                desiredPanelWidth, keyInputKeyboardPrompt);

            keyInputPromptKeyboardText.gameObject.AddComponent<Outline>();
            keyInputPromptKeyboardText.gameObject.AddComponent<Outline>();
            keyInputPromptKeyboardText.gameObject.AddComponent<Outline>();

            relatedCategoryNameText.transform.SetParent(this.gameObject.transform, false);
            keyInputPromptKeyboardText.transform.SetParent(this.gameObject.transform, false);

            Text keyInputPromptGamepadText = InputPromptTextUiProvider(
                desiredPanelWidth, keyInputGamepadPrompt);

            keyInputPromptGamepadText.gameObject.AddComponent<Outline>();
            keyInputPromptGamepadText.gameObject.AddComponent<Outline>();
            keyInputPromptGamepadText.gameObject.AddComponent<Outline>();

            keyInputPromptGamepadText.transform.SetParent(this.gameObject.transform, false);

            this.relatedCategoryNameText = relatedCategoryNameText;
            this.keyInputPromptKeyboardText = keyInputPromptKeyboardText;
            this.keyInputPromptGamepadText = keyInputPromptGamepadText;

            this.gameObject.AddComponent<VerticalLayoutGroup>();

            GetComponent<RectTransform>().sizeDelta =
                new Vector2(desiredPanelWidth, desiredPanelHeight);
        }

        private void Update()
        {
            this.timeProgress += Time.deltaTime;
            this.keyInputPromptKeyboardText.rectTransform.localScale =
                new Vector3(1f, 1f) * (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress *
                        this.optionOscillationSpeed));
            this.keyInputPromptGamepadText.rectTransform.localScale =
                new Vector3(1f, 1f) * (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress *
                        this.optionOscillationSpeed));
        }

        public void HidePrompt()
        {
            this.gameObject.SetActive(false);
        }
        public void ChangeCategoryNameInPrompt(string categoryName)
        {
            RepropagatePanel(
                this.desiredPanelWidth, this.desiredPanelHeight, categoryName);
        }
        public void ShowPrompt()
        {
            this.gameObject.SetActive(true);
        }

        protected abstract string GetInputPromptKeyboardText();

        protected abstract string GetInputPromptGamepadText();

        protected abstract string GetCategoryNamePromptText();
    }
}
