﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories
{
    public class CategoriesSwitchPromptsContainerBehaviour : MonoBehaviour
    {
        public PreviousCategoryPromptBehaviour previousCategoryPromptPanel;
        public NextCategoryPromptBehaviour nextCategoryPromptPanel;

        public void SetGameObjectActiveIfMoreThanOneCategory(int categoriesCount)
        {
            if (categoriesCount > 1)
            {
                this.gameObject.SetActive(true);
            }
        }

        public void ShowAndHideAccordingly(
            int activeSettingsCategoryIndex,
            List<SettingsCategoryInfo> settingsCategoriesInfos)
        {
            if (activeSettingsCategoryIndex == 0)
            {
                this.previousCategoryPromptPanel.HidePrompt();
                if (settingsCategoriesInfos.Count > 1)
                {
                    this.nextCategoryPromptPanel.ShowPrompt();
                    this.nextCategoryPromptPanel.ChangeCategoryNameInPrompt(
                        settingsCategoriesInfos[
                            activeSettingsCategoryIndex + 1].GetCategoryName()
                    );
                }
            }
            else if (activeSettingsCategoryIndex ==
              settingsCategoriesInfos.Count - 1)
            {
                this.nextCategoryPromptPanel.HidePrompt();
                if (settingsCategoriesInfos.Count > 1)
                {
                    this.previousCategoryPromptPanel.ShowPrompt();
                    this.previousCategoryPromptPanel.ChangeCategoryNameInPrompt(
                        settingsCategoriesInfos[
                            activeSettingsCategoryIndex - 1].GetCategoryName()
                    );
                }
            }
            else
            {
                this.previousCategoryPromptPanel.ShowPrompt();
                this.nextCategoryPromptPanel.ShowPrompt();

                this.previousCategoryPromptPanel.ChangeCategoryNameInPrompt(
                        settingsCategoriesInfos[
                            activeSettingsCategoryIndex - 1].GetCategoryName()
                    );
                this.nextCategoryPromptPanel.ChangeCategoryNameInPrompt(
                        settingsCategoriesInfos[
                            activeSettingsCategoryIndex + 1].GetCategoryName()
                    );
            }
        }
    }
}
