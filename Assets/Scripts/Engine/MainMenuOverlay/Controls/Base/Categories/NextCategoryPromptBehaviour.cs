﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories
{
    public class NextCategoryPromptBehaviour : CategorySwitchingPromptBehaviourBase
    {
        protected override string GetCategoryNamePromptText()
        {
            return "Next Settings Category";
        }

        protected override string GetInputPromptGamepadText()
        {
            return "R1";
        }

        protected override string GetInputPromptKeyboardText()
        {
            return "RCtrl";
        }

        protected override Text InputPromptTextUiProvider(float desiredPanelWidth, string keyInputPrompt)
        {
            return UnityUiTextFactory.GetCenteredTextLabel(
                CategorySwitchPromptParts.keyInputPromptTextPanel,
                this.font, this.inputPromptFontSize, keyInputPrompt,
                this.categoryChangePromptKeyTextColor, desiredPanelWidth);
        }
    }
}
