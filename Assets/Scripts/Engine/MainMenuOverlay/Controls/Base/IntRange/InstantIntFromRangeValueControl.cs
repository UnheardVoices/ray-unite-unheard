﻿using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange
{
    public abstract class InstantIntFromRangeValueControl : IntFromRangeValueControl
    {
        public InstantIntFromRangeValueControl(
            string label,
            string description,
            string controlId,
            int rangeStart,
            int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues = null) :
                base(label, description, controlId,
                    rangeStart, rangeEnd, additionalValues)
        {
        }

        public override void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            UpdateTextValueToCurrentNumberValue();
            InitStateUi();
        }

        public override bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            return true;
        }
    }
}
