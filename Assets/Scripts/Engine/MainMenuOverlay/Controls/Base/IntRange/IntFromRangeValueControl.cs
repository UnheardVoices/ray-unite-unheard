﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base
{
    public class AdditionalIntValueInfo
    {
        public string label;
        public int value;

        public AdditionalIntValueInfo(string label, int value)
        {
            this.label = label;
            this.value = value;
        }
    }

    public abstract class IntFromRangeValueControl : SettingControlInfo
    {
        protected int rangeStart;
        protected int rangeEnd;

        protected int currentValue;

        protected List<AdditionalIntValueInfo> additionalValues;

        public IntFromRangeValueControl(
            string label,
            string description,
            string controlId,
            int rangeStart,
            int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues = null) :
                base(label, controlId, description)
        {
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;

            if (additionalValues != null)
            {
                this.additionalValues = additionalValues;
            }
            else
            {
                this.additionalValues = new List<AdditionalIntValueInfo>();
            }
        }

        protected void UpdateTextValueToCurrentNumberValue()
        {
            SetValueText(GetSelectedValueAsString());
        }

        protected string GetSelectedValueAsString()
        {
            string result = "";
            if (this.currentValue >= this.rangeStart && this.currentValue <= this.rangeEnd)
            {
                result = this.currentValue.ToString();
            }
            else
            {
                AdditionalIntValueInfo additionalValueCandidate =
                    this.additionalValues.Where(x => x.value == this.currentValue)
                        .FirstOrDefault();

                result = additionalValueCandidate != null
                    ? additionalValueCandidate.label
                    : this.currentValue.ToString();
            }
            return result;
        }

        public override bool SelectOptionLeft(bool fromRepeatedInput = false)
        {
            if (this.currentValue > this.rangeStart && this.currentValue <= this.rangeEnd)
            {
                this.currentValue--;
            }
            else
            {
                this.currentValue =
                    IntFromRangeValueHelper.GetFirstLessOrEqualValueThan(
                        this.currentValue, this.rangeStart, this.rangeEnd, this.additionalValues);
            }
            UpdateTextValueToCurrentNumberValue();
            OnSelectedValueChange();
            return true;
        }

        public override bool SelectOptionRight(bool fromRepeatedInput = false)
        {
            if (this.currentValue >= this.rangeStart && this.currentValue < this.rangeEnd)
            {
                this.currentValue++;
            }
            else
            {
                this.currentValue =
                    IntFromRangeValueHelper.GetFirstBiggerOrEqualValueThan(
                        this.currentValue, this.rangeStart, this.rangeEnd, this.additionalValues);
            }
            UpdateTextValueToCurrentNumberValue();
            OnSelectedValueChange();
            return true;
        }
    }
}
