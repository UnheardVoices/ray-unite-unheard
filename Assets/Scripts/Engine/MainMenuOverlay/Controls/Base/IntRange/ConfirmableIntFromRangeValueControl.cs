﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange
{
    public abstract class ConfirmableIntFromRangeValueControl : IntFromRangeValueControl
    {
        protected bool isSelectedOptionApplied;

        public ConfirmableIntFromRangeValueControl(
            string label,
            string description,
            string controlId,
            int rangeStart,
            int rangeEnd,
            List<AdditionalIntValueInfo> additionalValues = null) :
                base(label, description, controlId,
                    rangeStart, rangeEnd, additionalValues)
        {
        }

        protected override void PreinitStateHandler()
        {
            this.isSelectedOptionApplied = true;
        }

        protected abstract void ConfirmedApplySelectedValue();

        public override void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            ConfirmedApplySelectedValue();
            UpdateTextValueToCurrentNumberValue();
            this.isSelectedOptionApplied = true;
            gameplayMenuBehaviour.SetActiveDialog(null);
            InitStateUi();
        }

        protected void OptionApplyRejectHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.SetActiveDialog(null);
            InitState();
        }

        public override bool OnControlLeaving(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.isSelectedOptionApplied)
            {
                return true;
            }
            else
            {
                Tuple<string, string, string>
                    dialogCaptions = GetOptionApplyConfirmDialogTexts();
                ShowDialogForConfirmingValueApplying(
                    gameplayMenuBehaviour,
                    dialogCaptions.Item1, dialogCaptions.Item2, dialogCaptions.Item3);
                return false;
            }
        }

        private void ShowDialogForConfirmingValueApplying(
            GameplayMenuBehaviour gameplayMenuBehaviour,
            string message, string confirmLabel,
            string rejectLabel)
        {
            gameplayMenuBehaviour
                .SetActiveDialog(
                    SettingsMenuSectionDialogs.SETTING_CONTROL_VALUE_APPLY_DIALOG);

            // this FindObjectOfType thing is a quick solution for now
            // as at the moment of writing this comment we have only
            // one settings section so far (graphics setting section)
            // in the future we need to ensure we always fetch
            // proper behaviour component for such setting value applying
            // dialog that is appropriate for the given menu settings section
            // in question
            SettingControlValueApplyDialogBehaviour dialogBehaviour =
                UnityEngine.Object
                    .FindObjectOfType<SettingControlValueApplyDialogBehaviour>();

            dialogBehaviour.SetLabels(message, confirmLabel, rejectLabel);
            dialogBehaviour.SetConfirmHandler(ApplySelectedOption);
            dialogBehaviour.SetRejectHandler(OptionApplyRejectHandler);
        }

        protected abstract Tuple<string, string, string>
            GetOptionApplyConfirmDialogTexts();

        protected override void OnSelectedValueChange()
        {
            this.isSelectedOptionApplied = false;
        }
    }
}
