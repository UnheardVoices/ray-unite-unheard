using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.SelectValue;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Controls.Sounds
{
    public class SoundUtils :
        InstantYesNoValueControl
    {
        public SoundUtils(
            string label, string controlId,
            string description = "") :
            base(label, controlId, description)
        { }

        protected override bool GetInitialTrueFalseControlValue()
        {
            int value = PlayerPrefs.GetInt(SoundUtilsInfo.SOUND_ENABLED, 0);
            MusicController.musicControllerInstance.ToggleMusic();
            return value == 1;

        }

        protected override void OnNoValue()
        {
            PlayerPrefs.SetInt(SoundUtilsInfo.SOUND_ENABLED, 0);
            MusicController.musicControllerInstance.ToggleMusic();
        }

        protected override void OnYesValue()
        {
            PlayerPrefs.SetInt(SoundUtilsInfo.SOUND_ENABLED, 1);
            MusicController.musicControllerInstance.ToggleMusic();
        }
    }
}

