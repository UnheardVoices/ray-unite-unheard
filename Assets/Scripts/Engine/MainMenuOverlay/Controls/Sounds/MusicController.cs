using UnityEngine;

public class MusicController : MonoBehaviour
{
    private AudioSource musicSource;
    public static MusicController musicControllerInstance;
    private void Awake()
    {
        if (musicControllerInstance == null)
        {
            musicControllerInstance = this;
        }
        this.musicSource = GameObject.FindWithTag("Music").GetComponent<AudioSource>();
        ToggleMusic();
    }

    public void ToggleMusic()
    {
        if (this.musicSource)
        {
            this.musicSource.mute = PlayerPrefs.GetInt("SOUND_ENABLED") == 0;
        }
    }
}
