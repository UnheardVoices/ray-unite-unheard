using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.IntRange;
using UnityEngine;

public class MusicSettingControlInfo : InstantIntFromRangeValueControl
{
    public MusicSettingControlInfo(
        string label, string description, string controlId) :
        base(label, description, controlId, 1, 100)
    { }

    public override void InitStateValue()
    {
        this.currentValue = PlayerPrefs.GetInt(
            SoundUtilsInfo.MUSIC_VOLUME_PLAYER_PREFS_KEY,
            SoundUtilsInfo.DEFAULT_MUSIC_VOLUME
        );
        UpdateTextValueToCurrentNumberValue();
    }

    protected override void OnSelectedValueChange()
    {
        PlayerPrefs.SetInt(
            SoundUtilsInfo.MUSIC_VOLUME_PLAYER_PREFS_KEY,
            this.currentValue);
    }
}
