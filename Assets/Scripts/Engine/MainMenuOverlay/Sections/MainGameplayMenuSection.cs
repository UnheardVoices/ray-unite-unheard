﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections
{
    public enum MainGameplayMenuSectionOption
    {
        RESUME,
        RESTART,
        SETTINGS,
        EXIT_GAME,
        MAIN_MENU
    }

    public static class MainGameplayMenuSectionDialogs
    {
        public const string RESTART_SCENE_DIALOG = "RESTART_SCENE_DIALOG";
        public const string EXIT_GAME_DIALOG = "EXIT_GAME_DIALOG";
        public const string MAIN_MENU_DIALOG = "MAIN_MENU_DIALOG";
    }

    //public class MainGameplayMenuSection : SimpleOptionsMenuSection
    public class MainGameplayMenuSection : SimpleOptionsMenuSection

    {
        public static MainGameplayMenuSection
            PropagatePanelUnderMenuCanvas(
                GameObject menuCanvasObject, Vector2 referenceResolution,
                Color backgroundColor)
        {
            return PropagateSimpleOptionsMenuPanel<MainGameplayMenuSection>(
                menuCanvasObject, referenceResolution, "MainMenuSectionPanel",
                backgroundColor);
        }

        protected override List<MenuDialogDefinition> GetDialogsDefinitions()
        {
            MenuDialogDefinition restartSceneDialog =
                new MenuDialogDefinition(
                    "Are you sure you want to restart the scene?",
                    "Yes", "No", MainGameplayMenuSectionDialogs.RESTART_SCENE_DIALOG,
                    DialogClass.RESTART_SCENE_DIALOG,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize);

            MenuDialogDefinition exitGameDialog =
                new MenuDialogDefinition(
                    "Are you sure you want to exit the game?",
                    "Yes", "No", MainGameplayMenuSectionDialogs.EXIT_GAME_DIALOG,
                    DialogClass.EXIT_GAME_DIALOG,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize
                    );

            MenuDialogDefinition mainMenuDialog =
                new MenuDialogDefinition(
                    "Are you sure you want to go to the main menu?",
                    "Yes", "No", MainGameplayMenuSectionDialogs.MAIN_MENU_DIALOG,
                    DialogClass.MAIN_MENU_DIALOG,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize
                    );

            return new List<MenuDialogDefinition>() {
                restartSceneDialog, exitGameDialog, mainMenuDialog };
        }

        protected override bool ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (!base.ApplySelectedOption(gameplayMenuBehaviour))
            {
                if (this.selectedOptionValue == (int)MainGameplayMenuSectionOption.EXIT_GAME)
                {
                    SetActiveDialog(MainGameplayMenuSectionDialogs.EXIT_GAME_DIALOG);
                }
                else if (this.selectedOptionValue == (int)MainGameplayMenuSectionOption.MAIN_MENU)
                {
                    SetActiveDialog(MainGameplayMenuSectionDialogs.MAIN_MENU_DIALOG);
                }
                else if (this.selectedOptionValue == (int)MainGameplayMenuSectionOption.RESTART)
                {
                    SetActiveDialog(MainGameplayMenuSectionDialogs.RESTART_SCENE_DIALOG);
                }
                else if (this.selectedOptionValue == (int)MainGameplayMenuSectionOption.SETTINGS)
                {
                    gameplayMenuBehaviour.SetActiveMenuMainSection(
                        MenuMainSectionEnum.SETTINGS_CHOICE_SECTION);
                }
                else if (this.selectedOptionValue == (int)MainGameplayMenuSectionOption.RESUME)
                {
                    gameplayMenuBehaviour.ExitMenu();
                }
            }
            return true;
        }

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.ExitMenu();
            return true;
        }

        protected override List<SimpleMenuOptionInfo> GetOptions()
        {
            return new List<SimpleMenuOptionInfo>()
            {
                new SimpleMenuOptionInfo("Resume",
                    (int)MainGameplayMenuSectionOption.RESUME),
                new SimpleMenuOptionInfo("Restart",
                    (int)MainGameplayMenuSectionOption.RESTART),
                new SimpleMenuOptionInfo("Settings",
                    (int)MainGameplayMenuSectionOption.SETTINGS),
                new SimpleMenuOptionInfo("Exit game",
                    (int)MainGameplayMenuSectionOption.EXIT_GAME),
                new SimpleMenuOptionInfo("Main menu",
                    (int)MainGameplayMenuSectionOption.MAIN_MENU)
            };
        }
    }
}
