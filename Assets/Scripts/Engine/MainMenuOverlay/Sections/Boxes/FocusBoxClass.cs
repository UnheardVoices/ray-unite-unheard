﻿namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes
{
    public enum FocusBoxClass
    {
        KEYBOARD_MOUSE_BINDING_READING_FOCUS_BOX,
        GAMEPAD_BINDING_READING_FOCUS_BOX
    }
}
