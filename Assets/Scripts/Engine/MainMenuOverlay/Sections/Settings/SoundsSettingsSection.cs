using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Sounds;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings
{
    public class SoundsSettingsSectionControls
    {
        public const string DIALOGUES_CONTROL = "DIALOGUES_CONTROL";
        public const string MUSIC_VOLUME = "MUSIC_VOLUME";
        public const string VARIOUS_SFX_CONTROL = "VARIOUS_SFX_CONTROL";
    }

    public class SoundsSettingsSection : SettingsMenuSectionBase
    {
        public static SoundsSettingsSection
            PropagatePanelUnderMenuCanvas(
                GameObject menuCanvasObject, Vector2 referenceResolution,
                Color backgroundColor)
        {
            return PropagateSettingsSectionMenuPanel<SoundsSettingsSection>(
                menuCanvasObject, referenceResolution, "SoundsSettingsSectionPanel",
                backgroundColor);
        }

        //Dont need panel propagation

        protected override List<SettingsCategoryInfo> GetSettingsCategories()
        {
            return new List<SettingsCategoryInfo>()
            {
                new ConstructedSettingsCategoryInfo(
                    "Sound",
                    this.settingsSingleCategoryUiInfo,
                    new List<SettingControlInfo>()
                    {
                        new SoundUtils("Enable Sound",
                            "ENABLE_SOUND",
                            "Enable the sound"),
                        
                        //Music
                        
                        new MusicSettingControlInfo("Music volume", "Manage the volume of the music", SoundsSettingsSectionControls.MUSIC_VOLUME)
                        
                        //DIALOGUES

                        //VARIOUS SFX
                    })
            };
        }

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.ExitSectionHandler(gameplayMenuBehaviour))
            {
                gameplayMenuBehaviour
                    .SetActiveMenuMainSection(
                        MenuMainSectionEnum.SETTINGS_CHOICE_SECTION);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
