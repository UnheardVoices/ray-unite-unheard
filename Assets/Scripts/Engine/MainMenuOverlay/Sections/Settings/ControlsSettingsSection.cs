﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Camera.Sensitivity;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera.Sensitivity;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Attack;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.AttackSecondary;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Jump;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Roll;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Gamepad.Strafe;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Attack;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Down;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Jump;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Left;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Right;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Roll;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Strafe;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Keyboard.Walk;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Up;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings
{
    public static class ControlSettingsSectionControls
    {
        public const string MOVE_INPUT_UP_MOUSE_KEYBOARD_CONTROL = "MOVE_INPUT_UP_MOUSE_KEYBOARD_CONTROL";
        public const string MOVE_INPUT_LEFT_MOUSE_KEYBOARD_CONTROL = "MOVE_INPUT_LEFT_MOUSE_KEYBOARD_CONTROL";
        public const string MOVE_INPUT_RIGHT_MOUSE_KEYBOARD_CONTROL = "MOVE_INPUT_RIGHT_MOUSE_KEYBOARD_CONTROL";
        public const string MOVE_INPUT_DOWN_MOUSE_KEYBOARD_CONTROL = "MOVE_INPUT_DOWN_MOUSE_KEYBOARD_CONTROL";

        public const string WALKING_MOUSE_KEYBOARD_CONTROL = "WALKING_KEYBOARD_CONTROL";
        public const string GROUND_ROLL_MOUSE_KEYBOARD_CONTROL = "GROUND_ROLL_KEYBOARD_CONTROL";
        public const string STRAFING_MOUSE_KEYBOARD_CONTROL = "STRAFING_KEYBOARD_CONTROL";
        public const string ATTACK_MOUSE_KEYBOARD_CONTROL = "ATTACK_KEYBOARD_CONTROL";
        public const string JUMP_MOUSE_KEYBOARD_CONTROL = "JUMP_KEYBOARD_CONTROL";

        public const string JUMP_GAMEPAD_CONTROL = "JUMP_GAMEPAD_CONTROL";
        public static string ATTACK_SECONDARY_GAMEPAD_CONTROL =
            "ATTACK_SECONDARY_GAMEPAD_CONTROL";
        public static string ATTACK_PRIMARY_GAMEPAD_CONTROL =
            "ATTACK_PRIMARY_GAMEPAD_CONTROL";
        public static string STRAFING_PRIMARY_GAMEPAD_CONTROL =
            "STRAFING_PRIMARY_GAMEPAD_CONTROL";
        public static string GROUND_ROLL_PRIMARY_GAMEPAD_CONTROL =
            "GROUND_ROLL_PRIMARY_GAMEPAD_CONTROL";

        public static string CAMERA_ROTATION_SENSITIVITY_CONTROL =
            "CAMERA_ROTATION_SENSITIVITY_CONTROL";
        public static string INVERT_CAMERA_HORIZONTAL_AXIS_CONTROL =
            "INVERT_CAMERA_HORIZONTAL_AXIS_CONTROL";
        public static string INVERT_CAMERA_VERTICAL_AXIS_CONTROL =
            "INVERT_CAMERA_VERTICAL_AXIS_CONTROL";
    }

    public static class ControlsSettingsMenuSectionFocusBoxes
    {
        public const string MOUSE_KEYBOARD_BINDING_READING_FOCUS_BOX =
            "MOUSE_KEYBOARD_BINDING_READING_FOCUS_BOX";
        public const string GAMEPAD_BINDING_READING_FOCUS_BOX =
            "GAMEPAD_BINDING_READING_FOCUS_BOX";
    }

    public class ControlsSettingsSection : SettingsMenuSectionBase
    {
        public static ControlsSettingsSection
            PropagatePanelUnderMenuCanvas(
                GameObject menuCanvasObject, Vector2 referenceResolution,
                Color backgroundColor)
        {
            return PropagateSettingsSectionMenuPanel<ControlsSettingsSection>(
                menuCanvasObject, referenceResolution, "ControlsSettingsSectionPanel",
                backgroundColor);
        }

        protected override List<MenuFocusBoxDefinition> GetFocusBoxesDefinitions()
        {
            MenuFocusBoxDefinition keyboardBindingReadingFocusBox =
                new MenuFocusBoxDefinition(
                    "MESSAGE",
                    ControlsSettingsMenuSectionFocusBoxes.MOUSE_KEYBOARD_BINDING_READING_FOCUS_BOX,
                    FocusBoxClass.KEYBOARD_MOUSE_BINDING_READING_FOCUS_BOX,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize);

            MenuFocusBoxDefinition gamepadBindingReadingFocusBox =
                new MenuFocusBoxDefinition(
                    "MESSAGE",
                    ControlsSettingsMenuSectionFocusBoxes.GAMEPAD_BINDING_READING_FOCUS_BOX,
                    FocusBoxClass.GAMEPAD_BINDING_READING_FOCUS_BOX,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize
                    );

            return new List<MenuFocusBoxDefinition>() {
                keyboardBindingReadingFocusBox, gamepadBindingReadingFocusBox };
        }

        protected override List<SettingsCategoryInfo> GetSettingsCategories()
        {
            return new List<SettingsCategoryInfo>()
            {
                new ConstructedSettingsCategoryInfo(
                    "Camera",
                    this.settingsSingleCategoryUiInfo,
                    new List<SettingControlInfo>()
                    {
                        new CameraRotationSensitivitySettingControlInfo(
                            "Rotation Sensitivity",
                            "Camera rotation sensitivity when moving mouse and gamepad right stick",
                            ControlSettingsSectionControls.CAMERA_ROTATION_SENSITIVITY_CONTROL
                            ),
                        new InvertHorizontalAxisSettingControlInfo(
                            "Invert X-Axis",
                            ControlSettingsSectionControls.INVERT_CAMERA_HORIZONTAL_AXIS_CONTROL,
                            "Inverts camera rotation's horizontal axis"
                            ),
                        new InvertVerticalAxisSettingControlInfo(
                            "Invert Y-Axis",
                            ControlSettingsSectionControls.INVERT_CAMERA_VERTICAL_AXIS_CONTROL,
                            "Inverts camera rotation's vertical axis"
                            )
                    }),
                new ConstructedSettingsCategoryInfo(
                    "Mouse&Keyboard",
                    this.settingsSingleCategoryUiInfo,
                    new List<SettingControlInfo>()
                    {
                        new MoveInputUpControlMouseKeyboardSettingControlInfo(
                            "Up", ControlSettingsSectionControls.MOVE_INPUT_UP_MOUSE_KEYBOARD_CONTROL),
                        new MoveInputLeftControlMouseKeyboardSettingControlInfo(
                            "Left", ControlSettingsSectionControls.MOVE_INPUT_LEFT_MOUSE_KEYBOARD_CONTROL),
                        new MoveInputRightControlMouseKeyboardSettingControlInfo(
                            "Right", ControlSettingsSectionControls.MOVE_INPUT_RIGHT_MOUSE_KEYBOARD_CONTROL),
                        new MoveInputDownControlMouseKeyboardSettingControlInfo(
                            "Down", ControlSettingsSectionControls.MOVE_INPUT_DOWN_MOUSE_KEYBOARD_CONTROL),
                        new WalkingControlMouseKeyboardSettingControlInfo(
                            "Walk", ControlSettingsSectionControls.WALKING_MOUSE_KEYBOARD_CONTROL),
                        new GroundRollControlMouseKeyboardSettingControlInfo(
                            "Ground Roll", ControlSettingsSectionControls.GROUND_ROLL_MOUSE_KEYBOARD_CONTROL),
                        new StrafingControlMouseKeyboardSettingControlInfo(
                            "Strafing", ControlSettingsSectionControls.STRAFING_MOUSE_KEYBOARD_CONTROL),
                        new AttackControlMouseKeyboardSettingControlInfo(
                            "Attack", ControlSettingsSectionControls.ATTACK_MOUSE_KEYBOARD_CONTROL),
                        new JumpControlMouseKeyboardSettingControlInfo(
                            "Jump", ControlSettingsSectionControls.JUMP_MOUSE_KEYBOARD_CONTROL)
                    }),
                new ConstructedSettingsCategoryInfo(
                    "Gamepad",
                    this.settingsSingleCategoryUiInfo,
                    new List<SettingControlInfo>()
                    {
                        new GroundRollControlGamepadSettingControlInfo(
                            "Ground Roll",
                                ControlSettingsSectionControls.GROUND_ROLL_PRIMARY_GAMEPAD_CONTROL),
                        new StrafingControlGamepadSettingControlInfo(
                            "Strafing",
                            ControlSettingsSectionControls.STRAFING_PRIMARY_GAMEPAD_CONTROL),
                        new AttackPrimaryControlGamepadSettingControlInfo(
                            "Attack",
                            ControlSettingsSectionControls.ATTACK_PRIMARY_GAMEPAD_CONTROL),
                        new AttackSecondaryControlGamepadSettingControlInfo(
                            "Attack 2",
                            ControlSettingsSectionControls.ATTACK_SECONDARY_GAMEPAD_CONTROL),
                        new JumpControlGamepadSettingControlInfo(
                            "Jump", ControlSettingsSectionControls.JUMP_GAMEPAD_CONTROL)
                    }),
            };
        }

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.ExitSectionHandler(gameplayMenuBehaviour))
            {
                gameplayMenuBehaviour
                    .SetActiveMenuMainSection(
                        MenuMainSectionEnum.SETTINGS_CHOICE_SECTION);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
