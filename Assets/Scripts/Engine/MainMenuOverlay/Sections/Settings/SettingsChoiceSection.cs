﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings
{
    public enum SettingsChoiceSectionOption
    {
        GRAPHICS_SETTINGS,
        SOUND_SETTINGS,
        CONTROLS_SETTINGS,
        BACK_TO_MAIN_MENU
    }

    public class SettingsChoiceSection : SimpleOptionsMenuSection
    {
        public static SettingsChoiceSection
            PropagatePanelUnderMenuCanvas(
                GameObject menuCanvasObject, Vector2 referenceResolution,
                Color backgroundColor)
        {
            return PropagateSimpleOptionsMenuPanel<SettingsChoiceSection>(
                menuCanvasObject, referenceResolution, "SettingsChoiceSectionPanel",
                backgroundColor);
        }

        //protected override List<MenuDialogDefinition> GetDialogsDefinitions()
        //{
        //    return base.GetDialogsDefinitions();
        //}

        protected override bool ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (!base.ApplySelectedOption(gameplayMenuBehaviour))
            {
                if (this.selectedOptionValue ==
                    (int)SettingsChoiceSectionOption.BACK_TO_MAIN_MENU)
                {
                    gameplayMenuBehaviour.SetActiveMenuMainSection(
                        MenuMainSectionEnum.MAIN_MENU_SECTION);
                }
                else if (this.selectedOptionValue ==
                         (int)SettingsChoiceSectionOption.GRAPHICS_SETTINGS)
                {
                    gameplayMenuBehaviour.SetActiveMenuMainSection(
                        MenuMainSectionEnum.GRAPHICS_SETTINGS_SECTION);
                }
                else if (this.selectedOptionValue ==
                         (int)SettingsChoiceSectionOption.SOUND_SETTINGS)
                {
                    gameplayMenuBehaviour.SetActiveMenuMainSection(
                        MenuMainSectionEnum.SOUNDS_SETTINGS_SECTION);
                }
                else if (this.selectedOptionValue ==
                         (int)SettingsChoiceSectionOption.CONTROLS_SETTINGS)
                {
                    gameplayMenuBehaviour.SetActiveMenuMainSection(
                        MenuMainSectionEnum.CONTROLS_SETTINGS_SECTION
                        );
                }
            }
            return true;
        }

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.SetActiveMenuMainSection(
                MenuMainSectionEnum.MAIN_MENU_SECTION);
            return true;
        }

        protected override List<SimpleMenuOptionInfo> GetOptions()
        {
            return new List<SimpleMenuOptionInfo>()
            {
                new SimpleMenuOptionInfo("Video",
                    (int)SettingsChoiceSectionOption.GRAPHICS_SETTINGS),
                new SimpleMenuOptionInfo("Audio",
                    (int)SettingsChoiceSectionOption.SOUND_SETTINGS),
                new SimpleMenuOptionInfo("Controls",
                    (int)SettingsChoiceSectionOption.CONTROLS_SETTINGS),
                new SimpleMenuOptionInfo("Back",
                    (int)SettingsChoiceSectionOption.BACK_TO_MAIN_MENU)
            };
        }
    }
}
