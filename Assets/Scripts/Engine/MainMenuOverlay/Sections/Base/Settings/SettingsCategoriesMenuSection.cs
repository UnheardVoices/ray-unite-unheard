﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using System.Collections.Generic;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings
{
    public abstract class SettingsCategoriesMenuSection : SettingsMenuSectionBase
    {
        protected List<SettingsCategoryInfo> settingsCategoriesInfos =
            new List<SettingsCategoryInfo>();

        protected int activeSettingsCategoryIndex = 0;
    }
}
