﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.BuildMainMenuOverlay.Settings;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Base.Settings
{
    public class SettingsSingleCategoryUiInfo
    {
        public int maximalAmountOfControlsVisibleAtOnce = 5;
        public int descriptionFontSize = 1;
        public int settingControlFontSize = 1;
        public float selectedOptionOscillationSpeed = 6f;
        public Font font;
        public int baseFontSize;
        public Color textColor;

        public Color scrollActiveColor;
        public Color scrollInactiveColor;
    }

    public abstract class SettingsMenuSectionBase : MenuSectionBehaviour
    {
        protected SettingsSingleCategoryUiInfo
            settingsSingleCategoryUiInfo;

        protected List<SettingsCategoryInfo> settingsCategoriesInfos =
            new List<SettingsCategoryInfo>();

        protected int activeSettingsCategoryIndex = 0;

        protected CategoriesSwitchPromptsContainerBehaviour
            categoryChangingPromptsHandle;
        protected SelectedCategoryPromptBehaviour
            selectedCategoryPromptHandle;

        protected Color categoryChangePromptKeyTextColor;

        protected override List<MenuDialogDefinition> GetDialogsDefinitions()
        {
            MenuDialogDefinition settingControlOptionApplyingDialog =
                new MenuDialogDefinition(
                    "MESSAGE",
                    "Yes", "No",
                    SettingsMenuSectionDialogs.SETTING_CONTROL_VALUE_APPLY_DIALOG,
                    DialogClass.SETTING_CONTROL_VALUE_APPLY_DIALOG,
                    this.backgroundColor, this.textColor, this.font, this.baseFontSize);

            return new List<MenuDialogDefinition>() { settingControlOptionApplyingDialog };
        }

        public override void FirstInitState(Color backgroundColor)
        {
            base.FirstInitState(backgroundColor);
            this.settingsSingleCategoryUiInfo = new SettingsSingleCategoryUiInfo
            {
                settingControlFontSize =
                (int)(this.baseFontSize / 1.6f)
            };
            this.settingsSingleCategoryUiInfo.descriptionFontSize =
                this.settingsSingleCategoryUiInfo.settingControlFontSize / 2;
            this.settingsSingleCategoryUiInfo
                .maximalAmountOfControlsVisibleAtOnce = 5;
            this.settingsSingleCategoryUiInfo.baseFontSize = this.baseFontSize;
            this.settingsSingleCategoryUiInfo.font = this.font;
            this.settingsSingleCategoryUiInfo.textColor = this.textColor;

            this.settingsSingleCategoryUiInfo.scrollActiveColor =
                this.textColor;
            this.settingsSingleCategoryUiInfo.scrollInactiveColor =
                Color.gray;

            this.categoryChangePromptKeyTextColor = Color.green;
        }

        public override void ReinitState()
        {
            base.ReinitState();
            this.activeSettingsCategoryIndex = 0;
            this.settingsCategoriesInfos.ForEach(x => x.ReinitStateAndUiState());
            this.settingsCategoriesInfos.ForEach(x => x.HideCategory());
            this.categoryChangingPromptsHandle.ShowAndHideAccordingly(
                 this.activeSettingsCategoryIndex, this.settingsCategoriesInfos);
            this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                .ShowCategory();
            this.selectedCategoryPromptHandle.ChangeCategoryTitle(
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .GetCategoryName());
        }

        protected override void OnLosingFocusInFavorOfDialog()
        {
            this.categoryChangingPromptsHandle.gameObject.SetActive(false);
            this.selectedCategoryPromptHandle.gameObject.SetActive(false);
        }

        protected override void OnLosingFocusInFavorOfFocusBox()
        {
            this.categoryChangingPromptsHandle.gameObject.SetActive(false);
            this.selectedCategoryPromptHandle.gameObject.SetActive(false);
        }

        protected override void OnRegainingFocusFromDialog()
        {
            this.categoryChangingPromptsHandle
                .SetGameObjectActiveIfMoreThanOneCategory(
                    this.settingsCategoriesInfos.Count);
            this.selectedCategoryPromptHandle.gameObject.SetActive(true);
        }

        protected override void OnRegainingFocusFromFocusBox()
        {
            this.categoryChangingPromptsHandle
                .SetGameObjectActiveIfMoreThanOneCategory(
                    this.settingsCategoriesInfos.Count);
            this.selectedCategoryPromptHandle.gameObject.SetActive(true);
        }

        protected static T PropagateSettingsSectionMenuPanel<T>(
            GameObject menuCanvasObject, Vector2 referenceResolution,
            string panelObjectName, Color backgroundColor) where T : SettingsMenuSectionBase
        {
            Func<GameObject> panelObjectProvider = () =>
                MenuPanelFactory
                    .CreatePanelObjectWithCenteredBoxBackgroundMarginReferenceResolution(
                        menuCanvasObject, referenceResolution,
                        panelObjectName,
                        0.12f, 0.1f, backgroundColor);
            return PropagateMenuPanel<T>(menuCanvasObject, referenceResolution,
                panelObjectName, panelObjectProvider, backgroundColor);
        }

        protected override void PropagatePanel(GameObject panelObject)
        {
            this.settingsCategoriesInfos = GetSettingsCategories();

            GameObject boxBackgroundObject = MenuPanelFactory
                .GetBackgroundBoxObject(panelObject);
            this.sectionContainer = boxBackgroundObject;

            RectTransform boxBackgroundObjectRectTransform
                = boxBackgroundObject.GetComponent<RectTransform>();

            float desiredCategoryChangingPromptsWidth =
                (this.referenceResolution.x - boxBackgroundObjectRectTransform.sizeDelta.x)
                    / 2f;

            CategoriesSwitchPromptsContainerBehaviour
                categoryChangingPromptsHandle =
                    SettingsSectPanelsFactory
                    .GetCategoryChangingPromptsUnder(
                        this.font,
                        this.settingsSingleCategoryUiInfo.settingControlFontSize,
                        this.baseFontSize,
                        this.textColor,
                        this.categoryChangePromptKeyTextColor,
                        panelObject,
                        this.referenceResolution.x,
                        this.referenceResolution.y,
                        desiredCategoryChangingPromptsWidth,
                        400,
                        this.settingsSingleCategoryUiInfo
                            .selectedOptionOscillationSpeed / 4f);

            float desiredSelectedCategoryPromptHeight =
                (this.referenceResolution.y);

            SelectedCategoryPromptBehaviour selectedCategoryPromptHandle =
                SettingsSectPanelsFactory.GetSelectedCategoryPromptUnder(
                    panelObject,
                    boxBackgroundObjectRectTransform.sizeDelta.x,
                    desiredSelectedCategoryPromptHeight,
                    this.font,
                    this.baseFontSize,
                    this.textColor
                    );

            this.selectedCategoryPromptHandle = selectedCategoryPromptHandle;
            this.categoryChangingPromptsHandle = categoryChangingPromptsHandle;

            foreach (SettingsCategoryInfo settingsCategoryInfo
                in this.settingsCategoriesInfos)
            {
                settingsCategoryInfo.PropagatePanel(panelObject);
            }

            if (this.settingsCategoriesInfos.Count < 2)
            {
                // if it has only 1 category, then do not show these prompts
                this.categoryChangingPromptsHandle
                    .gameObject.SetActive(false);
            }
        }

        protected override void Update()
        {
            base.Update();
            int selectedSettingControlIndexInCategory =
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                .selectedSettingControlIndex;

            this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                .settingControlsInfos[selectedSettingControlIndexInCategory].settingControlBehaviour
                .SetLabelTextLocalScale(
                    new Vector3(1f, 1f) * (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress *
                        this.settingsSingleCategoryUiInfo.selectedOptionOscillationSpeed)));
        }

        protected override bool ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool wasHandledAbove = base.ApplySelectedOption(gameplayMenuBehaviour);
            if (!wasHandledAbove)
            {
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .ApplySelectedOption(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        public override bool SelectOptionDown(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.SelectOptionDown(gameplayMenuBehaviour))
            {
                SelectOptionDownHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        public override bool SelectOptionUp(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.SelectOptionUp(gameplayMenuBehaviour))
            {
                SelectOptionUpHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        public override bool ChangeCategoryToNext(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.ChangeCategoryToNext(gameplayMenuBehaviour))
            {
                ChangeCategoryToNextHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        private void ChangeCategoryToNextHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveCategory =
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .OnCategoryLeaving(gameplayMenuBehaviour);

            if (canLeaveCategory)
            {
                if (this.activeSettingsCategoryIndex <
                        this.settingsCategoriesInfos.Count - 1)
                {
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .HideCategory();
                    this.activeSettingsCategoryIndex++;
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ClearUiState();
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ReinitState();
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ShowCategory();
                    this.categoryChangingPromptsHandle.ShowAndHideAccordingly(
                        this.activeSettingsCategoryIndex, this.settingsCategoriesInfos);
                    this.selectedCategoryPromptHandle.ChangeCategoryTitle(
                        this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .GetCategoryName());
                }
            }
        }

        private void ChangeCategoryToPreviousHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            bool canLeaveCategory =
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .OnCategoryLeaving(gameplayMenuBehaviour);

            if (canLeaveCategory)
            {
                if (this.activeSettingsCategoryIndex > 0)
                {
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .HideCategory();
                    this.activeSettingsCategoryIndex--;
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ClearUiState();
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ReinitState();
                    this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .ShowCategory();
                    this.categoryChangingPromptsHandle.ShowAndHideAccordingly(
                        this.activeSettingsCategoryIndex, this.settingsCategoriesInfos);
                    this.selectedCategoryPromptHandle.ChangeCategoryTitle(
                        this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                        .GetCategoryName());
                }
            }
        }

        public override bool ChangeCategoryToPrevious(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (base.ChangeCategoryToPrevious(gameplayMenuBehaviour))
            {
                ChangeCategoryToPreviousHandler(gameplayMenuBehaviour);
                return true;
            }
            return false;
        }

        private void SelectOptionUpHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                .SelectOptionUp(gameplayMenuBehaviour);
        }

        private void SelectOptionDownHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                .SelectOptionDown(gameplayMenuBehaviour);
        }

        public override bool SelectOptionLeft(bool fromRepeatedInput = false)
        {
            bool handled = base.SelectOptionLeft();
            if (!handled)
            {
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .SelectOptionLeft(fromRepeatedInput);
                return true;
            }
            return false;
        }

        public override bool SelectOptionRight(bool fromRepeatedInput = false)
        {
            bool handled = base.SelectOptionRight();
            if (!handled)
            {
                this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .SelectOptionRight(fromRepeatedInput);
                return true;
            }
            return false;
        }

        protected abstract List<SettingsCategoryInfo> GetSettingsCategories();

        protected override bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            return this.settingsCategoriesInfos[this.activeSettingsCategoryIndex]
                    .ExitSectionHandler(gameplayMenuBehaviour);
        }
    }
}
