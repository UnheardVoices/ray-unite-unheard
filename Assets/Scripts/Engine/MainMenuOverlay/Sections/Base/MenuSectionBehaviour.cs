﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Base;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs;
using Assets.Scripts.Utils.Assets;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections
{
    public abstract class MenuSectionBehaviour : MonoBehaviour
    {
        protected float timeProgress = 0f;
        protected float menuNavigationFixedUpdateInputFrequency = 10f;
        protected float nextFixedUpdateNavigationInputTimeThreshold = 0f;

        protected List<DialogSectionBehaviour> dialogs;
        protected List<FocusBoxSectionBehaviour> focusBoxes;

        protected GameObject sectionContainer;

        protected DialogSectionBehaviour activeDialog;
        protected FocusBoxSectionBehaviour activeFocusBox;

        protected Font font;
        protected int baseFontSize;
        protected Color backgroundColor;
        protected Color textColor;

        protected Vector2 referenceResolution;

        protected abstract void PropagatePanel(GameObject panelObject);
        protected virtual List<MenuDialogDefinition> GetDialogsDefinitions()
        {
            return new List<MenuDialogDefinition>();
        }

        protected virtual List<MenuFocusBoxDefinition> GetFocusBoxesDefinitions()
        {
            return new List<MenuFocusBoxDefinition>();
        }

        protected static T PropagateMenuPanel<T>(
            GameObject menuCanvasObject, Vector2 referenceResolution,
            string panelObjectName,
            Func<GameObject> menuPanelObjectProvider,
            Color backgroundColor) where T : MenuSectionBehaviour
        {
            GameObject panelObject = menuPanelObjectProvider();
            T result =
                panelObject.AddComponent<T>();
            result.referenceResolution = referenceResolution;
            result.FirstInitState(backgroundColor);
            result.PropagatePanel(panelObject);
            List<MenuDialogDefinition> dialogDefinitions = result.GetDialogsDefinitions();
            List<MenuFocusBoxDefinition> focusBoxesDefinitions = result.GetFocusBoxesDefinitions();
            result.PropagateDialogsPanels(dialogDefinitions, panelObject);
            result.PropagateFocusBoxesPanels(focusBoxesDefinitions, panelObject);
            return result;
        }

        public void SetActiveDialog(string? dialogId)
        {
            if (dialogId != null)
            {
                DialogSectionBehaviour dialogSectionBehaviour =
                    this.dialogs.Find(x => x.dialogId.Equals(dialogId));

                dialogSectionBehaviour.gameObject.SetActive(true);
                this.sectionContainer.gameObject.SetActive(false);
                this.activeDialog = dialogSectionBehaviour;
                this.activeDialog.ResetChoice();
                OnLosingFocusInFavorOfDialog();
            }
            else
            {
                this.dialogs.ForEach(x => x.gameObject.SetActive(false));
                this.sectionContainer.gameObject.SetActive(true);
                this.activeDialog = null;
                OnRegainingFocusFromDialog();
            }
        }

        public void SetActiveFocusBox(string? focusBoxId)
        {
            if (focusBoxId != null)
            {
                FocusBoxSectionBehaviour focusBoxSectionBehaviour =
                    this.focusBoxes.Find(x => x.focusBoxId.Equals(focusBoxId));

                focusBoxSectionBehaviour.gameObject.SetActive(true);
                this.sectionContainer.gameObject.SetActive(false);
                this.activeFocusBox = focusBoxSectionBehaviour;
                this.activeFocusBox.ResetState();
                OnLosingFocusInFavorOfFocusBox();
            }
            else
            {
                this.focusBoxes.ForEach(x => x.gameObject.SetActive(false));
                this.sectionContainer.gameObject.SetActive(true);
                this.activeFocusBox = null;
                OnRegainingFocusFromFocusBox();
            }
        }

        protected virtual void OnRegainingFocusFromDialog() { }

        protected virtual void OnLosingFocusInFavorOfDialog() { }

        protected virtual void OnRegainingFocusFromFocusBox() { }

        protected virtual void OnLosingFocusInFavorOfFocusBox() { }

        public virtual void FirstInitState(
            Color backgroundColor)
        {
            this.menuNavigationFixedUpdateInputFrequency = 10f;
            this.nextFixedUpdateNavigationInputTimeThreshold = 0f;

            this.font = FontsFactory.GetMenuFont();
            this.baseFontSize = 72;
            this.backgroundColor = backgroundColor;

            this.textColor = Color.white;

            this.timeProgress = 0f;
        }

        public virtual void ReinitState()
        {
            SetActiveDialog(null);
            SetActiveFocusBox(null);
        }

        protected void PropagateFocusBoxesPanels(
            List<MenuFocusBoxDefinition> focusBoxesDefinitions, GameObject panelObject)
        {
            GameObject focusBoxesContainer = new GameObject(MenuPanelParts.focusBoxesContainer,
                typeof(RectTransform));
            focusBoxesContainer.transform.SetParent(
                panelObject.transform, worldPositionStays: false
                );

            List<FocusBoxSectionBehaviour> result = new List<FocusBoxSectionBehaviour>();
            foreach (MenuFocusBoxDefinition focusBoxDef in focusBoxesDefinitions)
            {
                result.Add(FocusBoxPanelFactory.CreateFocusBoxPanel(
                    focusBoxesContainer,
                    focusBoxDef.backgroundColor,
                    focusBoxDef.textColor,
                    focusBoxDef.font,
                    focusBoxDef.fontSize,
                    focusBoxDef.focusBoxClass,
                    focusBoxDef.message,
                    focusBoxDef.focusBoxId
                ));
            }

            this.focusBoxes = result;
        }

        protected void PropagateDialogsPanels(
            List<MenuDialogDefinition> dialogsDefinitions, GameObject panelObject)
        {
            GameObject dialogsContainer = new GameObject(MenuPanelParts.dialogsContainer,
                typeof(RectTransform));
            dialogsContainer.transform.SetParent(
                panelObject.transform, worldPositionStays: false);

            List<DialogSectionBehaviour> result = new List<DialogSectionBehaviour>();

            foreach (MenuDialogDefinition dialogDef in dialogsDefinitions)
            {
                result.Add(DialogPanelFactory.CreateDialogPanel(
                    dialogsContainer,
                    dialogDef.backgroundColor,
                    dialogDef.textColor,
                    dialogDef.font,
                    dialogDef.fontSize,
                    dialogDef.dialogClass,
                    dialogDef.message,
                    dialogDef.confirmCaption,
                    dialogDef.rejectCaption,
                    dialogDef.dialogId));
            }

            this.dialogs = result;
        }

        protected virtual void Update()
        {
            this.timeProgress += Time.deltaTime;
        }

        public void OnExitSection(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog == null)
            {
                ExitSectionHandler(gameplayMenuBehaviour);
            }
            else
            {
                this.activeDialog.OnExitSection(gameplayMenuBehaviour);
            }
        }

        protected abstract bool ExitSectionHandler(
            GameplayMenuBehaviour gameplayMenuBehaviour);

        public virtual bool ChangeCategoryToPrevious(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool ChangeCategoryToNext(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool SelectOptionUp(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public virtual bool SelectOptionDown(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool SelectOptionLeft(bool fromRepeatedInput = false)
        {
            if (this.activeDialog && !fromRepeatedInput)
            {
                this.activeDialog.SelectOptionLeft();
                return true;
            }
            return false;
        }

        public virtual bool SelectOptionRight(bool fromRepeatedInput = false)
        {
            if (this.activeDialog && !fromRepeatedInput)
            {
                this.activeDialog.SelectOptionRight();
                return true;
            }
            return false;
        }

        protected virtual bool ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.activeDialog != null)
            {
                this.activeDialog.ApplySelectedOption(gameplayMenuBehaviour);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void OnOptionApply(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            ApplySelectedOption(gameplayMenuBehaviour);
        }

        public virtual void SelectOptionUpRepeatedly(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.timeProgress >= this.nextFixedUpdateNavigationInputTimeThreshold)
            {
                this.nextFixedUpdateNavigationInputTimeThreshold =
                    this.timeProgress + 1f / this.menuNavigationFixedUpdateInputFrequency;
                SelectOptionUp(gameplayMenuBehaviour);
            }
        }

        public virtual void SelectOptionDownRepeatedly(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.timeProgress >= this.nextFixedUpdateNavigationInputTimeThreshold)
            {
                this.nextFixedUpdateNavigationInputTimeThreshold =
                    this.timeProgress + 1f / this.menuNavigationFixedUpdateInputFrequency;
                SelectOptionDown(gameplayMenuBehaviour);
            }
        }

        public virtual void SelectOptionLeftRepeatedly()
        {
            if (this.timeProgress >= this.nextFixedUpdateNavigationInputTimeThreshold)
            {
                this.nextFixedUpdateNavigationInputTimeThreshold =
                    this.timeProgress + 1f / this.menuNavigationFixedUpdateInputFrequency;
                SelectOptionLeft(true);
            }
        }

        public virtual void SelectOptionRightRepeatedly()
        {
            if (this.timeProgress >= this.nextFixedUpdateNavigationInputTimeThreshold)
            {
                this.nextFixedUpdateNavigationInputTimeThreshold =
                    this.timeProgress + 1f / this.menuNavigationFixedUpdateInputFrequency;
                SelectOptionRight(true);
            }
        }
    }
}
