﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay.Sections.Dialogs
{
    public abstract class DialogSectionBehaviour : MonoBehaviour
    {
        public string dialogId { get; protected set; }

        protected float timeProgress = 0f;

        protected int baseFontSize;
        protected Text messageText;
        protected Text confirmLabel;
        protected Text rejectLabel;
        protected Image dialogBackgroundImage;

        protected GameObject dialogsContainer;

        protected float messageAndButtonsSpacing;
        protected float messageMargin;
        protected Font font;
        protected Color textColor;
        protected Color backgroundColor;

        protected bool isConfirmSelected = false;

        protected float selectedOptionOscillationSpeed = 6f;

        private void Start()
        {
            this.timeProgress = 0f;
        }

        public void ResetChoice()
        {
            this.isConfirmSelected = false;
        }

        public void ReinitUiState(
            string dialogId,
            GameObject dialogsContainer,
            float messageAndButtonsSpacing,
            float messageMargin,
            int baseFontSize,
            Font font,
            Color textColor,
            Color backgroundColor)
        {
            this.dialogId = dialogId;
            this.baseFontSize = baseFontSize;
            this.dialogsContainer = dialogsContainer;
            this.messageAndButtonsSpacing = messageAndButtonsSpacing;
            this.messageMargin = messageMargin;
            this.font = font;
            this.textColor = textColor;
            this.backgroundColor = backgroundColor;
            this.selectedOptionOscillationSpeed = 6f;
        }

        public virtual void OnExitSection(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.ClearActiveDialog();
        }

        private void Update()
        {
            this.timeProgress += Time.deltaTime;

            if (this.isConfirmSelected)
            {
                this.confirmLabel.rectTransform.localScale = Vector2.one *
                    (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress * this.selectedOptionOscillationSpeed));
            }
            else
            {
                this.rejectLabel.rectTransform.localScale = Vector2.one *
                    (float)(1f * MathHelper.SinusInRange(1.25f, 1.5f,
                    this.timeProgress * this.selectedOptionOscillationSpeed));
            }
        }

        public void RepropagateDialog(
            string message,
            string confirmCaption,
            string rejectCaption)
        {
            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            Text messageText = UnityUiTextFactory.GetCenteredTextLabel(
                DialogParts.message, this.font, this.baseFontSize, message, this.textColor);
            float preferredBoxWidthForMessage =
                TextLabelDimensionHelper.GetPreferredWidthFor(messageText);
            float preferredBoxHeightForMessage =
                TextLabelDimensionHelper.GetPreferredHeightFor(messageText);

            Text confirmLabel = UnityUiTextFactory.GetCenteredTextLabel(
                DialogParts.confirmLabel, this.font, this.baseFontSize, confirmCaption,
                this.textColor
                );
            float preferredBoxWidthForConfirmLabel =
                TextLabelDimensionHelper.GetPreferredWidthFor(confirmLabel);
            float preferredBoxHeightForConfirmLabel =
                TextLabelDimensionHelper.GetPreferredHeightFor(confirmLabel);

            Text rejectLabel = UnityUiTextFactory.GetCenteredTextLabel(
                DialogParts.rejectLabel, this.font, this.baseFontSize, rejectCaption,
                this.textColor
                );
            float preferredBoxWidthForRejectLabel =
                TextLabelDimensionHelper.GetPreferredWidthFor(rejectLabel);
            float preferredBoxHeightForRejectLabel =
                TextLabelDimensionHelper.GetPreferredHeightFor(rejectLabel);

            Image dialogBackgroundImage =
                UnityUiImageFactory.GetCenteredSimpleImagePanel(
                    DialogParts.dialogBackgroundBox, this.backgroundColor,
                    preferredBoxWidthForMessage + 2 * this.messageMargin,
                    preferredBoxHeightForMessage
                    + this.messageAndButtonsSpacing + preferredBoxHeightForConfirmLabel
                    + 4 * this.messageMargin);

            dialogBackgroundImage.gameObject
                .AddComponent<VerticalLayoutGroup>();

            GameObject dialogButtonsContainer =
                new GameObject(DialogParts.dialogButtonsContainer,
                    typeof(RectTransform));
            dialogButtonsContainer.AddComponent<HorizontalLayoutGroup>();

            messageText.transform.SetParent(
                dialogBackgroundImage.transform, worldPositionStays: false);
            dialogButtonsContainer.transform.SetParent(
                dialogBackgroundImage.transform, worldPositionStays: false);
            confirmLabel.transform.SetParent(
                dialogButtonsContainer.transform, worldPositionStays: false);
            rejectLabel.transform.SetParent(dialogButtonsContainer.transform,
                worldPositionStays: false);

            this.messageText = messageText;
            this.confirmLabel = confirmLabel;
            this.rejectLabel = rejectLabel;

            this.dialogBackgroundImage = dialogBackgroundImage;

            dialogBackgroundImage.transform.SetParent(
                this.gameObject.transform, worldPositionStays: false);
        }

        public void ApplySelectedOption(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            if (this.isConfirmSelected)
            {
                OnDialogConfirm(gameplayMenuBehaviour);
            }
            else
            {
                OnDialogCancel(gameplayMenuBehaviour);
            }
        }

        protected abstract void OnDialogConfirm(
            GameplayMenuBehaviour gameplayMenuBehaviour);

        protected virtual void OnDialogCancel(
            GameplayMenuBehaviour gameplayMenuBehaviour)
        {
            gameplayMenuBehaviour.ClearActiveDialog();
        }

        public void SelectOptionLeft()
        {
            if (!this.isConfirmSelected)
            {
                this.isConfirmSelected = true;
                this.rejectLabel.rectTransform.localScale = Vector2.one;
            }
        }

        public void SelectOptionRight()
        {
            if (this.isConfirmSelected)
            {
                this.isConfirmSelected = false;
                this.confirmLabel.rectTransform.localScale = Vector2.one;
            }
        }

        public void RecalculateBackgroundSizes(float additionalLeftRightMargin = 0)
        {
            float preferredBoxWidthForMessage =
                TextLabelDimensionHelper.GetPreferredWidthFor(
                    this.messageText);
            float preferredBoxHeightForMessage =
                TextLabelDimensionHelper.GetPreferredHeightFor(
                    this.messageText);

            float preferredBoxHeightForConfirmLabel =
                TextLabelDimensionHelper.GetPreferredHeightFor(
                    this.confirmLabel);

            float messageAndButtonsSpacing = 40;
            float messageMargin = 40;

            float sizeDeltaX = preferredBoxWidthForMessage +
                2 * messageMargin + additionalLeftRightMargin;
            float sizeDeltaY = preferredBoxHeightForMessage
                    + messageAndButtonsSpacing + preferredBoxHeightForConfirmLabel
                    + 4 * messageMargin;

            this.dialogBackgroundImage.rectTransform.anchoredPosition =
                new Vector2((-sizeDeltaX) / 2f, (-sizeDeltaY) / 2f);
            this.dialogBackgroundImage.rectTransform.sizeDelta =
                new Vector2(sizeDeltaX, sizeDeltaY);
        }
    }
}
