﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.MainMenuOverlay
{
    public class MenuPropagator : MonoBehaviour
    {
        private void Start()
        {
            GameObject menuSuperObject = this.gameObject;
            GameObject canvasGameObject =
                GameplayMenuBuilder.CreateMenuCanvas(menuSuperObject);
            Vector2 referenceResolution =
                canvasGameObject.GetComponent<CanvasScaler>().referenceResolution;
            GameplayMenuBuilder.CreateMainMenuOverlayPanel(
                canvasGameObject, referenceResolution);
            GameplayMenuBuilder.CreateSettingsSectionChoicePanel(
                canvasGameObject, referenceResolution);
            GameplayMenuBuilder.CreateGraphicsSettingsPanel(
                canvasGameObject, referenceResolution);
            GameplayMenuBuilder.CreateControlsSettingsPanel(
                canvasGameObject, referenceResolution);
            GameplayMenuBuilder.CreateSoundsSettingsPanel(
                canvasGameObject, referenceResolution);

            FindObjectsOfType<MonoBehaviour>()
                .OfType<IGameplayMenuInScenePropagationListener>()
                .ToList()
                .ForEach(x =>
                    x.OnGameplayMenuInScenePropagationFinished(canvasGameObject));
        }
    }
}
