using Assets.Scripts.Engine.MainMenuOverlay;
using System;
using UnityEngine;

public class MainMenuOverlayListener : MonoBehaviour
{
    private GameGeneralInput gameGeneralInput;
    private GameStateInfo gameStateInfo;
    private GameplayMenuBehaviour gameplayMenuBehaviour;

    // Start is called before the first frame update
    private void Start()
    {
        this.gameGeneralInput = FindObjectOfType<GameGeneralInput>();
        this.gameStateInfo = FindObjectOfType<GameStateInfo>();
        this.gameplayMenuBehaviour = GetComponent<GameplayMenuBehaviour>();
    }

    // Update is called once per frame
    private void Update()
    {
        bool isPressingDownMenuKey = this.gameGeneralInput.IsPressingDownMenuKey();
        bool isPressingDownMenuBackKey = this.gameGeneralInput.IsPressingDownMenuBackKey();

        if (isPressingDownMenuKey || isPressingDownMenuBackKey)
        {
            if (this.gameStateInfo.IsGameplayState())
            {
                this.gameStateInfo.SetGameState(GameState.GAMEPLAY_MENU_STATE);
                this.gameplayMenuBehaviour.OpenMenu();
            }
            else if (isPressingDownMenuBackKey && this.gameStateInfo.IsGameplayMenuState())
            {
                this.gameplayMenuBehaviour.ExitMenuKeyHandler();
            }
            else if (!this.gameStateInfo.IsGameplayState() && !this.gameStateInfo.IsGameplayMenuState())
            {
                throw new InvalidOperationException("Invalid game state!");
            }
        }
    }
}
