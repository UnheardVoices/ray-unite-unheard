﻿using Assets.Scripts.Engine.BuildMainMenuOverlay;
using Assets.Scripts.Engine.MainMenuOverlay.Sections;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Engine.MainMenuOverlay
{
    public static class MenuPanels
    {
        public static MainGameplayMenuSection
            GetMainGameplayMenuSection(GameObject menuCanvasObject)
        {
            return menuCanvasObject.GetComponentInChildren<MainGameplayMenuSection>(true);
        }

        public static void HideAllMenuSections(GameObject menuCanvasObject)
        {
            menuCanvasObject
                .GetComponentsInChildren<MenuSectionBehaviour>()
                .ToList().ForEach(x => x.gameObject.SetActive(false));
        }

        public static SettingsChoiceSection GetSettingsChoiceMenuSection(
            GameObject menuCanvasObject)
        {
            return menuCanvasObject.GetComponentInChildren<SettingsChoiceSection>(true);
        }

        public static MenuSectionBehaviour GetGraphicsSettingsMenuSection(
            GameObject menuCanvasGameObject)
        {
            return menuCanvasGameObject.GetComponentInChildren<GraphicsSettingsSection>(true);
        }

        public static MenuSectionBehaviour GetSoundsSettingsMenuSection(
            GameObject menuCanvasGameObject)
        {
            return menuCanvasGameObject.GetComponentInChildren<SoundsSettingsSection>(true);
        }

        public static MenuSectionBehaviour GetControlsSettingsMenuSection(
            GameObject menuCanvasGameObject)
        {
            return menuCanvasGameObject.GetComponentInChildren<ControlsSettingsSection>(true);
        }
    }

    public class GameplayMenuBehaviour : MonoBehaviour,
        IGameplayMenuInScenePropagationListener
    {
        private MenuSectionBehaviour currentGameplayMenuSection;
        private GameGeneralInput gameGeneralInput;
        private GameStateInfo gameStateInfo;

        private GameObject menuCanvasGameObject;

        private void Start()
        {
            //currentGameplayMenuSection = new MainGameplayMenuSection();
            this.gameStateInfo = FindObjectOfType<GameStateInfo>();
            this.gameGeneralInput = FindObjectOfType<GameGeneralInput>();
        }

        //public void ClearDialog()
        //{
        //    currentGameplayMenuSection.ClearDialog();
        //}

        public void OnGameplayMenuInScenePropagationFinished(
            GameObject menuCanvasGameObject)
        {
            this.menuCanvasGameObject = menuCanvasGameObject;
            // let's hide whole menu after its loaded
            this.menuCanvasGameObject.SetActive(false);
        }

        private void ReinitState()
        {
            // throw new NotImplementedException();
            this.currentGameplayMenuSection =
                MenuPanels.GetMainGameplayMenuSection(this.menuCanvasGameObject);
            this.currentGameplayMenuSection.ReinitState();
        }

        public void ClearActiveDialog()
        {
            this.currentGameplayMenuSection.SetActiveDialog(null);
            this.currentGameplayMenuSection.ReinitState();
        }

        public void ClearActiveFocusBox()
        {
            this.currentGameplayMenuSection.SetActiveFocusBox(null);
            this.currentGameplayMenuSection.ReinitState();
        }

        public void SetActiveDialog(string? dialogId)
        {
            this.currentGameplayMenuSection.SetActiveDialog(dialogId);
        }

        public void SetActiveFocusBox(string? focusBoxId)
        {
            this.currentGameplayMenuSection.SetActiveFocusBox(focusBoxId);
        }

        public void OpenMenu()
        {
            this.menuCanvasGameObject.SetActive(true);
            ReinitState();
            MenuPanels.HideAllMenuSections(this.menuCanvasGameObject);
            this.currentGameplayMenuSection.gameObject.SetActive(true);
        }
        public void ExitMenu()
        {
            this.gameStateInfo.SetGameState(GameState.GAMEPLAY_STATE);
            this.menuCanvasGameObject.SetActive(false);
        }

        private void Update()
        {
            if (this.gameStateInfo.IsGameplayMenuState())
            {
                //currentGameplayMenuSection.UpdateTick(Time.deltaTime);
                if (this.gameGeneralInput.IsPressingDownMenuOptionUpKey())
                {
                    this.currentGameplayMenuSection.SelectOptionUp(this);
                }
                else if (this.gameGeneralInput.IsPressingDownMenuOptionDownKey())
                {
                    this.currentGameplayMenuSection.SelectOptionDown(this);
                }
                else if (this.gameGeneralInput.IsPressingDownMenuOptionLeftKey())
                {
                    this.currentGameplayMenuSection.SelectOptionLeft();
                }
                else if (this.gameGeneralInput.IsPressingDownMenuOptionRightKey())
                {
                    this.currentGameplayMenuSection.SelectOptionRight();
                }
                else if (this.gameGeneralInput.IsPressingDownMenuPreviousCategoryKey())
                {
                    this.currentGameplayMenuSection.ChangeCategoryToPrevious(this);
                }
                else if (this.gameGeneralInput.IsPressingDownMenuNextCategoryKey())
                {
                    this.currentGameplayMenuSection.ChangeCategoryToNext(this);
                }
                else if (this.gameGeneralInput.IsPressingDownMenuOptionApplyKey())
                {
                    this.currentGameplayMenuSection.OnOptionApply(this);
                }

                if (this.gameGeneralInput.IsPressingMenuOptionUpKeyRepeatedly())
                {
                    this.currentGameplayMenuSection.SelectOptionUpRepeatedly(this);
                }
                else if (this.gameGeneralInput.IsPressingMenuOptionDownKeyRepeatedly())
                {
                    this.currentGameplayMenuSection.SelectOptionDownRepeatedly(this);
                }
                else if (this.gameGeneralInput.IsPressingMenuOptionLeftKeyRepeatedly())
                {
                    this.currentGameplayMenuSection.SelectOptionLeftRepeatedly();
                }
                else if (this.gameGeneralInput.IsPressingMenuOptionRightKeyRepeatedly())
                {
                    this.currentGameplayMenuSection.SelectOptionRightRepeatedly();
                }
            }
        }

        public void ExitMenuKeyHandler()
        {
            this.currentGameplayMenuSection.OnExitSection(this);
        }

        public void SetActiveMenuMainSection(MenuMainSectionEnum mainMenuSection)
        {
            this.currentGameplayMenuSection.gameObject.SetActive(false);
            if (mainMenuSection == MenuMainSectionEnum.MAIN_MENU_SECTION)
            {
                this.currentGameplayMenuSection =
                    MenuPanels.GetMainGameplayMenuSection(this.menuCanvasGameObject);
            }
            else if (mainMenuSection == MenuMainSectionEnum.SETTINGS_CHOICE_SECTION)
            {
                this.currentGameplayMenuSection =
                    MenuPanels.GetSettingsChoiceMenuSection(this.menuCanvasGameObject);
            }
            else if (mainMenuSection == MenuMainSectionEnum.GRAPHICS_SETTINGS_SECTION)
            {
                this.currentGameplayMenuSection =
                    MenuPanels.GetGraphicsSettingsMenuSection(this.menuCanvasGameObject);
            }
            else if (mainMenuSection == MenuMainSectionEnum.SOUNDS_SETTINGS_SECTION)
            {
                this.currentGameplayMenuSection =
                    MenuPanels.GetSoundsSettingsMenuSection(this.menuCanvasGameObject);
            }
            else if (mainMenuSection == MenuMainSectionEnum.CONTROLS_SETTINGS_SECTION)
            {
                this.currentGameplayMenuSection =
                    MenuPanels.GetControlsSettingsMenuSection(this.menuCanvasGameObject);
            }

            this.currentGameplayMenuSection.gameObject.SetActive(true);
            this.currentGameplayMenuSection.ReinitState();
        }
    }
}
