﻿using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;

namespace Assets.Scripts.Engine.Quality.Levels
{
    public class MediumFrameSettingsSet : FrameSettingsSet
    {
        public MediumFrameSettingsSet() : base()
        {
            //this.frameSettingsValues =
            //    new Dictionary<FrameSettingsField, bool>()
            //    {
            //        { FrameSettingsField.AfterPostprocess, false },
            //        { FrameSettingsField.AlphaToMask, false },
            //        { FrameSettingsField.Antialiasing, true },
            //        { FrameSettingsField.AsyncCompute, false },
            //        { FrameSettingsField.AtmosphericScattering, true },
            //        { FrameSettingsField.BigTilePrepass, false },
            //        { FrameSettingsField.Bloom, true },
            //        { FrameSettingsField.ChromaticAberration, false },
            //        { FrameSettingsField.ClearGBuffers, false },
            //        { FrameSettingsField.ColorGrading, true },
            //        { FrameSettingsField.ComputeLightEvaluation, false },
            //        { FrameSettingsField.ComputeLightVariants, false },
            //        { FrameSettingsField.ComputeMaterialVariants, false },
            //        { FrameSettingsField.ContactShadows, false },
            //        { FrameSettingsField.ContactShadowsAsync, false },
            //        { FrameSettingsField.CustomPass, false },
            //        { FrameSettingsField.CustomPostProcess, false },
            //        { FrameSettingsField.DecalLayers, false },
            //        { FrameSettingsField.Decals, false },
            //        { FrameSettingsField.DeferredTile, false },
            //        { FrameSettingsField.DepthOfField, true },
            //        { FrameSettingsField.DepthPrepassWithDeferredRendering, false },
            //        { FrameSettingsField.DirectSpecularLighting, true },
            //        { FrameSettingsField.Distortion, false },
            //        { FrameSettingsField.Dithering, false },
            //        { FrameSettingsField.ExposureControl, true },
            //        { FrameSettingsField.FilmGrain, false },
            //        { FrameSettingsField.FPTLForForwardOpaque, false },
            //        { FrameSettingsField.FullResolutionCloudsForSky, false },
            //        { FrameSettingsField.LensDistortion, false },
            //        { FrameSettingsField.LensFlareDataDriven, false },
            //        { FrameSettingsField.LightLayers, false },
            //        { FrameSettingsField.LightListAsync, false },
            //        { FrameSettingsField.LitShaderMode, true },
            //        { FrameSettingsField.LODBias, false },
            //        { FrameSettingsField.LODBiasMode, false },
            //        { FrameSettingsField.LODBiasQualityLevel, false },
            //        { FrameSettingsField.LowResTransparent, false },
            //        { FrameSettingsField.MaterialQualityLevel, false },
            //        { FrameSettingsField.MaximumLODLevel, false },
            //        { FrameSettingsField.MaximumLODLevelMode, false },
            //        { FrameSettingsField.MaximumLODLevelQualityLevel, false },
            //        { FrameSettingsField.MotionBlur, false },
            //        { FrameSettingsField.MotionVectors, false },
            //        { FrameSettingsField.MSAAMode, false },
            //        { FrameSettingsField.None, false },
            //        { FrameSettingsField.NormalizeReflectionProbeWithProbeVolume, false },
            //        { FrameSettingsField.ObjectMotionVectors, false },
            //        { FrameSettingsField.OpaqueObjects, true },
            //        { FrameSettingsField.PaniniProjection, false },
            //        { FrameSettingsField.PlanarProbe, true },
            //        { FrameSettingsField.Postprocess, true },
            //        { FrameSettingsField.ProbeVolume, false },
            //        { FrameSettingsField.RayTracing, false },
            //        { FrameSettingsField.ReflectionProbe, true },
            //        { FrameSettingsField.Refraction, false },
            //        { FrameSettingsField.ReplaceDiffuseForIndirect, false },
            //        { FrameSettingsField.ReprojectionForVolumetrics, true },
            //        { FrameSettingsField.RoughDistortion, false },
            //        { FrameSettingsField.ScreenSpaceShadows, false },
            //        { FrameSettingsField.ShadowMaps, true },
            //        { FrameSettingsField.Shadowmask, false },
            //        { FrameSettingsField.SkyReflection, true },
            //        { FrameSettingsField.SSAO, false },
            //        { FrameSettingsField.SSAOAsync, false },
            //        { FrameSettingsField.SSGI, false },
            //        { FrameSettingsField.SSR, false },
            //        { FrameSettingsField.SSRAsync, false },
            //        { FrameSettingsField.SssCustomSampleBudget, false },
            //        { FrameSettingsField.SssQualityLevel, false },
            //        { FrameSettingsField.SssQualityMode, false },
            //        { FrameSettingsField.StopNaN, false },
            //        { FrameSettingsField.SubsurfaceScattering, false },
            //        { FrameSettingsField.Tonemapping, true },
            //        { FrameSettingsField.Transmission, false },
            //        { FrameSettingsField.TransparentObjects, true },
            //        { FrameSettingsField.TransparentPostpass, false },
            //        { FrameSettingsField.TransparentPrepass, false },
            //        { FrameSettingsField.TransparentSSR, false },
            //        { FrameSettingsField.TransparentsWriteMotionVector, false },
            //        { FrameSettingsField.Vignette, false },
            //        { FrameSettingsField.VirtualTexturing, false },
            //        { FrameSettingsField.VolumetricClouds, false },
            //        { FrameSettingsField.Volumetrics, true },
            //        { FrameSettingsField.VolumeVoxelizationsAsync, false },
            //        { FrameSettingsField.ZTestAfterPostProcessTAA, false },
            //    };
        }
    }
}
