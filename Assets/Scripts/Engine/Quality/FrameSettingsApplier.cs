﻿using Assets.Scripts.Engine.Quality.Levels;
using System.Collections.Generic;
using UnityEngine.Rendering;

namespace Assets.Scripts.Engine.Quality
{
    public static class BitArrayHelper
    {
        public static BitArray128 GetArrayBit128OfOnes()
        {
            List<uint> ones = new List<uint>();
            for (int i = 0; i < 128; i++)
            {
                ones.Add((uint)i);
            }
            return new BitArray128(ones);
        }
    }

    public static class FrameSettingsApplier
    {
        public static void ApplySettings(
            FrameSettingsSet frameSettingsSet)
        {
            //List<HDAdditionalCameraData> camerasInScene =
            //    Object
            //        .FindObjectsOfType<HDAdditionalCameraData>()
            //            .ToList();

            //FrameSettingsOverrideMask mask = new FrameSettingsOverrideMask
            //{
            //    mask = BitArrayHelper.GetArrayBit128OfOnes()
            //};

            //foreach (HDAdditionalCameraData cameraData in camerasInScene)
            //{
            //    cameraData.customRenderingSettings = true;
            //    cameraData.renderingPathCustomFrameSettingsOverrideMask = mask;

            //    foreach (
            //        KeyValuePair<FrameSettingsField, bool> entry in
            //        frameSettingsSet.frameSettingsValues)
            //    {
            //        cameraData.renderingPathCustomFrameSettings
            //            .SetEnabled(entry.Key, entry.Value);
            //    }
            //}
            //throw new NotImplementedException();
        }
    }
}
