﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.FOV
{
    public static class DebugMarkupsVisualizationHelper
    {
        public static void DrawMarkup(
            Vector3 markupPosition,
            Color color,
            float markupScaleX = 1f,
            float markupScaleY = 1f,
            float markupScaleZ = 1f
            )
        {
            Color backup = Gizmos.color;
            Gizmos.color = color;
            Gizmos.DrawWireCube(
                markupPosition,
                new Vector3(markupScaleX, markupScaleY, markupScaleZ));
            Gizmos.color = backup;
        }
    }

    [System.Serializable]
    public class DebugMarkupConfig
    {
        [Range(0.1f, 5f)]
        public float markupScaleX = 1f;

        [Range(0.1f, 5f)]
        public float markupScaleY = 1f;

        [Range(0.1f, 5f)]
        public float markupScaleZ = 1f;

        public Vector3 markupOriginOffset;
        public Transform markedObject;
    }

    public enum FieldOfViewDetectionState
    {
        NOT_DETECTED,
        NOT_DETECTED_WARNING,
        DETECTED
    }

    public static class FieldOfViewTargetDetectionHelper
    {
        public static Tuple<FieldOfViewDetectionState, Vector3> Inspect(
            Vector3 origin,
            Transform originTransform,
            float maxDistance,
            float horizontalAngleDegrees,
            float verticalAngleDegrees,
            Transform fovTarget,
            Vector3? fovTargetPositionOffset = null)
        {
            if (fovTargetPositionOffset == null)
            {
                fovTargetPositionOffset = Vector3.zero;
            }

            Vector3 targetPosition =
                fovTarget.position + (Vector3)fovTargetPositionOffset;

            float distanceToTarget =
                    Vector3.Distance(origin, targetPosition);

            Vector3 directionToTarget =
                Vector3.ClampMagnitude(
                    targetPosition - origin, maxDistance);

            Vector3 directionToTargetLocalSpace =
                originTransform.InverseTransformDirection(
                    directionToTarget
                    );

            Vector3 projectedHorizontalPart =
                Vector3.ProjectOnPlane(
                    directionToTargetLocalSpace, Vector3.up);

            float horizontalDirectionAngle =
                Vector3.Angle(Vector3.forward, projectedHorizontalPart);
            float verticalDirectionAngle =
                Vector3.Angle(projectedHorizontalPart, directionToTargetLocalSpace);

            if (Mathf.Abs(horizontalDirectionAngle) <= horizontalAngleDegrees * 0.50f
                && Mathf.Abs(verticalDirectionAngle) <= verticalAngleDegrees * 0.50f)
            {
                bool hasHit =
                    Physics.Raycast(
                        origin,
                        directionToTarget,
                        out RaycastHit hit,
                        directionToTarget.magnitude,
                        Layers.environmentLayerMask);

                Vector3 endRayDirection =
                    !hasHit ? directionToTarget : hit.point - origin;

                return Tuple.Create(distanceToTarget
                        > maxDistance || hasHit ?
                        FieldOfViewDetectionState.NOT_DETECTED_WARNING
                        : FieldOfViewDetectionState.DETECTED,
                            endRayDirection);
            }
            else
            {
                bool hasHit =
                    Physics.Raycast(
                        origin,
                        directionToTarget,
                        out RaycastHit hit,
                        directionToTarget.magnitude,
                        Layers.environmentLayerMask);

                Vector3 endRayDirection =
                    !hasHit ? directionToTarget : hit.point - origin;

                UnityEngine.Debug.DrawRay(
                    origin,
                    endRayDirection,
                    distanceToTarget > maxDistance ? Color.green : Color.yellow);

                return Tuple.Create(distanceToTarget
                        > maxDistance || hasHit ?
                        FieldOfViewDetectionState.NOT_DETECTED
                        : FieldOfViewDetectionState.NOT_DETECTED_WARNING,
                            endRayDirection);
            }
        }
    }

    public static class FieldOfViewVisualizationHelper
    {
        public static Dictionary<int, Dictionary<int, Vector3>> startPositionsInEllipses =
            new Dictionary<int, Dictionary<int, Vector3>>();

        public static void DrawDebugFieldOfViewConeConsistingOfRays(
            Transform originTransform,
            Vector3 origin,
            Vector3 forwardDirection,
            Vector3 rightDirection,
            Vector3 upDirection,
            float maxDistance,
            float horizontalAngleDegrees,
            float verticalAngleDegrees,
            float raysOpacity,
            int numberOfRaysLayers,
            int numberOfRaysInALayer,
            bool shadeRaysLuminocity,
            bool visualizeAllFOVRays,
            List<DebugMarkupConfig> fieldOfViewTargetsDebugConfigs)
        {
            if (numberOfRaysInALayer > 0 && numberOfRaysLayers > 0)
            {
                if (visualizeAllFOVRays)
                {
                    for (int layerIndex = 0;
                        layerIndex < numberOfRaysLayers;
                        layerIndex++)
                    {
                        float baseVerticalAngleForLayer =
                            -(verticalAngleDegrees * 0.5f) +
                                ((verticalAngleDegrees / numberOfRaysLayers) * layerIndex);

                        Vector3 baseConstructingRay =
                            Quaternion.AngleAxis(
                                baseVerticalAngleForLayer, Vector3.right) *
                                    Vector3.forward * maxDistance;

                        for (int rayInLayerIndex = 0;
                            rayInLayerIndex < numberOfRaysInALayer;
                            rayInLayerIndex++)
                        {
                            float baseHorizontalAngleInLayer =
                                (-horizontalAngleDegrees * 0.5f) +
                                ((horizontalAngleDegrees / numberOfRaysInALayer)
                                    * rayInLayerIndex);

                            Vector3 rayDirection =
                                originTransform.TransformDirection(
                                    Quaternion.AngleAxis(
                                        baseHorizontalAngleInLayer, Vector3.up)
                                            * baseConstructingRay);

                            bool hasHit =
                                Physics.Raycast(
                                    origin,
                                    rayDirection,
                                    out RaycastHit hit,
                                    rayDirection.magnitude,
                                    Layers.environmentLayerMask);

                            Color visualizationRayColor = new Color(
                                1f, 1f, 1,
                                0f
                                +
                                    Mathf.Pow(
                                    0.5f * raysOpacity * Mathf.Sin(
                                        (horizontalAngleDegrees * 0.5f
                                            + baseHorizontalAngleInLayer) / horizontalAngleDegrees * Mathf.PI)
                                    + 0.5f * raysOpacity * Mathf.Sin(
                                        (verticalAngleDegrees * 0.5f
                                            + baseVerticalAngleForLayer) / verticalAngleDegrees * Mathf.PI),
                                    1f)
                                );

                            if (!hasHit)
                            {
                                UnityEngine.Debug
                                    .DrawRay(origin, rayDirection,
                                    shadeRaysLuminocity ? visualizationRayColor : Color.white);
                            }
                            else
                            {
                                UnityEngine.Debug
                                    .DrawRay(origin, hit.point - origin,
                                    shadeRaysLuminocity ? visualizationRayColor : Color.white);
                            }
                        }
                    }
                }

                if (fieldOfViewTargetsDebugConfigs != null)
                {
                    for (int i = 0; i < fieldOfViewTargetsDebugConfigs.Count; i++)
                    {
                        Transform fovTarget = fieldOfViewTargetsDebugConfigs[i].markedObject;

                        if (fovTarget == null)
                        {
                            continue;
                        }

                        Vector3 fovTargetPositionOffset =
                            fieldOfViewTargetsDebugConfigs[i].markupOriginOffset;

                        Tuple<FieldOfViewDetectionState, Vector3> fieldOfViewDetectionState =
                            FieldOfViewTargetDetectionHelper
                                .Inspect(
                                    origin,
                                    originTransform,
                                    maxDistance,
                                    horizontalAngleDegrees,
                                    verticalAngleDegrees,
                                    fovTarget,
                                    fovTargetPositionOffset
                                );

                        if (fieldOfViewDetectionState.Item1 ==
                            FieldOfViewDetectionState.NOT_DETECTED)
                        {
                            UnityEngine.Debug.DrawRay(
                                origin,
                                fieldOfViewDetectionState.Item2,
                                Color.green
                                );
                            DebugMarkupsVisualizationHelper.DrawMarkup(
                                origin + fieldOfViewDetectionState.Item2,
                                Color.green,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleX,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleY,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleZ
                                );
                        }
                        else if (fieldOfViewDetectionState.Item1 ==
                            FieldOfViewDetectionState.NOT_DETECTED_WARNING)
                        {
                            UnityEngine.Debug.DrawRay(
                                origin,
                                fieldOfViewDetectionState.Item2,
                                Color.yellow
                                );
                            DebugMarkupsVisualizationHelper.DrawMarkup(
                                origin + fieldOfViewDetectionState.Item2,
                                Color.yellow,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleX,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleY,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleZ
                                );
                        }
                        else if (fieldOfViewDetectionState.Item1 ==
                            FieldOfViewDetectionState.DETECTED)
                        {
                            UnityEngine.Debug.DrawRay(
                                origin,
                                fieldOfViewDetectionState.Item2,
                                Color.red
                                );
                            DebugMarkupsVisualizationHelper.DrawMarkup(
                                origin + fieldOfViewDetectionState.Item2,
                                Color.red,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleX,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleY,
                                fieldOfViewTargetsDebugConfigs[i].markupScaleZ
                                );
                        }
                    }
                }
            }
        }
    }
}
