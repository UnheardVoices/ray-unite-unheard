﻿using UnityEngine;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public interface IGameplayMenuInScenePropagationListener
    {
        void OnGameplayMenuInScenePropagationFinished(
            GameObject menuCanvasGameObject);
    }
}
