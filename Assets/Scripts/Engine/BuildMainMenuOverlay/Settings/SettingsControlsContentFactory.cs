﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Settings
{
    public static class SettingsControlsContentFactory
    {
        public static Tuple<GameObject, GameObject>
            GetViewportWithContentChildUnder(
                float elementsSpacing,
                GameObject parentContainerViewRectPanelObject,
                float marginTop,
                float marginBottom)
        {
            GameObject viewportObject = new GameObject(
                SettingsMenuSectionParts.settingControlsViewport,
                typeof(RectTransform));

            //viewportObject.transform.SetParent(
            //    parentContainerViewRectPanelObject.transform, false);

            RectTransform parentRectTransform =
                parentContainerViewRectPanelObject
                    .GetComponent<RectTransform>();

            RectTransform rectTransform =
                viewportObject.GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0.5f, 1f);
            rectTransform.anchorMax = new Vector2(0.5f, 1f);
            rectTransform.pivot = new Vector2(0.5f, 1f);

            rectTransform.sizeDelta = new Vector2(
                parentRectTransform.sizeDelta.x,
                parentRectTransform.sizeDelta.y - marginTop - marginBottom);

            Mask mask = viewportObject.AddComponent<Mask>();
            viewportObject.AddComponent<Image>();
            mask.showMaskGraphic = false;

            GameObject contentObject = GetSettingsControlsContentPanelUnderViewport(
                viewportObject);

            VerticalLayoutGroup verticalLayoutGroup = contentObject.AddComponent<VerticalLayoutGroup>();
            verticalLayoutGroup.childForceExpandHeight = false;
            verticalLayoutGroup.childControlHeight = false;

            verticalLayoutGroup.spacing = elementsSpacing;

            return Tuple.Create(viewportObject, contentObject);
        }

        public static GameObject GetSettingsControlsContentPanelUnderViewport(
            GameObject parentViewport)
        {
            GameObject contentObject = new GameObject(
                SettingsMenuSectionParts.settingControlsContent,
                typeof(RectTransform));

            contentObject.transform.SetParent(
                parentViewport.transform, false);

            RectTransform parentRectTransform =
                parentViewport
                    .GetComponent<RectTransform>();

            RectTransform rectTransform =
                contentObject.GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0.5f, 1f);
            rectTransform.anchorMax = new Vector2(0.5f, 1f);
            rectTransform.pivot = new Vector2(0.5f, 1f);

            rectTransform.sizeDelta = new Vector2(
                parentRectTransform.sizeDelta.x, 1000);

            return contentObject;
        }
    }
}
