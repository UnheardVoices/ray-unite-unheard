﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.Categories;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Settings
{
    public static class CategoriesSwitchingElementsFactory
    {
        public static PreviousCategoryPromptBehaviour
            GetPreviousCategoryPromptPanel(
            Font font,
            int fontSize,
            int categoryChangeKeyPromptFontSize,
            Color textColor,
            Color categoryChangePromptKeyTextColor,
            float desiredPanelWidth,
            float desiredPanelHeight,
            float optionOscillationSpeed)
        {
            GameObject panel = new GameObject(
                SettingsMenuSectionParts.settingsCategoryPreviousPromptContainer,
                typeof(RectTransform));
            PreviousCategoryPromptBehaviour behaviour =
                panel.AddComponent<PreviousCategoryPromptBehaviour>();

            behaviour.InitGuiInfo(
                font,
                fontSize,
                categoryChangeKeyPromptFontSize,
                textColor,
                categoryChangePromptKeyTextColor,
                optionOscillationSpeed,
                45f * Mathf.Deg2Rad);
            behaviour.RepropagatePanel(desiredPanelWidth, desiredPanelHeight);
            return behaviour;
        }

        public static NextCategoryPromptBehaviour
            GetNextCategoryPromptPanel(
            Font font,
            int fontSize,
            int categoryChangeKeyPromptFontSize,
            Color textColor,
            Color categoryChangePromptKeyTextColor,
            float desiredPanelWidth,
            float desiredPanelHeight,
            float optionOscillationSpeed)
        {
            GameObject panel = new GameObject(
                SettingsMenuSectionParts.settingsCategoryNextPromptContainer,
                typeof(RectTransform));

            NextCategoryPromptBehaviour behaviour =
                panel.AddComponent<NextCategoryPromptBehaviour>();

            behaviour.InitGuiInfo(
                font,
                fontSize,
                categoryChangeKeyPromptFontSize,
                textColor,
                categoryChangePromptKeyTextColor,
                optionOscillationSpeed,
                135f * Mathf.Deg2Rad);
            behaviour.RepropagatePanel(desiredPanelWidth, desiredPanelHeight);
            return behaviour;
        }

        public static CategoriesSwitchPromptsContainerBehaviour
            GetCategoriesSwitchPromptsContainerPanel(
                float referenceResolutionWidth,
                float referenceResolutionHeight
            )
        {
            GameObject panel = new GameObject(
                SettingsMenuSectionParts.settingsCategoriesSwitchingPromptsContainer,
                typeof(RectTransform));

            RectTransform rectTransform = panel.GetComponent<RectTransform>();
            rectTransform.sizeDelta =
                new Vector2(
                    referenceResolutionWidth, referenceResolutionHeight);

            CategoriesSwitchPromptsContainerBehaviour behaviour =
                panel.AddComponent<CategoriesSwitchPromptsContainerBehaviour>();

            return behaviour;
        }
    }
}
