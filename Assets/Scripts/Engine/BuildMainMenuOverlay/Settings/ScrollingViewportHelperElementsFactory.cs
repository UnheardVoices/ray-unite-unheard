﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Settings
{
    public static class ScrollingViewportHelperElementsFactory
    {
        public static GameObject GetUpScrollingArrowPanel(
            GameObject parentPanel,
            Font font,
            int fontSize,
            Color textColor
            )
        {
            Text panelText =
                UnityUiTextFactory.GetCenteredTextLabel(
                    SettingsMenuSectionParts.settingsScrollUpArrowContainer,
                    font, fontSize, "^", textColor);
            GameObject panelContainer = panelText.gameObject;

            //GameObject panelContainer = new GameObject(
            //    SettingsMenuSectionParts.settingsScrollUpArrowContainer,
            //    typeof(RectTransform));

            //Text text = panelContainer.AddComponent<Text>();
            //text.text = "^";
            //text.alignment = TextAnchor.MiddleCenter;
            //text.font = font;
            //text.fontSize = fontSize;
            //text.color = textColor;

            RectTransform parentRectTransform =
                parentPanel.GetComponent<RectTransform>();

            RectTransform rectTransform =
                panelContainer.GetComponent<RectTransform>();
            rectTransform.sizeDelta = new Vector2(
                parentRectTransform.sizeDelta.x, rectTransform.sizeDelta.y);

            //panelContainer.transform.SetParent(
            //    parentPanel.transform, false);

            return panelContainer;
        }

        public static GameObject GetDownScrollingArrowPanel(
            GameObject parentPanel,
            Font font,
            int fontSize,
            Color textColor
            )
        {
            Text panelText =
                UnityUiTextFactory.GetCenteredTextLabel(
                    SettingsMenuSectionParts.settingsScrollDownArrowContainer,
                    font, fontSize, "^", textColor);
            GameObject panelContainer = panelText.gameObject;

            //GameObject panelContainer = new GameObject(
            //    SettingsMenuSectionParts.settingsScrollUpArrowContainer,
            //    typeof(RectTransform));

            //Text text = panelContainer.AddComponent<Text>();
            //text.text = "v";
            //text.alignment = TextAnchor.MiddleCenter;
            //text.font = font;
            //text.fontSize = fontSize;
            //text.color = textColor;

            RectTransform parentRectTransform =
                parentPanel.GetComponent<RectTransform>();

            RectTransform rectTransform =
                panelContainer.GetComponent<RectTransform>();
            rectTransform.sizeDelta = new Vector2(
                parentRectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
            rectTransform.eulerAngles =
                new Vector3(
                    rectTransform.eulerAngles.x,
                    rectTransform.eulerAngles.y,
                    180f);

            //panelContainer.transform.SetParent(
            //    parentPanel.transform, false);

            return panelContainer;
        }
    }
}
