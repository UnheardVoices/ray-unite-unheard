﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections;
using Assets.Scripts.Engine.MainMenuOverlay.Sections.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class GameplayMenuBuilder
    {
        public static Color backgroundColor = new Color(0f, 0f, 0f, 0.8f);

        public static MainGameplayMenuSection CreateMainMenuOverlayPanel(
            GameObject menuCanvasObject, Vector2 referenceResolution)
        {
            return MainGameplayMenuSection.PropagatePanelUnderMenuCanvas(
                menuCanvasObject, referenceResolution, backgroundColor);
        }

        public static SettingsChoiceSection CreateSettingsSectionChoicePanel(
            GameObject menuCanvasObject, Vector2 referenceResolution)
        {
            return SettingsChoiceSection.PropagatePanelUnderMenuCanvas(
                menuCanvasObject, referenceResolution, backgroundColor);
        }

        public static GraphicsSettingsSection CreateGraphicsSettingsPanel(
            GameObject menuCanvasObject, Vector2 referenceResolution)
        {
            return GraphicsSettingsSection.PropagatePanelUnderMenuCanvas(
                menuCanvasObject, referenceResolution, backgroundColor
                );
        }

        public static ControlsSettingsSection CreateControlsSettingsPanel(
            GameObject menuCanvasObject, Vector2 referenceResolution)
        {
            return ControlsSettingsSection.PropagatePanelUnderMenuCanvas(
                menuCanvasObject, referenceResolution, backgroundColor
                );
        }

        public static SoundsSettingsSection CreateSoundsSettingsPanel(
            GameObject menuCanvasObject, Vector2 referenceResolution)
        {
            return SoundsSettingsSection.PropagatePanelUnderMenuCanvas(
                menuCanvasObject, referenceResolution, backgroundColor
                );
        }

        public static GameObject CreateMenuCanvas(GameObject menuSuperObject)
        {
            GameObject menuCanvasObject = new GameObject("GameplayMenuCanvas");
            menuCanvasObject.transform.parent = menuSuperObject.transform;
            menuCanvasObject.AddComponent<Canvas>();

            Canvas canvas = menuCanvasObject.GetComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            CanvasScaler canvasScaler =
                menuCanvasObject.AddComponent<CanvasScaler>();

            //canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
            canvasScaler.referenceResolution = new Vector2(1920, 1080);
            //canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;


            return menuCanvasObject;
        }
    }
}
