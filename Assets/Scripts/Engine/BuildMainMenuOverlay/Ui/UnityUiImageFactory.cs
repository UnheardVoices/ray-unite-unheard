﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay.Ui
{
    public static class UnityUiImageFactory
    {
        public static Image GetCenteredSimpleImagePanel(
            string name, Color backgroundColor, float width, float height)
        {
            GameObject imageObject = new GameObject(name);

            Image image = imageObject.AddComponent<Image>();
            image.rectTransform.pivot = new Vector2(0f, 0f);

            image.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            image.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);

            image.color = backgroundColor;

            image.rectTransform.anchoredPosition =
                new Vector2((-width) / 2f, (-height) / 2f);

            image.rectTransform.sizeDelta = new Vector2(width, height);
            return image;
        }
    }
}
