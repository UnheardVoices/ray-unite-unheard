﻿using Assets.Scripts.Engine.BuildMainMenuOverlay.Ui;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class MenuPanelParts
    {
        public const string backgroundBox = "BackgroundBox";
        public const string focusBoxesContainer = "FocusBoxesContainer";
        public const string dialogsContainer = "DialogsContainer";
    }

    public static class MenuPanelFactory
    {
        public static GameObject CreatePanelObjectWithCenteredBoxBackgroundSizeReferenceResolution(
            GameObject menuCanvasObject,
            Vector2 referenceResolution,
            string panelObjectName,
            float normalizedBoxWidth,
            float normalizedBoxHeight,
            Color backgroundColor)
        {
            GameObject panelObject = new GameObject(panelObjectName, typeof(RectTransform));
            panelObject.transform.SetParent(
                menuCanvasObject.transform, worldPositionStays: false);

            panelObject.AddComponent<CanvasGroup>();

            RectTransform panelObjectRectTransform = panelObject.GetComponent<RectTransform>();
            panelObjectRectTransform.anchoredPosition = new Vector2(0f, 0f);
            panelObjectRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            panelObjectRectTransform.anchorMax = new Vector2(0.5f, 0.5f);

            Image boxImage = UnityUiImageFactory
                .GetCenteredSimpleImagePanel(
                    MenuPanelParts.backgroundBox, backgroundColor,
                    normalizedBoxWidth * referenceResolution.x,
                    normalizedBoxHeight * referenceResolution.y);
            boxImage.transform.SetParent(panelObject.transform, worldPositionStays: false);
            return panelObject;
        }

        public static GameObject GetBackgroundBoxObject(
            GameObject panelObject)
        {
            return panelObject.transform.Find(MenuPanelParts.backgroundBox).gameObject;
        }

        public static GameObject
            CreatePanelObjectWithCenteredBoxBackgroundMarginReferenceResolution(
                GameObject menuCanvasObject,
                Vector2 referenceResolution,
                string panelObjectName,
                float normalizedBoxMarginX,
                float normalizedBoxMarginY,
                Color backgroundColor)
        {
            float normalizedBoxWidth = 1f - 2 * normalizedBoxMarginX;
            float normalizedBoxHeight = 1f - 2 * normalizedBoxMarginY;

            return CreatePanelObjectWithCenteredBoxBackgroundSizeReferenceResolution(
                menuCanvasObject,
                referenceResolution,
                panelObjectName,
                normalizedBoxWidth,
                normalizedBoxHeight,
                backgroundColor
                );
        }

        public static GameObject CreateEmptyPlaceholderPanelObject(
            GameObject menuCanvasObject,
            Vector2 referenceResolution,
            string panelObjectName)
        {
            throw new NotImplementedException();
        }
    }
}
