﻿using Assets.Scripts.Engine.MainMenuOverlay.Sections.Boxes;
using System;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildMainMenuOverlay
{
    public static class FocusBoxParts
    {
        public const string message = "message";
        public const string focusBoxBackgroundBox = "focusBoxBackgroundBox";
    }

    public static class FocusBoxPanelFactory
    {
        public static FocusBoxSectionBehaviour CreateFocusBoxPanel(
            GameObject focusBoxesContainer,
            Color backgroundColor,
            Color textColor,
            Font font,
            int fontSize,
            FocusBoxClass focusBoxClass,
            string message,
            string focusBoxId)
        {
            GameObject focusBoxPanelObject =
                new GameObject("FocusBox - " + focusBoxId, typeof(RectTransform));
            focusBoxPanelObject.transform.SetParent(
                focusBoxesContainer.transform, worldPositionStays: false);

            FocusBoxSectionBehaviour focusBoxSectionBehaviour;

            if (focusBoxClass == FocusBoxClass.KEYBOARD_MOUSE_BINDING_READING_FOCUS_BOX)
            {
                focusBoxSectionBehaviour = focusBoxPanelObject
                    .AddComponent<KeyboardMouseGameplayControlBindingReadingFocusBoxBehaviour>();
            }
            else if (focusBoxClass == FocusBoxClass.GAMEPAD_BINDING_READING_FOCUS_BOX)
            {
                focusBoxSectionBehaviour = focusBoxPanelObject
                    .AddComponent<GamepadGameplayControlBindingReadingFocusBoxBehaviour>();
            }
            else
            {
                throw new InvalidOperationException(
                    "Not implemented other focus boxes classes handling!");
            }

            float messageMargin = 40;

            focusBoxSectionBehaviour.ReinitUiState(
                focusBoxId,
                focusBoxesContainer,
                messageMargin,
                fontSize, font, textColor, backgroundColor);
            focusBoxSectionBehaviour.RepropagateFocusBox(
                message);
            return focusBoxSectionBehaviour;
        }
    }
}
