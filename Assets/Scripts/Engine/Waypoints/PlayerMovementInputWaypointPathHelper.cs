﻿using UnityEngine;

namespace Assets.Scripts.Engine.Waypoints
{
    public static class PlayerMovementInputWaypointPathHelper
    {
        public static Vector3 CalculatePlayerInputGroundTranslationToFollowWaypoint(
            Vector3 playerPosition,
            Vector3 waypointPosition,
            PlayerWaypointMovementType waypointMovementType)
        {
            Vector3 translationDirection = (
                Vector3.ProjectOnPlane(
                (waypointPosition - playerPosition).normalized, Vector3.up).normalized);
            if (waypointMovementType == PlayerWaypointMovementType.RUN)
            {
                return translationDirection * 1f;
            }
            else
            {
                return translationDirection * 0.5f;
            }
        }
    }
}
