﻿using Assets.Scripts.Engine.Waypoints;

namespace Assets.Scripts.Engine
{
    public enum PlayerWaypointMovementType
    {
        RUN,
        WALK
    }

    public class PlayerWaypoint : WaypointBase<PlayerWaypoint>
    {
        public PlayerWaypointMovementType movementType;
    }
}
