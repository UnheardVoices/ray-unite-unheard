﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.Input
{
    public class PlayerInputHub : MonoBehaviour
    {
        protected Dictionary<string,
            Tuple<Action<InputAction.CallbackContext>, long>> anyMouseKeyboardInputListeners =
                new Dictionary<string,
                    Tuple<Action<InputAction.CallbackContext>, long>>();

        protected Dictionary<string,
            Tuple<Action<InputAction.CallbackContext>, long>>
                anyGameplaySignificantGamepadInputListeners =
                new Dictionary<string,
                    Tuple<Action<InputAction.CallbackContext>, long>>();

        public Vector2 moveInputRaw { get; protected set; } = new Vector2();

        public Vector2 menuMoveInputRaw { get; protected set; } = new Vector2();

        public Vector2 lookInputRaw { get; protected set; } = new Vector2();

        public bool menuKey { get; protected set; } = false;
        public bool menuOptionApplyKey { get; protected set; } = false;
        public bool menuPreviousCategoryKey { get; protected set; } = false;

        public bool menuNextCategoryKey { get; protected set; } = false;

        public bool cameraLeftButton { get; protected set; } = false;
        public bool cameraRightButton { get; protected set; } = false;
        public bool cameraUpButton { get; protected set; } = false;
        public bool cameraDownButton { get; protected set; } = false;

        public bool walkingButton { get; protected set; } = false;

        public bool groundRollButton { get; protected set; } = false;
        public bool strafingButton { get; protected set; } = false;

        public bool fireButton { get; protected set; } = false;
        public bool jumpButton { get; protected set; } = false;
        public bool menuBackKey { get; protected set; } = false;


        public void RegisterAnyMouseKeyboardInputListener(
            string listenerId, Action<InputAction.CallbackContext> listener)
        {
            if (!this.anyMouseKeyboardInputListeners.ContainsKey(listenerId))
            {
                this.anyMouseKeyboardInputListeners[listenerId] =
                    Tuple.Create(listener, System.DateTime.Now.Ticks);
            }
            else
            {
                throw new InvalidOperationException(
                    "PlayerInputHub already contains AnyMouseKeyboardListener of id " +
                    listenerId);
            }
        }

        public void UnregisterAnyMouseKeyboardInputListener(
            string listenerId
            )
        {
            if (this.anyMouseKeyboardInputListeners.ContainsKey(listenerId))
            {
                this.anyMouseKeyboardInputListeners.Remove(listenerId);
            }
            else
            {
                throw new InvalidOperationException(
                    "PlayerInputHub does not contain AnyMouseKeyboardListener of id " +
                    listenerId + " to unregister");
            }
        }

        public void RegisterAnyGameplaySignificantGamepadInputListener(
            string listenerId, Action<InputAction.CallbackContext> listener)
        {
            if (!this.anyGameplaySignificantGamepadInputListeners.ContainsKey(listenerId))
            {
                this.anyGameplaySignificantGamepadInputListeners[listenerId] =
                    Tuple.Create(listener, System.DateTime.Now.Ticks);
            }
            else
            {
                throw new InvalidOperationException(
                    "PlayerInputHub already contains AnySignificantGameplayGamepadListener of id " +
                    listenerId);
            }
        }

        public void UnregisterAnyGameplaySignificantGamepadInputListener(
            string listenerId)
        {
            if (this.anyGameplaySignificantGamepadInputListeners.ContainsKey(listenerId))
            {
                this.anyGameplaySignificantGamepadInputListeners.Remove(listenerId);
            }
            else
            {
                throw new InvalidOperationException(
                    "PlayerInputHub does not contain " +
                    "AnySignificantGameplayGamepadListener of id " +
                    listenerId + " to unregister");
            }
        }


        public void OnMenuMoveInputAction(InputAction.CallbackContext context)
        {
            this.menuMoveInputRaw = context.ReadValue<Vector2>();
        }


        public void OnMoveInputAction(InputAction.CallbackContext context)
        {
            this.moveInputRaw = context.ReadValue<Vector2>();
        }

        public void OnLookInputAction(InputAction.CallbackContext context)
        {
            this.lookInputRaw = context.ReadValue<Vector2>();
        }



        public void OnMenuKeyInputAction(InputAction.CallbackContext context)
        {
            this.menuKey = context.ReadValueAsButton();
        }

        public void OnMenuBackKeyInputAction(InputAction.CallbackContext context)
        {
            this.menuBackKey = context.ReadValueAsButton();
        }

        public void OnMenuOptionApplyKeyInputAction(InputAction.CallbackContext context)
        {
            this.menuOptionApplyKey = context.ReadValueAsButton();
        }

        public void OnMenuPreviousCategoryKeyInputAction(InputAction.CallbackContext context)
        {
            this.menuPreviousCategoryKey = context.ReadValueAsButton();
        }

        public void OnMenuNextCategoryKeyInputAction(InputAction.CallbackContext context)
        {
            this.menuNextCategoryKey = context.ReadValueAsButton();
        }



        public void OnCameraLeftButtonInputAction(InputAction.CallbackContext context)
        {
            this.cameraLeftButton = context.ReadValueAsButton();
        }

        public void OnCameraRightButtonInputAction(InputAction.CallbackContext context)
        {
            this.cameraRightButton = context.ReadValueAsButton();
        }



        public void OnCameraUpButtonInputAction(InputAction.CallbackContext context)
        {
            this.cameraUpButton = context.ReadValueAsButton();
        }

        public void OnCameraDownButtonInputAction(InputAction.CallbackContext context)
        {
            this.cameraDownButton = context.ReadValueAsButton();
        }



        public void OnWalkingButtonInputAction(InputAction.CallbackContext context)
        {
            this.walkingButton = context.ReadValueAsButton();
        }


        public void OnGroundRollInputAction(InputAction.CallbackContext context)
        {
            this.groundRollButton = context.ReadValueAsButton();
        }

        public void OnStrafingButtonInputAction(InputAction.CallbackContext context)
        {
            this.strafingButton = context.ReadValueAsButton();
        }



        public void OnJumpButtonInputAction(InputAction.CallbackContext context)
        {
            this.jumpButton = context.ReadValueAsButton();
        }


        public void OnFireInputAction(InputAction.CallbackContext context)
        {
            this.fireButton = context.ReadValueAsButton();
        }


        public void OnAnyMouseKeyboardInput(InputAction.CallbackContext context)
        {
            const float waitingTimeThresholdForListenersToInvokeSeconds = 0.5f;
            this.anyMouseKeyboardInputListeners.Values
                .ToList().ForEach(
                    x =>
                    {
                        TimeSpan timeSpan =
                            new TimeSpan(DateTime.Now.Ticks - x.Item2);

                        if (timeSpan.TotalSeconds >=
                            waitingTimeThresholdForListenersToInvokeSeconds)
                        {
                            x.Item1.Invoke(context);
                        }
                    });
        }

        public void OnAnyGameplaySignificantGamepadInput(InputAction.CallbackContext context)
        {
            const float waitingTimeThresholdForListenersToInvokeSeconds = 0.5f;
            this.anyGameplaySignificantGamepadInputListeners.Values
                .ToList().ForEach(
                    x =>
                    {
                        TimeSpan timeSpan =
                            new TimeSpan(DateTime.Now.Ticks - x.Item2);

                        if (timeSpan.TotalSeconds >=
                            waitingTimeThresholdForListenersToInvokeSeconds)
                        {
                            x.Item1.Invoke(context);
                        }
                    });
        }
    }
}
