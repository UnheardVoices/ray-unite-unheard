﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Assets.Scripts.Engine.Input.Bindings
{
    public static class DeviceInputPathDisplayHelper
    {
        public static string GetDisplayBindingName(
            string inputBindingPath)
        {
            string upperCaseSeparateRegex = @"(?<!^)(?=[A-Z])";

            string controlName = inputBindingPath.Split("/").Last();
            if (controlName.Length > 1)
            {
                return string.Join(" ", Regex.Split(char.ToUpper(controlName[0]) +
                    controlName.Substring(1), upperCaseSeparateRegex));
            }
            else
            {
                return char.ToUpper(controlName[0]).ToString();
            }
        }
    }
}
