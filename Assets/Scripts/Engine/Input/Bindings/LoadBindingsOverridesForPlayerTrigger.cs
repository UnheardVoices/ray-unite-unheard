﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.Input.Bindings
{
    public class LoadBindingsOverridesForPlayerTrigger : MonoBehaviour
    {
        private void Start()
        {
            PlayerInput playerInput = FindObjectOfType<PlayerInput>();
            InputActionAsset playerInputActionAsset = playerInput.actions;

            string? inputBindingsOverrides =
                PlayerInputBindingsOverridesPersistenceHelper
                    .LoadBindingsOverridesAsJson();

            if (inputBindingsOverrides != null)
            {
                playerInputActionAsset
                    .LoadBindingOverridesFromJson(inputBindingsOverrides);
            }

            playerInput.actions = playerInputActionAsset;
        }
    }
}
