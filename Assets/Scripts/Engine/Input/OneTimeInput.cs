﻿using System;

namespace Assets.Scripts.Engine.Input
{
    public class OneTimeInput
    {
        public bool isPressingKey;
        public bool hasCheckedKeyPress;

        protected Func<bool> inputProviderFunction;

        protected float keyPressTimeSeconds;

        protected float repeatedNavigationInputTimeWaitThresholdSeconds = 0.5f;

        public void Init(Func<bool> inputProviderFunction)
        {
            this.isPressingKey = false;
            this.hasCheckedKeyPress = false;

            this.inputProviderFunction = inputProviderFunction;
            this.keyPressTimeSeconds = 0f;
            this.repeatedNavigationInputTimeWaitThresholdSeconds = 0.5f;
        }

        public void Update(float deltaTime)
        {
            this.isPressingKey = this.inputProviderFunction();
            if (!this.isPressingKey)
            {
                this.hasCheckedKeyPress = false;
                this.keyPressTimeSeconds = 0f;
            }
            else
            {
                this.keyPressTimeSeconds += deltaTime;
            }
        }

        public bool IsPressingDownKey()
        {
            if (this.isPressingKey && !this.hasCheckedKeyPress)
            {
                this.hasCheckedKeyPress = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsPressingKeyRepeatedly()
        {
            return this.isPressingKey && this.hasCheckedKeyPress
                && this.keyPressTimeSeconds >
                this.repeatedNavigationInputTimeWaitThresholdSeconds;
        }
    }
}
