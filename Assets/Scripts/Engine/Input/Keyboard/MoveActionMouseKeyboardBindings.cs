﻿namespace Assets.Scripts.Engine.Input.Keyboard
{
    public static class MoveActionMouseKeyboardBindings
    {
        public const string UP = "up";
        public const string DOWN = "down";
        public const string LEFT = "left";
        public const string RIGHT = "right";
    }
}
