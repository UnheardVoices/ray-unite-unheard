﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.Input.InputSystem
{
    public static class InputActionAssetProvider
    {
        public static InputActionAsset GetPlayerInputActionAsset()
        {
            InputActionAsset playerInputActionAsset =
                GameObject.FindObjectOfType<PlayerInput>().actions;
            return playerInputActionAsset;
        }
    }
}
