﻿namespace Assets.Scripts.Engine.Input
{
    public static class GameplayInputActions
    {
        public const string MOVE = "Move";
        public const string LOOK = "Look";
        public const string FIRE = "Fire";
        public const string FIRE_2 = "Fire2";
        public const string JUMP = "Jump";
        public const string STRAFING = "Strafing";
        public const string GROUND_ROLL = "GroundRoll";
        public const string WALKING = "Walking";
    }
}
