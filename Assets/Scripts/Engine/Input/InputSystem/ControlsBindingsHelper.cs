﻿using Assets.Scripts.Engine.Input.Bindings;
using Assets.Scripts.Engine.MainMenuOverlay.Controls.Base.ControlBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Engine.Input.InputSystem
{
    public static class ControlsBindingsHelper
    {
        public static ControlBindingInfo GetCompositePartControlInfo(
            string actionName,
            List<string> exclusiveDevicesTypesPathParts,
            string controlName
            )
        {
            InputBinding respectiveInputBinding = InputActionAssetProvider.GetPlayerInputActionAsset()
                .FindAction(actionName)
                    .bindings.Where(x => x.isPartOfComposite &&
                        exclusiveDevicesTypesPathParts.Any(y => x.path.Contains(y)) &&
                        x.name.Equals(controlName)).First();
            return ControlBindingInfo.FromInputSystemInfo(
                displayName:
                    DeviceInputPathDisplayHelper.GetDisplayBindingName(
                        respectiveInputBinding.effectivePath),
                inputBindingPath: respectiveInputBinding.effectivePath
                );
        }

        public static ControlBindingInfo GetRegularButtonControlInfo(
            string actionName,
            List<string> exclusiveDevicesTypesPathParts
            )
        {
            InputBinding respectiveInputBinding = InputActionAssetProvider.GetPlayerInputActionAsset()
                .FindAction(actionName)
                    .bindings.Where(x =>
                        exclusiveDevicesTypesPathParts.Any(y => x.path.Contains(y))).First();

            return ControlBindingInfo.FromInputSystemInfo(
                displayName:
                    DeviceInputPathDisplayHelper.GetDisplayBindingName(
                        respectiveInputBinding.effectivePath),
                inputBindingPath: respectiveInputBinding.effectivePath
                );
        }

        public static ControlBindingInfo SetRegularButtonControlBinding(
            string actionName,
            List<string> exclusiveDevicesTypesPathParts,
            ControlBindingInfo newControlBindingInfo,
            Func<ControlBindingInfo> updatedBindingProvider
            )
        {
            InputActionAsset playerInputActionAsset =
                InputActionAssetProvider.GetPlayerInputActionAsset();

            InputAction action = playerInputActionAsset
                .FindAction(actionName);

            InputBinding respectiveInputBinding =
                action.bindings.Where(x =>
                        exclusiveDevicesTypesPathParts.Any(y => x.path.Contains(y))).First();

            action.ApplyBindingOverride(
                newPath: newControlBindingInfo.inputBindingPath,
                path: respectiveInputBinding.path);

            PlayerInputBindingsOverridesPersistenceHelper
                .SaveBindingsOverridesAsJson(
                    playerInputActionAsset.SaveBindingOverridesAsJson()
                );

            GameObject.FindObjectOfType<PlayerInput>().actions =
                playerInputActionAsset;

            return updatedBindingProvider.Invoke();
        }

        public static ControlBindingInfo SetCompositePartControlBinding(
            string actionName,
            List<string> exclusiveDevicesTypesPathParts,
            string controlName,
            ControlBindingInfo newControlBindingInfo,
            Func<ControlBindingInfo> updatedBindingProvider
            )
        {
            InputActionAsset playerInputActionAsset =
                InputActionAssetProvider.GetPlayerInputActionAsset();

            InputAction action = playerInputActionAsset
                .FindAction(actionName);

            InputBinding respectiveInputBinding =
                action.bindings.Where(x => x.isPartOfComposite &&
                        exclusiveDevicesTypesPathParts.Any(y => x.path.Contains(y)) &&
                        x.name.Equals(controlName)).First();

            respectiveInputBinding.overridePath =
                newControlBindingInfo.inputBindingPath;

            action.ApplyBindingOverride(
                respectiveInputBinding);

            PlayerInputBindingsOverridesPersistenceHelper
                .SaveBindingsOverridesAsJson(
                    playerInputActionAsset.SaveBindingOverridesAsJson()
                );

            GameObject.FindObjectOfType<PlayerInput>().actions =
                playerInputActionAsset;

            return updatedBindingProvider.Invoke();
        }
    }
}
