﻿using UnityEngine;

namespace Assets.Scripts.Collectibles
{
    public interface ICollectible
    {
        void InvokeCollectedBehaviour(Transform playerTransform);
        bool IsCanBeCollected();
    }
}
