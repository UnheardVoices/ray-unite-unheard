﻿using Assets.Scripts.Common;
using Assets.Scripts.ScenePreparation.LedgeGrabbing;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation
{
    public class CreateLedgeColliders : MonoBehaviour
    {
        private void Start()
        {
            GameObject[] ledgeGrabbableObjects = GameObject.FindGameObjectsWithTag(Tags.ledgeGrabbableTag);
            Debug.Log("Found " + ledgeGrabbableObjects.Length);

            foreach (GameObject ledgeGrabbableObj in ledgeGrabbableObjects)
            {
                InstantiateLedgeColliders(ledgeGrabbableObj);
            }
        }

        private void InstantiateLedgeColliders(GameObject ledgeGrabbableObj)
        {
            Mesh mesh = ledgeGrabbableObj.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;
            Vector3[] verticesInWorldTransform =
                vertices.Select(x => ledgeGrabbableObj.transform.TransformPoint(x)).ToArray();

            Vector3[] rotatedNormals = mesh.normals.Select(x => ledgeGrabbableObj.transform.TransformDirection(x)).ToArray();

            Debug.Log("TRIANGLES: " + mesh.triangles.Length);

            List<Triangle> triangles =
                MeshHelper.GetFacesInWorldSpaceWithNormals(mesh.triangles,
                    verticesInWorldTransform, rotatedNormals);

            List<Triangle> appropriateFacesForLedgeColliders =
                LedgeColliderEdgesHelper.GetFacesWithRightAngleNormalToHorizontal(ledgeGrabbableObj, triangles);

            List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals =
                LedgeColliderEdgesHelper.GetEdgesWithLedgeGrabNormals(ledgeGrabbableObj, appropriateFacesForLedgeColliders);

            List<EdgeWithLedgeColliderNormalInfo> edgesEligibleForLedgeColliders =
                LedgeColliderEdgesHelper.GetUniqueEdges(edgesWithLedgeGrabNormals);

            foreach (EdgeWithLedgeColliderNormalInfo edge in edgesEligibleForLedgeColliders)
            {
                LedgeColliderCreator.CreateGrabLedgeCollider(
                    ledgeGrabbableObj,
                    edge.pointA, edge.pointB, edge.ledgeGrabNormalWorldTransform);
            }
        }
    }
}
