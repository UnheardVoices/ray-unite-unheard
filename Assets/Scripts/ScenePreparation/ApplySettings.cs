﻿using Assets.Scripts.Engine.MainMenuOverlay.Controls.Graphics.QualityLevel;
using Assets.Scripts.Engine.Quality;
using Assets.Scripts.Settings;
using System;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation
{
    public static class GraphicsQualityApplier
    {
        public static void ApplyCurrentGraphicsEngineQualitySettings()
        {
            int inEngineQualityLevelIndex = QualitySettings.GetQualityLevel();

            if (inEngineQualityLevelIndex == QualityLevels.lowQualityLevelIndex)
            {
                FrameSettingsHelper.ApplyLowQualityValues();
            }
            else if (inEngineQualityLevelIndex == QualityLevels.mediumQualityLevelIndex)
            {
                FrameSettingsHelper.ApplyMediumQualityValues();
            }
            else if (inEngineQualityLevelIndex == QualityLevels.highQualityLevelIndex)
            {
                FrameSettingsHelper.ApplyHighQualityValues();
            }
            else
            {
                throw new InvalidOperationException(
                    "Unrecognized Unity engine quality level index to apply Frame Settings!");
            }
        }
    }

    public static class VSyncCountApplier
    {
        public static void ApplyVSyncCount()
        {
            int vsyncCount =
                PlayerPrefs.GetInt(
                    PlayerPreferences.VSYNC_COUNT_PLAYER_PREFS_KEY);
            QualitySettings.vSyncCount = vsyncCount;
        }
    }

    public static class TargetFramerateApplier
    {
        public static void ApplyTargetFramerate()
        {
            int targetFramerate = PlayerPrefs.GetInt(
                PlayerPreferences.TARGET_FRAMERATE_PLAYER_PREFS_KEY);
            Application.targetFrameRate = targetFramerate;
        }
    }

    public static class PlayerPrefsApplier
    {
        public static void ApplyPlayerPrefsSettings()
        {
            VSyncCountApplier.ApplyVSyncCount();
            TargetFramerateApplier.ApplyTargetFramerate();
        }
    }

    public class ApplySettings : MonoBehaviour
    {
        private void Start()
        {
            GraphicsQualityApplier
                .ApplyCurrentGraphicsEngineQualitySettings();
            PlayerPrefsApplier.ApplyPlayerPrefsSettings();
        }
    }
}
