﻿using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing
{
    public static class PointsHelper
    {
        public static bool ArePointsRoughlyTheSame(Vector3 pointA, Vector3 pointB)
        {
            float epsilon = 0.0001f;
            float distance = (pointA - pointB).magnitude;
            return distance < epsilon;
        }

        public static List<int>
            GetAllVerticesIndexesEquivalentToVertex(int vertexIndex, Vector3[] vertices)
        {
            Vector3 pointToCompareTo = vertices[vertexIndex];
            List<int> result = new List<int>();
            for (int i = 0; i < vertices.Count(); i++)
            {
                if (PointsHelper.ArePointsRoughlyTheSame(pointToCompareTo, vertices[i]))
                {
                    result.Add(i);
                }
            }
            return result;
        }
    }

    public static class MeshFacesHelper
    {
        public static Vector3 GetFaceNormal(Vector3 vertexA, Vector3 vertexB, Vector3 vertexC)
        {
            return (new Plane(vertexA, vertexB, vertexC)).normal;
        }

        public static Vector3 GetSummedNormalFromEquivalentPoints(
            int vertexIndex,
            Vector3[] vertices,
            Vector3[] normals)
        {
            List<int> equivalentVerticesIndexes = PointsHelper.GetAllVerticesIndexesEquivalentToVertex(vertexIndex, vertices);
            Vector3 resultNormal = new Vector3(0.0f, 0.0f, 0.0f);
            foreach (int vertexIndexInList in equivalentVerticesIndexes)
            {
                resultNormal += normals[vertexIndexInList];
            }
            return resultNormal.normalized;
        }
    }

    public static class TrianglesHelper
    {
        public static IEnumerable<Tuple<int, int, int>> IterateTriangles(int[] triangles)
        {
            for (int i = 0; i < triangles.Length; i += 3)
            {
                yield return Tuple.Create(triangles[i], triangles[i + 1], triangles[i + 2]);
            }
        }
    }

    public static class MeshHelper
    {
        public static List<Triangle> GetFacesInWorldSpaceWithNormals(
            int[] triangles,
            Vector3[] vertices,
            Vector3[] normals
            )
        {
            List<Triangle> result = new List<Triangle>();
            foreach (Tuple<int, int, int> triangleVerticesIndices in TrianglesHelper.IterateTriangles(triangles))
            {
                Vector3 vertexA = vertices[triangleVerticesIndices.Item1];
                Vector3 vertexB = vertices[triangleVerticesIndices.Item2];
                Vector3 vertexC = vertices[triangleVerticesIndices.Item3];

                Vector3 normalA =
                    MeshFacesHelper
                        .GetSummedNormalFromEquivalentPoints(
                            triangleVerticesIndices.Item1,
                            vertices,
                            normals);

                Vector3 normalB =
                    MeshFacesHelper
                        .GetSummedNormalFromEquivalentPoints(
                            triangleVerticesIndices.Item2,
                            vertices,
                            normals);

                Vector3 normalC =
                    MeshFacesHelper
                        .GetSummedNormalFromEquivalentPoints(
                            triangleVerticesIndices.Item3,
                            vertices,
                            normals);

                Vector3 faceNormal = MeshFacesHelper.GetFaceNormal(vertexA, vertexB, vertexC);
                result.Add(new Triangle(
                    vertexA,
                    vertexB,
                    vertexC,
                    triangleVerticesIndices.Item1,
                    triangleVerticesIndices.Item2,
                    triangleVerticesIndices.Item3,
                    normalA,
                    normalB,
                    normalC,
                    faceNormal));
            }
            return result;
        }
    }
}
