﻿using Assets.Scripts.Common;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing
{
    public static class LedgeColliderCreator
    {
        public static GameObject CreateGrabLedgeCollider(
            GameObject parent, Vector3 pointA, Vector3 pointB, Vector3 ledgeGrabNormalWorldTransform)
        {
            GameObject ledgeColliderGameObject =
                StretchedBoxesConstructingHelper
                    .InstantiateStretchedBoxBetweenPoints(parent, pointA, pointB, depth: 0.05f, height: 0.5f);
            LedgeColliderInfo info = ledgeColliderGameObject.AddComponent<LedgeColliderInfo>();
            info.normal = ledgeGrabNormalWorldTransform;
            info.edgePointALocal = ledgeColliderGameObject.transform.InverseTransformPoint(pointA);
            info.edgePointBLocal = ledgeColliderGameObject.transform.InverseTransformPoint(pointB);
            ledgeColliderGameObject.GetComponent<MeshRenderer>().enabled = false;
            ledgeColliderGameObject.layer = Layers.ledgeCollidersLayerIndex;
            ledgeColliderGameObject.tag = Tags.ledgeColliderTag;
            return ledgeColliderGameObject;
        }
    }
}
