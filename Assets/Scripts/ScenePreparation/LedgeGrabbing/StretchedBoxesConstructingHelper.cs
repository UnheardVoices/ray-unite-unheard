﻿using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing
{
    public static class StretchedBoxesConstructingHelper
    {
        public static GameObject InstantiateStretchedBoxBetweenPoints(
            GameObject parent,
            Vector3 pointA,
            Vector3 pointB,
            float depth = 1.0f,
            float height = 1.0f)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(parent.transform, true);

            Vector3 between = pointB - pointA;
            float distance = between.magnitude;
            cube.transform.localScale = new Vector3(distance, height, depth);
            cube.transform.position = pointA + (between / 2.0f);
            cube.transform.LookAt(pointB);

            cube.transform.Rotate(0.0f, 90f, 0.0f, Space.Self);
            return cube;
        }
    }
}
