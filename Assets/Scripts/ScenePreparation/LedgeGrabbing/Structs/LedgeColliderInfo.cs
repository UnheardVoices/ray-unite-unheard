﻿using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs
{
    public class LedgeColliderInfo : MonoBehaviour
    {
        public Vector3 normal = new Vector3(0.0f, 0.0f, 0.0f);
        public Vector3 edgePointALocal = new Vector3(0, 0, 0);
        public Vector3 edgePointBLocal = new Vector3(0, 0, 0);
    }
}
