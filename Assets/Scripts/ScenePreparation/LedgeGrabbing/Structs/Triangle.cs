﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs
{
    public static class EdgeHelper
    {
        public static Vector3 CalculateLedgeGrabNormal(Vector3 normalA, Vector3 normalB)
        {
            return Vector3.ProjectOnPlane((normalA + normalB) / 2.0f, Vector3.up).normalized;
        }
    }

    public struct Triangle
    {
        public Vector3 pointA;
        public Vector3 pointB;
        public Vector3 pointC;

        public int vertexAIndex;
        public int vertexBIndex;
        public int vertexCIndex;

        public Vector3 normalA;
        public Vector3 normalB;
        public Vector3 normalC;

        public Vector3 faceNormalWorldSpace;

        public Triangle(
            Vector3 pointA,
            Vector3 pointB,
            Vector3 pointC,
            int vertexAIndex,
            int vertexBIndex,
            int vertexCIndex,
            Vector3 normalA,
            Vector3 normalB,
            Vector3 normalC,
            Vector3 faceNormalWorldSpace)
        {
            this.pointA = pointA;
            this.pointB = pointB;
            this.pointC = pointC;
            this.vertexAIndex = vertexAIndex;
            this.vertexBIndex = vertexBIndex;
            this.vertexCIndex = vertexCIndex;
            this.normalA = normalA;
            this.normalB = normalB;
            this.normalC = normalC;
            this.faceNormalWorldSpace = faceNormalWorldSpace;
        }

        public List<EdgeWithLedgeColliderNormalInfo>
            GetEdgesWithLedgeColliderNormalInfo()
        {
            EdgeWithLedgeColliderNormalInfo edgeA =
                new EdgeWithLedgeColliderNormalInfo(
                    this.pointA,
                    this.pointB,
                    EdgeHelper.CalculateLedgeGrabNormal(this.normalA, this.normalB),
                    this.normalA,
                    this.normalB,
                    this.vertexAIndex,
                    this.vertexBIndex
                    );

            EdgeWithLedgeColliderNormalInfo edgeB =
               new EdgeWithLedgeColliderNormalInfo(
                   this.pointB,
                   this.pointC,
                   EdgeHelper.CalculateLedgeGrabNormal(this.normalB, this.normalC),
                   this.normalB,
                   this.normalC,
                   this.vertexBIndex,
                   this.vertexCIndex
                   );

            EdgeWithLedgeColliderNormalInfo edgeC =
               new EdgeWithLedgeColliderNormalInfo(
                   this.pointC,
                   this.pointA,
                   EdgeHelper.CalculateLedgeGrabNormal(this.normalC, this.normalA),
                   this.normalC,
                   this.normalA,
                   this.vertexCIndex,
                   this.vertexAIndex
                   );

            return new List<EdgeWithLedgeColliderNormalInfo>() { edgeA, edgeB, edgeC };
        }
    }
}
