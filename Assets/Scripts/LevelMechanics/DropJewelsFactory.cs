﻿using Assets.Scripts.LevelMechanics.Jewels;
using UnityEngine;

namespace Assets.Scripts.LevelMechanics
{
    public static class DropJewelsFactory
    {
        public static void SpawnDropGoldJewel(
            Vector3 spawnOriginPosition,
            float dropFlyingHeight,
            float spawnRadius,
            Vector3 spawnFlyingDirection)
        {
            GameObject goldJewelPrototypeObject = ScenePrototypesHelper.GetGoldJewelPrototypeObject();

            GameObject spawnedGoldJewel = GameObject.Instantiate(goldJewelPrototypeObject);
            spawnedGoldJewel.transform.position = spawnOriginPosition;
            spawnedGoldJewel.GetComponent<CollectibleCrystal>().enabled = false;
            SpawnedJewelBehaviour spawnedGoldJewelBehaviour = spawnedGoldJewel.AddComponent<SpawnedJewelBehaviour>();

            spawnedGoldJewelBehaviour.dropFlyingHeight = dropFlyingHeight;
            spawnedGoldJewelBehaviour.dropDistance = spawnRadius;
            spawnedGoldJewelBehaviour.flyingDirection = spawnFlyingDirection;
        }
    }
}
