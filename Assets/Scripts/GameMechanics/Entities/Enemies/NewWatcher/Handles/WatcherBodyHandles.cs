﻿namespace Assets.Scripts.GameMechanics.Entities.Watcher.Handles
{
    public class WatcherBodyHandles
    {
        public WatcherEyeHandle watcherEyeHandle;
        public WatcherBodyColorHandle watcherBodyColorHandle;

        public WatcherBodyHandles(
            WatcherEyeHandle watcherEyeHandle,
            WatcherBodyColorHandle watcherBodyColorHandle)
        {
            this.watcherEyeHandle = watcherEyeHandle;
            this.watcherBodyColorHandle = watcherBodyColorHandle;
        }
    }
}
