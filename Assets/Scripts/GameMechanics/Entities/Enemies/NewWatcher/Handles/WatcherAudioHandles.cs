﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles
{
    public class WatcherAudioHandles
    {
        public EyeAudioHandles eyeAudioHandles = new();
        public AmbientAudioHandles ambientAudioHandles = new();
        public AlertedAudioHandles alertedAudioHandles = new();
        public TailAudioHandles tailAudioHandles = new();
        public DamageTakingAudioHandles damageTakingAudioHandles = new();
        public BeforeDyingAudioHandles beforeDyingAudioHandles = new();
        public DyingAudioHandles dyingAudioHandles = new();       

        public WatcherAudioHandles PropagateWithAudioSourcesRefs(
            Transform watcherSubject)
        {
            this.eyeAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.ambientAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.alertedAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.tailAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.damageTakingAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.beforeDyingAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);
            this.dyingAudioHandles.PropagateWithAudioSourcesRefs(watcherSubject);

            return this;
        }
    }
}
