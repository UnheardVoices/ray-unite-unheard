﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class AmbientAudioSources
    {
        public const string ROAR_AUDIO_SOURCE = "roar_audio_source";
        public const string AMBIENT_AUDIO_SOURCE = "ambient_audio_source";
    }

    public class AmbientAudioHandles
    {
        public AudioSource roarAudioSource;
        public AudioSource ambientAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.roarAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, AmbientAudioSources.ROAR_AUDIO_SOURCE).GetComponent<AudioSource>();
            this.ambientAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, AmbientAudioSources.AMBIENT_AUDIO_SOURCE).GetComponent<AudioSource>();
        }
    }
}
