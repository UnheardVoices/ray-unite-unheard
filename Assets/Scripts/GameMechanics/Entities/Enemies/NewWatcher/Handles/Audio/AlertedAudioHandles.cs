﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio
{
    public static class AlertedAudioSources
    {
        public const string ALERTED_AUDIO_SOURCE = "alerted_audio_source";
    }

    public class AlertedAudioHandles
    {
        public AudioSource alertedAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform watcherSubject)
        {
            this.alertedAudioSource =
                TransformHelper.RecursiveFindChild(
                    watcherSubject, AlertedAudioSources.ALERTED_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
