﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using Assets.Scripts.PlayerMechanics.Shooting;
using Assets.Scripts.PlayerMechanics.Targeting;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Targeting
{
    public class NewWatcherHitHdlBeh : TargetHitHandlingBeh
    {
        protected NewWatcherController newWatcherController;

        protected override void BehaviourStart()
        {
            this.newWatcherController = GetComponent<NewWatcherController>();
        }

        public override void OnTargetHit(
           HandProjectileBehaviour handProjectileBehaviour)
        {
            string ghostModeAspectTypeName = 
                this.newWatcherController
                    .controllerContext
                    .ghostModeAspect.GetType().Name;

            string actualGhostModeAspectTypeName =
                typeof(NewWatcherGhostModeAspect).Name;

            if (!ghostModeAspectTypeName.Equals(actualGhostModeAspectTypeName))
            {
                this.newWatcherController.controllerContext.watcherRulesTransitions
                    .EnterDamageTaking(
                        handProjectileBehaviour.chargePower,
                        handProjectileBehaviour.maxChargePower,
                        handProjectileBehaviour.trajectoryForward,
                        handProjectileBehaviour.trajectoryUp
                    );
            }
        }

        protected override void GameplayFixedUpdate()
        {
        }
    }
}
