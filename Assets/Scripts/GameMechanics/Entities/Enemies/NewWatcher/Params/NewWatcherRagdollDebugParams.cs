﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherRagdollDebugParams
    {
        public bool startInRagdollMode = false;
        public bool useGravity = false;
    }
}
