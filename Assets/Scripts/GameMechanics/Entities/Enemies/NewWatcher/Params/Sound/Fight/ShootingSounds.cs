﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight
{
    [Serializable]
    public class ShootingSounds
    {
        public SimpleSound eyeClosingSound = new();
        public SimpleSound eyeOpeningSound = new();
        public SoundWithVariants projectileShootingSound = new();
    }
}
