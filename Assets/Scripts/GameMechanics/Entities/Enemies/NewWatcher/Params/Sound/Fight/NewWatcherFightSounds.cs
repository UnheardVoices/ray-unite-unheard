﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class NewWatcherFightSounds
    {
        public ProjectileSounds projectileSounds = new();
        public ShootingSounds shootingSounds = new();
        public TailWavingSounds tailWavingSounds = new();
        public AmbientSounds ambientSounds = new();
    }
}
