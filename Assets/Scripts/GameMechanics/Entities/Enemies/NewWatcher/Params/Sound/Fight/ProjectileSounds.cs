﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Fight
{
    [Serializable]
    public class ProjectileSounds
    {
        public SoundWithVariants flyingSound = new();
        public SoundWithVariants explosionSound = new();
    }
}
