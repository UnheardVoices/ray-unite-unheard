﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Common;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class  NewWatcherDamageTakingSounds
    {
        public SoundWithVariants punchHitSound = new();
        public SoundWithVariants hitMoanSound = new();

        public SoundWithVariants hitBodyDeflectionSound = new();
        public SoundWithVariants hitBodyGoingToNormalAfterDeflectionSound = new();
        public AmbientSounds ambientSounds = new();
    }
}
