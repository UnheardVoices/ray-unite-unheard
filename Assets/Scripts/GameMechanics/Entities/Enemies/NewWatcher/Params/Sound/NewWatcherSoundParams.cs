﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherSoundParams
    {
        public NewWatcherIdlePatrollingSounds idlePatrollingSounds = new();
        public NewWatcherAlertedSounds alertedSounds = new();

        public NewWatcherFightSounds normalFightSounds = new();
        public NewWatcherFightSounds ghostModeFightSounds = new();

        public NewWatcherDamageTakingSounds damageTakingSounds = new();
        public NewWatcherBeforeDyingSounds beforeDyingSounds = new();
        public NewWatcherDyingSounds dyingSounds = new();
    }
}
