﻿using Assets.Scripts.GameMechanics.Entities.Explosion;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherFightParams
    {
        [Range(0.0f, 2f)]
        public float fightEyeSize = 0.06f;

        [Range(0.0f, 1f)]
        public float eyeSizeFightInterpolation = 0.03f;

        [Range(0.1f, 5f)]
        public float shootingIntervalSeconds = 2f;

        [Range(0.1f, 1f)]
        public float eyeClosingInterpolation = 0.5f;

        [Range(0.1f, 1f)]
        public float eyeOpeningInterpolation = 0.5f;

        [Range(0f, 1f)]
        public float timeProgressFixedStep = 0.15f;

        public ExplosionDescriptiveParams projectileExplosionParams = new();

        public float eyeLightIntensity = 80000f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;

        [Range(0.1f, 5f)]
        public float projectileSpeed = 1f;

        public GameObject projectilePrefab;

        public GameObject projectileExplosionPrefab;
    }
}
