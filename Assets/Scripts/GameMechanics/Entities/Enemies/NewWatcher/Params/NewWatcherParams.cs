﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherParams
    {
        public NewWatcherSoundParams soundParams = new();

        public NewWatcherRagdollDebugParams ragdollDebugParams = new();

        public NewWatcherGeneralParams generalParams = new();
        public NewWatcherDyingParams dyingParams = new();

        public NewWatcherGhostModeParams ghostModeParams = new();
        public NewWatcherIdlePatrollingParams idlePatrollingParams = new();
        public NewWatcherAlertedParams alertedParams = new();
        public NewWatcherFieldOfViewParams fieldOfViewParams = new();
        public NewWatcherFightParams normalFightParams = new();
        public NewWatcherFightParams ghostModeFightParams = new();

        public NewWatcherDamageTakingParams damageTakingParams = new();
    }
}
