﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherAlertedParams
    {
        [Range(0.0f, 2f)]
        public float alertedEyeSize = 0.06f;

        [Range(0.0f, 1f)]
        public float eyeSizeAlertedInterpolation = 0.03f;

        [Range(0.0f, 10f)]
        public float alertedTimeToTransitionToFightSeconds = 2f;

        public float eyeLightIntensity = 80000f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;
    }
}
