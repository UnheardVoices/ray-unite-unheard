﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherGeneralParams
    {
        [Range(1f, 300f)]
        public float healthPoints = 50f;
    }
}
