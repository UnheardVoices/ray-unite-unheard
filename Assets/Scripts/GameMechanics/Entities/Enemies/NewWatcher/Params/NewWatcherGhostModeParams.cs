﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class NewWatcherGhostModeParams
    {
        [Range(0.0001f, 15f)]
        public float fadeOutSeconds = 4f;

        [Range(0.0001f, 1f)]
        public float colorFadeInInterpolation = 0.1f;

        public Color bodyEmissionColor = NewWatcherContext.DEFAULT_BODY_EMISSION_COLOR;
        public Color flowerPartEmissionColor = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_COLOR;

        public float bodyEmissionStrength = NewWatcherContext.DEFAULT_BODY_EMISSION_STRENGTH;
        public float flowerPartEmissionStrength = NewWatcherContext.DEFAULT_FLOWER_PART_EMISSION_STRENGTH;
    }
}
