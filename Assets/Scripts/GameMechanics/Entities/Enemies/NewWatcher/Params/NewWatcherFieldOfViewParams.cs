﻿using Assets.Scripts.Engine.FOV;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params
{
    [Serializable]
    public class FieldOfViewVisualizationParams
    {
        [Range(4, 100)]
        public int numberOfRaysInALayer = 4;

        [Range(3, 100)]
        public int numberOfRaysLayers = 1;

        [Range(0f, 1f)]
        public float raysOpacity = 1f;

        public bool visualizeAllFOVRays = true;

        public List<DebugMarkupConfig>
            debugWatcherFieldOfViewTargets;
    }

    [Serializable]
    public class NewWatcherFieldOfViewParams
    {
        [Range(1f, 100f)]
        public float maxDistance = 5f;

        [Range(1f, 360f)]
        public float horizontalAngleDegrees = 30f;

        [Range(1f, 360f)]
        public float verticalAngleDegrees = 30f;

        public bool visualize = false;

        public FieldOfViewVisualizationParams 
            fieldOfViewVisualizationParams = new();
    }
}
