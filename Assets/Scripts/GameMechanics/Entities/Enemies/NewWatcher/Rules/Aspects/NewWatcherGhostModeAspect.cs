﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects
{
    public static class NewWatcherGhostModeAspectState
    {
        public const string GHOST_MODE_NORMALIZED_PROGRESS = 
            "GHOST_MODE_NORMALIZED_PROGRESS";
        public const string HAS_PASSED_PEAK = "HAS_PASSED_PEAK";
    }

    public class NewWatcherGhostModeAspect : NewWatcherMovementRule, INewWatcherGhostModeAspect
    {
        public const string ASPECT_ID = "GHOST_MODE_ASPECT";

        public NewWatcherGhostModeAspect(NewWatcherController newWatcherController) :
            base(ASPECT_ID, newWatcherController)
        { }

        public override void GameplayFixedUpdate()
        {
            throw new InvalidOperationException();
        }

        public (NewWatcherFightParams, NewWatcherFightSounds, INewWatcherGhostModeAspect) EvaluateFixedUpdate()
        {
            var args = UseEntityContext();

            (float ghostModeNormalizedProgress, Action<float> SetGhostModeNormalizedProgress) = 
                UseState(
                    NewWatcherGhostModeAspectState.GHOST_MODE_NORMALIZED_PROGRESS,
                    0f);
            (bool hasPassedPeak, Action<bool> SetHasPassedPeak) =
                UseState(
                    NewWatcherGhostModeAspectState.HAS_PASSED_PEAK,
                    false);

            if (!hasPassedPeak)
            {
                SetGhostModeNormalizedProgress(
                    Mathf.Lerp(
                        ghostModeNormalizedProgress, 0.5f,
                        args.watcherParams.ghostModeParams.colorFadeInInterpolation));
            }

            if (ghostModeNormalizedProgress >= 0.49f)
            {
                SetHasPassedPeak(true);
            }

            if (hasPassedPeak)
            {
                SetGhostModeNormalizedProgress(
                    ghostModeNormalizedProgress +
                        0.53f *
                        (Time.fixedDeltaTime / 
                            args.watcherParams.ghostModeParams.fadeOutSeconds)
                );
            }

            if (ghostModeNormalizedProgress >= 0.99f)
            {
                return (args.watcherParams.normalFightParams,
                        args.watcherParams.soundParams.normalFightSounds,
                        new NewWatcherGhostModeInactiveAspect(this.entityController));
            } else
            {
                if (!hasPassedPeak)
                {
                    args.watcherBodyHandles.watcherBodyColorHandle.SetBodyEmissionColor(
                        Color.Lerp(
                            args.watcherBodyHandles.watcherBodyColorHandle.GetBodyEmissionColor(),
                            args.watcherParams.ghostModeParams.bodyEmissionColor,
                            args.watcherParams.ghostModeParams.colorFadeInInterpolation)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle.SetFlowerPartsEmissionColor(
                        Color.Lerp(
                            args.watcherBodyHandles.watcherBodyColorHandle.GetFlowerPartsEmissionColor(),
                            args.watcherParams.ghostModeParams.flowerPartEmissionColor,
                            args.watcherParams.ghostModeParams.colorFadeInInterpolation)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle
                        .SetBodyEmissionStrength(
                            Mathf.Lerp(
                                args.watcherBodyHandles.watcherBodyColorHandle.GetBodyEmissionStrength(),
                                args.watcherParams.ghostModeParams.bodyEmissionStrength,
                                args.watcherParams.ghostModeParams.colorFadeInInterpolation)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle
                        .SetFlowerPartsEmissionStrength(
                            Mathf.Lerp(
                                args.watcherBodyHandles.watcherBodyColorHandle.GetFlowerPartsEmissionStrength(),
                                args.watcherParams.ghostModeParams.flowerPartEmissionStrength,
                                args.watcherParams.ghostModeParams.colorFadeInInterpolation)
                    );
                }
                else
                {
                    args.watcherBodyHandles.watcherBodyColorHandle.SetBodyEmissionColor(
                        Color.Lerp(
                            args.watcherParams.ghostModeParams.bodyEmissionColor,
                            GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().bodyEmissionColorProvider.Invoke(),
                            (ghostModeNormalizedProgress - 0.5f) * 2f)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle.SetFlowerPartsEmissionColor(
                        Color.Lerp(
                            args.watcherParams.ghostModeParams.bodyEmissionColor,
                            GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().flowerPartEmissionColorProvider.Invoke(),
                            (ghostModeNormalizedProgress - 0.5f) * 2f)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle.SetBodyEmissionStrength(
                        Mathf.Lerp(
                            args.watcherParams.ghostModeParams.bodyEmissionStrength,
                            GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().bodyEmissionStrengthProvider.Invoke(),
                            (ghostModeNormalizedProgress - 0.5f) * 2f)
                    );
                    args.watcherBodyHandles.watcherBodyColorHandle.SetFlowerPartsEmissionStrength(
                        Mathf.Lerp(
                            args.watcherParams.ghostModeParams.flowerPartEmissionStrength,
                            GetController().GetCurrentRule().GetBodyEmissionColorAndStrengthContext().flowerPartEmissionStrengthProvider.Invoke(),
                            (ghostModeNormalizedProgress - 0.5f) * 2f)
                    );
                }
                return (args.watcherParams.ghostModeFightParams,
                        args.watcherParams.soundParams.ghostModeFightSounds, this);
            }
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            throw new InvalidOperationException();
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            throw new InvalidOperationException();
        }

        public bool IsGhostModeActive()
        {
            return true;
        }
    }
}
