﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects
{
    public interface INewWatcherGhostModeAspect
    {
        (NewWatcherFightParams fightParams,
            NewWatcherFightSounds fightSounds,
            INewWatcherGhostModeAspect nextGhostModeAspect)
            EvaluateFixedUpdate();

        bool IsGhostModeActive();
    }
}
