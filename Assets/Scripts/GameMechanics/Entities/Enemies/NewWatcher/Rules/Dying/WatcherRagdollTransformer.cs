﻿
using JigglePhysics;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Dying
{
    public static class WatcherRagdollTransformer
    {
        public static void EnsureItIsNotInRagdollState(
            JiggleRigBuilder watcherJiggleRigBuilderHandle,
            List<Rigidbody> ragdollRigidbodies,
            List<Collider> ragdollColliders)
        {
            watcherJiggleRigBuilderHandle.enabled = true;
            ragdollRigidbodies.ForEach(x =>
            {
                x.isKinematic = true;
                x.useGravity = false;
            });
            ragdollColliders.ForEach(x =>
            {
                x.enabled = false;
            });
        }

        public static void EnsureItIsInRagdollState(
            JiggleRigBuilder watcherJiggleRigBuilderHandle,
            List<Rigidbody> ragdollRigidbodies,
            List<Collider> ragdollColliders,
            bool useGravity)
        {
            watcherJiggleRigBuilderHandle.enabled = false;
            ragdollRigidbodies.ForEach(x =>
            {
                x.isKinematic = false;
                x.useGravity = useGravity;
            });
            ragdollColliders.ForEach(x =>
            {
                x.enabled = true;
            });
        }

        //public static void MakeRagdoll(Transform watcherSubject)
        //{
        //    List<List<Transform>> flowerPartsBoneChains = 
        //        WatcherBonesFetcher
        //            .GetFlowerPartsBoneChainsAndEnsureTheyAllHaveRigidbodyComponents(watcherSubject);

        //    List<Transform> tailBonesChain = 
        //        WatcherBonesFetcher
        //            .GetTailBonesChainAndEnsureTheyAllHaveRigidbodyComponent(watcherSubject);

        //    Rigidbody watcherSubjectRigidbody = 
        //        watcherSubject.gameObject.AddComponent<Rigidbody>();
        //    //watcherSubject.gameObject.AddComponent<CapsuleCollider>();
        //    var collider = watcherSubject.gameObject.AddComponent<BoxCollider>();
        //    collider.size = new Vector3(4f, 4f, 4f);

        //    for (int i = 0; i < flowerPartsBoneChains.Count; i++)
        //    {
        //        int boneIndexInChain = 0;

        //        for (int j = flowerPartsBoneChains[i].Count - 1;
        //            j >= 1; j--)
        //        {
        //            Transform child = flowerPartsBoneChains[i][j];
        //            Transform parent = flowerPartsBoneChains[i][j - 1];

        //            WatcherBoneRagdollTransformer
        //                .MakeBoneWithColliderAndParentItWithCharacterJointToTheOtherBone
        //                    (child, parent,
        //                        childOrderIndexInChain: boneIndexInChain,
        //                        bonesInChainTotalAmount: flowerPartsBoneChains[i].Count
        //                    );
        //            boneIndexInChain++;
        //        }

        //        Transform chainRootChild = flowerPartsBoneChains[i][0];

        //        chainRootChild.gameObject.AddComponent<CharacterJoint>().connectedBody =
        //            watcherSubjectRigidbody;
        //        //chainRootChild.gameObject.AddComponent<CapsuleCollider>();
        //        //var chainRootCollider = chainRootChild.gameObject.AddComponent<BoxCollider>();
        //        //chainRootCollider.size = new Vector3(4f, 4f, 4f);

        //        //throw new NotImplementedException("Parent each flower part bone chain root bone to the" +
        //        //    " meaningful Watcher root transform not implemented!");
        //    }

        //    int boneIndexInTailChain = 0;

        //    for (int i = tailBonesChain.Count - 1; i >= 1; i--)
        //    {
        //        Transform child = tailBonesChain[i];
        //        Transform parent = tailBonesChain[i - 1];

        //        WatcherBoneRagdollTransformer
        //            .MakeBoneWithColliderAndParentItWithCharacterJointToTheOtherBone
        //                 (child, parent,
        //                    childOrderIndexInChain: boneIndexInTailChain,
        //                    bonesInChainTotalAmount: tailBonesChain.Count
        //                 );

        //        boneIndexInTailChain++;
        //    }

        //    Transform tailChainRootChild = tailBonesChain[0];
        //    tailChainRootChild.gameObject.AddComponent<CharacterJoint>().connectedBody =
        //            watcherSubjectRigidbody;
        //    //tailChainRootChild.gameObject.AddComponent<CapsuleCollider>();
        //    //var tailRootCollider = tailChainRootChild.gameObject.AddComponent<BoxCollider>();
        //    //tailRootCollider.size = new Vector3(4f, 4f, 4f);

        //    //throw new NotImplementedException("Parent tail bone chain root bone to the" +
        //    //        " meaningful Watcher root transform not implemented!");

        //    //throw new NotImplementedException();
        //}
    }
}
