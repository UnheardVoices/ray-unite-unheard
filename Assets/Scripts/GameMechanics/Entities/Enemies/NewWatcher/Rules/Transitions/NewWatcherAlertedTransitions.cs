﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public static class NewWatcherAlertedTransitions
    {
        public static AlertedContext GetAlertedContext()
        {
            return new() { };
        }
    }
}
