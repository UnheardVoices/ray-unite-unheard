﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public static class NewWatcherDyingTransitions
    {
        public static DyingContext GetDyingContext(
            float hitProjectileStrength,
            float maxHitProjectileStrength,
            Vector3 hitProjectileDirection)
        {
            return new() {
                hitProjectileStrength = hitProjectileStrength,
                maxHitProjectileStrength = maxHitProjectileStrength,
                hitProjectileDirection = hitProjectileDirection
            };
        }
    }
}
