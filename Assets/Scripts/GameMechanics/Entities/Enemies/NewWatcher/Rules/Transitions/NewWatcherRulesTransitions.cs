﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public class NewWatcherRulesTransitions
    {
        protected NewWatcherController watcherController;

        public NewWatcherRulesTransitions(
            NewWatcherController watcherController)
        {
            this.watcherController = watcherController;
        }

        public void EnterAlerted()
        {
            PrepareTransition();

            this.watcherController.controllerContext.contextsHolder
                .SetContext(NewWatcherMovementAlertedRule.RULE_ID,
                    NewWatcherAlertedTransitions.GetAlertedContext());

            this.watcherController.SetCurrentRule(
                new NewWatcherMovementAlertedRule(this.watcherController));
        }

        public void EnterDamageTaking(
            float hitProjectileStrength,
            float maxHitProjectileStrength,
            Vector3 hitProjectileDirection,
            Vector3 hitProjectileUpVector
            )
        {
            PrepareTransition();

            var savedContext = this.watcherController
                .controllerContext.contextsHolder.UseContext<DamageTakingContext>(
                NewWatcherMovementDamageTakingRule.RULE_ID);

            this.watcherController.controllerContext.contextsHolder.
                SetContext(NewWatcherMovementDamageTakingRule.RULE_ID,
                    NewWatcherDamageTakingTransitions.GetDamageTakingContext(
                        savedContext,
                        this.watcherController.GetCurrentRuleClassName(),
                        this.watcherController.GetCurrentRule().GetEyeLightIntensityParamValueProvider(),
                        hitProjectileStrength,
                        maxHitProjectileStrength,
                        hitProjectileDirection,
                        hitProjectileUpVector
                    ));
            this.watcherController.SetCurrentRule(
                new NewWatcherMovementDamageTakingRule(this.watcherController));
        }

        public void EnterDying(
            float hitProjectileStrength,
            float maxHitProjectileStrength,
            Vector3 hitProjectileDirection)
        {
            PrepareTransition();

            this.watcherController.controllerContext.contextsHolder
                .SetContext(NewWatcherMovementDyingRule.RULE_ID,
                NewWatcherDyingTransitions.GetDyingContext(
                    hitProjectileStrength,
                    maxHitProjectileStrength,
                    hitProjectileDirection
                    ));

            this.watcherController.SetCurrentRule(
                new NewWatcherMovementDyingRule(this.watcherController));
        }

        public void EnterRagdollDebug()
        {
            PrepareTransition();

            this.watcherController.controllerContext.contextsHolder
                .SetContext(NewWatcherMovementRagdollDebugRule.RULE_ID,
                NewWatcherRagdollDebugTransitions.GetRagdollDebugContext());

            this.watcherController.SetCurrentRule(
                new NewWatcherMovementRagdollDebugRule(this.watcherController));
        }

        public void EnterIdlePatrolling()
        {
            PrepareTransition();

            var savedContext = this.watcherController
                .controllerContext.contextsHolder.UseContext<IdlePatrollingContext>(
                NewWatcherMovementIdlePatrollingRule.RULE_ID);

            this.watcherController.controllerContext.contextsHolder
                .SetContext(NewWatcherMovementIdlePatrollingRule.RULE_ID,
                NewWatcherIdlePatrollingTransitions.GetIdlePatrollingContext(
                    savedContext.initialSplinePathInfoState,
                    savedContext.patrollingBezierPathProgress
                    ));

            this.watcherController.SetCurrentRule(
                new NewWatcherMovementIdlePatrollingRule(this.watcherController));
        }

        public void EnterFight(float timeProgress)
        {
            PrepareTransition();

            this.watcherController.controllerContext.contextsHolder
                .SetContext(NewWatcherMovementFightRule.RULE_ID,
                NewWatcherFightTransitions.GetFightContext(timeProgress));

            this.watcherController.SetCurrentRule(
                new NewWatcherMovementFightRule(this.watcherController));
        }

        public void LeaveDamageTaking(string ruleClassNameToGoBackTo)
        {
            PrepareTransition();

            if (ruleClassNameToGoBackTo.Equals(typeof(NewWatcherMovementAlertedRule).Name))
            {
                EnterFight(0f);
            } else if (ruleClassNameToGoBackTo.Equals(typeof(NewWatcherMovementFightRule).Name))
            {
                EnterFight(0f);
            } else if (ruleClassNameToGoBackTo.Equals(typeof(NewWatcherMovementIdlePatrollingRule).Name))
            {
                EnterIdlePatrolling();
            } else if (ruleClassNameToGoBackTo.Equals(typeof(NewWatcherMovementDamageTakingRule).Name))
            {
                throw new InvalidOperationException("Cannot explicitly go from damage taking rule" +
                    " to another damage taking rule this way!");
            } else
            {
                throw new InvalidOperationException("Unknown rule class name to" +
                    " go to from damage taking: " + ruleClassNameToGoBackTo);
            }
        }

        public void PrepareTransition()
        {
            this.watcherController.GetCurrentRule()?.StopRuleAmbientSounds();
            this.watcherController.controllerContext.kinematics.velocity 
                = Vector3.zero;
        }
    }
}
