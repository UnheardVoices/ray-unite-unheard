﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions
{
    public static class NewWatcherRagdollDebugTransitions
    {
        public static RagdollDebugContext GetRagdollDebugContext()
        {
            return new();
        }
    }
}
