﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public abstract class NewWatcherMovementRule : NewEntityRule<NewWatcherContext>
    {
        public NewWatcherMovementRule(string ruleId, object controller) 
            : base(ruleId, controller) { }

        //common logic between WatcherMovementRules

        #region ContextRegion

        protected override NewWatcherContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected NewWatcherController GetController()
        {
            return GetCast<NewWatcherController>(this.entityController);
        }

        public virtual void StopRuleAmbientSounds() {}

        public abstract BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext();
        public abstract Func<float> GetEyeLightIntensityParamValueProvider();

        #endregion
    }
}
