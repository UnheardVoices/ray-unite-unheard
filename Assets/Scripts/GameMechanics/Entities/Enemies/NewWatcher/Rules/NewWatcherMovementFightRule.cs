﻿using Assets.Scripts.Engine.FOV;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Fight;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementRuleFightState
    {
        public const string TIME_PROGRESS_STATE = "TIME_PROGRESS_STATE";
        public const string TIME_PROGRESS_ELAPSED_TIME =
            "TIME_PROGRESS_ELAPSED_TIME";
        public const string IS_SHOOTING_ACTING_STATE
            = "IS_SHOOTING_ACTING_STATE";
        public const string TAIL_TIME_PROGRESS_PERIODS = "TAIL_TIME_PROGRESS_PERIODS";
    }

    public static class NewWatcherMovementRuleFightEffects
    {
        public const string INITIALIZATION = "INITIALIZATION";
    }

    public class NewWatcherMovementFightRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "FIGHT_PLACEHOLDER";

        protected NewWatcherMovementFightShootingSubrule 
            shootingSubrule;
        protected NewWatcherFightParams lastFightParams;
        protected NewWatcherFightSounds lastFightSounds;

        protected BodyEmissionColorAndStrengthContext bodyEmissionColorAndStrengthContext;

        public NewWatcherMovementFightRule(object controller)
            : base(RULE_ID, controller) {
            this.shootingSubrule = 
                new NewWatcherMovementFightShootingSubrule(controller);
            this.lastFightParams = ((NewWatcherController)controller).watcherParams.normalFightParams;
            this.lastFightSounds = ((NewWatcherController)controller).watcherParams.soundParams.normalFightSounds;
            this.bodyEmissionColorAndStrengthContext = new()
            {
                bodyEmissionColorProvider = () => this.lastFightParams.bodyEmissionColor,
                flowerPartEmissionColorProvider = () => this.lastFightParams.flowerPartEmissionColor,
                bodyEmissionStrengthProvider = () => this.lastFightParams.bodyEmissionStrength,
                flowerPartEmissionStrengthProvider = () => this.lastFightParams.flowerPartEmissionStrength
            };
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<FightContext>();

            var ghostModeEvaluationInfo =
                args.ghostModeAspect.EvaluateFixedUpdate();

            args.ghostModeAspect = ghostModeEvaluationInfo.nextGhostModeAspect;
            var fightParams = ghostModeEvaluationInfo.fightParams;
            var fightSounds = ghostModeEvaluationInfo.fightSounds;

            const float velocityFadeInterpolation = 0.3f;
            const float headDeflectionAngleDegrees = 30f;
            const float lookingDirectionInterpolation = 0.3f;
            float timeProgressFixedStep = 
                fightParams.timeProgressFixedStep;

            this.lastFightParams = fightParams;
            this.lastFightSounds = fightSounds;

            (float timeProgressElapsedTime, Action<float> SetTimeProgressElapsedTime) =
                UseState<float>(
                    NewWatcherMovementRuleFightState.TIME_PROGRESS_ELAPSED_TIME, 0f);
            (bool isShootingActing, Action<bool> SetIsShootingActing) =
                UseState(
                    NewWatcherMovementRuleFightState.IS_SHOOTING_ACTING_STATE,
                    false);

            (float timeProgress, Action<float> SetTimeProgress) =
                UseState<float>(
                    NewWatcherMovementRuleFightState.TIME_PROGRESS_STATE,
                    context.initialTimeProgress);
            (int tailTimeProgressPeriods, Action<int> SetTailTimeProgressPeriods)
               = UseState(NewWatcherMovementRuleFightState.TAIL_TIME_PROGRESS_PERIODS, 0);

            UseEffect(
                NewWatcherMovementRuleFightEffects.INITIALIZATION,
                () =>
                {
                    fightSounds.ambientSounds
                       .Play(
                          GetController(),
                          roarAudioSource:
                              args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                          ambientAudioSource:
                              args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
                       );
                }, Tuple.Create(0));

            args.kinematics.velocity =
                Vector3.Lerp(
                    args.kinematics.velocity,
                    Vector3.zero,
                    velocityFadeInterpolation
                    );

            Vector3 playerPosition = args.playerObject.position +
                args.playerObjectFovPositionOffset;

            Vector3 localUpVector =
                args.subject
                    .InverseTransformDirection(Vector3.up);

            args.subject.forward =
                Vector3.Lerp(
                    args.subject.forward,
                    (Quaternion.AngleAxis(
                        Mathf.Sin(timeProgress) * headDeflectionAngleDegrees, localUpVector) *
                        (playerPosition - args.subject.position)).normalized
                    , lookingDirectionInterpolation
                    );

            args.baseWatcherForwardDirection =
                (playerPosition - args.subject.position).normalized;
            args.baseWatcherPosition = args.subject.position;

            Tuple<FieldOfViewDetectionState, Vector3>
                playerFieldOfViewDetectionInfo =
                    FieldOfViewTargetDetectionHelper.Inspect(
                        args.subject.position,
                        args.subject,
                        args.watcherParams.fieldOfViewParams.maxDistance,
                        args.watcherParams.fieldOfViewParams.horizontalAngleDegrees,
                        args.watcherParams.fieldOfViewParams.verticalAngleDegrees,
                        args.playerObject,
                        args.playerObjectFovPositionOffset
                    );

            if (playerFieldOfViewDetectionInfo.Item1 !=
                FieldOfViewDetectionState.DETECTED)
            {
                fightSounds.ambientSounds.ambientSound.StopPlayingWithAudioSource(
                    args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource);
                fightSounds.ambientSounds.roarSound
                    .StopPlayingWithAudioSource(
                          args.watcherAudioHandles.ambientAudioHandles.roarAudioSource);
                args.watcherRulesTransitions.EnterIdlePatrolling();
            }

            if (!isShootingActing)
            {
                args.watcherBodyHandles.watcherEyeHandle
                    .SetEyeSize(
                        Mathf.Lerp(
                            args.watcherBodyHandles.watcherEyeHandle.GetEyeSize(),
                            fightParams.fightEyeSize,
                            fightParams.eyeSizeFightInterpolation));
                args.watcherBodyHandles.watcherEyeHandle
                    .SetEyeClosedBlendshapeValue(
                        Mathf.Lerp(
                            args.watcherBodyHandles.watcherEyeHandle.GetEyeClosedBlendshapeValue(),
                            0f,
                            fightParams.eyeSizeFightInterpolation));
                args.watcherBodyHandles.watcherEyeHandle
                    .SetEyeLightIntensity(
                        Mathf.Lerp(
                            args.watcherBodyHandles.watcherEyeHandle.GetEyeLightIntensity(),
                            fightParams.eyeLightIntensity,
                            fightParams.eyeSizeFightInterpolation));           
            }         
            
            SetTimeProgress(timeProgress + timeProgressFixedStep);

            if (((int)Mathf.Floor(timeProgress / (1.5f * (float)Math.PI)))
                    > tailTimeProgressPeriods)
            {
                int newTimeProgressPeriods = tailTimeProgressPeriods + 1;
                if (newTimeProgressPeriods % 2 == 1)
                {
                    fightSounds.tailWavingSounds
                        .tailWavingSecondDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                }
                else
                {
                    fightSounds.tailWavingSounds
                        .tailWavingFirstDirection
                            .PlayOnceWithAudioSource(
                            args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource);
                }

                SetTailTimeProgressPeriods(newTimeProgressPeriods);
            }

            if (!isShootingActing)
            {
                SetTimeProgressElapsedTime(
                    timeProgressElapsedTime + Time.fixedDeltaTime);
            }            

            if (timeProgressElapsedTime >= 
                fightParams.shootingIntervalSeconds
                && !isShootingActing)
            {
                fightSounds.shootingSounds.eyeClosingSound
                    .PlayOnceWithAudioSource(
                    args.watcherAudioHandles.eyeAudioHandles.blinkingAudioSource);
                SetIsShootingActing(true);
                SetTimeProgressElapsedTime(0f);
            }

            if (isShootingActing)
            {
                this.shootingSubrule
                   .EvaluateFixedUpdate(
                        fightSounds: fightSounds,
                        eyeClosingInterpolation: 
                            fightParams.eyeClosingInterpolation,
                        eyeOpeningInterpolation: 
                            fightParams.eyeOpeningInterpolation,
                        eyeLightIntensity: 
                            fightParams.eyeLightIntensity,
                        onShootingEnded: () => {
                            SetIsShootingActing(false);
                        },
                        onProjectileEmission: () =>
                        {
                            WatcherProjectileSpawner.SpawnProjectile(
                                args.subject.position,
                                args.playerObject.position + Vector3.up,
                                fightParams.projectileSpeed,
                                fightParams.projectilePrefab,
                                fightParams.projectileExplosionPrefab,
                                fightParams.projectileExplosionParams,
                                flyingAudioClipWithAudioSourceParams: 
                                    fightSounds.projectileSounds.flyingSound
                                        .GetAudioClipWithAudioSourceParams(),
                                explosionAudioClipWithAudioSourceParams: 
                                    fightSounds.projectileSounds.explosionSound
                                        .GetAudioClipWithAudioSourceParams()
                                );
                        });
            }
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            return this.bodyEmissionColorAndStrengthContext;
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            return () => this.lastFightParams.eyeLightIntensity;
        }

        public override void StopRuleAmbientSounds()
        {
            var args = UseEntityContext();
            args.watcherParams.soundParams.normalFightSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
            args.watcherParams.soundParams.ghostModeFightSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
            args.watcherParams.soundParams.normalFightSounds.tailWavingSounds.Stop(
                tailWavingAudioSource: args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource
            );
            args.watcherParams.soundParams.ghostModeFightSounds.tailWavingSounds.Stop(
                tailWavingAudioSource: args.watcherAudioHandles.tailAudioHandles.tailWavingAudioSource
            );
        }
    }
}
