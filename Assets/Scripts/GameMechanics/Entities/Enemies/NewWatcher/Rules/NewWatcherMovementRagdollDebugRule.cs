﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Dying;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementRagdollDebugRuleEffects
    {
        public const string GRAVITY_FLAG_UPDATE_EFFECT = 
            "GRAVITY_FLAG_UPDATE_EFFECT";
    }

    public class NewWatcherMovementRagdollDebugRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "RAGDOLL_DEBUG_PLACEHOLDER";

        public NewWatcherMovementRagdollDebugRule(object controller)
            : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();

            UseEffect(
                NewWatcherMovementRagdollDebugRuleEffects.GRAVITY_FLAG_UPDATE_EFFECT, 
                () =>
                {
                    WatcherRagdollTransformer.EnsureItIsInRagdollState(
                        args.watcherJiggleRigBuilderHandle, 
                        args.ragdollRigidbodies,
                        args.ragdollColliders,
                        args.watcherParams.ragdollDebugParams.useGravity
                    );
                },
                Tuple.Create(args.watcherParams.ragdollDebugParams.useGravity));
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            throw new InvalidOperationException();
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            throw new NotImplementedException();
        }
    }
}
