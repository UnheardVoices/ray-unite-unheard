﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Dying;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.Utils.Spawners;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules
{
    public static class NewWatcherMovementDyingRuleEffects
    {
        public const string DYING_INIT_EFFECT = "DYING_INIT_EFFECT";    
    }

    public static class NewWatcherMovementDyingRuleState 
    {
        public const string RULE_INITIALIZED = "RULE_INITIALIZED";
        public const string TIME_PROGRESS = "TIME_PROGRESS";
    }

    public class NewWatcherMovementDyingRule : NewWatcherMovementRule
    {
        public const string RULE_ID = "DYING_PLACEHOLDER";
        protected BodyEmissionColorAndStrengthContext bodyEmissionColorAndStrengthContext;

        public NewWatcherMovementDyingRule(object controller)
            : base(RULE_ID, controller) {
            this.bodyEmissionColorAndStrengthContext = new()
            {
                bodyEmissionColorProvider = () => UseEntityContext().watcherParams.dyingParams.bodyEmissionColor,
                flowerPartEmissionColorProvider = () => UseEntityContext().watcherParams.dyingParams.flowerPartEmissionColor,
                bodyEmissionStrengthProvider = () => UseEntityContext().watcherParams.dyingParams.bodyEmissionStrength,
                flowerPartEmissionStrengthProvider = () => UseEntityContext().watcherParams.dyingParams.flowerPartEmissionStrength
            };
        }

        public override BodyEmissionColorAndStrengthContext GetBodyEmissionColorAndStrengthContext()
        {
            return this.bodyEmissionColorAndStrengthContext;
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<DyingContext>();

            args.ghostModeAspect =
                args.ghostModeAspect.EvaluateFixedUpdate().nextGhostModeAspect;

            (bool ruleInitialized, Action<bool> SetRuleInitialized)
                = UseState(NewWatcherMovementDyingRuleState.RULE_INITIALIZED,
                false);
            (float timeProgress, Action<float> SetTimeProgress)
                = UseState(NewWatcherMovementDyingRuleState.TIME_PROGRESS, 0f);

            UseEffect(
                NewWatcherMovementDyingRuleEffects.DYING_INIT_EFFECT,
                () =>
                {
                    if (!ruleInitialized) {
                        TargettingAspect targettingAspect =
                            UnityEngine.Object.FindObjectOfType
                                <TargettingAspect>();

                        targettingAspect.targettables.Remove(args.subject.gameObject);
                        targettingAspect.currentTarget = null;
                        WatcherRagdollTransformer.EnsureItIsInRagdollState(
                            args.watcherJiggleRigBuilderHandle,
                            args.ragdollRigidbodies, args.ragdollColliders,
                            useGravity: true);
                        args.headRigidbody.AddForce(
                            context.hitProjectileDirection.normalized
                            * (context.hitProjectileStrength / context.maxHitProjectileStrength)
                            * args.watcherParams.dyingParams.maximumHitForceNewtons,
                            UnityEngine.ForceMode.Impulse);
                        args.watcherParams.soundParams.beforeDyingSounds
                            .finalHitSound.PlayOnceInPosition(args.subject.position);
                        args.watcherParams.soundParams.beforeDyingSounds
                            .beforeDeathExplosionBoilingSound
                            .PlayLoopedWithAudioSource(
                                args.watcherAudioHandles.beforeDyingAudioHandles.boilingAudioSource);
                        args.watcherParams.soundParams.beforeDyingSounds.ambientSounds.Play(
                            GetController(),
                            roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                            ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
                        );
                        SetRuleInitialized(true);
                    }
                }, Tuple.Create(0));

            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeClosedBlendshapeValue(
                 Mathf.Lerp(
                     args.watcherBodyHandles.watcherEyeHandle.GetEyeClosedBlendshapeValue(),
                     100f,
                     args.watcherParams.dyingParams.eyeClosingInterpolation));
            args.watcherBodyHandles.watcherEyeHandle
                .SetEyeLightIntensity(
                Mathf.Lerp(
                    args.watcherBodyHandles.watcherEyeHandle.GetEyeLightIntensity(),
                    0f,
                    args.watcherParams.dyingParams.eyeClosingInterpolation
                    )
                );

            SetTimeProgress(timeProgress + Time.fixedDeltaTime);

            if (timeProgress >= args.watcherParams.dyingParams.timeToExplosionSeconds)
            {
                ExplosionSpawner.SpawnExplosion(
                    args.subject.position,
                    args.watcherParams.dyingParams.explosionPrefab);
                args.watcherParams.soundParams.dyingSounds
                    .dyingExplosionSound.PlayOnceInPosition(args.subject.position);
                args.watcherParams.soundParams.dyingSounds
                    .dyingMoanSound.PlayOnceInPosition(args.subject.position);
                UnityEngine.Object.Destroy(args.parent);
            }
        }

        public override Func<float> GetEyeLightIntensityParamValueProvider()
        {
            return () => UseEntityContext().watcherParams
                .dyingParams.peakEyeLightIntensity;
        }

        public override void StopRuleAmbientSounds()
        {
            var args = UseEntityContext();
            args.watcherParams.soundParams.beforeDyingSounds.ambientSounds.Stop(
                roarAudioSource: args.watcherAudioHandles.ambientAudioHandles.roarAudioSource,
                ambientAudioSource: args.watcherAudioHandles.ambientAudioHandles.ambientAudioSource
            );
        }
    }
}
