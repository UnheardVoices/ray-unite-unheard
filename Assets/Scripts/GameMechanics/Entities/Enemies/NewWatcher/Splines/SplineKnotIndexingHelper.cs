﻿using System;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplineKnotIndexingHelper
    {
        public static int GetNextKnotIndexInSpline(
            Spline spline, int knotIndex)
        {
            if (spline.Closed)
            {
                if (knotIndex < spline.Count - 1)
                {
                    return knotIndex + 1;
                } else
                {
                    return 0;
                }
            } else
            {
                if (knotIndex < spline.Count - 1)
                {
                    return knotIndex + 1;
                } else
                {
                    return knotIndex - 1;
                }
            }
        }

        public static int GetPreviousKnotIndexInSpline(
            Spline spline, int knotIndex)
        {
            if (spline.Closed)
            {
                if (knotIndex > 0)
                {
                    return knotIndex - 1;
                }
                else
                {
                    return spline.Count - 1;
                }
            }
            else
            {
                if (knotIndex > 0)
                {
                    return knotIndex - 1;
                }
                else
                {
                    return knotIndex + 1;
                }
            }
        }
    }
}
