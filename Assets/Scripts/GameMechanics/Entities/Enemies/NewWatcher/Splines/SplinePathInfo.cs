﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public class SplinePathInfo
    {
        public SplineKnotIndex startSplineKnotIndex;
        public SplineKnotIndex endSplineKnotIndex;

        public bool ConcernsOneSpline()
        {
            return 
                this.startSplineKnotIndex.Spline
                == this.endSplineKnotIndex.Spline;
        }

        public SplinePathInfo(
            SplineKnotIndex startSplineKnotIndex,
            SplineKnotIndex endSplineKnotIndex
            )
        {
            this.startSplineKnotIndex = startSplineKnotIndex;
            this.endSplineKnotIndex = endSplineKnotIndex;
        }
    }
}
