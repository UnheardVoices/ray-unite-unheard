﻿using System;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplineTraversingDirectionHelper
    {
        public static TraversingDirection 
            GetTraversingDirectionForSplinePathInfo(
                SplineContainer splineContainer,
                SplinePathInfo splinePathInfo)
        {
            if (!splinePathInfo.ConcernsOneSpline())
            {
                throw new InvalidOperationException(
                    "Cannot determine traversing direction " +
                    "if knots lie in different splines!");
            }

            Spline spline = splineContainer.Splines[
                    splinePathInfo.startSplineKnotIndex.Spline];

            bool traversingLoopBackwardIndicator =
                splinePathInfo.startSplineKnotIndex.Knot == 0 &&
                splinePathInfo.endSplineKnotIndex.Knot == spline.Count - 1
                && spline.Closed;

            bool traversingLoopForwardIndicator =
                splinePathInfo.startSplineKnotIndex.Knot == spline.Count - 1 &&
                splinePathInfo.endSplineKnotIndex.Knot == 0
                && spline.Closed;

            if (((splinePathInfo.startSplineKnotIndex.Knot <=
                splinePathInfo.endSplineKnotIndex.Knot)
                && !traversingLoopBackwardIndicator)
                || traversingLoopForwardIndicator
                )
            {
                return TraversingDirection.FORWARD;
            }
            else
            {
                return TraversingDirection.BACKWARD;
            }
        }
    }
}
