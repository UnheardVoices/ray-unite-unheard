﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules;
using Assets.Scripts.GameMechanics.Entities.Watcher.Rules;
using System;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines.Validation
{
    public static class SplineKnotIndexValidator
    {
        public static bool IsEntireSplineKnotIndexValid(
            SplineContainer splineContainer,
            SplineKnotIndex? splineKnotIndex)
        {
            return splineKnotIndex != null &&
                IsSplineIndexValid(splineContainer, splineKnotIndex)
                &&
                splineKnotIndex?.Knot != null
                && splineKnotIndex?.Knot >= 0
                && splineKnotIndex?.Knot <
                splineContainer.Splines[(int)splineKnotIndex?.Spline].ToArray().Length
                && splineKnotIndex?.Knot !=
                    NewWatcherMovementIdlePatrollingRule.NO_PATROLLING_KNOT_INDEX;
        }

        public static bool IsSplineIndexValid(
            SplineContainer splineContainer,
            SplineKnotIndex? splineKnotIndex)
        {
            return splineKnotIndex != null &&
                splineKnotIndex?.Spline != null &&
                splineKnotIndex?.Spline >= 0
                && splineKnotIndex?.Spline < splineContainer.Splines.Count
                && splineKnotIndex?.Spline !=
                    NewWatcherMovementIdlePatrollingRule.NO_PATROLLING_SPLINE_INDEX;
        }
    }
}
