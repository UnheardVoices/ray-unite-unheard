﻿using Assets.Scripts.GameMechanics.Entities.Watcher.Splines.Validation;
using System;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public static class SplinePathInfoValidator
    {
        public static bool IsEntireSplinePathInfoValid(
            SplineContainer splineContainer,
            SplinePathInfo splinePathInfo)
        {
            return splinePathInfo != null && 
                SplineKnotIndexValidator
                .IsEntireSplineKnotIndexValid(
                    splineContainer, splinePathInfo.startSplineKnotIndex) &&
                    SplineKnotIndexValidator
                        .IsEntireSplineKnotIndexValid(
                        splineContainer, splinePathInfo.endSplineKnotIndex
                        );
        }

        public static void OnInvalidSplinePathInfo(
            SplineContainer splineContainer,
            SplinePathInfo splinePathInfo,
            Action onInvalidSplinePathInfoAction)
        {
            if (!IsEntireSplinePathInfoValid(
                splineContainer, splinePathInfo))
            {
                onInvalidSplinePathInfoAction.Invoke();
            }
        }

        public static void OnValidSplinePathInfo(
            SplineContainer splineContainer,
            SplinePathInfo splinePathInfo,
            Action onValidSplinePathInfoAction)
        {
            if (IsEntireSplinePathInfoValid(
                splineContainer, splinePathInfo))
            {
                onValidSplinePathInfoAction.Invoke();
            }
        }
    }
}
