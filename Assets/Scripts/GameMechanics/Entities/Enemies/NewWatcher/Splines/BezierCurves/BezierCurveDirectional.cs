﻿using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.Watcher.Splines
{
    public struct BezierCurveDirectional
    {
        public BezierCurve curve;
        public TraversingDirection direction;

        public BezierCurveDirectional(
            BezierCurve curve,
            TraversingDirection direction)
        {
            this.curve = curve;
            this.direction = direction;
        }

        public float3 EvaluateTangent(
            Transform splineContainerTransform,
            float bezierCurveProgress)
        {
            float3 baseTangent = 
              splineContainerTransform.TransformDirection(             
                 CurveUtility.EvaluateTangent(
                    this.curve,
                    this.direction == TraversingDirection.FORWARD
                        ? bezierCurveProgress : (1f - bezierCurveProgress))).normalized;

            if (this.direction == TraversingDirection.FORWARD)
            {
                return baseTangent;
            } else
            {
                return -baseTangent;
            }
        }

        public float GetProgressFromNormalized(float bezierCurveProgress)
        {
            if (this.direction == TraversingDirection.FORWARD)
            {
                return bezierCurveProgress;
            } else
            {
                return 1f - bezierCurveProgress;
            }
        }

        public float3 EvaluateUpVector(
            Spline spline,
            int curveIndex,
            float bezierCurveProgress)
        {
            float bezierCurveProgressToAdd =
                this.direction == TraversingDirection.FORWARD ?
                    bezierCurveProgress : (1f - bezierCurveProgress);

            float mappedGlobalSplineProgress = 
                (curveIndex + bezierCurveProgressToAdd) / spline.Count;

            return SplineUtility.EvaluateUpVector(
                spline, mappedGlobalSplineProgress);
        }
    }
}
