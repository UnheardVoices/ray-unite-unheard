﻿using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Body
{
    public static class WatcherBonesFetcher
    {
        public const string WATCHER_FLOWER_BONES_CHAIN_PARENT = 
            "Watcher_flower";
        public const string WATCHER_FLOWER_BONE_NAME_MATCHING_PATTERN
            = @"^(flower)[0-9]+$";
        public const string WATCHER_TAIL_BONE_MATCHING_PATTERN
            = @"^(tail)[0-9]+$";

        public static List<List<Transform>> 
            GetFlowerPartsBoneChainsAndEnsureTheyAllHaveRigidbodyComponents(
                Transform watcherSubject)
        {
            List<Transform> flowerChainsParents =
                TransformHelper.RecursiveFindChildrenWithName(
                    watcherSubject, WATCHER_FLOWER_BONES_CHAIN_PARENT
                    );

            List<List<Transform>> result = new List<List<Transform>>();

            foreach (var chainParent in flowerChainsParents)
            {
                result.Add(
                    TransformHelper.RecursiveFindChildrenWithNamePattern(
                        chainParent,
                        WATCHER_FLOWER_BONE_NAME_MATCHING_PATTERN,
                        forEachFoundAction:
                        (element) =>
                        {
                            element.gameObject.AddComponent<Rigidbody>();
                        })
                );
            }

            return result;
        }

        public static List<Transform> 
            GetTailBonesChainAndEnsureTheyAllHaveRigidbodyComponent(
                Transform watcherSubject)
        {
            return TransformHelper.RecursiveFindChildrenWithNamePattern(
                watcherSubject,
                WATCHER_TAIL_BONE_MATCHING_PATTERN,
                forEachFoundAction:
                (element) =>
                {
                    element.gameObject.AddComponent<Rigidbody>();
                });
        }
    }
}
