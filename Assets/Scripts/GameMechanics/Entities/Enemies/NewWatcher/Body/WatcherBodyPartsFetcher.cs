﻿using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Body
{
    public static class WatcherBodyPartsFetcher
    {
        public const string WATCHER_EYE_OBJ_NAME = "Watcher_Eye";
        public const string WATCHER_BODY_LOW_OBJ_NAME = "watcher_body_low";
        public const string WATCHER_FLOWER_PART = "flower_part";
        public const string WATCHER_EYE_LIGHT = "eye_light";

        public static Transform GetEyeTransform(Transform parent)
        {
            return TransformHelper.RecursiveFindChild(
                parent, WATCHER_EYE_OBJ_NAME);
        }

        public static Transform GetEyeLight(Transform parent)
        {
            return TransformHelper.RecursiveFindChild(
                parent, WATCHER_EYE_LIGHT);
        }

        public static Transform GetWatcherBodyLowTransform(
            Transform parent)
        {
            return TransformHelper.RecursiveFindChild(
                parent, WATCHER_BODY_LOW_OBJ_NAME
                );
        }

        public static List<Transform> GetWatcherFlowerPartsTransforms(
            Transform parent)
        {
            return TransformHelper.RecursiveFindChildrenWithName(
                parent, WATCHER_FLOWER_PART);
        }
    }
}
