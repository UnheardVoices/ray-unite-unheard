﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Body.Ragdoll
{
    public static class WatcherBoneCharacterJointInitializer
    {
        public static void Initialize(
            Transform parent, CharacterJoint boneCharacterJoint,
            int childOrderIndexInChain, int bonesInChainTotalAmount)
        {
            boneCharacterJoint.connectedBody = parent.GetComponent<Rigidbody>();

            // throw new NotImplementedException();
        }
    }
}
