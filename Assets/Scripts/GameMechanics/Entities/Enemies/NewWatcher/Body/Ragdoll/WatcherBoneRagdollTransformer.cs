﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Body.Ragdoll
{
    public static class WatcherBoneRagdollTransformer
    {
        public static void 
            MakeBoneWithColliderAndParentItWithCharacterJointToTheOtherBone(
                Transform child, Transform parent,
                int childOrderIndexInChain, int bonesInChainTotalAmount)
        {
            if (child.GetComponent<CharacterJoint>() != null)
            {
                throw new InvalidOperationException("Bone already has CharacterJoint component!");
            }
            if (child.GetComponent<Rigidbody>() == null)
            {
                throw new InvalidOperationException("Bone does not have Rigidbody component!");
            }
            if (child.GetComponent<Collider>() != null)
            {
                throw new InvalidOperationException("Bone already has Collider component!");
            }

            Rigidbody boneRigidbody = 
                child.gameObject.GetComponent<Rigidbody>();
            WatcherBoneRigidbodyInitializer.Initialize(
                boneRigidbody, childOrderIndexInChain, bonesInChainTotalAmount);

            //Collider boneCollider =
            //    child.gameObject.AddComponent<CapsuleCollider>();
            //var boneCollider =
            //    child.gameObject.AddComponent<BoxCollider>();
            

            //boneCollider.size = new Vector3(4f, 4f, 4f);
            //WatcherBoneColliderInitializer.Initialize(
            //    boneCollider, childOrderIndexInChain, bonesInChainTotalAmount);

            CharacterJoint boneCharacterJoint =
                child.gameObject.AddComponent<CharacterJoint>();
            WatcherBoneCharacterJointInitializer.Initialize(
                 parent, boneCharacterJoint, childOrderIndexInChain, bonesInChainTotalAmount);

            // throw new NotImplementedException();
        }
    }
}
