﻿
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Transitions;
using Assets.Scripts.GameMechanics.Entities.Watcher.Handles;
using JigglePhysics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher
{
    public class NewWatcherContext : NewEntityContext
    {
        public static Color DEFAULT_BODY_EMISSION_COLOR = new Color(r: 178, g: 9, b: 177, a: 0);
        public static Color DEFAULT_FLOWER_PART_EMISSION_COLOR = new Color(r: 91, g: 9, b: 178, a: 0);
        public static float DEFAULT_BODY_EMISSION_STRENGTH = 24.09f;
        public static float DEFAULT_FLOWER_PART_EMISSION_STRENGTH = 3.14f;

        public GameObject parent;

        public SplineContainer patrollingPathsSplinesContainer;

        public Transform playerObject;
        public Vector3 playerObjectFovPositionOffset;

        public NewWatcherRulesTransitions watcherRulesTransitions;
        public WatcherBodyHandles watcherBodyHandles;
        public WatcherAudioHandles watcherAudioHandles;

        public NewWatcherParams watcherParams;

        public Vector3 baseWatcherForwardDirection;
        public Vector3 baseWatcherPosition;

        public Color baseWatcherBodyColor;
        public Color baseWatcherFlowerPartColor;
        public float baseEyeLightIntensity;
        public INewWatcherGhostModeAspect ghostModeAspect;

        public float currentHealthPoints;

        public JiggleRigBuilder watcherJiggleRigBuilderHandle;
        public Rigidbody headRigidbody;
        public List<Rigidbody> ragdollRigidbodies;
        public List<Collider> ragdollColliders;
    }
}
