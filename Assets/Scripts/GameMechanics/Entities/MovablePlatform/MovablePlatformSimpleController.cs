﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.MovablePlatform
{
    public class MovablePlatformSimpleController : MonoBehaviour
    {
        private float baseHeight;
        private float baseX;
        private float baseZ;

        private void Start()
        {
            this.baseHeight = this.transform.position.y;
            this.baseX = this.transform.position.x;
            this.baseZ = this.transform.position.z;
        }

        private void FixedUpdate()
        {
            this.transform.position = 
                new Vector3(
                    this.baseX + (Mathf.Sin(Time.realtimeSinceStartup * 1f) * 10f) * this.transform.right.x,
                    this.baseHeight,
                    this.baseZ);          
        }
    }
}
