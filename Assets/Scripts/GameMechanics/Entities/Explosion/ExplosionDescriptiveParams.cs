﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Explosion
{
    [Serializable]
    public class ExplosionDescriptiveParams
    {
        [Range(0.1f, 500f)]
        public float minimumExplosionRadius = 1f;

        [Range(0.1f, 500f)]
        public float maximumExplosionRadius = 20f;

        [Range(0.1f, 10000f)]
        public float minimumExplosionForce = 1f;

        [Range(0.1f, 10000f)]
        public float maximumExplosionForce = 700f;

        public ExplosionAttributes GetExplosionAttributes()
        {
            return new()
            {
                explosionForce = 
                    UnityEngine.Random.Range(this.minimumExplosionForce, this.maximumExplosionForce),
                explosionRadius = 
                    UnityEngine.Random.Range(this.minimumExplosionRadius, this.maximumExplosionRadius),
            };
        }
    }
}
