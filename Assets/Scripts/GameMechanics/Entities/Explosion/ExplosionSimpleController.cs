﻿using UnityEngine;
using System.Linq;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectileExplosion
{
    public class ExplosionSimpleController : MonoBehaviour
    {
        public float baseExplosionTimeSecondsLightsDestroyThreshold = 0.4f;

        public ParticleSystem baseExplosionParticleSystem;
        public ParticleSystem shrapnelParticleSystem;

        public void FixedUpdate()
        {
            if (this.baseExplosionParticleSystem &&
                this.baseExplosionParticleSystem != null &&
                this.baseExplosionParticleSystem.time >= this.baseExplosionTimeSecondsLightsDestroyThreshold)
            {
                var mainPotentialLight = GetComponent<Light>();

                if (mainPotentialLight != null)
                {
                    Destroy(mainPotentialLight.gameObject);
                }
                GetComponentsInChildren<Light>().ToList().ForEach(x => Destroy(x.gameObject));
            }

            if (this.shrapnelParticleSystem &&
                this.shrapnelParticleSystem != null &&
                !this.shrapnelParticleSystem.IsAlive())
            {
                Destroy(this.gameObject);
            }
        }
    }
}
