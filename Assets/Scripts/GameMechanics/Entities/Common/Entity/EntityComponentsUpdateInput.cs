﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity
{
    public class EntityComponentsUpdateInput
    {
        public Transform subject;

        public EntityComponentsUpdateInput(Transform subject)
        {
            this.subject = subject;
        }
    }
}
