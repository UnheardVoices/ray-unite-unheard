﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity.Rules.Utils
{
    public static class GravityUtils
    {
        public static Vector3 AccelerateGravitationallyUpUntilLimitFallSpeed(
            Vector3 velocity,
            float gravityAcceleration,
            float limitFallSpeed)
        {
            return new Vector3(
                    velocity.x,
                    velocity.y > -limitFallSpeed ?
                        velocity.y - gravityAcceleration :
                        velocity.y,
                    velocity.z
                    );
        }
    }
}
