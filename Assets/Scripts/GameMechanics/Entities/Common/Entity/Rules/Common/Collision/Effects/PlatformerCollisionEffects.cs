﻿using Assets.Scripts.GameMechanics.Collisions;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision
{
    public class PlatformerCollisionEffects
    {
        private PlatformerCollisionHub platformerCollisionHub;

        public PlatformerCollisionEffects(
            PlatformerCollisionHub platformerCollisionHub)
        {
            this.platformerCollisionHub = platformerCollisionHub;
        }

        public void OnCollisionWithSteadyGroundWithVelocity(
            Action<Vector3, Vector3> onCollisionAction)
        {
            if (this.platformerCollisionHub
                    .steadyEnvironmentVelocityGroundCollisionNormal != null
                    &&
                this.platformerCollisionHub
                    .steadyEnvironmentVelocityGroundCollisionPoint != null)
            {
                onCollisionAction.Invoke(
                    (Vector3)this.platformerCollisionHub.steadyEnvironmentVelocityGroundCollisionNormal,
                    (Vector3)this.platformerCollisionHub.steadyEnvironmentVelocityGroundCollisionPoint);
            }
        }
    }
}
