﻿using Assets.Scripts.Engine.Behaviours;

namespace Assets.Scripts.GameMechanics.Entities.Entity
{
    public class EntityController<T, U> : GameplayOnlyMonoBehaviour
        where T : EntityRule<U>
        where U : EntityComponentsUpdateInput
    {
        protected T currentRule;
        protected U componentsUpdateInput;

        public string GetCurrentRuleClassName()
        {
            return this.currentRule.GetRuleClassName();
        }

        public void SetCurrentRule(T currentRule)
        {
            this.currentRule = currentRule;
        }

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            base.GameplayUpdate(deltaTime);
            this.currentRule.GameplayUpdate(
                deltaTime, this.componentsUpdateInput);
        }

        protected override void GameplayFixedUpdate()
        {
            base.GameplayFixedUpdate();
            this.currentRule.GameplayFixedUpdate(
                this.componentsUpdateInput);
        }
    }
}
