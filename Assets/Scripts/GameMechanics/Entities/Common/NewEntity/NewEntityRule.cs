using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity
{
    public class RefObject<T>
    {
        public T current;

        public RefObject(T initialValue) {
            this.current = initialValue;
        }
    }

    public class NewEntityRule<T>
        where T : NewEntityContext, new()
    {
        private Dictionary<string, EffectInfo> effects =
            new Dictionary<string, EffectInfo>();
        protected Dictionary<string, object> states = new();
        private Dictionary<string, object> refs = new();

        public string ruleId { get; private set; }

        public object entityController;

        public NewEntityRule()
        {

        }

        public NewEntityRule(string ruleId, object entityController)
        {
            this.ruleId = ruleId;
            this.entityController = entityController;
        }

        //instead of UpdateInputArgs, use UseEntityContext() to get EntityContext (which contains all vars from UpdateInputArgs
        public virtual void GameplayUpdate(float deltaTime) { }
        public virtual void GameplayFixedUpdate() { }

        public void OnStateNotNull<T>(
            string stateKey, Action<T> stateValueAction)
        {
            if (this.states.ContainsKey(stateKey))
            {
                T stateValue = (T)this.states[stateKey];
                if (stateValue != null)
                {
                    stateValueAction(stateValue);
                }
            }
        }

        public void OnStateNotEqual<T>(
            string stateKey,
            T value,
            Action<T> stateValueAction)
        {
            if (this.states.ContainsKey(stateKey))
            {
                T stateValue = (T)this.states[stateKey];
                if (!stateValue.Equals(value))
                {
                    stateValueAction(stateValue);
                }
            }
        }

        public RefObject<T> UseRef<T>(string identifier, T initialValue)
        {
            if (!this.refs.ContainsKey(identifier))
            {
                this.refs[identifier] = new RefObject<T>(initialValue);
            }
            return (RefObject<T>)this.refs[identifier];
        }

        public Tuple<T, Action<T>> UseState<T>(
            string identifier, T initialValue)
        {
            if (!this.states.ContainsKey(identifier))
            {
                this.states[identifier] = initialValue;
            }
            return Tuple.Create<T, Action<T>>(
                (T)this.states[identifier],
                (value) => { this.states[identifier] = value; });
        }

        protected V UseContext<V>() where V : Context, new()
        {
            return UseContext<V>(this.ruleId);
        }

        protected void SetContext<V>(V contextValue) where V : Context, new()
        {
            SetContext<V>(this.ruleId, contextValue);
        }

        protected virtual void SetContext<V>(
            string contextIdent, V contextValue) where V : Context, new()
        {
            UseEntityContext().contextsHolder.SetContext(
                contextIdent, contextValue);
        }

        protected virtual V UseContext<V>(string contextIdent) where V : Context, new()
        {
            return UseEntityContext().contextsHolder.
                UseContext<V>(contextIdent);
        }

        protected virtual T UseEntityContext()
        {
            return (T)GetController<NewEntityRule<T>>().controllerContext;
        }

        private protected virtual NewEntityController<R, T> GetController<R>()
            where R : NewEntityRule<T>, new()
        {
            return GetCast<NewEntityController<R, T>>(this.entityController);
        }

        protected V GetCast<V>(object obj)
        {
            return (V)obj;
        }

        protected void UseEffect(
            string identifier, Action effectAction,
            ITuple effectDependencies = null)
        {
            if (!this.effects.ContainsKey(identifier))
            {
                this.effects[identifier] =
                    new EffectInfo(effectAction, effectDependencies);
            }
            EffectInfo effectInfo = this.effects[identifier];
            if (effectInfo.CanBeCalled(effectDependencies))
            {
                effectInfo.Invoke();
            }
        }

        public string GetRuleClassName()
        {
            return GetType().Name;
        }
    }
}
