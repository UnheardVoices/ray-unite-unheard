using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity
{
    public class NewEntityContext
    {
        public ContextsHolder contextsHolder = new();

        //common characteristics for EntityContext
        public Transform subject;

        public Kinematics kinematics { get; set; }

        public NewEntityContext()
        {

        }

        //public NewEntityContext(Transform subject)
        //{
        //    this.subject = subject;
        //}
    }
}
