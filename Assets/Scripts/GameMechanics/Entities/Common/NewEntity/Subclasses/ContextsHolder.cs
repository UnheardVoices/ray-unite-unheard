using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses
{
    public class ContextsHolder
    {
        protected Dictionary<string, Context> contexts = new();

        public T UseContext<T>(string identifier) where T : Context, new()
        {
            return UseContext<T>(identifier, new());
        }

        public T UseContext<T>(string identifier, T initValue) where T : Context
        {
            if (!this.contexts.ContainsKey(identifier))
            {
                this.contexts.Add(identifier, initValue);
            }

            return (T)this.contexts[identifier];
        }

        public void SetContext<T>(string identifier, T value) where T : Context
        {
            if (!this.contexts.ContainsKey(identifier))
            {
                this.contexts.Add(identifier, value);
            }
            else
            {
                this.contexts[identifier] = value;
            }
        }
    }
}
