using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses
{
    public class EffectInfo
    {
        public Action effectAction;
        protected bool calledAtLeastOnce;
        protected ITuple effectDependenciesLastValues;

        public EffectInfo(
            Action effectAction,
            ITuple effectDependenciesLastValues)
        {
            this.effectAction = effectAction;
            this.calledAtLeastOnce = false;
            this.effectDependenciesLastValues = effectDependenciesLastValues;
        }

        public void Invoke()
        {
            this.effectAction.Invoke();
            this.calledAtLeastOnce = true;
        }

        public bool CanBeCalled(ITuple effectDependenciesCurrentValues)
        {
            bool haveDependenciesChanged =

                (this.effectDependenciesLastValues == null
                    && effectDependenciesCurrentValues != null)

                    ||

                (this.effectDependenciesLastValues != null
                    && effectDependenciesCurrentValues == null)

                    ||

                (this.effectDependenciesLastValues != null
                    &&
                    !this.effectDependenciesLastValues.Equals(
                    effectDependenciesCurrentValues));

            if (haveDependenciesChanged)
            {
                this.effectDependenciesLastValues =
                    effectDependenciesCurrentValues;
            }

            return haveDependenciesChanged || !this.calledAtLeastOnce;
        }
    }
}
