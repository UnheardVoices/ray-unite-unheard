﻿using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.Utils.Portals;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Device;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Common;
using static UnityEditor.PlayerSettings;
using System.Drawing;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalRenderingBehaviour : MonoBehaviour
    {
        public PortalController portalController;
        [Header("Advanced Settings")]
        public float nearClipOffset = 0.05f;
        public float nearClipLimit = 0.2f;

        protected Transform threshold;
        protected MeshFilter screenMeshFilter;

        public Vector3 portalDerivedForward
        {
            get
            {
                return this.portalController.portalParams.invertForwardDirection ? -this.transform.forward : this.transform.forward;
            }
        }

        public List<PortalTraveller> trackedTravellers;

        void Start()
        {
            this.portalController = GetComponent<PortalController>();
            this.threshold = GetComponentsInChildren<Transform>().First(x => x.name.Equals("Threshold"));
            this.screenMeshFilter = GetComponentInChildren<MeshFilter>();
        }

        public Vector3 InverseTransformDirection(Vector3 dir)
        {
            GameObject temp = new GameObject();
            temp.transform.up = this.transform.up;
            //temp.transform.forward = this.portalDerivedForward;
            temp.transform.forward = this.portalDerivedForward;
            var result = temp.transform.InverseTransformDirection(dir);
            Destroy(temp);
            return result;
        }

        public Vector3 InverseTransformPoint(Vector3 pos)
        {
            GameObject temp = new GameObject();
            temp.transform.up = this.transform.up;
            //temp.transform.forward = this.portalDerivedForward;
            temp.transform.forward = this.portalDerivedForward;
            var result = temp.transform.InverseTransformPoint(pos);
            Destroy(temp);
            return result;
        }

        public Vector3 TransformDirection(Vector3 dir)
        {
            GameObject temp = new GameObject();
            temp.transform.up = this.transform.up;
            //temp.transform.forward = this.portalDerivedForward;
            temp.transform.forward = this.portalDerivedForward;
            var result = temp.transform.TransformDirection(dir);
            Destroy(temp);
            return result;
        }

        public Vector3 TransformPoint(Vector3 pos)
        {
            GameObject temp = new GameObject();
            temp.transform.up = this.transform.up;
            //temp.transform.forward = this.portalDerivedForward;
            temp.transform.forward = this.portalDerivedForward;
            var result = temp.transform.TransformPoint(pos);
            Destroy(temp);
            return result;
        }

        public Matrix4x4 localToWorldMatrix
        {
            get
            {
                GameObject temp = new GameObject();
                temp.transform.up = this.transform.up;
                //temp.transform.forward = this.portalDerivedForward;
                temp.transform.forward = this.portalDerivedForward;
                temp.transform.position = this.transform.position;
                var result = temp.transform.localToWorldMatrix;
                Destroy(temp);
                return result;
            }
        }

        public Matrix4x4 worldToLocalMatrix
        {
            get
            {
                GameObject temp = new GameObject();
                temp.transform.up = this.transform.up;
                //temp.transform.forward = this.portalDerivedForward;
                temp.transform.forward = this.portalDerivedForward;
                temp.transform.position = this.transform.position;
                var result = temp.transform.worldToLocalMatrix;
                Destroy(temp);
                return result;
            }
        }

        private void Awake()
        {
            this.trackedTravellers = new List<PortalTraveller>();
        }

        private void FixedUpdate()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            Debug.DrawRay(this.transform.position, this.portalDerivedForward * 10f);
            HandleTravellers();
            ProtectScreenFromClipping(playerCam.transform.position);
            //HandleTravellers();
        }

        private void LateUpdate()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            HandleTravellers();
            ProtectScreenFromClipping(playerCam.transform.position);
        }

        private void Update()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;

            Render();
            ProtectScreenFromClipping(playerCam.transform.position);
            // HandleTravellers();
        }

        public bool ContainsTravellerWithinBoundaries(PortalTraveller traveller)
        {
            Vector3 direction1 = this.portalDerivedForward;
            Vector3 direction2 = -this.portalDerivedForward;
            Vector3 startingPoint = this.transform.position;

            Ray ray1 = new Ray(startingPoint, direction1);
            Ray ray2 = new Ray(startingPoint, direction2);
            float distance1 = Vector3.Cross(ray1.direction, traveller.transform.position - ray1.origin).magnitude;
            float distance2 = Vector3.Cross(ray2.direction, traveller.transform.position - ray2.origin).magnitude;
            return distance1 < 10f || distance2 < 10f;
        }

        private void HandleTravellers()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>();

            for (int i = 0; i < this.trackedTravellers.Count; i++)
            {
                PortalTraveller traveller = this.trackedTravellers[i];
                Transform travellerT = traveller.transform;
                //var m = linkedPortal.transform.localToWorldMatrix * this.transform.worldToLocalMatrix * travellerT.localToWorldMatrix;
                var m = linkedPortal.localToWorldMatrix * this.worldToLocalMatrix * travellerT.localToWorldMatrix;

                Vector3 offsetFromPortal = travellerT.position - this.transform.position;
                int portalSide = System.Math.Sign(Vector3.Dot(offsetFromPortal, this.portalDerivedForward));
                int portalSideOld = System.Math.Sign(Vector3.Dot(traveller.previousOffsetFromPortal, this.portalDerivedForward));

                // Teleport the traveller if it has crossed from one side of the portal to the other
                if (portalSide != portalSideOld)
                {
                    var positionOld = travellerT.position;
                    var rotOld = travellerT.rotation;

                    if (traveller.name.Equals("Player"))
                    {
                        //Debug.Break();
                        Debug.Log("TELELELELELELE");
                    }


                    //traveller.OnPortalTravel(linkedPortal.controllerContext.renderingBehaviour);
                    //traveller.OnPortalEnter(this);
                    //if (!traveller.entranceExitCalled)
                    //{
                    //Vector3 projectedExitPosition = linkedPortal.TransformPoint(InverseTransformPoint(travellerT.position));
                    //Vector3 projectedExitForward = linkedPortal.TransformDirection(InverseTransformDirection(travellerT.forward));

                    //traveller.Teleport(this.transform, linkedPortal.transform, m.GetColumn(3), m.rotation);
                    traveller.Teleport(this.transform, linkedPortal.transform, m.GetColumn(3), m.rotation);
                    //traveller.OnPortalExit(this);

                    
                    linkedPortal.portalController.controllerContext.renderingBehaviour.OnTravellerEnterPortal(traveller);
                    //traveller.previousOffsetFromPortal = offsetFromPortal;

                    //}                    
                    //traveller.OnPortalTravel(linkedPortal.controllerContext.renderingBehaviour);
                    // traveller.graphicsClone.transform.SetPositionAndRotation(positionOld, rotOld);
                    // Can't rely on OnTriggerEnter/Exit to be called next frame since it depends on when FixedUpdate runs
                    //                      


                    this.trackedTravellers.RemoveAt(i);
                    i--;

                    //traveller.previousOffsetFromPortal = -traveller.previousOffsetFromPortal;

                    //FindObjectsOfType<PortalRenderingBehaviour>().Where(x => !x.name.Equals(linkedPortal.name)).ToList().ForEach(x =>
                    //{
                    //    x.trackedTravellers.Remove(traveller);
                    //});
                }
                else
                {
                    //traveller.graphicsClone.transform.SetPositionAndRotation(m.GetColumn(3), m.rotation);
                    //UpdateSliceParams (traveller);

                    //this.trackedTravellers.RemoveAt(i);
                    //i--;
                    traveller.previousOffsetFromPortal = offsetFromPortal;
                }
            }
        }

        void CreateViewTexture()
        {
            if (this.portalController.controllerContext.renderTexture == null ||
                this.portalController.controllerContext.renderTexture.width != UnityEngine.Screen.width ||
                this.portalController.controllerContext.renderTexture.height != UnityEngine.Screen.height)
            {
                if (this.portalController.controllerContext.renderTexture != null)
                {
                    this.portalController.controllerContext.renderTexture.Release();
                }
                this.portalController.controllerContext.renderTexture = new RenderTexture(UnityEngine.Screen.width, UnityEngine.Screen.height, 0);
                this.portalController.controllerContext.portalCamera.targetTexture = this.portalController.controllerContext.renderTexture;
                this.portalController.portalParams.linkedPortal.controllerContext
                    .portalScreenHandle.renderMaterial.SetTexture("_MainTex", this.portalController.controllerContext.renderTexture);
            }
        }

        // Called before any portal cameras are rendered for the current frame
        public void PrePortalRender()
        {
            foreach (var traveller in this.trackedTravellers)
            {
                UpdateSliceParams(traveller);
            }
        }

        public void PostPortalRender()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;

            foreach (var traveller in this.trackedTravellers)
            {
                UpdateSliceParams(traveller);
            }
            ProtectScreenFromClipping(playerCam.transform.position);
        }

        public float ProtectScreenFromClipping(Vector3 viewPoint)
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;

            float halfHeight = playerCam.nearClipPlane * Mathf.Tan(playerCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float halfWidth = halfHeight * playerCam.aspect;
            float dstToNearClipPlaneCorner = new Vector3(halfWidth, halfHeight, playerCam.nearClipPlane).magnitude;
            float screenThickness = dstToNearClipPlaneCorner;

            Transform screenT = screen.transform;
            bool camFacingSameDirAsPortal = Vector3.Dot(this.transform.forward, this.transform.position - viewPoint) > 0;
            screenT.localScale = new Vector3(screenThickness, screenT.localScale.y, screenT.localScale.z);
            screenT.localPosition = Vector3.forward * screenThickness * ((camFacingSameDirAsPortal) ? 0.5f : -0.5f);
            return screenThickness;
        }

        void UpdateSliceParams(PortalTraveller traveller)
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;
            var playerCam = this.portalController.controllerContext.playerCamera;

            // Calculate slice normal
            int side = SideOfPortal(traveller.transform.position);
            Vector3 sliceNormal = this.portalDerivedForward * -side;
            Vector3 cloneSliceNormal = linkedPortal.controllerContext.renderingBehaviour.portalDerivedForward * side;

            // Calculate slice centre
            Vector3 slicePos = this.transform.position;
            Vector3 cloneSlicePos = linkedPortal.transform.position;

            // Adjust slice offset so that when player standing on other side of portal to the object, the slice doesn't clip through
            float sliceOffsetDst = 0;
            float cloneSliceOffsetDst = 0;
            float screenThickness = screen.transform.localScale.z;

            bool playerSameSideAsTraveller = SameSideOfPortal(playerCam.transform.position, traveller.transform.position);
            if (!playerSameSideAsTraveller)
            {
                sliceOffsetDst = -screenThickness;
            }
            bool playerSameSideAsCloneAppearing = side != linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(playerCam.transform.position);
            if (!playerSameSideAsCloneAppearing)
            {
                cloneSliceOffsetDst = -screenThickness;
            }

            // Apply parameters
            for (int i = 0; i < traveller.originalMaterials.Length; i++)
            {
                traveller.originalMaterials[i].SetVector("sliceCentre", slicePos);
                traveller.originalMaterials[i].SetVector("sliceNormal", sliceNormal);
                traveller.originalMaterials[i].SetFloat("sliceOffsetDst", sliceOffsetDst);

                traveller.cloneMaterials[i].SetVector("sliceCentre", cloneSlicePos);
                traveller.cloneMaterials[i].SetVector("sliceNormal", cloneSliceNormal);
                traveller.cloneMaterials[i].SetFloat("sliceOffsetDst", cloneSliceOffsetDst);
            }
        }

        public void Render()
        {
            var playerCam = this.portalController.controllerContext.playerCamera;
            
            int recursionLimit = 3;
            var portalCam = this.portalController.controllerContext.portalCamera;
            
            //portalCam.enabled = true;
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var screen = this.portalController.controllerContext.portalScreenHandle.screenHandle;

            // Skip rendering the view from this portal if player is not looking at the linked portal
            if (!CameraUtility.VisibleFromCamera(linkedPortal.controllerContext.portalScreenHandle.screenHandle, playerCam))
            {
                return;
            }

            portalCam.enabled = true;

            CreateViewTexture();

            var localToWorldMatrix = playerCam.transform.localToWorldMatrix;
            var renderPositions = new Vector3[recursionLimit];
            var renderRotations = new Quaternion[recursionLimit];

            //var localToWorldMatrics = 

            int startIndex = 0;
            portalCam.projectionMatrix = playerCam.projectionMatrix;
            for (int i = 0; i < recursionLimit; i++)
            {
                if (i > 0)
                {
                    //// No need for recursive rendering if linked portal is not visible through this portal
                    if (!CameraUtility.BoundsOverlap(this.screenMeshFilter, linkedPortal.controllerContext.renderingBehaviour.screenMeshFilter, portalCam))
                    {
                        break;
                    }
                }
                localToWorldMatrix = this.transform.localToWorldMatrix * linkedPortal.transform.worldToLocalMatrix * localToWorldMatrix;
                
                // localToWorldMatrix = linkedPortal.transform.localToWorldMatrix * this.transform.worldToLocalMatrix * localToWorldMatrix;

                int renderOrderIndex = recursionLimit - i - 1;
                renderPositions[renderOrderIndex] = localToWorldMatrix.GetColumn(3);
                renderRotations[renderOrderIndex] = localToWorldMatrix.rotation;

                portalCam.transform.SetPositionAndRotation(renderPositions[renderOrderIndex], renderRotations[renderOrderIndex]);

                startIndex = renderOrderIndex;
            }

            // Hide screen so that camera can see through portal
            screen.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
            linkedPortal.controllerContext.portalScreenHandle.screenHandle.material.SetInt("displayMask", 0);

            for (int i = startIndex; i < recursionLimit; i++)
            {
                portalCam.transform.SetPositionAndRotation(renderPositions[i], renderRotations[i]);
                SetNearClipPlane();
                //HandleClipping();
                portalCam.Render();

                if (i == startIndex)
                {
                    linkedPortal.controllerContext.portalScreenHandle.screenHandle.material.SetInt("displayMask", 1);
                }
            }

            // Unhide objects hidden at start of render
            screen.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            portalCam.enabled = false;
        }

        // Use custom projection matrix to align portal camera's near clip plane with the surface of the portal
        // Note that this affects precision of the depth buffer, which can cause issues with effects like screenspace AO
        void SetNearClipPlane()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var playerCam = this.portalController.controllerContext.playerCamera;
            var portalCam = this.portalController.controllerContext.portalCamera;

            // Learning resource:
            // http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
            Transform clipPlane = this.transform;
            int dot = System.Math.Sign(Vector3.Dot(clipPlane.forward, this.transform.position - portalCam.transform.position));

            Vector3 camSpacePos = portalCam.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
            Vector3 camSpaceNormal = portalCam.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
            float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) + this.nearClipOffset;

            // Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
            if (Mathf.Abs(camSpaceDst) > this.nearClipLimit)
            {
                Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

                // Update projection based on new clip plane
                // Calculate matrix with player cam so that player camera settings (fov, etc) are used
                portalCam.projectionMatrix = playerCam.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }
            else
            {
                portalCam.projectionMatrix = playerCam.projectionMatrix;
            }
        }

        void OnTravellerEnterPortal(PortalTraveller traveller)
        {
            if (!this.trackedTravellers.Contains(traveller))
            {
                traveller.EnterPortalThreshold();
                traveller.previousOffsetFromPortal = traveller.transform.position - this.transform.position;
                //traveller.lastPortal = this.portalController.portalParams.linkedPortal.transform;
                this.trackedTravellers.Add(traveller);
            }
        }

        void HandleClipping()
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;
            var playerCam = this.portalController.controllerContext.playerCamera;
            var portalCam = this.portalController.controllerContext.portalCamera;

            var portalCamPos = portalCam.transform.position;

            // There are two main graphical issues when slicing travellers
            // 1. Tiny sliver of mesh drawn on backside of portal
            //    Ideally the oblique clip plane would sort this out, but even with 0 offset, tiny sliver still visible
            // 2. Tiny seam between the sliced mesh, and the rest of the model drawn onto the portal screen
            // This function tries to address these issues by modifying the slice parameters when rendering the view from the portal
            // Would be great if this could be fixed more elegantly, but this is the best I can figure out for now
            const float hideDst = -1000;
            const float showDst = 1000;
            float screenThickness = linkedPortal.controllerContext.renderingBehaviour.ProtectScreenFromClipping(portalCam.transform.position);

            foreach (var traveller in this.trackedTravellers)
            {
                if (SameSideOfPortal(traveller.transform.position, portalCamPos))
                {
                    // Addresses issue 1
                    traveller.SetSliceOffsetDst(hideDst, false);
                }
                else
                {
                    // Addresses issue 2
                    traveller.SetSliceOffsetDst(showDst, false);
                }

                // Ensure clone is properly sliced, in case it's visible through this portal:
                int cloneSideOfLinkedPortal = -SideOfPortal(traveller.transform.position);
                bool camSameSideAsClone = linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(portalCamPos) == cloneSideOfLinkedPortal;
                if (camSameSideAsClone)
                {
                    traveller.SetSliceOffsetDst(screenThickness, true);
                }
                else
                {
                    traveller.SetSliceOffsetDst(-screenThickness, true);
                }
            }

            var offsetFromPortalToCam = portalCamPos - this.transform.position;
            foreach (var linkedTraveller in linkedPortal.controllerContext.renderingBehaviour.trackedTravellers)
            {
                var travellerPos = linkedTraveller.graphicsObject.transform.position;
                var clonePos = linkedTraveller.graphicsClone.transform.position;
                // Handle clone of linked portal coming through this portal:
                bool cloneOnSameSideAsCam = linkedPortal.controllerContext.renderingBehaviour.SideOfPortal(travellerPos) != SideOfPortal(portalCamPos);
                if (cloneOnSameSideAsCam)
                {
                    // Addresses issue 1
                    linkedTraveller.SetSliceOffsetDst(hideDst, true);
                }
                else
                {
                    // Addresses issue 2
                    linkedTraveller.SetSliceOffsetDst(showDst, true);
                }

                // Ensure traveller of linked portal is properly sliced, in case it's visible through this portal:
                bool camSameSideAsTraveller = linkedPortal.controllerContext.renderingBehaviour.SameSideOfPortal(linkedTraveller.transform.position, portalCamPos);
                if (camSameSideAsTraveller)
                {
                    linkedTraveller.SetSliceOffsetDst(screenThickness, false);
                }
                else
                {
                    linkedTraveller.SetSliceOffsetDst(-screenThickness, false);
                }
            }
        }

        public void OnThresholdCollisionEnter(PortalTraveller portalTraveller)
        {
            OnTravellerEnterPortal(portalTraveller);            
        }

        public void OnThresholdCollisionExit(PortalTraveller portalTraveller)
        {
            if (portalTraveller && this.trackedTravellers.Contains(portalTraveller))
            {
                //traveller.ExitPortalThreshold();
                this.trackedTravellers.Remove(portalTraveller);
            }
        }

        void OnTriggerExit(Collider other)
        {
            //var traveller = other.GetComponent<PortalTraveller>();
            //if (traveller && this.trackedTravellers.Contains(traveller))
            //{
            //    traveller.ExitPortalThreshold();
            //    this.trackedTravellers.Remove(traveller);
            //}
        }

        /*
        ** Some helper/convenience stuff:
        */

        int SideOfPortal(Vector3 pos)
        {
            //return System.Math.Sign(Vector3.Dot(pos - this.transform.position, this.transform.forward));
            return System.Math.Sign(Vector3.Dot(pos - this.transform.position, this.portalDerivedForward));
        }

        bool SameSideOfPortal(Vector3 posA, Vector3 posB)
        {
            return SideOfPortal(posA) == SideOfPortal(posB);
        }

        public (Vector3, float) GetProjectedPositionAndDistanceThrough(Vector3 positionFrom, Vector3 positionTo)
        {
            var linkedPortal = this.portalController.portalParams.linkedPortal;

            Vector3 closestPointOnEntrancePortal = GetPortalCollider().ClosestPoint(positionFrom);
            Vector3 closestPointToTargetOnExitPortal =
                linkedPortal
                  .controllerContext
                  .renderingBehaviour
                  .GetPortalCollider().ClosestPoint(positionTo);

            Vector3 projectedPosition = 
                this.transform.TransformPoint(linkedPortal.transform.InverseTransformPoint(positionTo));

            //Vector3 rayToProjectedPosition = projectedPosition - positionFrom;

            return (
                projectedPosition,
                Vector3.Distance(positionFrom, closestPointOnEntrancePortal)
                +
                Vector3.Distance(positionTo, closestPointToTargetOnExitPortal)
            );

            // Vector3? rayToProjectedPositionToEntrancePortal = null;
            //Vector3 rayToProjectedPositionFromExitPortal;

            //RaycastHit rayToProjectedPositionToEntrancePortalHit;

            //if (PhysicsRaycaster.Raycast(positionFrom, rayToProjectedPosition.normalized,
            //        out rayToProjectedPositionToEntrancePortalHit, rayToProjectedPosition.magnitude,
            //        Layers.portalsLayerMask 
            //        |
            //        Layers.generalEnvironmentLayersMask
            //        ))
            //{
            //    if (rayToProjectedPositionToEntrancePortalHit.collider.gameObject.layer == Layers.portalsLayerIndex)
            //    {
            //        //Vector3 hitPointOnEntrancePortalWorldSpace = rayToProjectedPositionToEntrancePortalHit.point;
            //        //var m = this.transform.worldToLocalMatrix * linkedPortal.transform.localToWorldMatrix;
            //        //Vector3 hitPointOnExitPortalWorldSpace = m * hitPointOnEntrancePortalWorldSpace;
            //        return (
            //            projectedPosition,
            //            Vector3.Distance(positionFrom, projectedPosition)
            //        );
            //    } else
            //    {
            //        return (
            //            projectedPosition,
            //            Mathf.Infinity
            //        );
            //    }
            //} else
            //{
            //    return (
            //        projectedPosition,
            //        Mathf.Infinity
            //    );
            //}

            //if (PhysicsRaycaster.Raycast(
            //    positionFrom, rayToProjectedPosition, rayToProjectedPosition.magnitude, Layers.portalsLayerMask, debugColor: Color.green, debug: true))
            //{
            //return (
            //        projectedPosition,
            //        rayToProjectedPosition.magnitude);
            //} else
            //{
            //    return (
            //        projectedPosition,
            //        Mathf.Infinity
            //    );
            //}

            
        }

        public Collider GetPortalCollider()
        {
            return GetComponentInChildren<Collider>();
        }
    }
}
