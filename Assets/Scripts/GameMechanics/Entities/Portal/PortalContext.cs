﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Portal.Handles;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalContext : NewEntityContext
    {
        public PortalRulesTransitions portalRulesTransitions;
        public UnityEngine.Camera portalCamera;
        public UnityEngine.Camera playerCamera;
        public PortalScreenHandle portalScreenHandle;
        public RenderTexture renderTexture;
        public PortalRenderingBehaviour renderingBehaviour;
    }
}
