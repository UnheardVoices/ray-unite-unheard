﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Portal.Handles;
using Assets.Scripts.GameMechanics.Entities.Portal.Params;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules;
using Assets.Scripts.GameMechanics.Entities.Portal.Rules.Transitions;
using Assets.Scripts.PlayerMechanics.Camera;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Portal
{
    public class PortalController : NewEntityController<PortalBehaviourRule, PortalContext>
    {
        public PortalParams portalParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            var kinematics = new Kinematics();

            var portalCamera = GetComponentInChildren<UnityEngine.Camera>();
            portalCamera.transform.SetParent(this.transform, false);
            portalCamera.enabled = false;

            // GetComponentInChildren<Renderer>().enabled = false;            

            this.controllerContext = new()
            {
                kinematics = kinematics,
                subject = this.transform,
                portalRulesTransitions = new PortalRulesTransitions(this),
                portalCamera = portalCamera,
                playerCamera = FindObjectOfType<CameraActorHandle>().GetComponent<UnityEngine.Camera>(),
                portalScreenHandle = new PortalScreenHandle(GetComponentInChildren<Renderer>(), GetComponentInChildren<Renderer>().material),
                renderingBehaviour = GetComponent<PortalRenderingBehaviour>()
            };
            this.controllerContext.portalRulesTransitions.EnterPortalOn();
        }
    }
}
