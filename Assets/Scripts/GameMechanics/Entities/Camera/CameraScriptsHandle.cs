﻿using Assets.Scripts.PlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Camera
{
    public class CameraScriptsHandle : MonoBehaviour
    {
        protected PlayerMovementMetrics playerMovementMetrics;

        public Vector3 cameraOrientedForward => this.playerMovementMetrics != null ?
                    this.playerMovementMetrics.forwardDirection : Vector3.zero;

        private void Start()
        {
            this.playerMovementMetrics =
                FindObjectOfType<PlayerMovementMetrics>();
        }
    }
}
