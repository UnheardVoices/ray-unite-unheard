﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitTimeoutSwitchOnParams
    {
        public float stateTimeoutSeconds = 1f;
        public Texture switchOnLookTexture;
        public SwitchStateBehaviour onBehaviour;
    }
}
