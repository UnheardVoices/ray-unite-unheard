﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitTimeoutSwitchParams
    {
        public OneHitTimeoutSwitchOffParams offParams = new();
        public OneHitTimeoutSwitchOnParams onParams = new();
    }
}
