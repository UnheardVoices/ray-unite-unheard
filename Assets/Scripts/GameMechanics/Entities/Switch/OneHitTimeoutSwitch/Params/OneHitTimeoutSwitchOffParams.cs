﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params
{
    [Serializable]
    public class OneHitTimeoutSwitchOffParams
    {
        public Texture switchOffLookTexture;
        public SwitchStateBehaviour offBehaviour;
    }
}
