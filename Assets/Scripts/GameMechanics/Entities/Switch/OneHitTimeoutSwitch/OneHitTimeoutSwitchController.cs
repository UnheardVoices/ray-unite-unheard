﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Aspects;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Handles;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch
{
    public class OneHitTimeoutSwitchController : 
        NewEntityController<OneHitTimeoutSwitchBehaviourRule, OneHitTimeoutSwitchContext>
    {
        public OneHitTimeoutSwitchParams switchParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            var kinematics = new Kinematics();

            this.controllerContext = new() {
                subject = this.transform,
                kinematics = kinematics,
                switchRulesTransitions = new OneHitTimeoutSwitchRulesTransitions(this),
                switchParams = this.switchParams,
                switchLookHandles = 
                    new OneHitTimeoutSwitchLookHandles(
                        switchTexture: new OneHitTimeoutSwitchTextureHandle(
                            switchController: this,
                            material: GetComponent<Renderer>().material
                        )    
                    ),
                targetableProperties = GetComponent<TargetableProperties>()
            };
            this.controllerContext.switchRulesTransitions.EnterOff();
        }
    }
}
