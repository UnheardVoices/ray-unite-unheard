﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Aspects;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Handles;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Params;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch
{
    public class OneHitTimeoutSwitchContext : NewEntityContext
    {
        public OneHitTimeoutSwitchRulesTransitions switchRulesTransitions;
        public OneHitTimeoutSwitchParams switchParams;
        public OneHitTimeoutSwitchLookHandles switchLookHandles;

        public TargetableProperties targetableProperties;
    }
}
