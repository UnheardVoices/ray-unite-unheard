﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Rules.Aspects;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules;
using Assets.Scripts.PlayerMechanics.Shooting;
using Assets.Scripts.PlayerMechanics.Targeting;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Targeting
{
    public class OneHitTimeoutSwitchHitHdlBeh : TargetHitHandlingBeh
    {
        protected OneHitTimeoutSwitchController switchController;

        protected override void BehaviourStart()
        {
            this.switchController = GetComponent<OneHitTimeoutSwitchController>();
        }

        public override void OnTargetHit(
           HandProjectileBehaviour handProjectileBehaviour)
        {
            string currentRule =
                this.switchController.GetCurrentRuleClassName();

            if (currentRule.Equals(typeof(OneHitTimeoutSwitchOffRule).Name))
            {
                this.switchController.switchParams.offParams.offBehaviour?.SwitchStateLeave();
                this.switchController.controllerContext.switchRulesTransitions.EnterOn();
            }
        }
    }
}
