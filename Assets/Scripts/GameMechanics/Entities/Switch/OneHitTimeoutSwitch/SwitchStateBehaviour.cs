﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch
{
    public abstract class SwitchStateBehaviour : MonoBehaviour
    {
        public virtual void SwitchStateEnter() { }
        public virtual void SwitchStateFixedUpdate() { }
        public virtual void SwitchStateUpdate() { }
        public virtual void SwitchStateLeave() { }
    }
}
