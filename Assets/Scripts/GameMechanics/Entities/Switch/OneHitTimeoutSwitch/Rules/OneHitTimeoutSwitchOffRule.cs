﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules
{
    public static class OneHitTimeoutSwitchOffRuleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public class OneHitTimeoutSwitchOffRule : OneHitTimeoutSwitchBehaviourRule
    {
        public const string RULE_ID = "OFF_PLACEHOLDER";

        public OneHitTimeoutSwitchOffRule(object controller)
            : base(RULE_ID, controller)
        {}

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<OffContext>();

            UseEffect(OneHitTimeoutSwitchOffRuleEffects .INIT_EFFECT, () =>
            {
                args.switchLookHandles.switchTexture.SetOffLook();
                args.switchParams.offParams.offBehaviour?.SwitchStateEnter();
                args.targetableProperties.drawShootSight = true;
            }, Tuple.Create(0));

            args.switchParams.offParams.offBehaviour?.SwitchStateFixedUpdate();
        }
    }
}
