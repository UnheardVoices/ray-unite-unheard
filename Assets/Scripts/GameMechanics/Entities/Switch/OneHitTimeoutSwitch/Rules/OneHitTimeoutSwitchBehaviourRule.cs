﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules
{
    public class OneHitTimeoutSwitchBehaviourRule : NewEntityRule<OneHitTimeoutSwitchContext>
    {
        public OneHitTimeoutSwitchBehaviourRule(string ruleId, object controller)
            : base(ruleId, controller) { }

        protected override OneHitTimeoutSwitchContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected OneHitTimeoutSwitchController GetController()
        {
            return GetCast<OneHitTimeoutSwitchController>(this.entityController);
        }
    }
}
