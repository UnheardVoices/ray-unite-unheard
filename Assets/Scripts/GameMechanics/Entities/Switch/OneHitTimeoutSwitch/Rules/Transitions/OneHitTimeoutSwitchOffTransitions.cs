﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions
{
    public class OneHitTimeoutSwitchOffTransitions
    {
        public static OffContext GetOffContext(OffContext savedContext)
        {
            return savedContext;
        }
    }
}
