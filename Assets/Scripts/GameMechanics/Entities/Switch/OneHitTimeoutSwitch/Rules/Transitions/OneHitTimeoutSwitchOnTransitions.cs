﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions
{
    public class OneHitTimeoutSwitchOnTransitions
    {
        public static OnContext GetOnContext(OnContext savedContext)
        {
            return savedContext;
        }
    }
}
