﻿using Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Contexts;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Rules.Transitions
{
    public class OneHitTimeoutSwitchRulesTransitions
    {
        protected OneHitTimeoutSwitchController switchController;

        public OneHitTimeoutSwitchRulesTransitions(OneHitTimeoutSwitchController switchController)
        {
            this.switchController = switchController;
        }

        public void EnterOff()
        {
            var savedContext = this.switchController
                .controllerContext.contextsHolder.UseContext<OffContext>(
                OneHitTimeoutSwitchOffRule.RULE_ID);

            this.switchController.controllerContext.contextsHolder.
                SetContext(OneHitTimeoutSwitchOffRule.RULE_ID,
                    OneHitTimeoutSwitchOffTransitions.GetOffContext(savedContext));
            this.switchController.SetCurrentRule(
                new OneHitTimeoutSwitchOffRule(this.switchController));
        }

        public void EnterOn()
        {
            var savedContext = this.switchController
                .controllerContext.contextsHolder.UseContext<OnContext>(
                OneHitTimeoutSwitchOnRule.RULE_ID);

            this.switchController.controllerContext.contextsHolder.
                SetContext(OneHitTimeoutSwitchOnRule.RULE_ID,
                    OneHitTimeoutSwitchOnTransitions.GetOnContext(savedContext));
            this.switchController.SetCurrentRule(
                new OneHitTimeoutSwitchOnRule(this.switchController));
        }
    }
}
