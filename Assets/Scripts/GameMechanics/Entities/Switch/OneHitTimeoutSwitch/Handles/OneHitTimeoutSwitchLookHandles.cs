﻿namespace Assets.Scripts.GameMechanics.Entities.Switch.OneHitTimeoutSwitch.Handles
{
    public class OneHitTimeoutSwitchLookHandles
    {
        public OneHitTimeoutSwitchTextureHandle switchTexture;

        public OneHitTimeoutSwitchLookHandles(
            OneHitTimeoutSwitchTextureHandle switchTexture)
        {
            this.switchTexture = switchTexture;
        }
    }
}
