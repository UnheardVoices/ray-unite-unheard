﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Params
{
    [Serializable]
    public class NewWatcherProjectileParams
    {
        public NewWatcherProjectileFlyingParams flyingParams = new();
    }
}
