﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Components
{
    public static class ProjectileRayColliderRules
    {
        public static Tuple<bool, GameObject> IsHittingByRaysSphereAround(
            int layersCount,
            int raysInLayer,
            float radius,
            Vector3 origin,
            out RaycastHit hit,
            bool debug = false,
            int layerMask = Physics.DefaultRaycastLayers
            )
        {
            if (layersCount < 3)
            {
                throw new InvalidOperationException(
                    "Cannot be less layers than 3 for projectile" +
                    " spherical ray collision detection!");
            }

            if (raysInLayer > 0)
            {
                for (int i = 0; i < layersCount; i++)
                {
                    float verticalAngleForLayerDegrees = 90.0f - (180.0f / (layersCount - 1)) * i;
                    for (int j = 0; j < raysInLayer; j++)
                    {
                        float horizontalAngleInLayerDegrees = (360f / raysInLayer) * j;

                        Vector3 rayDirectionVector =
                            (Quaternion.AngleAxis(verticalAngleForLayerDegrees, Vector3.right) *
                            Quaternion.AngleAxis(horizontalAngleInLayerDegrees, Vector3.up) *
                            Vector3.forward).normalized;

                        bool hitWithRay =
                            PhysicsRaycaster.Raycast(
                                origin, rayDirectionVector, out hit, radius,
                                layerMask: layerMask, debug: debug);

                        if (hitWithRay)
                        {
                            return Tuple.Create(true, hit.collider.gameObject);
                        }
                    }
                }
            }

            hit = new RaycastHit();
            return Tuple.Create<bool, GameObject>(false, null);
        }
    }

    public class ProjectileRayCollider
    {
        protected Transform projectile;

        public ProjectileRayCollider(Transform projectile)
        {
            this.projectile = projectile;
        }

        public Tuple<bool, GameObject> IsHittingEnvironment()
        {
            const int colliderLayersCount = 10;
            const int colliderRaysInLayer = 10;
            const float colliderRadius = 1f;
            const bool debug = true;

            RaycastHit hit = new RaycastHit();

            return ProjectileRayColliderRules.IsHittingByRaysSphereAround(
                colliderLayersCount,
                colliderRaysInLayer,
                colliderRadius,
                this.projectile.position,
                out hit,
                debug,
                Layers.generalEnvironmentLayersMask
                );
        }
    }
}
