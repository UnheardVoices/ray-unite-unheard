﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Components;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules.Transitions;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile
{
    public class NewWatcherProjectileContext : NewEntityContext
    {
        public NewWatcherProjectileRulesTransitions watcherProjectileRulesTransitions;
        public NewWatcherProjectileParams watcherProjectileParams;
        public ProjectileRayCollider projectileRayCollider;
        public AudioSource flyingAudioSource;
    }
}
