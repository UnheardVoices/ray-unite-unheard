﻿using Assets.Scripts.GameMechanics.Destructible;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Contexts;
using Assets.Scripts.Utils.Spawners;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules
{
    public static class NewWatcherProjectileMovementFlyingRuleEffects
    {
        public const string INIT_EFFECT = "INIT_EFFECT";
    }

    public static class NewWatcherProjectileMovementFlyingRuleState
    {
        public const string INITIALIZED = "INITIALIZED";
    }

    public class NewWatcherProjectileMovementFlyingRule : NewWatcherProjectileMovementRule
    {
        public const string RULE_ID = "FLYING_PLACEHOLDER";

        public NewWatcherProjectileMovementFlyingRule(object controller)
            : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<FlyingContext>();

            Vector3 startPosition =
                args.watcherProjectileParams
                    .flyingParams.startPosition;

            Vector3 destination = 
                args.watcherProjectileParams
                    .flyingParams.destination;

            (bool initialized, Action<bool> SetInitialized) =
                UseState(NewWatcherProjectileMovementFlyingRuleState.INITIALIZED, false);

            UseEffect(
                NewWatcherProjectileMovementFlyingRuleEffects.INIT_EFFECT,
                () =>
                {
                    if (!initialized)
                    {
                        args.kinematics.velocity =
                            (destination - startPosition).normalized
                            * args.watcherProjectileParams.flyingParams.maxSpeed;

                        if (args.watcherProjectileParams.flyingParams
                            .flyingAudioClipWithAudioSourceParams != null)
                        {
                            args.flyingAudioSource.loop = true;
                            args.flyingAudioSource.clip =
                                args.watcherProjectileParams.flyingParams
                                .flyingAudioClipWithAudioSourceParams.clip;
                            args.flyingAudioSource.volume = 
                                args.watcherProjectileParams.flyingParams
                                .flyingAudioClipWithAudioSourceParams.volume;
                            args.flyingAudioSource.maxDistance =
                                args.watcherProjectileParams.flyingParams
                                .flyingAudioClipWithAudioSourceParams.maxDistance;
                            args.flyingAudioSource.pitch =
                                args.watcherProjectileParams.flyingParams
                                .flyingAudioClipWithAudioSourceParams.pitch;
                            args.flyingAudioSource.Play();
                        }                      

                        SetInitialized(true);
                    }                   
                }, Tuple.Create(0));

            var isHittingEnvironmentInfo = args.projectileRayCollider.IsHittingEnvironment();

            if (isHittingEnvironmentInfo.Item1)
            {
                ExplosionSpawner.SpawnExplosion(
                    args.subject.position,
                    args.watcherProjectileParams.flyingParams.explosionPrefab);

                var potentialDestructibleHandlingBehaviour
                    = isHittingEnvironmentInfo.Item2.GetComponent<DestructibleHitHandlingBeh>();

                var explosionAttributes =
                    args.watcherProjectileParams
                    .flyingParams.explosionParams.GetExplosionAttributes();

                foreach (Rigidbody part in UnityEngine.Object.FindObjectsOfType<Rigidbody>())
                {
                    part.AddExplosionForce(
                        explosionForce: explosionAttributes.explosionForce,
                        args.subject.position,
                        explosionRadius: explosionAttributes.explosionRadius);
                }

                if (potentialDestructibleHandlingBehaviour != null)
                {
                    potentialDestructibleHandlingBehaviour.OnHit(
                        args.subject.gameObject);
                }

                if (args.watcherProjectileParams.flyingParams
                    .explosionAudioClipWithAudioSourceParams != null)
                {
                    var newAudioSource =
                        new GameObject().AddComponent<AudioSource>();
                    newAudioSource.transform.position = args.subject.position;
                    newAudioSource.pitch = 
                        args.watcherProjectileParams.flyingParams
                            .explosionAudioClipWithAudioSourceParams.pitch;
                    newAudioSource.volume =
                        args.watcherProjectileParams.flyingParams
                            .explosionAudioClipWithAudioSourceParams.volume;
                    newAudioSource.clip =
                        args.watcherProjectileParams.flyingParams
                            .explosionAudioClipWithAudioSourceParams.clip;
                    newAudioSource.Play();
                    newAudioSource.gameObject.AddComponent<AudioSourceDestroyAfterClipFinish>();
                }
                UnityEngine.Object.Destroy(args.subject.gameObject);
            }
        }
    }
}
