﻿using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Params;
using Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile.Rules;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcherProjectile
{
    public class NewWatcherProjectileController : 
        NewEntityController<NewWatcherProjectileMovementRule, NewWatcherProjectileContext>
    {
        public NewWatcherProjectileParams watcherProjectileParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            var kinematics = new Kinematics();

            AudioSource flyingAudioSource = this.gameObject.AddComponent<AudioSource>();
            flyingAudioSource.rolloffMode = AudioRolloffMode.Linear;
            flyingAudioSource.spatialBlend = 1.0f;

            this.controllerContext = new()
            {
                subject = this.transform,
                kinematics = kinematics,
                watcherProjectileRulesTransitions = new(this),
                watcherProjectileParams = this.watcherProjectileParams,
                projectileRayCollider = new Components.ProjectileRayCollider(this.transform),
                flyingAudioSource = flyingAudioSource
            };
            this.controllerContext
                .watcherProjectileRulesTransitions.EnterFlying();
        }

        protected override void GameplayFixedUpdate()
        {
            base.GameplayFixedUpdate();
            this.controllerContext.kinematics
                .GameplayFixedUpdateSubject(this.transform);
        }
    }
}
