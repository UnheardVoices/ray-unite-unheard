using Assets.Scripts.GameMechanics.Entities.NewCamera.Rules;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewCamera
{
    public enum CameraHeight
    {
        FollowTargetHeight,
        OffsetOverGround,
        Mix
    }

    public class NewCameraController : NewEntityController<NewCameraRule, NewCameraContext>
    {
        [Serializable]
        public class BaseCameraParameters
        {
            public Transform target;

            public Transform pivotYaw;
            public Transform pivotPitch;

            [Range(0f, 1f)]
            public float mainCameraSmoothing;

            public Vector3 offset;
        }

        [Serializable]
        public class CameraPositionParameters
        {
            [Header("Height")]
            public CameraHeight heightType;
            public float height;

            [Range(0f, 1f)]
            public float heightSmoothing;

            [Header("Distance")]
            //public float minimumDistance;
            //public float maximumDistance;
            public float distance;

            [Range(0f, 1f)]
            public float distanceSmoothing;
        }

        [Serializable]
        public class CameraRotationParameters
        {
            public float cameraLockX;

            [Range(0f, 1f)]
            public float yawSmoothing;

            [Range(0f, 1f)]
            public float pitchSmoothing;

            [Range(0f, 1f)]
            public float lookAtTargetInterpolation;
        }

        public BaseCameraParameters parameters = new();
        public CameraPositionParameters positionParameters = new();
        public CameraRotationParameters rotationParameters = new();

        protected override void BehaviourStart()
        {
            this.controllerContext = new();
            //this.controllerContext.parameters = this.parameters;
            this.controllerContext.subject = this.transform;

            this.controllerContext.baseParameters = this.parameters;
            this.controllerContext.positionParameters = this.positionParameters;
            this.controllerContext.rotationParameters = this.rotationParameters;

            base.BehaviourStart();
            this.currentRule = (new NewCameraFollowRule(this));
        }
    }
}