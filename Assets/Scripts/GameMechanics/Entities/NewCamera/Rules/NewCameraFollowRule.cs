using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewCamera.Rules
{
    public class NewCameraFollowRule : NewCameraRule
    {
        public const string RULE_ID = "CAMERA_FOLLOW_RULE_PLACEHOLDER";

        public NewCameraFollowRule(object controller) : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            base.GameplayFixedUpdate();

            var Args = UseEntityContext();

            //prymityw - totalny prymityw

            //Args.subject.position = Args.baseParameters.target.position + Args.baseParameters.offset;


            //float height = Args.subject.position.y;

            //Ray ray = new(Args.subject.position, Vector3.down);

            //if (Physics.Raycast(ray, out RaycastHit hit))
            //{
            //    var targetHeight = hit.point.y + Args.positionParameters.height;

            //    height = Mathf.Lerp(Args.subject.position.y, targetHeight, Args.positionParameters.heightSmoothing);
            //}

            var direction = (Args.subject.position - Args.baseParameters.target.position).normalized;

            var posSub = (Args.subject.position - Args.baseParameters.target.position);

            var targetDistance = Args.positionParameters.distance - posSub.magnitude;

            var distance = Vector3.Lerp(Args.subject.position, Args.subject.position + direction * targetDistance, Args.positionParameters.distanceSmoothing);


            Args.subject.position = distance;

            var heightVector = Args.subject.position;
            heightVector.y = Args.baseParameters.target.position.y + Args.positionParameters.height;

            Args.subject.position = Vector3.Lerp(Args.subject.position, heightVector, Args.positionParameters.heightSmoothing);

            //Args.subject.position = Vector3.Lerp(Args.subject.position, heightVector, Args.positionParameters.distanceSmoothing);

            var rotation = Quaternion.LookRotation(-direction);

            Args.subject.rotation = Quaternion.Lerp(Args.subject.rotation, rotation, Args.rotationParameters.lookAtTargetInterpolation);
        }
    }
}