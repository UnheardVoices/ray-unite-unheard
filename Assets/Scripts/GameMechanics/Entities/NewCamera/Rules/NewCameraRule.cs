using Assets.Scripts.GameMechanics.Entities.NewEntity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewCamera.Rules
{
    public class NewCameraRule : NewEntityRule<NewCameraContext>
    {
        public NewCameraRule(string ruleId, object controller) : base(ruleId, controller) { }

        protected override NewCameraContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected NewCameraController GetController()
        {
            return GetCast<NewCameraController>(this.entityController);
        }
    }
}