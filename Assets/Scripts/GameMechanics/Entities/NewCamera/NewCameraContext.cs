using Assets.Scripts.GameMechanics.Entities.NewEntity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Assets.Scripts.GameMechanics.Entities.NewCamera.NewCameraController;

namespace Assets.Scripts.GameMechanics.Entities.NewCamera
{
    public class NewCameraContext : NewEntityContext
    {
        public BaseCameraParameters baseParameters { get; set; }
        public CameraPositionParameters positionParameters { get; set; }
        public CameraRotationParameters rotationParameters { get; set; }
    }
}
