using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class WaterContext : IntentionalContext
    {
        public float intentionalVelocity;

        public float currentVelocity;

        public float currentDrag;

        public float time;
    }
}
