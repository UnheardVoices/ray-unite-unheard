using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class IntentionalContext : Context
    {
        /// <summary>
        /// Intetional movement direction from Input - legacy
        /// </summary>
        public Vector3 intentionalMovementDirection;

        /// <summary>
        /// Intentional movement with velocity applied. - legacy
        /// </summary>
        public Vector3 intentionalMovement;
        /// <summary>
        /// legacy
        /// </summary>
        public Vector3 intentionalAnimation;

        public VelocityIntentional velocityIntentional = new();
        public MovementIntentional movementIntentional = new();
        public AnimationIntentional animationIntentional = new();
    }

}