using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class LedgeContext : IntentionalContext
    {
        public Vector3 forwardToLedge;
        public Vector3 targetPosition;
        public Vector3 normalVector;

        public bool isGoingToStand;
        public float time = 0f;

        public LedgeContext()
        {
            this.isGoingToStand = false;
        }
    }
}
