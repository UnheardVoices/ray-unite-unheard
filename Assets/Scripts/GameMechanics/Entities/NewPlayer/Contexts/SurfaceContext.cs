using Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.Timer;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class SurfaceContext : IntentionalContext
    {
        public bool isAttachedToSurface = false;
        public float currentDrag;
        public SimpleTimer timer = new();
        public SimpleTimer transitionTimer = new(true);
        public float startDepth = -1.7f;

        public new MovementIntentional movementIntentional;

        public SurfaceContext()
        {
            this.movementIntentional = new(0.11f);
        }
    }
}
