using Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.Timer;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class SubmergedContext : IntentionalContext
    {
        //public float waterHeight;
        public bool transitionEnded = false;
        public float currentDrag;
        public SimpleTimer timer = new(true);
    }
}
