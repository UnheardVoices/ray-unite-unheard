using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals
{
    public class VelocityIntentional
    {
        public Vector3 velocity
        {
            get
            { 
                return this.magnitude * this.direction.normalized;
            }
            set
            { 
                this.magnitude = value.magnitude;
                this.direction = value.normalized;
            }
        }


        public Vector3 interpolatedVelocity;

        //private float magnitude = 0f;
        //private Vector3 direction = Vector3.zero;

        private float _magnitude;
        public float magnitude
        {
            get { return this._magnitude; }
            set
            { 
                this._magnitude = value;
                this.interpolatedMagnitude = Mathf.Lerp(this.interpolatedMagnitude,
                    value, this.interpolation);
            }
        }

        private Vector3 _direction;
        public Vector3 direction
        {
            get { return this._direction; }
            set
            { 
                this._direction = value.normalized;
                this.interpolatedDirection = Vector3.Lerp(this.interpolatedDirection,
                    value, this.interpolation);
            }
        }

        private float interpolatedMagnitude;
        private Vector3 interpolatedDirection;

        public float interpolation = 0.5f;


        public void ClampMagnitude(float delta, float min, float max)
        {
            this.magnitude = Mathf.Clamp(this.magnitude + delta, min, max);
        }

        //public void ApplyRaw(Vector3 vector)
        //{
        //    ApplyInterpolated(vector, 1f);
        //}
        //public void ApplyRaw(float magnitude)
        //{
        //    ApplyInterpolated(magnitude, 1f);
        //}

        //public void ApplyRaw(Vector3 vector, float magnitude)
        //{
        //    ApplyInterpolated(vector, magnitude, 1f);
        //}

        //public void ApplyInterpolated(Vector3 vector, float interpolation = 0.5f)
        //{
        //    Apply(vector.normalized, vector.magnitude, interpolation);
        //}

        //public void ApplyInterpolated(float magnitude, float interpolation = 0.5f)
        //{
        //    Apply(this.direction.normalized, magnitude, interpolation);
        //}

        //public void ApplyInterpolated(Vector3 direction, float magnitude, float interpolation = 0.5f)
        //{
        //    Apply(direction.normalized, magnitude, interpolation);
        //}


        //private void Apply(Vector3 direction, float magnitude, float interpolation = 0.5f)
        //{
        //    this.magnitude = Mathf.Lerp(this.magnitude, magnitude, interpolation);
        //    this.direction = Vector3.Lerp(this.direction, direction, interpolation);
        //}
    }
}
