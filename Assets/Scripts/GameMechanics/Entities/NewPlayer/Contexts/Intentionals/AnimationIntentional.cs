using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals
{
    public class AnimationIntentional
    {
        public Vector2 interpolatedVector =>
            this.interpolatedDirection * this.interpolatedMagnitude;

        public Vector2 vector
        {
            get 
            { 
                return this.magnitude * this.direction;
            }
            set
            { 
                this.magnitude = value.magnitude;
                this.direction = value.normalized;
            }
        }


        private float _magnitude;
        public float magnitude
        {
            get { return this._magnitude; }
            set
            { 
                this._magnitude = value;
                this.interpolatedMagnitude = Mathf
                    .Lerp(this.interpolatedMagnitude, value, this.intepolation);
            }
        }

        private Vector2 _direction;
        public Vector2 direction
        {
            get { return this._direction; }
            set
            { 
                this._direction = value.normalized;
                this.interpolatedDirection = Vector2
                    .Lerp(this.interpolatedDirection, value, this.intepolation);
            }
        }

        private float interpolatedMagnitude;
        private Vector2 interpolatedDirection;

        public float intepolation = 0.15f;
    }
}
