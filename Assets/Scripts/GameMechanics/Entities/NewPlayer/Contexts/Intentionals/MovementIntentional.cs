using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals
{
    public class MovementIntentional
    {
        public Vector3 movement => this._input * this.movementSpeed;
        public Vector3 interpolatedMovement => this.interpolatedInput * this.movementSpeed;

        private Vector3 _input;
        public Vector3 input
        {
            get { return this._input; }
            set
            { 
                this._input = value;
                this.interpolatedInput = Vector3
                    .Lerp(this.interpolatedInput, value, this.interpolation);
            }
        }

        private Vector3 interpolatedInput;

        public float movementSpeed = 0.21f;
        public float interpolation = 0.5f;

        public MovementIntentional(float movementSpeed = 0.21f)
        {
            this.movementSpeed = movementSpeed;
        }
    }
}
