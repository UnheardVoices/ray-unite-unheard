using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts
{
    public class AirContext : IntentionalContext
    {
        public bool IsHelicopter;
        public bool IsJumping;

        public float VerticalSpeed;
    }
}
