using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementWaterRule : NewPlayerMovementRule
    {
        public const string RULE_ID = "WATER_RULE_PLACEHOLDER";

        public NewPlayerMovementWaterRule(object controller) : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<WaterContext>();

            base.GameplayFixedUpdate();

            args.platformerCollisionEffects
                .OnWaterSurface((hit) =>
                {
                    SurfaceRule(hit);
                });

            args.platformerCollisionEffects
                .OnWaterSubmerged((hit) =>
                {
                    SubmergedRule();
                });

            args.kinematics.velocity = context.intentionalMovement;

            args.playerAnimationHub.SetupIntentionalAnimation(context.intentionalAnimation);
            args.playerAnimationHub.ManualFixedUpdate();

            context.time += Time.fixedDeltaTime;

            if (context.time > 1.3f)
            {
                context.time = 0f;
            }
        }

        private void SurfaceRule(RaycastHit waterHit)
        {
            const float surfaceVelocity = 0.11f;
            const float surfaceAcceleration = 0.005f;
            const float surfaceDrag = 0.012f;
            const float surfaceRotation = 15f;

            const float moveDrag = 0.003f;


            var args = UseEntityContext();
            var context = UseContext<WaterContext>();

            context.intentionalVelocity = args.playerInputHub.moveInput
                .interpolatedAnalogValue.magnitude * surfaceVelocity;

            #region VelocityControl

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, surfaceVelocity,
                (velocity, rotation) =>
                {
                    //args.subject.forward = args.subject.forward = Vector3.Lerp(args.subject.forward,
                    //    rotation, 0.25f);

                    if (context.intentionalVelocity > context.currentVelocity)
                    {
                        if (context.time > 0.83f)
                        {
                            context.currentVelocity = Mathf.Clamp(context.currentVelocity - moveDrag,
                                0f,
                                context.currentVelocity);
                        }
                        else
                        {
                            context.currentDrag = 0f;
                            context.currentVelocity = Mathf.Clamp(
                                context.currentVelocity + surfaceAcceleration,
                                context.currentVelocity,
                                context.intentionalVelocity);
                        }
                    }
                    else if (context.intentionalVelocity < context.currentVelocity)
                    {
                        context.currentDrag -= surfaceDrag;
                        context.currentVelocity = Mathf.Clamp(context.currentVelocity - context.currentDrag,
                            context.intentionalVelocity,
                            context.currentVelocity);
                    }

                    context.intentionalMovement = Vector3.Lerp(args.kinematics.velocity,
                        args.subject.forward * context.currentVelocity,
                        0.6f);

                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, surfaceVelocity,
                (velocity, rotation) =>
                {
                    context.currentDrag -= surfaceDrag;

                    context.currentVelocity = Mathf.Clamp(
                        context.currentVelocity - surfaceDrag, 0f, float.MaxValue);

                    context.intentionalMovement = Vector3.Lerp(args.kinematics.velocity,
                        context.currentVelocity * args.subject.transform.forward,
                        0.5f);

                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            #endregion

            #region DepthControl

            args.playerInputEffects
                .OnDepthControlBiggerOrEqualThan(0.1f, surfaceRotation,
                (rotation) =>
                {
                    if (waterHit.distance < 0.15f)
                    {
                        rotation = Mathf.Clamp(rotation, float.MinValue, 0f);
                        context.intentionalMovement.y = Mathf
                            .Clamp(context.intentionalMovement.y, float.MinValue, 0f);
                    }

                    args.subject.forward = new Vector3(
                        args.subject.forward.x,
                        rotation,
                        args.subject.forward.z).normalized;
                });

            args.playerInputEffects
                .OnDepthControlDeadZone(0.1f, () =>
                {
                    args.subject.forward = new Vector3(
                        args.subject.forward.x,
                        0f,
                        args.subject.forward.z).normalized;
                });

            #endregion

            args.playerAnimationHub.Diving(false);
        }

        private void SubmergedRule()
        {
            const float submergedVelocity = 0.21f;
            const float submergedAcceleration = 0.012f;
            const float submergedDrag = 0.004f;
            const float submergedRotation = 35f;

            var args = UseEntityContext();
            var context = UseContext<WaterContext>();

            context.intentionalVelocity = args.playerInputHub.moveInput
                .interpolatedAnalogValue.magnitude * submergedVelocity;

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, submergedVelocity,
                (velocity, rotation) =>
                {
                    //args.subject.forward = Vector3.Lerp(args.subject.forward,
                        //rotation, 0.25f);

                    if (context.intentionalVelocity > context.currentVelocity)
                    {
                        context.currentDrag = 0f;
                        context.currentVelocity = Mathf.Clamp(
                            context.currentVelocity + submergedAcceleration,
                            context.currentVelocity,
                            context.intentionalVelocity);
                    }
                    else if (context.intentionalVelocity < context.currentVelocity)
                    {
                        context.currentDrag -= submergedDrag;
                        context.currentVelocity = Mathf.Clamp(context.currentVelocity - context.currentDrag,
                            context.intentionalVelocity,
                            context.currentVelocity);
                    }

                    context.intentionalMovement = Vector3.Lerp(args.kinematics.velocity,
                        args.subject.forward * context.currentVelocity,
                        0.6f);

                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, submergedVelocity,
                (velocity, rotation) =>
                {
                    context.currentDrag -= submergedDrag;

                    context.currentVelocity = Mathf.Clamp(
                        context.currentVelocity - submergedDrag, 0f, float.MaxValue);

                    context.intentionalMovement = Vector3.Lerp(args.kinematics.velocity,
                        context.currentVelocity * args.subject.transform.forward,
                        0.5f);

                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            args.playerInputEffects
                .OnDepthControlBiggerOrEqualThan(0.1f, submergedRotation,
                (rotation) =>
                {
                    args.subject.forward = new Vector3(
                        args.subject.forward.x,
                        rotation,
                        args.subject.forward.z).normalized;
                });

            args.playerInputEffects
                .OnDepthControlDeadZone(0.1f, () =>
                {
                    args.subject.forward = new Vector3(
                        args.subject.forward.x,
                        0f,
                        args.subject.forward.z).normalized;
                });

            args.playerAnimationHub.Diving(true);
        }
    }
}
