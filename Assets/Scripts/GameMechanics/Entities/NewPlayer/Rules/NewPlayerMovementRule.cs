using Assets.Scripts.GameMechanics.Entities.NewEntity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementRule : NewEntityRule<NewPlayerContext>
    {
        public NewPlayerMovementRule(string ruleId, object controller) : base(ruleId, controller) { }

        //common logic between PlayerMovementRules

        #region ContextRegion

        protected override NewPlayerContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected NewPlayerController GetController()
        {
            return GetCast<NewPlayerController>(this.entityController);
        }

        #endregion
    }
}
