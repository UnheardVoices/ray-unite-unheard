using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementSubmergedRule : NewPlayerMovementRule
    {
        public const string RULE_ID = "SUBMERGED_RULE_PLACEHOLDER";

        public NewPlayerMovementSubmergedRule(object controller) : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<SubmergedContext>();
            var ruleParams = UseEntityContext().playerParams.submergedParams;
            var baseParams = args.playerParams.baseParams;

            var animIntent = context.animationIntentional;
            var velIntent = context.velocityIntentional;
            var moveIntent = context.movementIntentional;


            base.GameplayFixedUpdate();
            //RaymanAnimations

            const float animationDiveDuration = 0.79f;
            const float transitionHeight = 2f;

            const float submergedVelocity = 0.21f;
            const float submergedAcceleration = 0.012f;
            const float submergedDrag = 0.007f;
            const float submergedRotation = 35f;

            moveIntent.input = args.playerInputHub.moveInput.GetInterpolated();

            velIntent.direction = args.subject.forward;
            velIntent.magnitude = args.kinematics.velocity.magnitude;

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, submergedVelocity,
                (velocity, rotation) =>
                {
                    //args.subject.forward = Vector3.Lerp(args.subject.forward,
                        //rotation, 0.25f);

                    args.subject.rotation = Quaternion.Lerp(args.subject.rotation, rotation, baseParams.rotationInterpolation);

                    if (moveIntent.movement.magnitude >  velIntent.magnitude)
                    {
                        context.currentDrag = 0f;
                        velIntent.ClampMagnitude(submergedAcceleration,
                            velIntent.magnitude, moveIntent.movement.magnitude);
                    }
                    else if (moveIntent.movement.magnitude < velIntent.magnitude)
                    {
                        context.currentDrag += submergedDrag;

                        velIntent.ClampMagnitude(-context.currentDrag,
                            moveIntent.movement.magnitude, velIntent.magnitude);
                    }
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, submergedVelocity,
                (velocity, rotation) =>
                {
                    context.currentDrag += submergedDrag;
                    velIntent.ClampMagnitude(-submergedDrag, 0f, float.MaxValue);
                });

            args.playerInputEffects
                .OnDepthControlBiggerOrEqualThan(0.1f, submergedRotation,
                (rotation) =>
                {
                    if (context.transitionEnded &&
                    args.playerInputHub.moveInput.interpolatedAnalogValue.magnitude > 0.1f)
                    {
                        var forward = args.subject.forward;
                        forward.y = Mathf.Lerp(forward.y, rotation, 0.5f);
                        forward.Normalize();

                        args.subject.forward = forward;
                    }
                    else
                    {
                        args.subject.forward = new Vector3(
                            args.subject.forward.x,
                            0f,
                            args.subject.forward.z).normalized;
                    }
                });

            args.playerInputEffects
                .OnDepthControlDeadZone(0.1f, () =>
                {
                    args.subject.forward = new Vector3(
                        args.subject.forward.x,
                        0f,
                        args.subject.forward.z).normalized;
                });

            args.platformerCollisionEffects
                .OnWaterSurface((hit) =>
                {
                    if (context.transitionEnded)
                    {
                        args.playerRulesTransitions.EnterWaterSurface();
                    }
                });

            args.platformerCollisionEffects
                .OnWater((waterHit) =>
                {
                    context.timer.timerEffects
                        .OnTimerResumed((time) =>
                        {
                            var normalizedTime = time / animationDiveDuration;
                            var position = args.subject.position;
                            position.y = Mathf.Lerp(position.y, (waterHit.point.y - args.playerParams.surfaceParams.heightBuffor) - transitionHeight *
                                ruleParams.transitionCurve.Evaluate(normalizedTime), ruleParams.transitionInterpolation);

                            args.subject.position = Vector3.Lerp(args.subject.position, position, 0.5f);
                        });

                    context.timer.timerEffects
                        .OnTimeBiggerThan(ruleParams.animationDiveDuration, (time) =>
                        {
                            context.transitionEnded = true;
                            context.timer.TimerStop();
                        });
                });

            animIntent.vector = args.playerInputHub.moveInput.rawAnalogValue;

            args.playerAnimationHub.SetupIntentionalAnimation(animIntent);
            args.playerAnimationHub.ManualFixedUpdate();

            args.kinematics.velocity = velIntent.velocity;

            context.timer.TimerUpdate(Time.fixedDeltaTime);
        }
    }
}