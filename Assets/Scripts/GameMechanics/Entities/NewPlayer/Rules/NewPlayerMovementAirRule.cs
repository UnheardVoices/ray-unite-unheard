using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics;
using Assets.Scripts.NewPlayerMechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementAirRule : NewPlayerMovementRule
    {
        public const string RULE_ID = "AIR_RULE_PLACEHOLDER";
        
        float test;

        public NewPlayerMovementAirRule(object controller) : base(RULE_ID, controller) { this.test = 0.28f; }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<AirContext>();
            var baseParams = args.playerParams.baseParams;

            args.dynamicLedgeGenerator.DynamicGenerate();

            base.GameplayFixedUpdate();

            const float playerInAirSpeed = 0.21f;
            const float gravityAcceleration = 0.012f;
            const float limitFallSpeed = 0.3f;
            const float limitFallSpeedHelicopter = 0.05f;

            args.playerInputEffects
                .OnJumpButtonDown(() =>
                {
                    context.IsHelicopter = true;
                    args.playerAnimationHub.HelicopterSequence(true);
                });

            args.playerInputEffects
                .OnJumpButtonUp(() =>
                {
                    context.IsHelicopter = false;
                    args.playerAnimationHub.HelicopterSequence(false);
                });

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, (velocity, rotation) =>
                {
                    float movementSmoothingInterpolation = 0.25f;
                    //args.subject.forward = Vector3.Lerp(args.subject.forward,
                    //    rotation, 0.25f);

                    args.subject.rotation = Quaternion.Lerp(args.subject.rotation, rotation, baseParams.rotationInterpolation);
                    context.intentionalMovement = Vector3.Lerp(context.intentionalMovement,
                        velocity, movementSmoothingInterpolation);
                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, (velocity, rotation) =>
                {
                    context.intentionalAnimation = Vector3.Lerp(context.intentionalAnimation, Vector3.zero, 5.75f * Time.fixedDeltaTime);
                    context.intentionalMovement = Vector3.Lerp(context.intentionalMovement, Vector3.zero, 8.25f * Time.fixedDeltaTime);
                });

            if (context.IsHelicopter)
            {
                const float helicopterInterpolation = 0.8f;
                context.VerticalSpeed = Mathf.Lerp(context.VerticalSpeed, -limitFallSpeedHelicopter, helicopterInterpolation);
            }
            else
            {
                context.VerticalSpeed -= 0.012f;
                context.VerticalSpeed = Mathf.Clamp(context.VerticalSpeed, -0.3f, 0.28f);
            }
            
            //Debug.LogError(Context.VerticalSpeed);

            args.kinematics.velocity = context.intentionalMovement + Vector3.up * context.VerticalSpeed;

            args.platformerCollisionEffects
                .OnCollisionWithGroundOnFalling(args.kinematics, () =>
                {
                    //Object.Instantiate(args.playerParams.effectsParams.landingPuff, args.subject.position, Quaternion.identity);
                    args.playerRulesTransitions.EnterGround(context.intentionalMovement);
                });

            args.platformerCollisionEffects
                .OnCollisionLedgeGrab((ledgeCollider, ledgeHit) =>
                {
                    args.playerRulesTransitions.EnterLedgeGrab(ledgeCollider);
                });

            args.platformerCollisionEffects
                .OnUnderwaterEnter(() =>
                {
                    //args.playerRulesTransitions.EnterWater();
                    args.playerRulesTransitions.EnterWaterSurface();
                });

            args.playerAnimationHub.SetupIntentionalAnimation(context.intentionalAnimation);
            args.playerAnimationHub.ManualFixedUpdate();
        }
    }
}
