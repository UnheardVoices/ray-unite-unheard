using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementGroundRule : NewPlayerMovementRule
    {
        public const string RULE_ID = "GROUND_RULE_PLACEHOLDER";

        public NewPlayerMovementGroundRule(object entityController) : base(RULE_ID, entityController) { }

        public override void GameplayFixedUpdate()
        {
            const float playerRunningSpeed = 0.21f;
            var args = UseEntityContext();
            var context = UseContext<IntentionalContext>();
            var baseParams = args.playerParams.baseParams;

            base.GameplayFixedUpdate();

            #region Input

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, (velocity, rotation) =>
                {
                    float movementSmoothingInterpolation = 0.25f;
                    //args.subject.forward = Vector3.Lerp(args.subject.forward,
                    //    rotation, 0.25f);
                    //context.intentionalMovement = rotation * args.playerInputHub.moveInput.GetInterpolated().magnitude * 0.21f;
                    args.subject.rotation = Quaternion.Lerp(args.subject.rotation, rotation, baseParams.rotationInterpolation);
                    context.intentionalMovement = Vector3.Lerp(context.intentionalMovement,
                        velocity, movementSmoothingInterpolation);
                    context.intentionalAnimation = args.playerInputHub.moveInput.interpolatedAnalogValue;
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, (velocity, rotation) =>
                {
                    context.intentionalAnimation = Vector3.Lerp(context.intentionalAnimation, Vector3.zero, 6f * Time.fixedDeltaTime);
                    context.intentionalMovement = Vector3.Lerp(context.intentionalMovement, Vector3.zero, 8.5f * Time.fixedDeltaTime);
                });

            //Args.playerInputEffects
            //    .OnStrafeButtonDown(() =>
            //    {
            //        //go to strafe transition
            //        Debug.Log("Go to strafe");
            //    });

            args.playerInputEffects
                .OnJumpButtonDown(() =>
                {
                    if (GroundCollider.IsHittingGround(args.subject))
                    {
                        args.playerRulesTransitions.EnterJump(context.intentionalMovement);
                    }
                });

            #endregion

            #region Collisions

            GroundCollider.AlignOnTopOfTheGround(args.subject);

            //Args.platformerCollisionEffects
            //    .OnCollisionWithWall(Context, Args.playerInputHub.moveInput.GetInterpolated(),
            //    (movementVelocity) =>
            //    {
            //        Context.intentionalMovement = movementVelocity;
            //    });

            args.platformerCollisionEffects
                .OnCollisionWithMultiWalls(context, args.playerInputHub.moveInput.GetInterpolated(),
                (movementVelocity) =>
                {
                    context.intentionalMovement = movementVelocity;
                });

            args.platformerCollisionEffects
                .OnCollisionWithGround(() =>
                {
                    //Args.playerAnimationHub.GroundSequence();
                });

            args.platformerCollisionEffects
                .OnNoCollisionWithGround(() =>
                {
                    args.playerRulesTransitions.EnterFreeFall(context.intentionalMovement);
                });
            #endregion

            args.kinematics.velocity = context.intentionalMovement;
            args.playerAnimationHub
                .SetupIntentionalAnimation(context.intentionalAnimation);
            args.playerAnimationHub.ManualFixedUpdate();
        }
    }
}