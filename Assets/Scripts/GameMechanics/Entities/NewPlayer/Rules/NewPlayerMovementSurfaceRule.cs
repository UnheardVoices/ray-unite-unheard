using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering.HighDefinition;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementSurfaceRule : NewPlayerMovementRule
    {

        public const string RULE_ID = "SURFACE_RULE_PLACEHOLDER";

        public NewPlayerMovementSurfaceRule(object controller) : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            const float surfaceAttachSpeed = 0.05f;

            const float surfaceVelocity = 0.11f;
            const float surfaceAcceleration = 0.005f;
            const float surfaceDrag = 0.012f;
            const float moveDrag = 0.0003f;

            var args = UseEntityContext();
            var ruleParams = UseEntityContext().playerParams.surfaceParams;
            var context = UseContext<SurfaceContext>();
            var baseParams = args.playerParams.baseParams;

            var animIntent = context.animationIntentional;
            var velIntent = context.velocityIntentional;
            var moveIntent = context.movementIntentional;

            base.GameplayFixedUpdate();

            Vector3 position = args.subject.position;
            moveIntent.input = args.playerInputHub.moveInput.GetInterpolated();

            velIntent.direction = args.subject.forward;
            velIntent.magnitude = args.kinematics.velocity.magnitude;

            args.platformerCollisionEffects
                .OnWater((waterHit) =>
                {
                    context.transitionTimer.timerEffects
                        .OnTimerResumed((time) =>
                        {
                            var normalizedTime = time / ruleParams.transitionDuration;
                            var height = ((waterHit.point.y - ruleParams.heightBuffor) - context.startDepth) *
                                ruleParams.transitionCurve.Evaluate(normalizedTime);

                            position.y = Mathf.Lerp(position.y,
                                context.startDepth + height,
                                ruleParams.transitionInterpolation);

                            var forward = args.subject.forward;
                            forward.y = Mathf.Lerp(forward.y,
                                ruleParams.transitionRotation * ruleParams.rotationCurve.Evaluate(normalizedTime),
                                0.5f);
                            forward.Normalize();

                            args.subject.forward = forward;
                        });

                    context.transitionTimer.timerEffects
                        .OnTimeBiggerThan(ruleParams.transitionDuration, (time) =>
                        {
                            context.transitionTimer.TimerStop();
                            context.isAttachedToSurface = true;
                        });

                    if (context.isAttachedToSurface)
                        position.y = Mathf.Lerp(position.y, waterHit.point.y - ruleParams.heightBuffor,
                            ruleParams.transitionInterpolation);

                    args.subject.position = Vector3.Lerp(args.subject.position, position, 0.5f);
                });

            args.playerInputEffects
                .OnMoveMagnitudeBiggerOrEqualThan(0.1f, surfaceVelocity,
                (velocity, rotation) =>
                {
                    //args.subject.forward = Vector3.Lerp(args.subject.forward,
                    //    rotation, 0.25f);

                    args.subject.rotation = Quaternion.Lerp(args.subject.rotation, rotation, baseParams.rotationInterpolation);

                    if (moveIntent.movement.magnitude > velIntent.magnitude)
                    {
                        if (context.timer.currentTime > 0.83f)
                        {
                            velIntent.ClampMagnitude(-moveDrag,
                                0f, velIntent.magnitude);
                        }
                        else
                        {
                            context.currentDrag = 0f;
                            velIntent.ClampMagnitude(surfaceAcceleration,
                                velIntent.magnitude,
                                moveIntent.movement.magnitude);
                        }
                    }
                    else if (moveIntent.movement.magnitude < velIntent.magnitude)
                    {
                        context.currentDrag += surfaceDrag;
                        velIntent.ClampMagnitude(-context.currentDrag,
                            moveIntent.movement.magnitude, velIntent.magnitude);
                    }
                });

            args.playerInputEffects
                .OnMoveMagnitudeLowerThan(0.1f, surfaceVelocity,
                (velocity, rotation) =>
                {
                    context.currentDrag += surfaceDrag;

                    velIntent.magnitude = Mathf.Clamp(velIntent.magnitude - surfaceDrag,
                        0f, float.MaxValue);

                });

            args.playerInputEffects
                .OnDepthControlBiggerOrEqualThan(0.1f, 20f,
                (rotation) =>
                {
                    if (rotation < 0f)
                    {
                        //enter submerged before that check collisions
                        args.playerRulesTransitions.EnterWaterSubmerged();
                    }
                    else
                    {
                        if (context.isAttachedToSurface)
                        {
                            args.subject.forward = new Vector3(args.subject.forward.x,
                                0f, args.subject.forward.z).normalized;
                        }
                    }
                });

            args.kinematics.velocity = velIntent.velocity;

            animIntent.vector = args.playerInputHub.moveInput.rawAnalogValue;

            args.playerAnimationHub.SetupIntentionalAnimation(animIntent);
            args.playerAnimationHub.ManualFixedUpdate();

            context.timer.TimerUpdate(Time.fixedDeltaTime);
            context.transitionTimer.TimerUpdate(Time.fixedDeltaTime);

            context.timer.timerEffects
                .OnTimerStoppedOrPaused((time) =>
                {
                    context.timer.TimerPlayResume();
                });

            context.timer.timerEffects
                .OnTimeBiggerThan(1.3f, (time) =>
                {
                    context.timer.TimerReset();
                });
        }
    }
}
