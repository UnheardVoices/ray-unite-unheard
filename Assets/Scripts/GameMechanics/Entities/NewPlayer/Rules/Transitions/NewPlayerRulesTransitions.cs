using Assets.Scripts.Animations;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Runtime.CompilerServices;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public static class EntityRulesTransitionsValidator
    {
        public static void ValidateRule<T, U>(
            NewEntityController<T, U> entityController, Type ruleType)
            where T : NewEntityRule<U>
            where U : NewEntityContext, new()
        {
            if (!entityController.GetCurrentRuleClassName()
                    .Equals(ruleType.Name))
            {
                throw new InvalidOperationException(
                    "Invalid rules transition access from " +
                        entityController.GetCurrentRuleClassName()
                        + " that is for " + ruleType.Name
                    );
            }
        }
    }

    public class NewPlayerRulesTransitions
    {
        protected NewPlayerController playerController;

        protected NewPlayerGroundTransitions groundTransitions;
        protected NewPlayerAirTransitions airTransitions;

        protected void ValidateRule(Type ruleType)
        {
            EntityRulesTransitionsValidator
                .ValidateRule(
                    this.playerController, ruleType);
        }

        public NewPlayerRulesTransitions(
            NewPlayerController playerController)
        {
            this.playerController = playerController;
        }

        public void EnterFreeFall(Vector3 initialIntentionalMovement)
        {
            PrepareTransition();

            //animation
            this.playerController.controllerContext.playerAnimationHub.LedgeGrabSequence(false);
            this.playerController.controllerContext.playerAnimationHub.FreeFallSequence();

            //context
            this.playerController.controllerContext.contextsHolder
                .SetContext(NewPlayerMovementAirRule.RULE_ID,
                NewPlayerAirTransitions.GetFreeFallContext(initialIntentionalMovement));

            //rule
            this.playerController.SetCurrentRule(new NewPlayerMovementAirRule(this.playerController));
        }

        public void EnterJump(Vector3 initialIntentionalMovement)
        {
            PrepareTransition();
            const float playerJumpSpeed = 0.28f;

            //animation
            this.playerController.controllerContext.playerAnimationHub.FreeFallSequence();
            this.playerController.controllerContext.playerAnimationHub.JumpSequence();
            this.playerController.controllerContext.playerAnimationHub.LedgeGrabSequence(false);

            //context
            this.playerController.controllerContext.contextsHolder
                .SetContext(NewPlayerMovementAirRule.RULE_ID,
                NewPlayerAirTransitions.GetJumpContext(playerJumpSpeed, initialIntentionalMovement));

            //rule
            this.playerController.SetCurrentRule(new NewPlayerMovementAirRule(this.playerController));
        }

        public void EnterGround(Vector3 initialIntentionalMovement)
        {
            PrepareTransition();

            //animation
            this.playerController.controllerContext.playerAnimationHub.GroundSequence();

            //context
            this.playerController.controllerContext.contextsHolder
                .SetContext(NewPlayerMovementGroundRule.RULE_ID,
                NewPlayerGroundTransitions.GetGroundContext(initialIntentionalMovement));

            //rule
            this.playerController.SetCurrentRule(new NewPlayerMovementGroundRule(this.playerController));
        }

        public void EnterLedgeGrab(LedgeColliderInfo ledgeCollider)
        {
            PrepareTransition();

            //animation
            this.playerController.controllerContext.playerAnimationHub.LedgeGrabSequence(true);

            //context
            this.playerController.controllerContext.contextsHolder
                .SetContext(NewPlayerMovementLedgeRule.RULE_ID,
                NewPlayerLedgeTransitions.GetLedgeGrabContext(
                    ledgeCollider,
                    this.playerController.controllerContext.subject));

            //rule
            this.playerController.SetCurrentRule(new NewPlayerMovementLedgeRule(this.playerController));
        }

        /// <summary>
        /// <b>Depraced</b> - will be deleted soon
        /// </summary>
        public void EnterWater()
        {
            PrepareTransition();

            //animation
            this.playerController.controllerContext.playerAnimationHub.WaterSequence();

            //context
            this.playerController.controllerContext.contextsHolder
                .SetContext(NewPlayerMovementWaterRule.RULE_ID,
                NewPlayerWaterTransitions.GetWaterContext());

            //rule
            this.playerController.SetCurrentRule(new NewPlayerMovementWaterRule(this.playerController));
        }

        public void EnterWaterSurface()
        {
            PrepareTransition();

            var transition = new NewPlayerSurfaceTransitions(this.playerController);
            transition.EnterSurface(transition.GetSurfaceContext());
        }

        public void EnterWaterSubmerged()
        {
            PrepareTransition();

            var transition = new NewPlayerSubmergedTransitions(this.playerController);
            transition.EnterSubmerged(transition.GetContext());
        }

        public void PrepareTransition()
        {
            this.playerController.controllerContext.kinematics.velocity = Vector3.zero;
        }
    }
}