using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerSubmergedTransitions : NewPlayerBaseTransitions
        <SubmergedContext, NewPlayerMovementSubmergedRule>
    {
        public NewPlayerSubmergedTransitions(NewPlayerController controller) : base(controller) { }

        public void EnterSubmerged(SubmergedContext context)
        {
            PrepareAnimation();

            this.controller.controllerContext.contextsHolder.SetContext(NewPlayerMovementSubmergedRule.RULE_ID,
                context);

            this.controller.SetCurrentRule(GetRule());
        }

        public override SubmergedContext GetContext()
        {
            return new()
            {
                timer = new(true),
                //waterHeight = waterHeight,
            };
        }

        public override NewPlayerMovementSubmergedRule GetRule()
        {
            return new(this.controller);
        }

        public override void PrepareAnimation()
        {
            var animHub = this.controller.controllerContext.playerAnimationHub;
            animHub.Diving(true);
        }
    }
}
