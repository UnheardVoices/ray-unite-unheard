using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public abstract class NewPlayerBaseTransitions<C, R>
        where C : Context, new()
        where R : NewPlayerMovementRule
    {
        protected NewPlayerController controller;

        public NewPlayerBaseTransitions(NewPlayerController controller)
        {
            this.controller = controller;
        }

        public abstract void PrepareAnimation();

        public abstract C GetContext();

        public abstract R GetRule();
    }
}
