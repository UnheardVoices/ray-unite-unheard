using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerSurfaceTransitions
    {
        private NewPlayerController controller;

        public NewPlayerSurfaceTransitions(NewPlayerController controller)
        {
            this.controller = controller;
        }
        private void PrepareAnimation()
        {
            var animHub = this.controller.controllerContext.playerAnimationHub;
            animHub.Diving(false);
            animHub.WaterSequence();
        }

        public void EnterSurface(SurfaceContext context)
        {
            PrepareAnimation();

            this.controller.controllerContext.contextsHolder.SetContext(NewPlayerMovementSurfaceRule.RULE_ID,
                context);

            this.controller.SetCurrentRule(GetRule());
        }

        public SurfaceContext GetSurfaceContext()
        {
            return new()
            {
                startDepth = this.controller.transform.position.y,
            };
        }

        public NewPlayerMovementSurfaceRule GetRule()
        {
            return new(this.controller);
        }
    }
}
