using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerAirTransitions
    {
        protected NewPlayerContext entityContext;
        public NewPlayerAirTransitions(NewPlayerContext entityContext)
        {
            this.entityContext = entityContext;
        }

        public static AirContext GetFreeFallContext(Vector3 initialIntentionalMovement)
        {
            return new AirContext()
            {
                IsHelicopter = false,
                IsJumping = false,
                intentionalMovement = initialIntentionalMovement
            };
        }

        public static AirContext GetJumpContext(float initialVerticalVelocity,
            Vector3 initialIntentionalMovement)
        {
            return new AirContext()
            {
                IsHelicopter = false,
                IsJumping = true,
                VerticalSpeed = initialVerticalVelocity,
                intentionalMovement = initialIntentionalMovement
            };
        }
    }
}
