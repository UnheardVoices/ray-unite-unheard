using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerWaterTransitions
    {
        public static WaterContext GetWaterContext()
        {
            return new WaterContext();
        }
    }
}
