using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerLedgeTransitions
    {
        public static LedgeContext GetLedgeGrabContext(
            LedgeColliderInfo ledgeCollider,
            Transform subject)
        {
            const float lenOffset = 0.83f;
            var normal = (ledgeCollider.edgePointALocal - ledgeCollider.edgePointBLocal).normalized;
            var rotation = Vector3.SignedAngle(Vector3.forward, normal, Vector3.up) - 90f;

            var forward = Quaternion.AngleAxis(rotation, Vector3.up) * Vector3.forward;

            var target = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(subject.position,
                ledgeCollider.edgePointALocal,
                ledgeCollider.edgePointBLocal);

            var normalVector = (Quaternion.Euler(0f, -90f, 0f) * normal).normalized;

            target.y -= 1.77f;

            target -= normalVector * lenOffset;

            return new LedgeContext()
            {
                forwardToLedge = forward,
                targetPosition = target,
                normalVector = normalVector
            };
        }
    }
}