using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions
{
    public class NewPlayerGroundTransitions
    {
        public static IntentionalContext GetGroundContext(Vector3 initialIntentionalMovement)
        {
            return new IntentionalContext()
            {
                intentionalMovement = initialIntentionalMovement,
            };
        }
    }
}
