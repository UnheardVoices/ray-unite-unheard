using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules
{
    public class NewPlayerMovementLedgeRule : NewPlayerMovementRule
    {
        public const string RULE_ID = "LEDGE_RULE_PLACEHOLDER";

        public NewPlayerMovementLedgeRule(object controller) : base(RULE_ID, controller) { }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<LedgeContext>();

            args.subject.forward = context.forwardToLedge;
            args.kinematics.velocity = Vector3.zero;
            args.subject.position = context.targetPosition;

            base.GameplayFixedUpdate();

            args.playerInputEffects
                .OnJumpButtonDown(() =>
                {
                    args.playerRulesTransitions.EnterJump(Vector3.zero);
                });

            args.playerInputEffects
                .OnAnalogForward(0.8f, 0.1f, () =>
                {
                    context.isGoingToStand = true;
                    args.playerAnimationHub.GroundSequence();
                });

            args.playerInputEffects
                .OnAnalogBackward(0.4f, 0.1f, () =>
                {
                    if (!context.isGoingToStand)
                    {
                        args.subject.position -= Vector3.up * 0.8f;
                        args.playerRulesTransitions.EnterFreeFall(Vector3.zero);
                    }
                });

            args.playerAnimationEffects
                .OnCurrentAnimation((time) =>
                {
                    if (context.isGoingToStand)
                    {
                        context.time += Time.fixedDeltaTime;
                        args.subject.position += 1.77f * context.time * Vector3.up;
                        args.subject.position += 1.8f * context.time * context.normalVector;

                        if (context.isGoingToStand && time >= 0.8947368f)
                        {
                            args.playerRulesTransitions.EnterGround(Vector3.zero);
                        }
                    }
                });

            args.playerAnimationHub.SetupIntentionalAnimation(Vector3.zero);
            args.playerAnimationHub.ManualFixedUpdate();
        }
    }
}
