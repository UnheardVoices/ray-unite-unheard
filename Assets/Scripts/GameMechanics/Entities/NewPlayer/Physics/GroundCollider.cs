using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics
{
    public static class GroundCollider
    {
        public static void AlignOnTopOfTheGround(Transform transform)
        {
            if (IsHittingGround(transform, out RaycastHit hit))
                transform.position = new(transform.position.x, hit.point.y, transform.position.z);
        }

        public static bool IsHittingGround(Transform transform)
        {
            return IsHittingFlatGround(transform, out RaycastHit hitFlat) || IsHittingSlope(transform, out RaycastHit hitSlope);
        }

        public static bool IsHittingGround(Transform transform, out RaycastHit hit)
        {
            return IsHittingFlatGround(transform, out hit) || IsHittingSlope(transform, out hit);
        }

        public static bool IsHittingFlatGround(
            Transform transform, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundRaycastDistance = 1.2f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, Vector3.down).normalized;
            }
            else
            {
                intentTranslation = Vector3.down;
            }

            return PhysicsRaycaster.Raycast(transform.position + Vector3.up,
                (Vector3)intentTranslation, out hit, maxGroundRaycastDistance,
                Layers.groundLayerMask, debug: debug) &&
                CommonCollisionUtils.IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingSlope(
            Transform transform, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundSlopeRaycastDistance = 1.5f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, Vector3.down).normalized;
            }
            else
            {
                intentTranslation = Vector3.down;
            }

            float angleThresholdDegrees = 1;
            if (PhysicsRaycaster.Raycast(
                transform.position + Vector3.up,
                (Vector3)intentTranslation, out hit,
                maxGroundSlopeRaycastDistance,
                Layers.groundLayerMask, debug: debug))
            {
                return (Vector3.Angle(Vector3.up, hit.normal) > angleThresholdDegrees) &&
                    CommonCollisionUtils.IsLegitSolidGroundOrWallForCollision(hit);
            }
            return false;
        }
    }
}
