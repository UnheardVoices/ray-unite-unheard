using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Common
{
    public class NewPlatformerCollisionEffects
    {
        private NewPlatformerCollisionHub platformerCollisionHub;

        public NewPlatformerCollisionEffects(
            NewPlatformerCollisionHub platformerCollisionHub)
        {
            this.platformerCollisionHub = platformerCollisionHub;
        }

        public void OnCollisionWithSteadyGroundWithVelocity(
            Action<Vector3, Vector3> onCollisionAction)
        {
            if (this.platformerCollisionHub
                    .steadyEnvironmentVelocityGroundCollisionNormal != null
                    &&
                this.platformerCollisionHub
                    .steadyEnvironmentVelocityGroundCollisionPoint != null)
            {
                onCollisionAction.Invoke(
                    (Vector3)this.platformerCollisionHub.steadyEnvironmentVelocityGroundCollisionNormal,
                    (Vector3)this.platformerCollisionHub.steadyEnvironmentVelocityGroundCollisionPoint);
            }
        }

        public void OnCollisionWithWall(
            IntentionalContext intentionalContext,
            Vector3 input,
            Action<Vector3> onCollisionAction)
        {
            if (this.platformerCollisionHub.currentCollisionState
                .isHittingWall)
            {
                onCollisionAction.Invoke(
                    CommonCollisionUtils
                    .MovementVelocityOnWallCollision(
                        this.platformerCollisionHub.subject.gameObject,
                        intentionalContext.intentionalMovement,
                        input
                        )
                    );
            }
        }

        public void OnCollisionWithMultiWalls(
            IntentionalContext intentionalContext,
            Vector3 input,
            Action<Vector3> onCollisionAction)
        {
            if (this.platformerCollisionHub.currentCollisionState
                .isHittingWall)
            {
                onCollisionAction.Invoke(
                    CommonCollisionUtils
                    .MovementVelocityOnWallsCollision(
                        this.platformerCollisionHub.subject.gameObject,
                        intentionalContext.intentionalMovement,
                        input
                        )
                    );
            }
        }

        public void OnCollisionWithGround(Action onCollisionAction)
        {
            if (this.platformerCollisionHub.isHittingGround)
            {
                onCollisionAction.Invoke();
            }
        }

        public void OnNoCollisionWithGround(Action onCollisionAction)
        {
            if (!this.platformerCollisionHub.isHittingGround)
            {
                onCollisionAction.Invoke();
            }
        }

        public void OnCollisionWithGroundOnFalling(Kinematics kinematics, Action onCollisionAction)
        {
            if (kinematics.velocity.y < Vector3.zero.y)
            {
                if (this.platformerCollisionHub.isHittingGround)
                {
                    onCollisionAction.Invoke();
                }
            }
        }

        public void OnCollisionLedgeGrab(Action<LedgeColliderInfo, RaycastHit> onCollisionAction)
        {
            if (this.platformerCollisionHub.isHittingLedgeGrab &&
                this.platformerCollisionHub.kinematics.velocity.y < 0f)
            {
                onCollisionAction.Invoke(this.platformerCollisionHub.ledgeColliderInfo,
                    this.platformerCollisionHub.ledgeHit);
            }
        }

        public void OnUnderwaterEnter(Action onCollisionAction)
        {
            if (this.platformerCollisionHub.isUnderwater)
            {
                onCollisionAction.Invoke();
            }
        }

        public void OnWater(Action<RaycastHit> onCollisionAction)
        {
            if (this.platformerCollisionHub.isUnderwater)
            {
                onCollisionAction.Invoke(this.platformerCollisionHub.waterHit);
            }
        }

        public void OnWaterSurface(Action<RaycastHit> onCollisionAction)
        {
            if (this.platformerCollisionHub.isUnderwater &&
                this.platformerCollisionHub.waterHit.distance < 
                this.platformerCollisionHub.controller.controllerContext.playerParams
                .surfaceParams.transitionHeight)
            {
                onCollisionAction.Invoke(this.platformerCollisionHub.waterHit);
            }
        }

        public void OnWaterSubmerged(Action<RaycastHit> onCollisionAction)
        {
            if (this.platformerCollisionHub.isUnderwater &&
                this.platformerCollisionHub.waterHit.distance >= 
                this.platformerCollisionHub.controller.controllerContext.playerParams
                .surfaceParams.transitionHeight)
            {
                onCollisionAction.Invoke(this.platformerCollisionHub.waterHit);
            }
        }
    }
}
