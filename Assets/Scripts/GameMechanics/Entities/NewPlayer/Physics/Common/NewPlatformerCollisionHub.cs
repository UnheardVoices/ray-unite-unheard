using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Common
{
    public class NewPlatformerCollisionHub
    {
        public NewPlatformerCollisionState previousCollisionState
        { get; protected set; } = new();

        public NewPlatformerCollisionState previousVelocityCollisionState
        { get; protected set; } = new();

        public NewPlatformerCollisionState currentCollisionState
        { get; protected set; } = new();

        public NewPlatformerCollisionState currentVelocityCollisionState
        { get; protected set; } = new();

        protected RaycastHit? GetSteadyGroundHit(NewPlatformerCollisionState collisionState)
        {
            if (!collisionState.isHittingGroundGeneral)
            {
                return null;
            }

            if (collisionState.isHittingFlatGround)
            {
                return collisionState.flatGroundHit;
            }
            else if (collisionState.isHittingSlope)
            {
                return collisionState.slopeHit;
            }
            else
            {
                return null;
            }
        }

        public Vector3? steadyEnvironmentVelocityGroundCollisionNormal =>
            GetSteadyGroundHit(this.currentVelocityCollisionState)?.normal;

        public Vector3? steadyEnvironmentVelocityGroundCollisionPoint =>
            GetSteadyGroundHit(this.currentVelocityCollisionState)?.point;


        #region Bools

        public bool isHittingGround =>
            this.currentCollisionState.isHittingGroundGeneral;

        public bool isHittingFlatGround =>
            this.currentCollisionState.isHittingFlatGround;

        public bool isHittingLedgeGrab =>
            this.currentCollisionState.isHittingLedgeGrab;

        public bool isUnderwater =>
            this.currentCollisionState.isUnderwater;

        #endregion

        #region Hits

        public RaycastHit ledgeHit =>
            this.currentCollisionState.ledgeHit;

        public RaycastHit waterHit =>
            this.currentCollisionState.waterHit;

        public LedgeColliderInfo ledgeColliderInfo =>
            this.currentCollisionState.ledgeColliderInfo;

        #endregion

        public Transform subject { get; protected set; }
        public Kinematics kinematics { get; protected set; }
        public NewPlayerController controller { get; }


        public NewPlatformerCollisionHub(Transform subject, Kinematics kinematics, NewPlayerController controller)
        {
            this.subject = subject;
            this.kinematics = kinematics;
            this.controller = controller;
        }

        public void GameplayFixedUpdate(
            Transform subject, Kinematics kinematics,
            Vector3? originPositionOffsetArg = null)
        {
            this.previousCollisionState
                .ApplyStateValues(this.currentCollisionState);
            this.previousVelocityCollisionState
                .ApplyStateValues(this.currentVelocityCollisionState);

            Vector3 originPositionOffset = Vector3.zero;
            if (originPositionOffsetArg != null)
            {
                originPositionOffset = (Vector3)originPositionOffsetArg;
            }

            const float maxGroundRaycastDistance = 1.2f;
            const float maxGroundSlopeRaycastDistance = 1.5f;

            this.currentCollisionState.isHittingFlatGround =
                GroundCollider.IsHittingFlatGround(
                    subject, out this.currentCollisionState.flatGroundHit,
                        null,
                        maxGroundRaycastDistance);

            this.currentCollisionState.isHittingSlope =
                GroundCollider.IsHittingSlope(
                    subject, out this.currentCollisionState.slopeHit,
                        null,
                        maxGroundSlopeRaycastDistance);

            this.currentVelocityCollisionState.isHittingFlatGround =
                GroundCollider.IsHittingFlatGround(
                    subject, out this.currentVelocityCollisionState.flatGroundHit,
                        kinematics.velocity,
                        maxGroundRaycastDistance);

            //this.currentVelocityCollisionState.isHittingSlope =
            //    GroundCollider.IsHittingSlope(
            //        subject, out this.currentVelocityCollisionState.slopeHit,
            //            kinematics.velocity,
            //            maxGroundSlopeRaycastDistance);

            this.currentCollisionState.isHittingWall =
                CommonCollisionUtils.IsHittingTheWall(
                    subject, out this.currentVelocityCollisionState.wallHit);

            this.currentCollisionState.isHittingLedgeGrab =
                AirCollider.IsHittingLedgeGrab(this.subject,
                2.0f, out this.currentCollisionState.ledgeHit,
                out this.currentCollisionState.ledgeColliderInfo);

            this.currentCollisionState.isUnderwater =
                WaterCollider.IsUnderWater(this.subject,
                out this.currentCollisionState.waterHit);
        }
    }
}