using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Common
{
    public class NewPlatformerCollisionState
    {
        public bool isHittingGroundGeneral => this.isHittingFlatGround || this.isHittingSlope;
        public bool isHittingFlatGround { get; set; }
        public bool isHittingSlope { get; set; }
        public bool isHittingWall { get; set; }

        public bool isHittingLedgeGrab { get; set; }
        public bool isUnderwater { get; set; }

        public RaycastHit flatGroundHit;
        public RaycastHit slopeHit;
        public RaycastHit wallHit;
        public RaycastHit ledgeHit;
        public RaycastHit waterHit;
        public LedgeColliderInfo ledgeColliderInfo;

        public void ApplyStateValues(
            NewPlatformerCollisionState collisionState)
        {
            this.isHittingFlatGround = collisionState.isHittingFlatGround;
            this.isHittingSlope = collisionState.isHittingSlope;
            this.isHittingWall = collisionState.isHittingWall;
            this.isHittingLedgeGrab = collisionState.isHittingLedgeGrab;
            this.isUnderwater = collisionState.isUnderwater;

            this.flatGroundHit = collisionState.flatGroundHit;
            this.slopeHit = collisionState.slopeHit;
            this.wallHit = collisionState.wallHit;
            this.ledgeHit = collisionState.ledgeHit;
            this.waterHit = collisionState.waterHit;

            this.ledgeColliderInfo = collisionState.ledgeColliderInfo;
        }
    }
}