using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils
{
    public static class Raycaster
    {
        public static bool VerticalCircleRaycaster(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out List<RaycastHit> hitList,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            hitList = new();

            for (int i = 0; i < raysCount; i++)
            {
                var RayVector = (Quaternion.AngleAxis(360f / raysCount * i, Vector3.up) * direction).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    RayVector,
                    out RaycastHit hit,
                    radius,
                    layerMask,
                    debugColor,
                    debug))
                {
                    //Debug.DrawRay(origin, RayVector, Color.red);


                    hitList.Add(hit);
                }
            }

            return hitList.Count > 0;
        }

        public static bool VerticalCircleRaycasterUnique(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out Dictionary<int, RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            hits = new();

            for (int i = 0; i < raysCount; i++)
            {
                var RayVector = (Quaternion.AngleAxis(360f / raysCount * i, Vector3.up) * direction).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    RayVector,
                    out RaycastHit hit,
                    radius,
                    layerMask,
                    debugColor,
                    debug))
                {
                    //Debug.DrawRay(origin, RayVector, Color.red);

                    if (!hits.ContainsKey(hit.collider.GetInstanceID()))
                        hits.Add(hit.collider.GetInstanceID(), hit);
                }
            }

            return hits.Count > 0;
        }

        public static bool CylinderRaycaster(
            Vector3 origin,
            Vector3 direction,
            float radius,
            float height,
            int layers,
            int raysCount,
            out List<RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            hits = new();

            for (int i = 0; i < layers; i++)
            {
                for (int j = 0; j < raysCount; j++)
                {
                    var Origin = origin + Vector3.up * height / layers * i;
                    var RayVector = (Quaternion.AngleAxis(360f / raysCount * j, Vector3.up) * direction).normalized;
                    if (PhysicsRaycaster.Raycast(
                        Origin,
                        RayVector,
                        out RaycastHit hit,
                        radius,
                        layerMask,
                        debugColor,
                        debug))
                    {
                        Debug.DrawRay(Origin, RayVector, Color.red);
                        hits.Add(hit);
                    }
                }
            }

            return hits.Count > 0;
        }

        public static bool CylinderRaycasterUniqe(
            Vector3 origin,
            Vector3 direction,
            float radius,
            float height,
            int layers,
            int raysCount,
            out Dictionary<int, RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            hits = new();

            for (int i = 0; i < layers; i++)
            {
                for (int j = 0; j < raysCount; j++)
                {
                    var Origin = origin + Vector3.up * height / layers * i;
                    var RayVector = (Quaternion.AngleAxis(360f / raysCount * j, Vector3.up) * direction).normalized;
                    if (PhysicsRaycaster.Raycast(
                        Origin,
                        RayVector,
                        out RaycastHit hit,
                        radius,
                        layerMask,
                        debugColor,
                        debug))
                    {
                        Debug.DrawRay(Origin, RayVector, Color.red);

                        if (!hits.ContainsKey(hit.collider.GetInstanceID()))
                            hits.Add(hit.collider.GetInstanceID(), hit);
                    }
                }
            }

            return hits.Count > 0;
        }

        public static bool CylinderRaycasterSingle(
            Vector3 origin,
            Vector3 direction,
            float radius,
            float height,
            int layers,
            int raysCount,
            out RaycastHit hit,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null)
        {
            for (int i = 0; i < layers; i++)
            {
                for (int j = 0; j < raysCount; j++)
                {
                    var Origin = origin + Vector3.up * height / layers * i;
                    var RayVector = (Quaternion.AngleAxis(360f / raysCount * j, Vector3.up) * direction).normalized;
                    if (PhysicsRaycaster.Raycast(
                        Origin,
                        RayVector,
                        out RaycastHit innerHit,
                        radius,
                        layerMask,
                        debugColor,
                        debug))
                    {
                        Debug.DrawRay(Origin, RayVector, Color.red);
                        hit = innerHit;
                        return true;
                    }
                }
            }

            hit = new();
            return false;
        }
    }
}
