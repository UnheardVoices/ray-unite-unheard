using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils
{
    public static class CommonCollisionHelper
    {
        public const float circleWallCollisionCheckingRadius = 0.8f;
    }

    public static class CommonCollisionUtils
    {
        public static bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) &&
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        public static Vector3 GetMovementVelocityConsideringWallCollisions(
            GameObject gameObject, Vector3 entityForwardDirectionVector,
            Vector3 velocity, float wallHitCheckingRadius = CommonCollisionHelper.circleWallCollisionCheckingRadius,
            Vector3 entityPositionOffsetForDebug = new Vector3(),
            bool debugInitialWallHitCheck = false,
            Color? debugInitialWallHitCheckColor = null,
            bool debugAveragingWallCollisionNormal = false,
            Color? debugAveragingWallCollisionNormalColor = null,
            bool debugHittingWallInGivenDirectionAtCorner = false,
            Color? debugHittingWallInGivenDirectionAtCornerColor = null,
            bool debugResultVelocity = false,
            float debugVelocityLengthMultiplier = 1.0f,
            Color? debugResultVelocityColor = null,
            bool debugResultWallNormal = false,
            Color? debugResultWallNormalColor = null,
            bool debugEntityForwardDirectionVector = false,
            float debugEntityForwardDirectionLengthMultiplier = 1.0f,
            Color? debugEntityForwardDirectionVectorColor = null)
        {
            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                entityForwardDirectionVector.normalized * debugEntityForwardDirectionLengthMultiplier,
                debugEntityForwardDirectionVectorColor,
                debugEntityForwardDirectionVector);

            if (IsHittingTheWall(
                gameObject.transform,
                out _,
                wallHitCheckingRadius,
                debugInitialWallHitCheck,
                debugInitialWallHitCheckColor))
            {
                Tuple<Vector3, List<RaycastHit>>
                    wallsNormalsInfoPlayerCollidesWith =
                        GetAveragedNormalFromAllTheWallsTheCollisionOccurs(
                            gameObject.transform,
                            default,
                            wallHitCheckingRadius,
                            debugAveragingWallCollisionNormal,
                            debugAveragingWallCollisionNormalColor);

                Vector3 flatVelocity = Vector3.ProjectOnPlane(velocity, Vector3.up).normalized;

                if (flatVelocity.magnitude < 0.01f)
                {
                    // if velocity magnitude even after normalization is not sufficient
                    // (in case no velocity but still..), then substitute
                    // it with entity forward direction for the sake of calculations

                    flatVelocity = entityForwardDirectionVector;
                }

                if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 0)
                {
                    // if not colliding with anything, just return the velocity
                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                       velocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);
                    return velocity;
                }
                else
                {
                    if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 1)
                    {
                        Vector3 wallNormal =
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized;

                        DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                            wallNormal, debugResultWallNormalColor, debugResultWallNormal);

                        // if colliding with only one wall, then its easy
                        // project player's velocity on wall's plane so we can glide along the wall nicely
                        if (Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) > 90
                            || Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) < -90)
                        {
                            Vector3 resultVelocity = Vector3.ProjectOnPlane(
                                velocity, Vector3.ProjectOnPlane(
                                // always flatten the wall normal to the Vector3.up plane
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized);

                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                resultVelocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);

                            return resultVelocity;
                        }
                        else
                        {
                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                velocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);
                            return velocity;
                        }
                    }
                    else
                    {
                        // colliding with more than wall, just use the heuristic that we can always get 
                        // two walls' normals that are at the corner, as long as it does the job, who cares :)
                        Vector3 wall1Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized;
                        Vector3 wall2Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[1].normal, Vector3.up).normalized;

                        Vector3 averagedWallsNormal = wallsNormalsInfoPlayerCollidesWith.Item1;

                        //DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                        //    averagedWallsNormal, debugResultWallNormalColor, debugResultWallNormal);

                        float angleBetweenAveragedWallsNormalAndWall1Normal =
                            Vector3.Angle(wall1Normal, averagedWallsNormal);
                        float angleBetweenAveragedWallsNormalAndWall2Normal =
                            Vector3.Angle(wall2Normal, averagedWallsNormal);

                        if (Vector3.Angle(entityForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall1Normal
                            && Vector3.Angle(entityForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall2Normal)
                        {
                            // if player tries to run in the direction outwards the corner in the direction towards the walls,
                            // simply do not let him move any further in that direction :)
                            // let him glide along one of the walls in the corner that is 'right' for that
                            Vector3 flattenedVelocity =
                                Vector3.ProjectOnPlane(velocity, Vector3.up).normalized *
                                    velocity.magnitude;

                            Vector3 potentialFlattenedVelocityAlongWall1 =
                                Vector3.ProjectOnPlane(flattenedVelocity, wall1Normal);
                            Vector3 potentialFlattenedVelocityAlongWall2 =
                                Vector3.ProjectOnPlane(flattenedVelocity, wall2Normal);

                            Vector3 potentialVelocityAlongWall1 =
                                Vector3.ProjectOnPlane(velocity, wall1Normal);
                            Vector3 potentialVelocityAlongWall2 =
                                Vector3.ProjectOnPlane(velocity, wall2Normal);

                            bool encountersWallAlongsideWall1 =
                                HitsTheWallCheckedInStackedRays(
                                    gameObject.transform,
                                    potentialFlattenedVelocityAlongWall1.normalized,
                                    debugHittingWallInGivenDirectionAtCorner,
                                    debugHittingWallInGivenDirectionAtCornerColor);

                            bool encountersWallAlongsideWall2 =
                                HitsTheWallCheckedInStackedRays(
                                    gameObject.transform,
                                    potentialFlattenedVelocityAlongWall2.normalized,
                                    debugHittingWallInGivenDirectionAtCorner,
                                    debugHittingWallInGivenDirectionAtCornerColor);

                            if (encountersWallAlongsideWall1 || encountersWallAlongsideWall2)
                            {
                                // allow player to move only in the direction that does not lead to another wall
                                if (!encountersWallAlongsideWall1)
                                {
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        potentialVelocityAlongWall1 * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return potentialVelocityAlongWall1;
                                }
                                else if
                                  (!encountersWallAlongsideWall2)
                                {
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        potentialVelocityAlongWall2 * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return potentialVelocityAlongWall2;
                                }
                                else
                                {
                                    Vector3 resultVelocity = new Vector3(0, velocity.y, 0);
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        resultVelocity * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return resultVelocity;
                                }
                            }
                            else
                            {
                                DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                    velocity * debugVelocityLengthMultiplier,
                                    debugResultVelocityColor, debugResultVelocity);
                                return velocity;
                            }
                        }
                        else
                        {
                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                 velocity * debugVelocityLengthMultiplier,
                                 debugResultVelocityColor, debugResultVelocity);
                            return velocity;
                        }
                    }
                }
            }
            else
            {
                DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                     velocity * debugVelocityLengthMultiplier,
                     debugResultVelocityColor, debugResultVelocity);
                return velocity;
            }
        }

        public static Tuple<Vector3, List<RaycastHit>> GetAveragedNormalFromAllTheWallsTheCollisionOccurs(
            Transform subject,
            float circleWallCollisionCheckingRadius = 0.8f,
            float? wallHitCheckingRadius = null,
            bool debug = false,
            Color? debugColor = null
            )
        {
            int layers = 10;
            int raysInLayer = 60;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            List<RaycastHit> hits = new List<RaycastHit>();
            List<Vector3> resultWallsNormals = new List<Vector3>();

            List<RaycastHit> resultHits = new List<RaycastHit>();

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVerticalListPoints(
                subject.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                wallHitCheckingRadius != null ? (float)wallHitCheckingRadius :
                    circleWallCollisionCheckingRadius * 1.1f,
                raysInLayer,
                out hits,
                Layers.generalEnvironmentLayersMask,
                debug,
                debugColor))
                {
                    for (int j = 0; j < hits.Count; j++)
                    {
                        RaycastHit hit = hits[j];
                        if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5)
                        {
                            hit.normal =
                                new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                            if (!Vectors3Helper.VectorsListContainsVectorWithSameDirection(hit.normal, resultWallsNormals))
                            {
                                resultWallsNormals.Add(hit.normal);
                                resultHits.Add(hit);
                            }
                        }
                    }
                }
            }
            if (resultWallsNormals.Count > 0)
            {
                Vector3 resultNormal =
                new Vector3(
                    resultWallsNormals.Average(x => x.x),
                    resultWallsNormals.Average(x => x.y),
                    resultWallsNormals.Average(x => x.z));
                return Tuple.Create(resultNormal, resultHits);
            }
            else
            {
                Debug.LogError("For some reason no normals to average in here, potentially clipping through wall...");
                return Tuple.Create(Vector3.zero, new List<RaycastHit>());
            }
        }

        public static bool HitsTheWallCheckedInStackedRays(
            Transform subject,
            Vector3 raysDirection, bool debug = false, Color? debugColor = null)
        {
            Vector3 originPosition = subject.position;
            return RayColliderRules.HitsTheWallCheckedInStackedRays(
                originPosition, raysDirection, out RaycastHit _,
                debug: debug, debugColor: debugColor);
        }

        public static bool IsHittingTheWall(
            Transform transform, out RaycastHit hit,
            float circleWallCollisionCheckingRadius = 1.1f,
            bool debug = false,
            Color? debugColor = null)
        {
            int layers = 10;
            int raysInLayer = 30;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                circleWallCollisionCheckingRadius,
                raysInLayer,
                out hit,
                Layers.collisionObjectLayerMask,
                debug,
                debugColor))
                {
                    if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static Vector3 MovementVelocityOnWallCollision(
            GameObject gameObject,
            Vector3 initialVelocity,
            Vector3 input,
            float playerMovementSpeed = 0.21f)
        {
            if (IsHittingTheWall(gameObject.transform, out RaycastHit hit))
            {
                var right = Vector3.SignedAngle(Vector3.forward, Vector3.right, Vector3.up);
                var normal = Vector3.SignedAngle(Vector3.forward, hit.normal.normalized, Vector3.up);
                var direction = Quaternion.Euler(0f, right - normal, 0f);

                var velocityOnPlane = direction * gameObject.transform.forward * input.magnitude;
                velocityOnPlane.x = Mathf.Clamp(velocityOnPlane.x, 0f, 1f);


                var forward = Quaternion.Euler(0f, normal - right, 0f);

                //Debug.DrawRay(gameObject.transform.position,
                //    forward * velocityOnPlane);

                //Debug.LogError(velocityOnPlane);

                return forward * velocityOnPlane * playerMovementSpeed;
            }
            else
            {
                return initialVelocity;
            }
        }

        public static Vector3 MovementVelocityOnWallsCollision(
            GameObject gameObject,
            Vector3 initialVelocity,
            Vector3 input,
            int raysCount = 60,
            float circleWallCollisionCheckingRadius = 1.1f,
            float playerMovementSpeed = 0.21f)
        {
            if (Raycaster.CylinderRaycaster(
                    gameObject.transform.position,
                    gameObject.transform.forward,
                    circleWallCollisionCheckingRadius,
                    2.0f,
                    30,
                    raysCount,
                    out List<RaycastHit> hits,
                    Layers.collisionObjectLayerMask))
            {
                Vector3 velocity = gameObject.transform.forward * input.magnitude;

                foreach (var hit in hits)
                {
                    velocity = MovementVelocityOnWallCollision(
                        velocity,
                        hit);
                }

                return velocity * playerMovementSpeed;
            }
            return initialVelocity;
        }

        public static Vector3 MovementVelocityOnWallCollision(
            Vector3 initValue,
            RaycastHit hit)
        {
            var right = Vector3.SignedAngle(Vector3.forward, Vector3.right, Vector3.up);
            var normal = Vector3.SignedAngle(Vector3.forward, hit.normal.normalized, Vector3.up);
            var direction = Quaternion.Euler(0f, right - normal, 0f);

            var velocityOnPlane = direction * initValue;
            velocityOnPlane.x = Mathf.Clamp(velocityOnPlane.x, 0f, 1f);
            var forward = Quaternion.Euler(0f, normal - right, 0f);

            return forward * velocityOnPlane;
        }
    }
}