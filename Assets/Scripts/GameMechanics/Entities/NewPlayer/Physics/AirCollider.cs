using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics
{
    public static class AirCollider
    {
        public static Vector3 AdjustVelocityIfIsHittingTheCeiling(Transform transform, Vector3 velocity)
        {
            RaycastHit ceilingHit = new RaycastHit();
            float ceilingCollisionCheckRayLength = 2.0f;

            //Vector3 velocityInVerticalUpDirection = Vector3.ProjectOnPlane(
            //    velocityInWorldSpace, Vector3.forward);

            //if (Vector3.Angle(velocityInVerticalUpDirection, Vector3.up) > 90f)
            //{
            //    velocityInVerticalUpDirection = Vector3.zero;
            //}

            if (PhysicsRaycaster.Raycast(transform.position + Vector3.up, transform.up,
                out ceilingHit, ceilingCollisionCheckRayLength, Layers.generalEnvironmentLayersMask) &&
                CommonCollisionUtils.IsLegitSolidGroundOrWallForCollision(ceilingHit))
            {
                velocity.y = Mathf.Clamp(velocity.y, float.MinValue, 0f);
                //return Vector3.ProjectOnPlane(velocityInWorldSpace, ceilingHit.normal);
            }
            return velocity;
        }

        public static Vector3 AccelerateGravitationallyUpUntilLimitFallSpeed(
            Vector3 velocity,
            float gravityAcceleration,
            float limitFallSpeed)
        {
            return new Vector3(
                    velocity.x,
                    velocity.y > -limitFallSpeed ?
                        velocity.y - gravityAcceleration :
                        velocity.y,
                    velocity.z
                    );
        }

        public static bool IsHittingLedgeGrab(
            Transform transform,
            float ledgeGrabCheckingRadius,
            out RaycastHit hit,
            out LedgeColliderInfo ledgeColliderInfo)
        {
            if (Raycaster.CylinderRaycasterSingle(transform.position + Vector3.up * 1.75f, transform.forward,
                ledgeGrabCheckingRadius,
                0.25f, 4, 30, out RaycastHit innerHit, Layers.ledgeCollidersLayerMask))
            {
                ledgeColliderInfo = innerHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
                hit = innerHit;
                return true;
            }

            hit = new();
            ledgeColliderInfo = null;
            return false;
        }
    }
}
