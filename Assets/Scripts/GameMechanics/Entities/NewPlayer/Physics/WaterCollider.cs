using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics
{
    public static class WaterCollider
    {
        public static bool IsUnderWater(Transform transform, out RaycastHit hit)
        {
            return PhysicsRaycaster.Raycast(
                transform.position,
                Vector3.up,
                out hit,
                float.MaxValue,
                Layers.waterLayerMask);
        }
    }
}
