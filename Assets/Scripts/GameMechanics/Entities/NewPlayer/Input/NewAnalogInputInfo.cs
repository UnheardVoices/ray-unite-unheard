using Assets.Scripts.GameMechanics.Entities.NewPlayer.Input.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewAnalogInputInfo
    {
        public Vector2 rawAnalogValue;
        public Vector3 interpolatedAnalogValue;

        public void ApplyValues(NewAnalogInputInfo other)
        {
            this.rawAnalogValue = other.rawAnalogValue;
            this.interpolatedAnalogValue = other.interpolatedAnalogValue;
        }

        public void DismissAnalogStateInfo()
        {
            this.rawAnalogValue = Vector2.zero;
            this.interpolatedAnalogValue = Vector2.zero;
        }

        public void UpdateInputState(Vector2 rawAnalogValue)
        {
            this.rawAnalogValue = rawAnalogValue;
            //this.interpolatedAnalogValue = InputInterpolation
            //    .InterpolationRotationWithLength(Vector3.forward, this.interpolatedAnalogValue,
            //    this.rawAnalogValue, this.interpolationValue);

            this.interpolatedAnalogValue = Vector3.Lerp(this.interpolatedAnalogValue,
                VectorsConvert.Convert2To3Input(rawAnalogValue), 0.125f);
        }

        public Vector3 GetInterpolated()
        {
            return this.interpolatedAnalogValue;
        }
    }
}
