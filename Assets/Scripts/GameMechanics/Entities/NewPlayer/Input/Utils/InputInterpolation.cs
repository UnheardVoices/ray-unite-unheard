using Assets.Scripts.PlayerMechanics.Rules.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input.Utils
{
    public enum ThresholdCheckManner
    {
        INCLUSIVE,
        EXCLUSIVE
    }

    public enum ThresholdCheckDirection
    {
        BIGGER,
        LOWER
    }

    public enum InputInterpolationMode
    {
        INTERPOLATED,
        NON_INTERPOLATED
    }

    public static class InputInterpolation
    {
        public static void OnMoveInputMagnitudeExceeding(
            Transform playerObjectTransform,
            PlayerMovementInput playerMovementInputWrapper,
            float magnitudeThreshold,
            ThresholdCheckDirection thresholdCheckDirection,
            ThresholdCheckManner thresholdCheckManner,
            InputInterpolationMode inputInterpolationMode,
            Action<Vector3> onInputAction
            )
        {
            Vector3 translation;

            if (inputInterpolationMode == InputInterpolationMode.INTERPOLATED)
            {
                //translation = playerMovementInputWrapper.GetTranslation();
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetTranslation());
            }
            else
            {
                //translation = playerMovementInputWrapper.GetNonInterpolatedTranslation();
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetNonInterpolatedTranslation());
            }

            if (thresholdCheckDirection == ThresholdCheckDirection.LOWER)
            {
                if (thresholdCheckManner == ThresholdCheckManner.EXCLUSIVE)
                {
                    if (translation.magnitude < magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
                else
                {
                    if (translation.magnitude <= magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
            }
            else
            {
                if (thresholdCheckManner == ThresholdCheckManner.EXCLUSIVE)
                {
                    if (translation.magnitude > magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
                else
                {
                    if (translation.magnitude >= magnitudeThreshold)
                    {
                        onInputAction.Invoke(translation);
                    }
                }
            }
        }

        public static void OnMoveInputMagnitudeBetween(
            Transform playerObjectTransform,
            PlayerMovementInput playerMovementInputWrapper,
            float magnitudeRangeA,
            float magnitudeRangeB,
            ThresholdCheckManner magnitudeRangeACheckManner,
            ThresholdCheckManner magnitudeRangeBCheckManner,
            InputInterpolationMode inputInterpolationMode,
            Action<Vector3> onInputAction
            )
        {
            Vector3 translation;

            if (inputInterpolationMode == InputInterpolationMode.INTERPOLATED)
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetTranslation());
            }
            else
            {
                translation = playerObjectTransform.TransformDirection(
                    playerMovementInputWrapper.GetNonInterpolatedTranslation());
            }

            bool rangeASatisfied =
                (magnitudeRangeACheckManner == ThresholdCheckManner.EXCLUSIVE &&
                    translation.magnitude > magnitudeRangeA)
                    ||
                (magnitudeRangeACheckManner == ThresholdCheckManner.INCLUSIVE &&
                    translation.magnitude >= magnitudeRangeA);
            bool rangeBSatisfied =
                (magnitudeRangeBCheckManner == ThresholdCheckManner.EXCLUSIVE &&
                    translation.magnitude < magnitudeRangeB)
                    ||
                (magnitudeRangeBCheckManner == ThresholdCheckManner.INCLUSIVE &&
                    translation.magnitude <= magnitudeRangeB); ;

            if (rangeASatisfied && rangeBSatisfied)
            {
                onInputAction.Invoke(translation);
            }
        }

        public static Vector3 InterpolationRotationWithLength(
            Vector3 forward, Vector3 start, Vector3 end, float t)
        {
            float interpolatedLength = Mathf.Lerp(start.magnitude, end.magnitude, t);

            float angle = Vector3.SignedAngle(forward.normalized, end.normalized, Vector3.up);

            float angleFromForwardToStart = Vector3.SignedAngle(forward, start, Vector3.up);
            float angleBetweenStartAndEnd = Vector3.SignedAngle(start, end, Vector3.up);

            return Quaternion.AngleAxis(angleFromForwardToStart +
                (angleBetweenStartAndEnd * t), Vector3.up) * Vector3.forward * interpolatedLength;
        }

        //public static Vector3 RotationInterpolation()
        //{
        //    return Vector3.zero;
        //}

        public static Vector2 InterpolationRotationMagnitude(Vector2 a, Vector2 b, float t)
        {
            var magnitude = Mathf.Lerp(a.magnitude, b.magnitude, t);

            var angleForward = Vector2.SignedAngle(Vector2.up, a);
            var angle = Vector2.SignedAngle(a, b);

            //var rotationA = Quaternion.LookRotation(a);
            //var rotationB = Quaternion.LookRotation(b);

            return Quaternion.AngleAxis(angleForward + angle * t, Vector2.up) * Vector2.right * magnitude;
        }

        public static Vector3 InterpolationRotationMagnitude(Vector3 a, Vector3 b, float t)
        {
            var magnitude = Mathf.Lerp(a.magnitude, b.magnitude, t);

            var rotationA = Quaternion.LookRotation(a);
            var rotationB = Quaternion.LookRotation(b);

            return Quaternion.Lerp(rotationA, rotationB, t) * Vector3.forward * magnitude;
        }
    }

    public static class VectorsConvert
    {
        public static Vector3 Convert2To3Input(Vector2 vector)
        {
            return vector.y * Vector3.forward + vector.x * Vector3.right;
        }
    }
}