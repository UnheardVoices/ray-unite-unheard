using Assets.Scripts.GameMechanics.Entities.NewCamera;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Input.Utils;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewPlayerInputEffects
    {
        protected NewPlayerInputHub playerInputHub;

        public NewPlayerInputEffects(
            NewPlayerInputHub playerInputHub)
        {
            this.playerInputHub = playerInputHub;
        }

        public void OnJumpButtonDown(Action onInputAction)
        {
            if (this.playerInputHub.jumpButton.buttonDown)
            {
                onInputAction.Invoke();
            }
        }

        public void OnJumpButtonUp(Action onInputAction)
        {
            if (this.playerInputHub.jumpButton.buttonUp)
            {
                onInputAction.Invoke();
            }
        }

        public void OnStrafeButtonDown(Action onInputAction)
        {
            if (this.playerInputHub.jumpButton.buttonDown)
            {
                onInputAction.Invoke();
            }
        }

        public void OnStrafeButtonUp(Action onInputAction)
        {
            if (this.playerInputHub.jumpButton.buttonUp)
            {
                onInputAction.Invoke();
            }
        }

        public void OnMoveMagnitudeBiggerOrEqualThan(float magnitudeThreshold,
            Action<Vector3, Quaternion> onInputAction)
        {
            OnMoveMagnitudeBiggerOrEqualThan(magnitudeThreshold, 0.21f, onInputAction);
        }

        public void OnMoveMagnitudeBiggerOrEqualThan(float magnitudeThreshold, float customPlayerSpeed,
            Action<Vector3, Quaternion> onInputAction)
        {
            if (this.playerInputHub.moveInput.rawAnalogValue.magnitude >= magnitudeThreshold)
            {
                onInputAction.Invoke(
                    NewPlayerInputEffectsHelper.PlayerVelocity(this.playerInputHub, customPlayerSpeed),
                    NewPlayerInputEffectsHelper.PlayerRotationQuaternion(this.playerInputHub)
                    );
            }
        }

        public void OnMoveMagnitudeLowerThan(float magnitudeThreshold,
            Action<Vector3, Vector3> onInputAction)
        {
            OnMoveMagnitudeLowerThan(magnitudeThreshold, 0.21f, onInputAction);
        }

        public void OnMoveMagnitudeLowerThan(float magnitudeThreshold, float customPlayerSpeed,
            Action<Vector3, Vector3> onInputAction)
        {
            if (this.playerInputHub.moveInput.rawAnalogValue.magnitude < magnitudeThreshold)
            {
                onInputAction.Invoke(
                    NewPlayerInputEffectsHelper.PlayerVelocity(this.playerInputHub, customPlayerSpeed),
                    NewPlayerInputEffectsHelper.PlayerRotation(this.playerInputHub));
            }
        }

        public void OnAnalogForward(float range, float magnitudeThreshold, Action onInputAction)
        {
            var analog = NewPlayerInputEffectsHelper.LedgeGrabInput(this.playerInputHub);

            if (analog.z > 0.0f &&
                analog.x < range &&
                analog.x > -range &&
                analog.magnitude >= magnitudeThreshold)
            {
                onInputAction.Invoke();
            }
        }

        public void OnAnalogBackward(float range, float magnitudeThreshold, Action onInputAction)
        {
            var analog = NewPlayerInputEffectsHelper.LedgeGrabInput(this.playerInputHub);

            if (analog.z < 0.0f &&
                analog.x < range &&
                analog.x > -range &&
                analog.magnitude >= magnitudeThreshold)
            {
                onInputAction.Invoke();
            }
        }

        public void OnDepthControlBiggerOrEqualThan(float magnitudeThreshold, float maxDepthAngle, Action<float> onInputAction)
        {
            if (Mathf.Abs(this.playerInputHub.depthInput.interpolatedAxis) >= magnitudeThreshold)
            {
                onInputAction.Invoke(this.playerInputHub.depthInput.interpolatedAxis * maxDepthAngle / 100f);
            }
        }

        public void OnDepthControlDeadZone(float magnitudeThreshold, Action onInputAction)
        {
            if (Mathf.Abs(this.playerInputHub.depthInput.interpolatedAxis) < magnitudeThreshold)
            {
                onInputAction.Invoke();
            }
        }
    }

    public static class NewPlayerInputEffectsHelper
    {
        public static Vector3 LedgeGrabInput(NewPlayerInputHub inputHub)
        {
            var primary = inputHub.moveInput.rawAnalogValue;
            var rotation1 = Vector3.SignedAngle(Vector3.forward,
                inputHub.playerController.transform.forward,
                Vector3.up);
            var forward = Quaternion.Euler(0f, -rotation1, 0f) * primary;

            var rotation2 = Vector3.SignedAngle(Vector3.forward,
                inputHub.playerController.controllerContext.cameraController.transform.forward,
                Vector3.up);

            return Quaternion.Euler(0f, -rotation2, 0f) * forward;
        }

        //public static Quaternion PlayerRotation(NewPlayerInputHub playerInputHub)
        //{
        //    var Args = playerInputHub.playerController.controllerContext;

        //    var cameraRotation = Vector3.SignedAngle(Vector3.forward,
        //        Args.cameraController.transform.forward, Vector3.up);

        //    var forward = Quaternion.Euler(0f, cameraRotation, 0f);
        //    var analog = playerInputHub.moveInput.rawAnalogValue;
        //    var direction = Quaternion.LookRotation(new Vector3(analog.x, 0f, analog.y));
        //    var raymanRotation = forward * direction;

        //    return raymanRotation;
        //}

        public static Vector3 PlayerRotation(NewPlayerInputHub playerInputHub)
        {
            var args = playerInputHub.playerController.controllerContext;
            var analog = Vector3.SignedAngle(Vector3.forward,
                VectorsConvert.Convert2To3Input(args.playerInputHub.moveInput.rawAnalogValue), Vector3.up);
            var camera = args.cameraController.transform.eulerAngles.y;

            return (Quaternion.Euler(0f, camera + analog, 0f) * Vector3.forward).normalized;
        }

        public static Quaternion PlayerRotationQuaternion(NewPlayerInputHub playerInputHub)
        {
            var args = playerInputHub.playerController.controllerContext;
            var analog = Vector3.SignedAngle(Vector3.forward,
                VectorsConvert.Convert2To3Input(args.playerInputHub.moveInput.rawAnalogValue), Vector3.up);
            var camera = args.cameraController.transform.eulerAngles.y;

            return Quaternion.Euler(0f, camera + analog, 0f);
        }

        public static Vector3 PlayerRotationUnderwater(NewPlayerInputHub playerInputHub, float maxDepthAngle)
        {
            var rotation = PlayerRotation(playerInputHub);
            return (Quaternion.AngleAxis(playerInputHub.depthInput.interpolatedAxis * maxDepthAngle,
                Vector3.right) * rotation).normalized;
        }

        //public static Quaternion PlayerRotationUnderwater(NewPlayerInputHub playerInputHub,
        //    float maxDepthAngle)
        //{
        //    var rotation = PlayerRotation(playerInputHub).eulerAngles;
        //    rotation.x = playerInputHub.depthInput.interpolatedAxis * maxDepthAngle;

        //    return Quaternion.Euler(rotation);
        //}

        public static Vector3 PlayerVelocity(NewPlayerInputHub playerInputHub, float playerRunningSpeed)
        {
            return playerInputHub.playerController.controllerContext.subject.forward *
               playerRunningSpeed *
               playerInputHub.moveInput.GetInterpolated().magnitude;
        }
    }
}
