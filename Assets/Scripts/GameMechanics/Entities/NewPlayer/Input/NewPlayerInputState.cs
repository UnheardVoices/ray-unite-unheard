using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewPlayerInputState
    {
        public NewAnalogInputInfo moveInput { get; set; }
            = new();
        public NewAxisInputInfo depthInput { get; set; }
            = new();
        public NewButtonInputInfo walkingButton { get; set; }
            = new();
        public NewButtonInputInfo groundRollButton { get; set; }
            = new();
        public NewButtonInputInfo strafingButton { get; set; }
            = new();
        public NewButtonInputInfo fireButton { get; set; }
            = new();
        public NewButtonInputInfo jumpButton { get; set; }
            = new();

        public void ApplyStateValues(
            NewPlayerInputState inputState)
        {
            this.moveInput.ApplyValues(inputState.moveInput);
            this.depthInput.ApplyValues(inputState.depthInput);
            this.walkingButton.ApplyValues(inputState.walkingButton);
            this.groundRollButton.ApplyValues(inputState.groundRollButton);
            this.strafingButton.ApplyValues(inputState.strafingButton);
            this.fireButton.ApplyValues(inputState.fireButton);
            this.jumpButton.ApplyValues(inputState.jumpButton);
        }

        internal void DismissButtonStates()
        {
            this.walkingButton.DismissUpAndDownStateInfo();
            this.groundRollButton.DismissUpAndDownStateInfo();
            this.strafingButton.DismissUpAndDownStateInfo();
            this.fireButton.DismissUpAndDownStateInfo();
            this.jumpButton.DismissUpAndDownStateInfo();
        }

        public void UpdateInputState(
            NewPlayerInputHandler playerInputHandler)
        {
            this.moveInput.UpdateInputState(playerInputHandler.moveInputRaw);
            this.depthInput.UpdateInputState(playerInputHandler.depthControlRaw);
            this.walkingButton.UpdateInputState(playerInputHandler.walkingButton);
            this.groundRollButton.UpdateInputState(playerInputHandler.groundRollButton);
            this.strafingButton.UpdateInputState(playerInputHandler.strafingButton);
            this.fireButton.UpdateInputState(playerInputHandler.fireButton);
            this.jumpButton.UpdateInputState(playerInputHandler.jumpButton);
        }
    }
}
