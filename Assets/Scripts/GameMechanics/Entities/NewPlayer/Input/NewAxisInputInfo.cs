using Assets.Scripts.GameMechanics.Entities.NewPlayer.Input.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewAxisInputInfo
    {
        public float rawAxis = 0f;
        public float interpolatedAxis = 0f;

        public void ApplyValues(NewAxisInputInfo other)
        {
            this.rawAxis = other.rawAxis;
            this.interpolatedAxis = other.interpolatedAxis;
        }

        public void DismissAxisStateInfo()
        {
            this.rawAxis = 0f;
            this.interpolatedAxis = 0f;
        }

        public void UpdateInputState(float axis)
        {
            this.rawAxis = axis;
            this.interpolatedAxis = Mathf.Lerp(this.interpolatedAxis,
                this.rawAxis,
                0.25f * Time.deltaTime / Time.fixedDeltaTime);
        }
    }
}
