using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewButtonInputInfo
    {
        public bool currentButtonState = false;
        public bool buttonDown = false;
        public bool buttonUp = false;

        public void ApplyValues(NewButtonInputInfo other)
        {
            this.currentButtonState = other.currentButtonState;
            this.buttonDown = other.buttonDown;
            this.buttonUp = other.buttonUp;
        }

        public void DismissUpAndDownStateInfo()
        {
            this.buttonDown = false;
            this.buttonUp = false;
        }

        public void UpdateInputState(
            bool currentState)
        {
            if (this.currentButtonState == false && currentState == true)
            {
                this.buttonDown = true;
            }
            else if (this.currentButtonState == true && currentState == false)
            {
                this.buttonUp = true;
            }
            this.currentButtonState = currentState;
        }
    }
}