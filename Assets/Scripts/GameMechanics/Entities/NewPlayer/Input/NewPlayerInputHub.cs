using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Entities.NewCamera;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Input
{
    public class NewPlayerInputHub : MonoBehaviour
    {
        protected NewPlayerInputHandler playerInputHandler;

        public NewPlayerController playerController;

        protected NewPlayerInputState previousInputState { get; set; }
            = new();
        protected NewPlayerInputState currentInputState { get; set; }
            = new();

        public NewButtonInputInfo jumpButton => this.currentInputState.jumpButton;
        public NewAnalogInputInfo moveInput => this.currentInputState.moveInput;
        public NewAxisInputInfo depthInput => this.currentInputState.depthInput;

        private void Start()
        {
            this.playerInputHandler = FindObjectOfType<NewPlayerInputHandler>();
            this.playerController = FindObjectOfType<NewPlayerController>();
        }

        public void DismissStatesFixedUpdate()
        {
            this.currentInputState.DismissButtonStates();
            this.previousInputState.DismissButtonStates();
        }

        private void FixedUpdate()
        {
            this.previousInputState.ApplyStateValues(
                this.currentInputState);
            this.currentInputState.UpdateInputState(
                this.playerInputHandler);
        }
    }
}
