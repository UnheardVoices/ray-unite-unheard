using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Params
{
    [Serializable]
    public class NewPlayerSurfaceParams
    {
        [Header("Transition")]

        [Tooltip("Height where transition begin.")]
        public float transitionHeight = 1.3f;

        [Tooltip("Duration of transition.")]
        public float transitionDuration;

        [Tooltip("Rotation with transition.")]
        public float transitionRotation;

        [Tooltip("Curve of transition rotation.")]
        public AnimationCurve rotationCurve;

        [Tooltip("Curve of transition.")]
        public AnimationCurve transitionCurve;

        [Range(0f, 1f)]
        public float transitionInterpolation = 0.5f;

        [Header("Movement")]
        public float surfaceVelocity = 0.11f;
        public float surfaceAcceleration = 0.005f;
        public float surfaceDrag = 0.012f;
        public float moveDrag = 0.0003f;
        public float heightBuffor = 0.15f;
    }
}
