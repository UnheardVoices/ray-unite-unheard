﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Params
{
    [Serializable]
    public class NewPlayerBaseParams
    {
        [Range(0f, 1f)]
        public float rotationInterpolation = 0.25f;
    }
}
