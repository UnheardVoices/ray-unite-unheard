using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Params
{
    [Serializable]
    public class NewPlayerEffectsParams
    {
        public GameObject landingPuff;
    }
}
