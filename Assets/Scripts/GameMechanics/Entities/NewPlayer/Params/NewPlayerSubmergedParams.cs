using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Params
{
    [Serializable]
    public class NewPlayerSubmergedParams
    {
        [Header("Transition")]
        public float transitionHeight = 2f;
        public AnimationCurve transitionCurve;
        public float animationDiveDuration = 0.79f;
        [Range(0f, 1f)]
        public float transitionInterpolation = 0.5f;

        [Header("Movement")]
        public float submergedVelocity = 0.21f;
        public float submergedAcceleration = 0.012f;
        public float submergedDrag = 0.007f;
        public float submergedRotation = 35f;
    }
}
