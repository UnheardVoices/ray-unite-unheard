using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Params
{
    [Serializable]
    public class NewPlayerParams
    {
        public NewPlayerBaseParams baseParams = new();
        [Header("Water Params")]
        public NewPlayerSurfaceParams surfaceParams = new();
        public NewPlayerSubmergedParams submergedParams = new();
        public NewPlayerEffectsParams effectsParams = new();
    }
}
