using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation
{
    public abstract class NewPlayerAnimationState
    {
        protected NewAnimationController controller;
        protected string initialAnimatorState;

        protected string currentAnimatorState;
        protected string finalAnimatorState;
        protected string[] animationSteps;
        protected int animationStep;
        protected int animationCountSteps => this.animationSteps.Length;
        
        public NewPlayerAnimationState(NewAnimationController controller, string initialAnimatorState)
        {
            this.controller = controller;
            this.initialAnimatorState = initialAnimatorState;
            InitializeAnimation();
        }

        protected abstract void InitializeAnimation();

        public abstract void GameplayUpdate();

        public bool IsStateEquals(string animatorState)
        {
            return this.finalAnimatorState == animatorState;
        }
    }
}