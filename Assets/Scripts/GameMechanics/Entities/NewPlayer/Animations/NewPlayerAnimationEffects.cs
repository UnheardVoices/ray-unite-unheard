using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation
{
    public static class AnimationStateActionHelper
    {
        public static void ExecuteOnAnimationEnd(
            NewAnimationController animationController,
            string animationStateName,
            Action action)
        {
            if (animationController.IsAnimationStateEquals(
                  animationStateName) &&
                  animationController.HasAnimationEnded())
            {
                action.Invoke();
            }
        }
    }

    public class NewPlayerAnimationEffects
    {
        protected NewAnimationController playerAnimationController;

        public NewPlayerAnimationEffects(
            NewAnimationController playerAnimationController
            )
        {
            this.playerAnimationController =
                playerAnimationController;
        }

        public void OnCurrentAnimation(string animationStateName,
            Action<float> onAnimationAction)
        {
            if (this.playerAnimationController.IsAnimationStateEquals(animationStateName))
            {
                onAnimationAction.Invoke(
                    this.playerAnimationController.GetAnimationNormalizedTime());
            }
        }

        public void OnCurrentAnimation(Action<float> onAnimationAction)
        {
            onAnimationAction.Invoke(
                this.playerAnimationController.GetAnimationNormalizedTime());
        }

        public void OnCurrentAnimationWithCondition(string animationStateName,
            bool condition,
            Action<float> onAnimationAction)
        {
            if (condition)
            {
                if (this.playerAnimationController.IsAnimationStateEquals(animationStateName))
                {
                    onAnimationAction.Invoke(
                        this.playerAnimationController.GetAnimationNormalizedTime());
                }
            }
        }

        public void OnAnimationEnd(string animationStateName,
            Action onAnimationAction)
        {
            AnimationStateActionHelper.
                ExecuteOnAnimationEnd(this.playerAnimationController,
                animationStateName,
                onAnimationAction);
        }

        public void OnLedgeToStandAnimation(Action<float> onAnimationAction)
        {
            if (this.playerAnimationController.IsAnimationStateEquals(
                RaymanAnimations.ClimbingFromHangingOnLedgeStateName()))
            {
                onAnimationAction.Invoke(
                    this.playerAnimationController.GetAnimationNormalizedTime());
            }
        }

        public void OnLedgeToStandAnimationEnd(Action onAnimationAction)
        {
            //if (this.playerAnimationController.IsAnimationStateEquals(
            //    RaymanAnimations.ClimbingFromHangingOnLedgeStateName()))
            //{
                if (this.playerAnimationController.HasAnimationEnded())
                {
                    onAnimationAction.Invoke();
                }
            //}
        }

        public void OnTransition(Action onAnimationAction)
        {
            if (this.playerAnimationController.IsInTransition())
            {
                onAnimationAction.Invoke();
            }
        }
    }
}
