using Assets.Scripts.Animations.Models;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.States
{
    public class IdleAnimationState : NewPlayerAnimationState
    {
        public static string IdleAnimationStateName = RaymanAnimations.IdleAnimationStateName();

        public IdleAnimationState(NewAnimationController controller, string initialAnimatorState) : base(controller, initialAnimatorState)
        {
            this.finalAnimatorState = IdleAnimationStateName;
        }
        protected override void InitializeAnimation()
        {
            throw new System.NotImplementedException();
        }

        public override void GameplayUpdate()
        {
            throw new System.NotImplementedException();
        }
    }
}
