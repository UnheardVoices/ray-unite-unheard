using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.Timer
{
    public class SimpleTimer
    {
        public SimpleTimerEffects timerEffects;
        public float currentTime => this.time;
        public bool isTicking => this.isStarted;

        private float time;
        private bool isStarted;

        public SimpleTimer(bool isStarted = false, float time = 0f)
        {
            this.isStarted = isStarted;
            this.time = time;
            this.timerEffects = new(this);
        }

        public void TimerUpdate(float deltaTime)
        {
            if (this.isStarted)
                this.time += deltaTime;
        }

        public void TimerReset()
        {
            this.time = 0f;
        }

        public void TimerPlayResume()
        {
            this.isStarted = true;
        }

        public void TimerPause()
        {
            this.isStarted = false;
        }

        public void TimerStop()
        {
            this.time = 0f;
            this.isStarted = false;
        }
    }
}