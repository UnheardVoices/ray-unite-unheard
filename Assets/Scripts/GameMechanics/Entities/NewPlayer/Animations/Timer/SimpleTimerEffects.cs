using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.Timer
{
    public class SimpleTimerEffects
    {
        protected SimpleTimer timer;

        public SimpleTimerEffects(SimpleTimer timer)
        {
            this.timer = timer;
        }

        public void OnTimeBiggerOrEqualsThan(float time, Action<float> onTimeAction)
        {
            if (this.timer.currentTime >= time)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }

        public void OnTimeBiggerThan(float time, Action<float> onTimeAction)
        {
            if (this.timer.currentTime > time)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }

        public void OnTimeLessOrEqualsThan(float time, Action<float> onTimeAction)
        {
            if (this.timer.currentTime <= time)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }

        public void OnTimeLessThan(float time, Action<float> onTimeAction)
        {
            if (this.timer.currentTime < time)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }

        public void OnTimerResumed(Action<float> onTimeAction)
        {
            if (this.timer.isTicking)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }

        public void OnTimerStoppedOrPaused(Action<float> onTimeAction)
        {
            if (!this.timer.isTicking)
            {
                onTimeAction.Invoke(this.timer.currentTime);
            }
        }
    }
}
