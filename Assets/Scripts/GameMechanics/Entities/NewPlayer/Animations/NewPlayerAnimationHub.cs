using Assets.Scripts.GameMechanics.Entities.NewPlayer.Contexts.Intentionals;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation
{
    public enum PlayerAnimationState
    {
        Initial,
        Transition,
        Final
    }

    public class NewPlayerAnimationHub
    {
        public Vector3 intentionalAnimation;

        protected NewAnimationController animationController;
        protected NewPlayerController playerController;
        protected Animator animator;

        public NewPlayerAnimationHub(NewAnimationController animationController,
            NewPlayerController playerController)
        {
            this.animationController = animationController;
            this.playerController = playerController;
            this.animator = animationController.GetAnimator();
        }

        public void SetupIntentionalAnimation(Vector3 intentionalAnimation)
        {
            this.intentionalAnimation = intentionalAnimation;
        }

        public void SetupIntentionalAnimation(AnimationIntentional intentional)
        {
            this.intentionalAnimation = intentional.direction * intentional.magnitude;
        }

        public void ManualFixedUpdate()
        {
            this.animator.SetFloat("InputMagnitude",
                this.intentionalAnimation.magnitude);
            this.animator.SetFloat("VerticalVelocity",
                this.playerController.controllerContext.kinematics.velocity.y);
        }

        public void GroundSequence()
        {
            this.animator.SetBool("Ground", true);
            this.animator.SetBool("Helicopter", false);
            this.animator.SetBool("Ledge", false);
            this.animator.SetBool("Water", false);
        }

        public void JumpSequence()
        {
            this.animator.SetTrigger("Jump");
        }

        public void FreeFallSequence()
        {
            this.animator.SetBool("Ground", false);
        }

        public void HelicopterSequence(bool value)
        {
            this.animator.SetBool("Helicopter", value);
        }

        public void LedgeGrabSequence(bool value)
        {
            this.animator.SetBool("Ledge", value);
            this.animator.SetBool("Helicopter", false);
        }

        public void WaterSequence()
        {
            this.animator.SetBool("Dive", false);
            this.animator.SetBool("Water", true);
            this.animator.SetBool("Ground", false);
            this.animator.SetBool("Helicopter", false);
        }

        public void Diving(bool value)
        {
            this.animator.SetBool("Dive", value);
        }
    }

    public class AnimationHelper
    {
        public bool IsAnimationEnded(string animationStateName,
            NewAnimationController animationController)
        {
            if (animationController.IsAnimationStateEquals(animationStateName))
            {
                return animationController.HasAnimationEnded();
            }
            else
            {
                return true;
            }
        }
    }
}
