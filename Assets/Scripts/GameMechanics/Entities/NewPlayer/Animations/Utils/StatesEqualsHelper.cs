using Assets.Scripts.Animations.Models;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation.Utils
{
    public static class AnimEqualsHelper
    {
        public static NewAnimationController controller;

        #region NormalGround

        public static bool IsIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.IdleAnimationStateName());
        }

        public static bool IsSlowWalk()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WalkingCycle1StateName());
        }

        public static bool IsWalk()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WalkingCycle2StateName());
        }

        public static bool IsWalkRun()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WalkingRunningCycle3StateName());
        }

        public static bool IsRun()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RunningAnimationStateName());
        }

        #endregion

        #region Air

        public static bool IsFreeFall()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.FallingAnimationStateName());
        }

        public static bool IsHelicopter()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.HelicopterStateName());
        }

        public static bool IsJumping()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.JumpStartBounceFromGroundAnimationStateName());
        }

        //public static bool IsRunJumping()
        //{
        //    return controller.IsAnimationStateEquals(RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName());
        //}

        #endregion

        #region Strafe

        public static bool IsStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingIdleStateName());
        }

        public static bool IsFwdStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingForwardStateName());
        }

        public static bool IsFwdRightStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingForwardRightStateName());
        }

        public static bool IsRightStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingRightStateName());
        }

        public static bool IsBckRightStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingBackwardsRightStateName());
        }

        public static bool IsBckStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingBackwardsStateName());
        }

        public static bool IsBckLeftStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingBackwardsLeftStateName());
        }

        public static bool IsLeftStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingLeftStateName());
        }

        public static bool IsFwdLeftStrafe()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingForwardLeftStateName());
        }

        #endregion

        #region Roof

        public static bool IsRoofIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RoofHangingIdleStateName());
        }

        public static bool IsRoofFwd()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RoofHangingGoingForwardStateName());
        }

        #endregion

        #region Wall

        public static bool IsWallIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingIdleStateName());
        }

        public static bool IsWallUp()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingUpStateName());
        }

        public static bool IsWallUpRight()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingUpRightStateName());

        }

        public static bool IsWallRight()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingRightStateName());

        }

        public static bool IsWallDnRight()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingDownRightStateName());;

        }

        public static bool IsWallDn()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingDownStateName());

        }

        public static bool IsWallDnLeft()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingDownLeftStateName());

        }

        public static bool IsWallLeft()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingLeftStateName());

        }

        public static bool IsWallUpLeft()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.WallClimbingUpLeftStateName());

        }

        #endregion

        #region Roll

        public static bool IsRollFwd()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingRollForwardStateName());
        }

        public static bool IsRollRight()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingRollRightStateName());
        }

        public static bool IsRollBck()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingRollBackwardsStateName());
        }

        public static bool IsRollLeft()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingRollLeftStateName());
        }

        #endregion

        #region Ledge

        public static bool IsLedge()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.HangingOnLedgeStateName());
        }

        #endregion

        #region Transitions

        #region Ground

        public static bool IsIdleToWalk()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName());
        }

        public static bool IsIdleToRun()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
        }

        public static bool IsRunToIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName());
        }

        public static bool IsWalkToIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
        }

        #endregion

        #region Landing

        public static bool IsHelicopterLandToIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName());
        }

        public static bool IsHelicopterLandToRun()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
        }

        public static bool IsLandToIdle()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToGroundIdleStateName());
        }

        public static bool IsLandToWalk()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToWalkingCycle2StateName());
        }

        public static bool IsLandToRun()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName());
        }

        #endregion

        #region Roof

        public static bool IsRoofEnterFwd()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RoofHangingStartingGoingForwardStateName());
        }

        public static bool IsRoofExitFwd()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RoofHangingEndingGoingForwardStateName());
        }

        public static bool IsRoofExit()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.RoofHangingLeavingStateName());
        }

        #endregion

        #region Strafe

        public static bool IsStrafingEnter()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingEnterFromIdleGroundStateName());
        }

        public static bool IsStrafingExit()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StrafingExitIntoIdleGroundStateName());
        }

        #endregion

        #region Ledge

        public static bool IsLedgeEnter()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.StartingHangingOnLedgeFromFallingStateName());
        }

        public static bool IsLedgeExit()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.ClimbingFromHangingOnLedgeStateName());
        }

        #endregion

        #region Air

        public static bool IsAirRoll()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
        }

        public static bool IsAirEnterHelicopter()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName());
        }

        public static bool IsAirEnterFall()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.JumpStartBounceFromGroundAnimationStateName());
        }

        #endregion


        #region Swing

        public static bool IsSwingBack()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName());
        }

        public static bool IsSwingFront()
        {
            return controller.IsAnimationStateEquals(RaymanAnimations.LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName());
        }

        #endregion

        #endregion
    }

    public static class StatesEqualsHelper
    {

    }

    public static class GroupsEqualsHelper
    {

    }

    public static class TypeEqualsHelper
    {

    }
}