using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer
{
    public class NewPlayerSounds
    {
        public AudioSource jumpSound;
        public AudioSource helicopterSound;

        //future possible examples of sounds effects
        //public AudioSource phewLedgeSound;
        //public AudioSource phewJumpSound;

        public NewPlayerSounds(AudioSource jumpSound,
                            AudioSource helicopterSound)
        {
            this.jumpSound = jumpSound;
            this.helicopterSound = helicopterSound;
        }
    }
}
