using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Entity;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using Assets.Scripts.PlayerMechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Input;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation;
using Assets.Scripts.GameMechanics.Entities.NewCamera;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Params;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer
{
    public class NewPlayerController :
        NewEntityController<NewPlayerMovementRule, NewPlayerContext>
    {
        public NewPlayerParams playerParams = new();

        protected override void BehaviourStart()
        {
            //SetCurrentRule(new NewPlayerMovementGroundRule(this));

            base.BehaviourStart();

            var kinematics = new Kinematics();

            var platformerHub = new NewPlatformerCollisionHub(this.transform, kinematics, this);
            //var MovementInputWrapper = FindObjectOfType<PlayerMovementInput>();

            //New Inputs
            var NewPlayerInputHandler = FindObjectOfType<NewPlayerInputHandler>();
            var NewPlayerInputHub = FindObjectOfType<NewPlayerInputHub>();
            var NewPlayerInputEffects = new NewPlayerInputEffects(NewPlayerInputHub);

            var animationContrl = FindObjectOfType<NewAnimationController>();

            this.controllerContext = new()
            {
                subject = this.transform,
                platformerCollisionHub = platformerHub,
                playerAnimationController =
                animationContrl,
                kinematics = kinematics,
                playerSounds =
                new NewPlayerSounds(
                    jumpSound: GetComponents<AudioSource>()[1],
                    helicopterSound: GetComponents<AudioSource>()[0]),
                platformerCollisionEffects =
                new NewPlatformerCollisionEffects(platformerHub),
                playerAnimationEffects = new NewPlayerAnimationEffects(animationContrl),
                playerInputHub = NewPlayerInputHub,
                playerInputEffects = NewPlayerInputEffects,
                playerInputHandler = NewPlayerInputHandler,
                cameraController = FindObjectOfType<NewCameraController>(),
                playerAnimationHub = new(animationContrl, this),
                playerRulesTransitions = new(this),
                dynamicLedgeGenerator = new(this.transform),
                playerParams = this.playerParams,
            };
            this.controllerContext.playerRulesTransitions.EnterGround(Vector3.zero);
        }

        protected override void GameplayFixedUpdate()
        {
            this.controllerContext.playerAnimationController.ManualGameplayFixedUpdate();
            if (IsValidMovementRule())
            {
                this.controllerContext.platformerCollisionHub.GameplayFixedUpdate(
                    this.transform, this.controllerContext.kinematics);
                base.GameplayFixedUpdate();
                this.controllerContext.kinematics.GameplayFixedUpdateSubject(this.transform);
                this.controllerContext.playerInputHub.DismissStatesFixedUpdate();
            }
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            this.controllerContext.playerAnimationController.ManualGameplayUpdate(deltaTime);
            if (IsValidMovementRule())
            {
                //this.componentsUpdateInput.cameraOrientedForward =
                //    this.cameraScriptsHandle.cameraOrientedForward;
                base.GameplayUpdate(deltaTime);
            }
        }

        private bool IsValidMovementRule()
        {
            return
                this.currentRule != null &&
                this.currentRule.ruleId != null &&
                !this.currentRule.ruleId.Equals(
                    /*EmptyNewPlayerMovementRule.RULE_ID*/"EMPTY");
        }
    }
}