using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using Assets.Scripts.PlayerMechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Rules.Transitions;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Input;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Animation;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.NewCamera;
using Assets.Scripts.GameMechanics.DynamicLedgeGenerator;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Params;

namespace Assets.Scripts.GameMechanics.Entities.NewPlayer
{
    public class NewPlayerContext : NewEntityContext
    {
        #region Legacy

        //legacy DO NOT USE!!!

        private PlayerInputHub legacyPlayerInputHub;
        private PlayerInputHubStateHub legacyPlayerInputHubStateHub;
        private PlayerInputEffects legacyPlayerInputEffects;
        private PlayerMovementInput legacyPlayerMovementInputWrapper;
        private PlayerInputWrapperEffects playerInputWrapperEffects;
        //public CameraScriptsHandle legacyCameraScriptsHandle;
        #endregion

        //new
        public NewPlayerInputHandler playerInputHandler; //old name - playerInputHub
        public NewPlayerInputHub playerInputHub; //old name - playerInputHubStateHub
        public NewPlayerInputEffects playerInputEffects;

        public NewPlatformerCollisionHub platformerCollisionHub;
        public NewPlatformerCollisionEffects platformerCollisionEffects;

        public NewAnimationController playerAnimationController;
        public NewPlayerAnimationEffects playerAnimationEffects;
        public NewPlayerAnimationHub playerAnimationHub;

        public NewCameraController cameraController;

        public NewPlayerSounds playerSounds;

        public DynamicLedge dynamicLedgeGenerator;

        public NewPlayerParams playerParams;

        public NewPlayerRulesTransitions playerRulesTransitions { get; set; }
    }
}