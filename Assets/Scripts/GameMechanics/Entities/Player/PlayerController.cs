﻿using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Entity.Physics;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player;
using Assets.Scripts.GameMechanics.Entities.Player.Handles;
using Assets.Scripts.GameMechanics.Entities.Player.Params;
using Assets.Scripts.GameMechanics.Entities.Player.Rules;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.NewPlayerMechanics
{
    public class PlayerController :
        NewEntityController<PlayerMovementRule, PlayerContext>
    {
        public PlayerParams playerParams = new();

        protected override void BehaviourStart()
        {
            base.BehaviourStart();

            var platformerCollisionHub = new PlatformerCollisionHub();
            var playerAnimationController = FindObjectOfType<AnimationController>();
            var playerMovementInputWrapper = FindObjectOfType<PlayerMovementInput>();
            var kinematics = new Kinematics();
            var playerInputHubStateHub = GetComponent<PlayerInputHubStateHub>();
            var cameraScriptsHandle = FindObjectOfType<CameraScriptsHandle>();

            this.controllerContext = new()
            {
                subject = this.transform,
                kinematics = kinematics,
                playerController = this,
                playerInputHub = FindObjectOfType<PlayerInputHub>(),
                platformerCollisionHub = platformerCollisionHub,
                cameraScriptsHandle = cameraScriptsHandle,
                playerMovementInputWrapper = playerMovementInputWrapper,
                playerAnimationController = playerAnimationController,
                legacyPlayerMovementInput = playerMovementInputWrapper,
                legacyPlayerMovementStateInfo = FindObjectOfType<PlayerMovementStateInfo>(),
                legacyPlayerMovementMetrics = FindObjectOfType<PlayerMovementMetrics>(),
                legacyRayCollider = GetComponent<RayCollider>(),
                legacyPlayerGroundRollingAspect = GetComponent<PlayerGroundRollingAspect>(),
                playerAudioHandles =
                    (new PlayerAudioHandles())
                        .PropagateWithAudioSourcesRefs(this.transform),
                platformerCollisionEffects = new PlatformerCollisionEffects(platformerCollisionHub),
                playerAnimationEffects = new PlayerAnimationEffects(
                                            playerAnimationController),
                playerInputWrapperEffects = new PlayerInputWrapperEffects(
                                                playerMovementInputWrapper,
                                                this.transform
                                            ),
                playerRulesTransitions = new PlayerRulesTransitions(this),
                playerAnimatedModel = new PlayerAnimatedModelHandle(
                                        playerAnimatedModelTransform:
                                        GetComponentInChildren<Animator>().transform),
                playerInputHubStateHub = playerInputHubStateHub,
                legacyPlayerShootingAspect = GetComponent<PlayerShootingAspect>(),
                playerInputEffects = new PlayerInputEffects(playerInputHubStateHub),
                cameraOrientedForward = cameraScriptsHandle.cameraOrientedForward,
                playerParams = this.playerParams
            };

            this.controllerContext.playerRulesTransitions.EnterAir(Vector3.zero);
        }

        protected override void GameplayFixedUpdate()
        {
            var args = this.controllerContext;

            args.playerAnimationController.ManualGameplayFixedUpdate();
            if (IsValidMovementRule())
            {
                args.platformerCollisionHub.GameplayFixedUpdate(
                    args.subject, args.kinematics);
                base.GameplayFixedUpdate();
                args.kinematics.GameplayFixedUpdateSubject(this.transform);
                args.playerInputHubStateHub.DismissStatesFixedUpdate();
            }
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            var args = this.controllerContext;

            args.playerAnimationController.ManualGameplayUpdate(deltaTime);
            if (IsValidMovementRule())
            {
                args.cameraOrientedForward =
                    args.cameraScriptsHandle.cameraOrientedForward;
                base.GameplayUpdate(deltaTime);
            }
        }

        private bool IsValidMovementRule()
        {
            return
                this.currentRule != null &&
                this.currentRule.ruleId != null &&
                !this.currentRule.ruleId.Equals(
                    EmptyNewPlayerMovementRule.RULE_ID);
        }

        public void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {
            var args = this.controllerContext;

            var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
            var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();
     
            //this.controllerContext.playerAnimatedModel.forward = 
            //    toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.controllerContext.playerAnimatedModel.forward));

            this.controllerContext.legacyPlayerMovementMetrics
                .OrientatePlayerAccordingToPortals(fromPortal, toPortal);
            this.controllerContext.legacyPlayerGroundRollingAspect
                .OrientatePlayerAccordingToPortals(fromPortal, toPortal);
            this.currentRule.OrientatePlayerAccordingToPortals(fromPortal, toPortal);
            args.cameraOrientedForward =
                args.cameraScriptsHandle.cameraOrientedForward;
            args.playerInputHubStateHub.DismissStatesFixedUpdate();

            //this.transform.forward = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.transform.forward));

            //this.controllerContext.playerAnimatedModel.forward =
            //    toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.controllerContext.playerAnimatedModel.forward));
        }
    }
}
