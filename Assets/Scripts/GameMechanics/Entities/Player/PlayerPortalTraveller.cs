﻿using Assets.Scripts.Animations;
using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player
{
    public class PlayerPortalTraveller : PortalTraveller
    {
        protected override bool CheckPortalCollision(out RaycastHit portalThresholdHit)
        {
            //return PhysicsRaycaster.Raycast(this.transform.position + Vector3.up, -GetComponentInChildren<AnimationController>().transform.forward,
            //    out portalThresholdHit,
            //    0.5f, Layers.portalsLayerMask, debug: true, debugColor: Color.yellow);
            return Raycaster.CylinderRaycasterSingle(
                   this.transform.position + Vector3.up, Vector3.forward,
                    3f, 0.5f, 5, 100, out portalThresholdHit, Layers.portalsLayerMask, debug: true, debugColor: Color.yellow);
        }

        protected override void OnPortalTravel(Transform fromPortal, Transform toPortal)
        {
            GetComponent<PlayerController>().OrientatePlayerAccordingToPortals(fromPortal, toPortal);
        }
    }
}
