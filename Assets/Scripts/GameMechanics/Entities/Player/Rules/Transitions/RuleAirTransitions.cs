﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Rules;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class RuleAirTransitionsBehaviours
    {
        public static void AirToGroundTransition(
            PlayerController playerController)
        {
            var args = playerController.controllerContext;

            // allow falling animation to be interrupted
            args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
            args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
            args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
            args.subject.GetComponent<RuleGround>().SetTransitionData(
                landing: true,
                withHelicopterFlag: args.legacyPlayerMovementStateInfo.isHelicopter);

            args.legacyPlayerMovementStateInfo.movementVelocity =
                args.subject.InverseTransformDirection(
                       args.kinematics.velocity
                    );
            args.kinematics.velocity = Vector3.zero;
            playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(playerController));
        }
    }


    public static class RuleAirTransitions
    {
        public static AirContext GetFreeFallingAirContext(
            Vector3 initialIntentionalMovementDirection
            )
        {
            return new()
            {
                initialVelocity = Vector3.zero,
                initialIntentionalMovementDirection 
                    = initialIntentionalMovementDirection,
                isJumping = false
            };
        }

        public static AirContext 
            GetAirContextFromLegacy(
            PlayerMovementRuleAirArgs playerMovementRuleAirArgs)
        {
            return new()
            {
                initialVelocity = 
                    playerMovementRuleAirArgs.initialVelocity,
                initialIntentionalMovementDirection
                    = playerMovementRuleAirArgs
                        .initialIntentionalMovementDirection,
                isJumping = playerMovementRuleAirArgs.isJumping
            };
        }
    }
}
