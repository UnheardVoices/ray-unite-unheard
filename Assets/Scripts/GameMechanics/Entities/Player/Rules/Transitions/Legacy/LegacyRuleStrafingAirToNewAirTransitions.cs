﻿using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class
        LegacyRuleStrafingAirToNewAirTransitions
    {
        public static void EnterRuleAirFromRuleStrafingAir(
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            PlayerMovementMetrics playerMovementMetrics,
            PlayerController playerController)
        {
            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;

            playerController.SetCurrentRule(
                RuleAirFactory.GetFreeFallingAirRule(
                    playerController,
                    playerMovementMetrics.intentionalMovingDirection
                    ));
        }
    }
}
