﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Factories;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy
{
    public static class
        LegacyRuleWallClimbingToNewAirTransitions
    {
        public static void EnterRuleAirJumpingFromRuleWallClimbing(
            PlayerMovementStateInfo legacyPlayerMovementStateInfo,
            Transform transform,
            GameObject animatedPlayerPart,
            RayCollider rayCollider,
            AnimationController animationController,
            PlayerMovementMetrics legacyPlayerMovementMetrics,
            PlayerController playerController)
        {
            transform.position = animatedPlayerPart.transform.position;
            animatedPlayerPart.transform.position = transform.position;
            rayCollider.DisableWallClimbCollisionDetectionForMilliseconds(400);

            // allow anims to be interrupted by whatever next rule will bring
            // since these anims from wall climbing are no longer relevant from this point on
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingIdleStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingUpStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingDownStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingLeftStateName(), 1);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingRightStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingUpRightStateName(), 1);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingUpLeftStateName(), 1);

            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingDownRightStateName(), 1);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.WallClimbingDownLeftStateName(), 1);


            legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.LEGACY_EMPTY_RULE;
            legacyPlayerMovementStateInfo.isJumping = true;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            legacyPlayerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

            playerController.SetCurrentRule(
                RuleAirFactory.GetJumpingAirRule(
                    playerController,
                    legacyPlayerMovementMetrics.intentionalMovingDirection
                    ));
        }
    }
}
