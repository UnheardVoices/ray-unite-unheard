﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Args;
using Assets.Scripts.NewPlayerMechanics;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions
{
    public static class EntityRulesTransitionsValidator
    {
        public static void ValidateRule<T, U>(
            NewEntityController<T,U> entityController, Type ruleType)
            where T : NewEntityRule<U>
            where U : NewEntityContext, new()
        {
            if (!entityController.GetCurrentRuleClassName()
                    .Equals(ruleType.Name))
            {
                throw new InvalidOperationException(
                    "Invalid rules transition access from " +
                        entityController.GetCurrentRuleClassName()
                        + " that is for " + ruleType.Name
                    );
            }
        }
    }

    public class PlayerRulesTransitions
    {
        protected PlayerController playerController;

        protected void ValidateRule(Type ruleType)
        {
            EntityRulesTransitionsValidator
                .ValidateRule(
                    this.playerController, ruleType);
        }

        public PlayerRulesTransitions(
            PlayerController playerController)
        {
            this.playerController = playerController;
        }

        public void EnterAir(Vector3 initialIntentionalMovementDirection)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleAir.RULE_ID,
                RuleAirTransitions
                    .GetFreeFallingAirContext(
                        initialIntentionalMovementDirection));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleAir(this.playerController));
        }

        public void EnterGround()
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(EmptyNewPlayerMovementRule.RULE_ID,
                EmptyPlayerTransitions.GetEmptyContext());

            this.playerController.SetCurrentRule(
                new EmptyNewPlayerMovementRule(this.playerController));

            RuleAirTransitionsBehaviours.AirToGroundTransition(
                this.playerController);
        }

        public void PrepareTransition()
        {
            this.playerController.controllerContext.kinematics.velocity
                = Vector3.zero;
        }

        public void EnterAirFromLegacy(
            PlayerMovementRuleAirArgs playerMovementRuleAirArgs)
        {
            PrepareTransition();

            this.playerController.controllerContext.contextsHolder
                .SetContext(PlayerMovementRuleAir.RULE_ID,
                RuleAirTransitions
                    .GetAirContextFromLegacy(
                        playerMovementRuleAirArgs));

            this.playerController.SetCurrentRule(
                new PlayerMovementRuleAir(this.playerController));
        }
    }
}
