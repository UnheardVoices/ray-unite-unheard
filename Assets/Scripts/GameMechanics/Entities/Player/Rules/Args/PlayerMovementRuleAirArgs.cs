﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Args
{
    public class PlayerMovementRuleAirArgsBuilder
    {
        private Vector3 initialVelocity = Vector3.zero;
        private Vector3 initialIntentionalMovementDirection = Vector3.zero;
        private bool isJumping = false;

        public PlayerMovementRuleAirArgsBuilder
            WithInitialVelocity(Vector3 initialVelocity)
        {
            this.initialVelocity = initialVelocity;
            return this;
        }

        public PlayerMovementRuleAirArgsBuilder
            WithInitialIntentionalMovementDirection(Vector3 initialIntentionalMovementDirection)
        {
            this.initialIntentionalMovementDirection =
                initialIntentionalMovementDirection;
            return this;
        }

        public PlayerMovementRuleAirArgsBuilder
            WithIsJumping(bool isJumping)
        {
            this.isJumping = isJumping;
            return this;
        }

        public PlayerMovementRuleAirArgs Build()
        {
            return new PlayerMovementRuleAirArgs(
                this.initialVelocity, this.initialIntentionalMovementDirection,
                this.isJumping);
        }
    }

    public class PlayerMovementRuleAirArgs
    {
        public Vector3 initialVelocity { get; }
        public bool isJumping { get; }

        public Vector3 initialIntentionalMovementDirection { get; }

        public PlayerMovementRuleAirArgs(
            Vector3 initialVelocity,
            Vector3 initialIntentionalMovementDirection,
            bool isJumping)
        {
            this.initialVelocity = initialVelocity;
            this.isJumping = isJumping;
            this.initialIntentionalMovementDirection =
                initialIntentionalMovementDirection;
        }
    }
}
