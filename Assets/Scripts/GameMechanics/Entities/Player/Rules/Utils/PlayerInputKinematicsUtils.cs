﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils
{
    public static class PlayerInputKinematicsUtils
    {
        public static void FadeOutHorizontalVelocityOnNoInput(
            PlayerContext args
            )
        {
            //throw new NotImplementedException();
            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeLowerThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    const float movementSmoothingInterpolation = 0.6f;
                    Vector3 velocity = args.kinematics.velocity;
                    Vector3 targetVelocity = new Vector3(0f, velocity.y, 0f);
                    args.kinematics.velocity =
                        Vector3.Lerp(velocity, targetVelocity, movementSmoothingInterpolation);
                });
        }

        public static void FadeInHorizontalVelocityOnInput(
            PlayerContext args,
            float maximumSpeed
            )
        {
            //throw new NotImplementedException();
            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeBiggerOrEqualThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    const float movementSmoothingInterpolation = 0.25f;
                    Vector3 translation =
                        wrappedPlayerMoveVector3 * maximumSpeed;
                    Vector3 velocity = args.kinematics.velocity;
                    Vector3 targetVelocity =
                        new Vector3(translation.x, velocity.y, translation.z);
                    args.kinematics.velocity =
                        Vector3.Lerp(velocity, targetVelocity, movementSmoothingInterpolation);
                    args.subject.forward = args.cameraOrientedForward;
                    args.playerAnimatedModel.forward = translation.normalized;
                });
        }
    }
}
