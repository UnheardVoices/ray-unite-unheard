﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Utils;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Contexts;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Utils;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public static class PlayerMovementRuleAirState
    {       
        public const string IS_JUMPING = "IS_JUMPING";
        public const string INTENTIONAL_MOVEMENT_DIRECTION = "INTENTIONAL_MOVEMENT_DIRECTION";
    }

    public static class PlayerMovementRuleAirRefs
    {
        public const string IS_HELICOPTER = "IS_HELICOPTER";
    }

    public static class PlayerMovementRuleAirEffects
    {
        public const string INITIAL_VELOCITY_EFFECT =
            "INITIAL_VELOCITY_EFFECT";
    }

    public class PlayerMovementRuleAir : PlayerMovementRule
    {
        public const string RULE_ID = "PLAYER_MOVEMENT_RULE_AIR";

        public PlayerMovementRuleAir(
            object controller) : base(RULE_ID, controller)
        {
        }

        public override void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {
            var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
            var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();

            var context = UseContext<AirContext>();
            var args = UseEntityContext();

            (Vector3 intentionalMovementDirection, Action<Vector3> SetIntentionalMovementDirection)
                = UseState<Vector3>(
                    PlayerMovementRuleAirState.INTENTIONAL_MOVEMENT_DIRECTION,
                    context.initialIntentionalMovementDirection);

            //this.states[PlayerMovementRuleAirState.INTENTIONAL_MOVEMENT_DIRECTION] =
            //    toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(
            //        (Vector3)this.states[PlayerMovementRuleAirState.INTENTIONAL_MOVEMENT_DIRECTION]));

            SetIntentionalMovementDirection(toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(intentionalMovementDirection)));
            var originalAnimatedForward = args.playerAnimatedModel.forward;
            var originalForward = args.subject.forward;

            //args.subject.forward = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(args.subject.forward));
            //args.playerAnimatedModel.forward = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(originalAnimatedForward));
            //args.subject.forward = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(originalForward));
            //args.playerAnimatedModel.forward = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(originalAnimatedForward));
            args.kinematics.velocity = toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(args.kinematics.velocity));
        }

        public override void OnPortalTravel()
        {
            // throw new NotImplementedException();
        }

        public override void GameplayFixedUpdate()
        {
            var args = UseEntityContext();
            var context = UseContext<AirContext>();

            const float playerInAirSpeed = 0.21f;
            const float gravityAcceleration = 0.012f;
            const float limitFallSpeed = 0.3f;
            const float limitFallSpeedHelicopter = 0.05f;

            RefObject<bool> isHelicopter =
                UseRef<bool>(PlayerMovementRuleAirRefs.IS_HELICOPTER, false);
            (bool isJumping, Action<bool> SetIsJumping) =
                UseState<bool>(PlayerMovementRuleAirState.IS_JUMPING,
                    context.isJumping);

            (Vector3 intentionalMovementDirection, Action<Vector3> SetIntentionalMovementDirection)
                = UseState<Vector3>(
                    PlayerMovementRuleAirState.INTENTIONAL_MOVEMENT_DIRECTION,
                    context.initialIntentionalMovementDirection);

            HandleShooting(args);

            UseEffect(PlayerMovementRuleAirEffects.INITIAL_VELOCITY_EFFECT, () =>
            {
                args.kinematics.velocity =
                    context.initialVelocity;
                args.platformerCollisionHub.GameplayFixedUpdate(
                    args.subject, args.kinematics);
                args.playerInputHubStateHub.DismissStatesFixedUpdate();

                if (isJumping)
                {
                    SetIsJumping(false);
                    
                    if (args.legacyPlayerMovementInput
                        .GetTranslation().magnitude > 0.7f)
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName(),
                            priority: 3);
                        args.playerParams.soundParams.airSounds.jumpingSounds
                            .runningJumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerAudioHandles.airAudioHandles.runningJumpOffTheGroundAudioSource);
                    }
                    else
                    {
                        args.playerAnimationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                            priority: 3);
                        args.playerParams.soundParams.airSounds.jumpingSounds
                            .standingJumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                args.playerAudioHandles.airAudioHandles.standingJumpOffTheGroundAudioSource);
                    }
                }
            });

            args.playerInputEffects.OnJumpButtonDown(() =>
            {
                isHelicopter.current = true;
                args.playerParams.soundParams
                    .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                        args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(),
                    priority: 4);
            });

            args.playerInputEffects.OnJumpButtonUp(() =>
            {
                if (isHelicopter.current)
                {
                    // if exiting helicopter case...
                    args.playerParams.soundParams.helicopterSounds.helicopterSound
                        .StopPlayingWithAudioSource(
                            args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource);
                    args.playerAnimationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.FallingAnimationStateName(), priority: 10);
                } else
                {
                    args.playerAnimationController.ChangeAnimationState(
                        RaymanAnimations.FallingAnimationStateName());
                }

                isHelicopter.current = false;
                //args.playerSounds.helicopterSound.Pause();               
            });

            args.playerInputWrapperEffects
                .OnMoveInputMagnitudeBiggerOrEqualThan(0.1f, (wrappedPlayerMoveVector3) =>
                {
                    var newVector = Vector3.ProjectOnPlane(
                        wrappedPlayerMoveVector3.normalized, Vector3.up)
                        .normalized;

                    SetIntentionalMovementDirection(newVector);
                    if (newVector.magnitude > 0.001f && Vector3.Angle(newVector, Vector3.up) > 80f && Vector3.Angle(newVector, Vector3.down) > 80f)
                    {
                        args.legacyPlayerMovementMetrics.intentionalMovingDirection = newVector;
                    }                   
                    
                });
            Debug.DrawRay(args.subject.transform.position, args.legacyPlayerMovementMetrics.intentionalMovingDirection * 100f, Color.red);

            args.platformerCollisionEffects.OnCollisionWithSteadyGroundWithVelocity(
                (collisionNormal, collisionPoint) =>
                {
                    args.playerRulesTransitions.EnterGround();
                });

            PlayerInputKinematicsUtils.FadeOutHorizontalVelocityOnNoInput(
                args);
            PlayerInputKinematicsUtils.FadeInHorizontalVelocityOnInput(
                args, playerInAirSpeed);

            args.kinematics.velocity = PlayerWallCollisionHelper
                .GetMovementVelocityConsideringWallCollisions(
                    args.subject.gameObject,
                    args.legacyRayCollider,
                    intentionalMovementDirection,
                    args.kinematics.velocity);

            args.kinematics.velocity = args.legacyRayCollider
                .AdjustVelocityIfIsHittingTheCeiling(
                   args.kinematics.velocity);

            if (!isHelicopter.current)
            {
                args.kinematics.velocity =
                    GravityUtils.AccelerateGravitationallyUpUntilLimitFallSpeed(
                        args.kinematics.velocity,
                        gravityAcceleration,
                        limitFallSpeed
                    );
            }
            else
            {
                args.kinematics.velocity =
                    new Vector3(
                        args.kinematics.velocity.x,
                        -limitFallSpeedHelicopter,
                        args.kinematics.velocity.z);
            }

            if (args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName()) &&
                  args.playerAnimationController.HasAnimationEnded())
            {
                if (args.kinematics.velocity.y < 0.1f)
                {
                    args.playerParams.soundParams.airSounds.jumpingSounds.airFlipRunningJumpSound
                        .PlayOnceWithAudioSource(
                            args.playerAudioHandles.airAudioHandles.airFlipRunningJumpAudioSource);
                    args.playerAnimationController
                        .ChangeAnimationState(
                        RaymanAnimations
                            .AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
                }
            }
            else if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName())
                    && args.playerAnimationController.HasAnimationEnded())
               )
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.FallingFromJumpFromRunningAnimationStateName());
            }
            else
                if ((args.kinematics.velocity.y < 0.1f &&
                    args.playerAnimationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!isHelicopter.current &&
                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&

                    !args.playerAnimationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()))
            {
                args.playerAnimationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if ((args.playerAnimationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  args.playerAnimationController.HasAnimationEnded()) ||
                  (args.playerAnimationController.GetCurrentAnimationState().Equals(RaymanAnimations.FallingAnimationStateName()))
                  && isHelicopter.current)
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                args.playerAnimationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.HelicopterStateName(), priority: 10);
            }

            //inputArgs.playerAnimationEffects
            //    .GetRuleAirEffects().OnJumpStartRunningBounceFromGroundEnded(() =>
            //    {
            //        if (inputArgs.kinematics.velocity.y < 0.1)
            //        {
            //            inputArgs.playerAnimationController.ChangeAnimationState(
            //                RaymanAnimations
            //                    .AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
            //        }
            //    });

            //inputArgs.playerAnimationEffects
            //    .GetRuleAirEffects().OnAirRollFromJumpStartRunningBounceFromGroundEnded(() =>
            //    {
            //        inputArgs.playerAnimationController.ChangeAnimationState(
            //            RaymanAnimations
            //               .FallingFromJumpFromRunningAnimationStateName());
            //    });

            //Action fallingAnimationAction = () =>
            //{
            //    inputArgs.playerAnimationController.ChangeAnimationState(
            //        RaymanAnimations.FallingAnimationStateName());
            //};

            //inputArgs.playerAnimationEffects
            //    .GetRuleAirEffects().OnJumpStartBounceFromGroundAnimation(
            //        (normalizedTime) => { 
            //            if (inputArgs.kinematics.velocity.y < 0.1f)
            //            {
            //                fallingAnimationAction.Invoke();
            //            }                        
            //        });

            //inputArgs.playerAnimationEffects
            //    .GetRuleAirEffects()
            //        .OnNOT_FallingFromJumpFromRunning_AND_AirRollFromJumpStartRunningBounceFromGround(
            //            (normalizedTime) =>
            //            {
            //                if (!isHelicopter)
            //                {
            //                    fallingAnimationAction.Invoke();
            //                }
            //            }
            //        );




            // <----------- COMPLETELY LEGACY BELOW ----------------->


            // completely legacy rules transitions, copy pasted for now directly
            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (args.legacyRayCollider.IsHittingGround()
                && args.kinematics.velocity.y <= 0)
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                // allow falling animation to be interrupted
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                args.subject.GetComponent<RuleGround>().SetTransitionData(
                    landing: true,
                    withHelicopterFlag: isHelicopter.current);

                args.kinematics.velocity = Vector3.zero;
                args.playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(args.playerController));

                return;
            }
            else if (args.legacyRayCollider
                .IsCollidingWithLedgeGrabCollider(out ledgeGrabHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                //args.playerSounds.helicopterSound.Pause();
                args.subject.GetComponent<RuleLedgeGrabbing>()
                    .ClearRuleState(ledgeGrabHit);

                args.kinematics.velocity = Vector3.zero;
                args.playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(args.playerController));

                return;
            }
            else if (args.legacyRayCollider
                .IsCollidingWithWallClimbObject(
                args.playerAnimatedModel.forward,
                args.playerAnimatedModel.up,
                args.playerAnimatedModel.right,
                out wallClimbHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                //args.playerSounds.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.subject.
                    GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);

                args.kinematics.velocity = Vector3.zero;
                args.playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(args.playerController));

                return;
            }
            else if (args.legacyRayCollider
                .IsHittingRoofHanging(out roofHangingHit))
            {
                args.playerParams.soundParams
                     .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                      args.playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.HelicopterStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), newPriority: 1);
                args.playerAnimationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);

                args.legacyPlayerMovementStateInfo
                    .currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                //args.playerSounds.helicopterSound.Pause();
                args.legacyPlayerMovementStateInfo.isHelicopter = false;
                args.subject
                    .GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);

                args.kinematics.velocity = Vector3.zero;
                args.playerController.SetCurrentRule(new EmptyNewPlayerMovementRule(args.playerController));

                return;
            }
            else if (!args.playerInputHub.jumpButton)
            {
                //inputArgs.legacyPlayerMovementStateInfo
                //    .playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }
    }
}
