﻿using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules.Input.Effects
{
    public class PlayerInputEffects
    {
        protected PlayerInputHubStateHub playerInputHubStateHub;

        public PlayerInputEffects(
            PlayerInputHubStateHub playerInputHubStateHub)
        {
            this.playerInputHubStateHub = playerInputHubStateHub;
        }

        public void OnJumpButtonDown(Action onInputAction)
        {
            if (this.playerInputHubStateHub.jumpButton.buttonDown)
            {
                onInputAction.Invoke();
            }
        }

        public void OnJumpButtonUp(Action onInputAction)
        {
            if (this.playerInputHubStateHub.jumpButton.buttonUp)
            {
                onInputAction.Invoke();
            }
        }
    }
}
