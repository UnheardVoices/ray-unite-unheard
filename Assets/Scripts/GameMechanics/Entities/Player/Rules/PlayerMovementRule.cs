﻿using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Rules;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Rules
{
    public abstract class PlayerMovementRule :
        NewEntityRule<PlayerContext>
    {
        protected bool IsInStrafingKindRule()
        {
            return false;
        }

        public virtual void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        {

        }

        protected void HandleShooting(
            PlayerContext args)
        {
            if (args.legacyPlayerShootingAspect.HasHandsToShoot()
                && args.legacyPlayerShootingAspect.CanTriggerShootingHand()
                && args.legacyPlayerShootingAspect.IsPressingShootingButtonInstantShot())
            {
                if (!IsInStrafingKindRule())
                {
                    args.legacyPlayerShootingAspect.ShootHand();
                    args.playerController.playerParams.soundParams
                        .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                            args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                }
                else
                {
                    if (!args.legacyPlayerShootingAspect.IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                      args.kinematics.velocity.normalized,
                                    Vector3.up),
                                args.playerAnimatedModel.forward.normalized,
                                args.playerAnimatedModel.right.normalized);
                        args.legacyPlayerShootingAspect
                            .ShootHandFreelyInCurvedPath(
                                shootingDirection);
                        args.playerController.playerParams.soundParams
                            .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                                args.playerController.controllerContext
                                    .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                    }
                    else
                    {
                        args.legacyPlayerShootingAspect.ShootHand();
                        args.playerController.playerParams.soundParams
                            .shootingSounds.unchargedPlainFistShootingSound.PlayOnceWithAudioSource(
                                args.playerController.controllerContext
                                    .playerAudioHandles.shootingAudioHandles.shootingAudioSource
                    );
                    }
                }
                return;
            }
            else if (args.legacyPlayerShootingAspect.HasHandsToShoot()
              && args.legacyPlayerShootingAspect.CanTriggerShootingHand()
              && args.legacyPlayerShootingAspect.IsPressingShootingButtonForCharging())
            {
                if (args.legacyPlayerShootingAspect
                        .HasJustStartedPressingShootingButtonForCharging())
                {
                    args.legacyPlayerShootingAspect
                        .InitiateFistChargingCycleForAppropriateHand();
                    return;
                }

                args.playerController.playerParams.soundParams
                    .shootingSounds.chargingFistSound.AlterAudioSourcePitch(
                       args.playerController.controllerContext
                       .playerAudioHandles.shootingAudioHandles.chargingAudioSource,
                       1.5f - args.legacyPlayerShootingAspect.GetCurrentChargingPowerNormalized()
                    );
                args.legacyPlayerShootingAspect.KeepChargingCycle();
                return;
            }
            else if (args.legacyPlayerShootingAspect
                .HasHandsToShoot()
              && args.legacyPlayerShootingAspect.WantsToFinishChargingAndShoot())
            {
                if (!IsInStrafingKindRule())
                {
                    args.legacyPlayerShootingAspect.FinishFistChargingCycleAndShootHand();
                    args.playerController.playerParams.soundParams
                        .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                            args.playerController.controllerContext
                            .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                    args.playerController.playerParams.soundParams
                        .shootingSounds.chargedFistShootingSound.PlayOnceWithAudioSourceAndPitchFactor(
                            args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.shootingAudioSource,
                            1.5f - args.legacyPlayerShootingAspect.GetCurrentChargingPowerNormalized()
                    );
                }
                else
                {
                    if (!args.legacyPlayerShootingAspect
                        .IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                        args.kinematics.velocity.normalized,
                                    Vector3.up),
                                args.playerAnimatedModel.forward.normalized,
                                args.playerAnimatedModel.right.normalized);
                        args.playerController.playerParams.soundParams
                            .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                                args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                        args.legacyPlayerShootingAspect
                            .FinishFistChargingCycleAndShootHandFreelyInCurvedPath(
                                shootingDirection);
                    }
                    else
                    {
                        args.playerController.playerParams.soundParams
                            .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                                args.playerController.controllerContext
                                .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                        );
                        args.legacyPlayerShootingAspect
                            .FinishFistChargingCycleAndShootHand();
                    }
                }
                return;
            }
        }

        public abstract void OnPortalTravel();

        public PlayerMovementRule(string ruleId, object controller) 
            : base(ruleId, controller) { }

        #region ContextRegion

        protected override PlayerContext UseEntityContext()
        {
            return GetController().controllerContext;
        }

        protected PlayerController GetController()
        {
            return GetCast<PlayerController>(this.entityController);
        }

        #endregion
    }
}
