﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Handles.Audio;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class AirAudioSources
    {
        public const string AIR_FLIP_RUNNING_JUMP_AUDIO_SOURCE = "air_flip_running_jump_audio_source";
        public const string RUNNING_JUMP_OFF_THE_GROUND_AUDIO_SOURCE = "running_jump_off_the_ground_audio_source";
        public const string STANDING_JUMP_OFF_THE_GROUND_AUDIO_SOURCE = "standing_jump_off_the_ground_audio_source";
    }

    public class AirAudioHandles
    {
        public AudioSource airFlipRunningJumpAudioSource;
        public AudioSource runningJumpOffTheGroundAudioSource;
        public AudioSource standingJumpOffTheGroundAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.airFlipRunningJumpAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, AirAudioSources.AIR_FLIP_RUNNING_JUMP_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.runningJumpOffTheGroundAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, AirAudioSources.RUNNING_JUMP_OFF_THE_GROUND_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.standingJumpOffTheGroundAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, AirAudioSources.STANDING_JUMP_OFF_THE_GROUND_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
