﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class StrafingGroundAudioSources
    {
        public const string ENTERING_LEAVING_STRAFING_AUDIO_SOURCE = 
            "entering_leaving_strafing_audio_source";
    }

    public class  StrafingGroundAudioHandles
    {
        public AudioSource enteringLeavingStrafingAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.enteringLeavingStrafingAudioSource =
                TransformHelper.RecursiveFindChild(
                  playerSubject, 
                    StrafingGroundAudioSources.ENTERING_LEAVING_STRAFING_AUDIO_SOURCE)
                  .GetComponent<AudioSource>();
        }
    }
}
