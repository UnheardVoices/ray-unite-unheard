﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class ShootingAudioSources
    {
        public const string CHARGING_CYCLE_START_AUDIO_SOURCE = "charging_cycle_start_audio_source";
        public const string CHARGING_AUDIO_SOURCE = "charging_audio_source";
        public const string SHOOTING_AUDIO_SOURCE = "shooting_audio_source";
    }

    public class  ShootingAudioHandles
    {
        public AudioSource chargingCycleStartAudioSource;
        public AudioSource chargingAudioSource;
        public AudioSource shootingAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.chargingCycleStartAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, ShootingAudioSources.CHARGING_CYCLE_START_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.chargingAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, ShootingAudioSources.CHARGING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.shootingAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, ShootingAudioSources.SHOOTING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
