﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class HelicopterAudioSources
    {
        public const string HELICOPTER_AUDIO_SOURCE = "helicopter_audio_source";
    }

    public class HelicopterAudioHandles
    {
        public AudioSource helicopterAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.helicopterAudioSource =
                TransformHelper.RecursiveFindChild(
                     playerSubject, HelicopterAudioSources.HELICOPTER_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
