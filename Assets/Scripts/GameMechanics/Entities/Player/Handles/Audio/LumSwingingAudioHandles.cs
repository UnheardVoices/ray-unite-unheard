﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class LumSwingingAudioSources
    {
        public const string LUM_CATCHING_LEAVING_AUDIO_SOURCE = "lum_catching_leaving_audio_source";
        public const string LUM_SWINGING_DIRECTION_AUDIO_SOURCE = "lum_swinging_direction_audio_source";
    }

    public class  LumSwingingAudioHandles
    {
        public AudioSource lumCatchingLeavingAudioSource;
        public AudioSource lumSwingingDirectionAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.lumCatchingLeavingAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, LumSwingingAudioSources.LUM_CATCHING_LEAVING_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
            this.lumSwingingDirectionAudioSource =
                TransformHelper.RecursiveFindChild(
                    playerSubject, LumSwingingAudioSources.LUM_SWINGING_DIRECTION_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
