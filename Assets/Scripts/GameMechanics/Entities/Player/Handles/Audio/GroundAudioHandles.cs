﻿
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Handles.Audio
{
    public static class GroundAudioSources
    {
        public const string ROLL_AUDIO_SOURCE = "roll_audio_source";
    }

    public class GroundAudioHandles
    {
        public AudioSource rollAudioSource;

        public void PropagateWithAudioSourcesRefs(Transform playerSubject)
        {
            this.rollAudioSource =
                TransformHelper.RecursiveFindChild(
                playerSubject, GroundAudioSources.ROLL_AUDIO_SOURCE)
                    .GetComponent<AudioSource>();
        }
    }
}
