﻿using Assets.Scripts.GameMechanics.Entities.NewEntity.Subclasses;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player.Contexts
{
    public class AirContext : Context
    {
        public bool isJumping;
        public Vector3 initialIntentionalMovementDirection;
        public Vector3 initialVelocity;
    }
}
