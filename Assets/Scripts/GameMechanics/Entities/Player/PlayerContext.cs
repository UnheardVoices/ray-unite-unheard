﻿using Assets.Scripts.Animations;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Collisions;
using Assets.Scripts.GameMechanics.Entities.Camera;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common;
using Assets.Scripts.GameMechanics.Entities.Entity.Rules.Common.Collision;
using Assets.Scripts.GameMechanics.Entities.NewEntity;
using Assets.Scripts.GameMechanics.Entities.Player.Handles;
using Assets.Scripts.GameMechanics.Entities.Player.Params;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Animations.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Input.Effects;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player
{
    public class PlayerContext : NewEntityContext
    {
        public PlayerInputHub playerInputHub;
        public PlayerInputHubStateHub playerInputHubStateHub;

        public PlayerController playerController;

        public PlayerInputEffects playerInputEffects;

        public PlatformerCollisionHub platformerCollisionHub;

        public PlatformerCollisionEffects platformerCollisionEffects;
        public PlayerAnimationEffects playerAnimationEffects;
        public PlayerMovementInput playerMovementInputWrapper;
        public PlayerInputWrapperEffects playerInputWrapperEffects;
        public PlayerAnimatedModelHandle playerAnimatedModel;

        public AnimationController playerAnimationController;

        public CameraScriptsHandle cameraScriptsHandle;

        public PlayerMovementStateInfo legacyPlayerMovementStateInfo;
        public PlayerMovementMetrics legacyPlayerMovementMetrics;
        public PlayerGroundRollingAspect legacyPlayerGroundRollingAspect;

        public RayCollider legacyRayCollider;

        public PlayerShootingAspect legacyPlayerShootingAspect;

        public PlayerRulesTransitions playerRulesTransitions;

        public PlayerMovementInput legacyPlayerMovementInput;
        public Vector3 cameraOrientedForward;

        public PlayerAudioHandles playerAudioHandles;

        public PlayerParams playerParams;

        public PlayerContext(): base() { }
    }
}
