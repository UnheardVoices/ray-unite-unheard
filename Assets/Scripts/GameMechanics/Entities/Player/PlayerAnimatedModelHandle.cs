﻿using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.Player
{
    public class PlayerAnimatedModelHandle
    {
        protected Transform playerAnimatedModelTransform;
        public PlayerAnimatedModelHandle(Transform playerAnimatedModelTransform)
        {
            this.playerAnimatedModelTransform = playerAnimatedModelTransform;
        }

        public Vector3 forward
        {
            set => this.playerAnimatedModelTransform.forward = -value;
            get => -this.playerAnimatedModelTransform.forward;
        }

        public Vector3 up
        {
            set => this.playerAnimatedModelTransform.up = value;
            get => -this.playerAnimatedModelTransform.up;
        }

        public Vector3 right
        {
            set => this.playerAnimatedModelTransform.right = -value;
            get => -this.playerAnimatedModelTransform.right;
        }
    }
}
