﻿using Assets.Scripts.GameMechanics.Entities.Player.Params.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params
{
    [Serializable]
    public class PlayerParams
    {
        public PlayerSoundParams soundParams = new();
    }
}
