﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerLedgeGrabbingSounds
    {
        public SimpleSound ledgeGrabbingSound = new();
        public SoundWithVariants ledgeLeavingJumpSound = new();
        public SoundWithVariants ledgeLeavingSoundDown = new();
        public SoundWithVariants ledgeClimbingSound = new();
    }
}
