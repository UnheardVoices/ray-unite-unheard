﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerRoofHangingSounds
    {
        public SoundWithVariants roofCatchingSound = new();
        public SoundWithVariants roofLeavingFallingSound = new();
        public ContinuousLoopedSoundModulated underRoofMovementSound = new();
    }
}
