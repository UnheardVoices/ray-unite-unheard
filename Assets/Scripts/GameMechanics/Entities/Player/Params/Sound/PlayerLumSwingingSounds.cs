﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerLumSwingingSounds
    {
        public SimpleSound swingingFrontToBackSound = new();
        public SimpleSound swingingBackToFrontSound = new();
        public SimpleSound lumCatchingSound = new();
        public SimpleSound lumLeavingSound = new();
    }
}
