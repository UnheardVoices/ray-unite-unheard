﻿using Assets.Scripts.GameMechanics.Entities.Player.Params.Sound.Jumping;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class PlayerStrafingAirSounds
    {
        public StrafingAirJumpingSounds jumpingSounds = new();
    }
}
