﻿using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound;
using Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound.Sound;
using System;

namespace Assets.Scripts.GameMechanics.Entities.Player.Params.Sound
{
    [Serializable]
    public class  PlayerWallClimbingSounds
    {
        public SoundWithVariants grabbingWallSound = new();
        public ContinuousLoopedSoundModulated climbingUpSound = new();
        public ContinuousLoopedSoundModulated climbingDownSound = new();
        public SoundWithVariants leavingWallJumpSound = new();
    }
}
