﻿using Assets.Scripts.Utils.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.Entities.NewWatcher.Params.Sound
{
    [Serializable]
    public class SoundVariant
    {
        public AudioClip clip;

        [Range(0.0f, 1.0f)]
        public float volume = 0.5f;

        [Range(0.0f, 100f)]
        public float maxDistance = 10f;

        [Min(1)]
        public int occurrenceFrequencyCoefficient = 1;

        [Range(0.25f, 5f)]
        public float minPitch = 0.75f;

        [Range(0.25f, 5f)]
        public float maxPitch = 1.5f;
    }

    public static class SoundVariantsHelper
    {
        public static Tuple<AudioClip, float, float, int> GetVariantToPlay(SoundVariant[] variants)
        {
            if (variants == null || variants.Length <= 0)
            {
                return null;
            }
            if (variants.Any((x) => x.occurrenceFrequencyCoefficient <= 0))
            {
                throw new InvalidOperationException(
                    "One of the sounds occurence frequency coefficient is less or equal zero!");
            }
            float totalDenominator = variants.Sum((x) => x.occurrenceFrequencyCoefficient);

            float currentProbabilitiesSum = 0f;
            List<float> probabilitiesThresholds = new List<float>() { 0f };

            for (int i = 0; i < variants.Length; i++)
            {
                currentProbabilitiesSum += (variants[i].occurrenceFrequencyCoefficient) / totalDenominator;
                probabilitiesThresholds.Add(currentProbabilitiesSum);
            }

            float chosenRandomValue = UnityEngine.Random.Range(0f, 1f);

            for (int i = 0; i < variants.Length; i++)
            {
                if (chosenRandomValue >= probabilitiesThresholds[i] 
                    && chosenRandomValue <= probabilitiesThresholds[i + 1])
                {
                    return Tuple.Create(
                        variants[i].clip, variants[i].volume, variants[i].maxDistance,
                        i);
                }
            }

            throw new InvalidOperationException("Should not get here");
        }
    }

    public class AudioSourceDestroyAfterClipFinish : MonoBehaviour
    {
        protected AudioSource audioSource;
        protected void Start()
        {
            this.audioSource = GetComponent<AudioSource>();
        }

        protected void FixedUpdate()
        {
            if (!this.audioSource.isPlaying)
            {
                Destroy(this.gameObject);
            }
        }
    }

    [Serializable]
    public class SoundWithVariants
    {
        public SoundVariant[] variants;

        [Range(0.0f, 1f)]
        public float probabilityOfBeingPlayedAtAll = 1f;

        protected float GetAlteredPitch(int variantIndex)
        {
            return UnityEngine.Random.Range(
                this.variants[variantIndex].minPitch,
                this.variants[variantIndex].maxPitch);
        }

        protected void OnSuccessPlay(Action onSuccessPlayAction)
        {
            float chosenRandom = UnityEngine.Random.Range(0f, 1f);
            if (chosenRandom < this.probabilityOfBeingPlayedAtAll)
            {
                onSuccessPlayAction.Invoke();
            }
        }

        public virtual AudioSource PlayOnceInPosition(Vector3 position)
        {
            AudioSource result = null;
            OnSuccessPlay(() =>
            {
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    var newAudioSource =
                        new GameObject().AddComponent<AudioSource>();
                    newAudioSource.transform.position = position;
                    newAudioSource.pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                    newAudioSource.volume = variantToPlayInfo.Item2;
                    newAudioSource.clip = variantToPlayInfo.Item1;
                    newAudioSource.Play();
                    newAudioSource.gameObject.AddComponent<AudioSourceDestroyAfterClipFinish>();
                    result = newAudioSource;
                }
            });       
            return null;
        }

        public virtual void PlayLoopedWithAudioSource(AudioSource audioSource, bool alwaysStopAudioSourceFirst = false)
        {
            if (alwaysStopAudioSourceFirst)
            {
                audioSource.Stop();
            }
            OnSuccessPlay(() =>
            {
                if (!alwaysStopAudioSourceFirst)
                {
                    audioSource.Stop();
                }                    
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    audioSource.loop = true;
                    audioSource.clip = variantToPlayInfo.Item1;
                    audioSource.volume = variantToPlayInfo.Item2;
                    audioSource.maxDistance = variantToPlayInfo.Item3;
                    audioSource.pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                    audioSource.Play();
                }
            });          
        }

        public virtual void PlayOnceWithAudioSourceAndPitchFactor(
            AudioSource audioSource, float pitchFactor, bool alwaysStopAudioSourceFirst = false)
        {
            if (alwaysStopAudioSourceFirst)
            {
                audioSource.Stop();
            }
            OnSuccessPlay(() =>
            {
                if (!alwaysStopAudioSourceFirst)
                {
                    audioSource.Stop();
                }
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    audioSource.loop = false;
                    audioSource.volume = variantToPlayInfo.Item2;
                    audioSource.maxDistance = variantToPlayInfo.Item3;
                    audioSource.pitch = pitchFactor;
                    audioSource.PlayOneShot(variantToPlayInfo.Item1);
                }
            });
        }

        public virtual void PlayOnceWithAudioSource(AudioSource audioSource, bool alwaysStopAudioSourceFirst = false)
        {
            if (alwaysStopAudioSourceFirst)
            {
                audioSource.Stop();
            }
            OnSuccessPlay(() =>
            {
                if (!alwaysStopAudioSourceFirst)
                {
                    audioSource.Stop();
                }                
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    audioSource.loop = false;
                    audioSource.volume = variantToPlayInfo.Item2;
                    audioSource.maxDistance = variantToPlayInfo.Item3;
                    audioSource.pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                    audioSource.PlayOneShot(variantToPlayInfo.Item1);
                }
            });           
        }

        public virtual AudioClipWithAudioSourceParams GetAudioClipWithAudioSourceParams()
        {
            AudioClipWithAudioSourceParams result = null;
            OnSuccessPlay(() =>
            {
                var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
                if (variantToPlayInfo != null)
                {
                    result = new AudioClipWithAudioSourceParams()
                    {
                        clip = variantToPlayInfo.Item1,
                        volume = variantToPlayInfo.Item2,
                        maxDistance = variantToPlayInfo.Item3,
                        variantIndex = variantToPlayInfo.Item4,
                        pitch = GetAlteredPitch(variantToPlayInfo.Item4)
                    };
                }
            });
            return result;
        }

        private IEnumerator SoundWithDeferredAction(
            AudioSource audioSource, Action afterAudioAction)
        {
            audioSource.Stop();
            var variantToPlayInfo = SoundVariantsHelper.GetVariantToPlay(this.variants);
            if (variantToPlayInfo != null)
            {
                audioSource.loop = false;
                audioSource.volume = variantToPlayInfo.Item2;
                audioSource.maxDistance = variantToPlayInfo.Item3;
                audioSource.pitch = GetAlteredPitch(variantToPlayInfo.Item4);
                audioSource.PlayOneShot(variantToPlayInfo.Item1);
                yield return new WaitForSeconds(variantToPlayInfo.Item1.length);
            }
            afterAudioAction.Invoke();
        }

        public virtual void PlayOnceWithAudioSourceAndThen(
            MonoBehaviour coroutineHost, AudioSource audioSource, Action afterAudioAction)
        {
            OnSuccessPlay(() =>
            {               
                coroutineHost.StartCoroutine(
                   SoundWithDeferredAction(audioSource, afterAudioAction));
            });        
        }

        public virtual void StopPlayingWithAudioSource(AudioSource audioSource)
        {
            audioSource.Stop();
        }
    }
}
