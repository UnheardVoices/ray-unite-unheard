using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameMechanics.DynamicLedgeGenerator
{
    public class LedgeParams : MonoBehaviour
    {
        public static LedgeParams instance;

        public GameObject emptyGameObject;

        private void Awake()
        {
            instance = this;
        }
    }
}
