﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models.Model;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using System.Linq;
#if (UNITY_EDITOR)
using UnityEditor;
using UnityEditor.Animations;
#endif
using UnityEngine;

namespace Assets.Scripts.EditorOnly.AssetsCreation.Animator
{
    public static class AnimatorLayersHelper
    {
#if (UNITY_EDITOR)
        public static void AddOverrideLayerToAnimator(
            string layerName,
            int layerIndex,
            AnimatorController animatorController)
        {
            AnimatorControllerLayer newLayer = new AnimatorControllerLayer
            {
                name = layerName
            };
            newLayer.stateMachine = new AnimatorStateMachine
            {
                name = newLayer.name,
                hideFlags = HideFlags.HideInHierarchy
            };
            if (AssetDatabase.GetAssetPath(animatorController) != "")
            {
                AssetDatabase.AddObjectToAsset(
                    newLayer.stateMachine, AssetDatabase.GetAssetPath(animatorController));
            }
            //Custom
            newLayer.blendingMode = AnimatorLayerBlendingMode.Override;
            newLayer.defaultWeight = 1f;

            animatorController.AddLayer(newLayer);

            ////we should have non-overriding curves animation state on any secondary animations layer
            ////we can always go to afterwards to enable animations on basic layer uderneath
            //// to play properly etc
            //var dummyEmptyAnimSlot = new AnimationClip();
            //dummyEmptyAnimSlot.legacy = false;
            //dummyEmptyAnimSlot.name = "DEFAULT_STATE_" + layerIndex;
            //var defaultAnimatorStateLayer1 = animatorController.AddMotion(dummyEmptyAnimSlot, layerIndex);

            //var anyStateTransition = newLayer.stateMachine.AddAnyStateTransition(defaultAnimatorStateLayer1);
            //anyStateTransition.hasExitTime = true;
            //anyStateTransition.hasFixedDuration = false;
            //anyStateTransition.duration = 1f;
            //anyStateTransition.exitTime = 1f;
        }


        public static List<int> GetRequiredAnimatorLayersInfos
            (List<AnimationClipConstructingRecipe>
                additionalDerivedAnimationsConstructingRecipes)
        {
            return additionalDerivedAnimationsConstructingRecipes
                .Select(x => x.animationClipLayerIndex).Distinct().ToList();
        }
#endif
    }

    public class AnimatorFactory
    {
        public static List<AnimationClip>
            GetBaseAnimationClipsAsImportedFromModel(
                string animatedModelResourcesPath)
        {
            return Resources.LoadAll(
                animatedModelResourcesPath, typeof(AnimationClip))
                    .Cast<AnimationClip>().ToList();
        }

        public void CreateAnimatorFor(
            GameObject gameObject,
            string unpackedAnimationsDirectoryPath,
            string additionalDerivedAnimationClipsResourceAssetsDirectoryPath,
            string animatorAssetPath,
            string animatedModelResourcesPath,
            Dictionary<string, Assets.Scripts.Animations.Models.AnimationInfo> basicAnimationsMetadata,
            List<AnimationClipConstructingRecipe> additionalDerivedAnimationsConstructingRecipes,
            Transform animatorTransform,
            List<Transform> animatorBonesTransforms)
        {
            Debug.Log("Invoking Animator Factory");
#if (UNITY_EDITOR)
            AnimatorController animatorController = UnityEditor.Animations.AnimatorController
                 .CreateAnimatorControllerAtPath(animatorAssetPath);

            List<AnimationClip> animationClips = GetBaseAnimationClipsAsImportedFromModel(animatedModelResourcesPath);

            foreach (int newLayerToAddIndex in AnimatorLayersHelper
                .GetRequiredAnimatorLayersInfos(additionalDerivedAnimationsConstructingRecipes))
            {
                string newLayerName = "SECONDARY_ANIMATIONS_LAYER_" + newLayerToAddIndex;
                AnimatorLayersHelper.AddOverrideLayerToAnimator(
                    newLayerName, newLayerToAddIndex, animatorController);
            }

            List<string> loopedAnimations = basicAnimationsMetadata
                .Where(x => x.Value.isLooped)
                .Select(x => x.Value.animationName).ToList();

            foreach (AnimationClip extractedAnimationClip in animationClips)
            {
                AnimationClip placementClip = new AnimationClip();
                EditorUtility.CopySerialized(extractedAnimationClip, placementClip);
                placementClip.name = PathsHelper.EscapeInvalidCharactersFromPath(placementClip.name);

                //var animatorState = animatorController.AddMotion(placementClip, 0);

                //if (basicAnimationsMetadata.Where(
                //    x => x.Value.animationName.Equals(placementClip.name)).Count() > 0)
                //{
                //    var speedDedicatedToThisAnimation = basicAnimationsMetadata.Where(
                //    x => x.Value.animationName.Equals(placementClip.name)).First().Value.playingSpeed;
                //    animatorState.speed = speedDedicatedToThisAnimation;
                //}
                //


                // animatorState.speed = 1f; // hopefully we should be able to set speed individually for all
                // animator states/motions separately and that should affect animator out of the box
                // without any additional steps

                if (loopedAnimations.Contains(placementClip.name))
                {
                    AnimationClipSettings animationClipSettings = AnimationUtility.GetAnimationClipSettings(placementClip);
                    placementClip.wrapMode = WrapMode.Loop;

                    animationClipSettings.loopTime = true;

                    AnimationUtility.SetAnimationClipSettings(placementClip, animationClipSettings);
                }

                AssetDatabase.CreateAsset(
                    placementClip,
                    PathsHelper
                        .EscapeInvalidCharactersFromPath(
                            "Assets/Resources/" + unpackedAnimationsDirectoryPath + placementClip.name + ".anim")
                    );
            }

            foreach (AnimationClip extractedAnimationClip in animationClips)
            {
                string animClipResourcesAssetPath =
                    PathsHelper.EscapeInvalidCharactersFromPath(
                        unpackedAnimationsDirectoryPath
                        + extractedAnimationClip.name);

                AnimationClip assetAnimationClip =
                    ((AnimationClip)Resources.Load(animClipResourcesAssetPath, typeof(AnimationClip)));

                AnimatorState animatorState = animatorController.AddMotion(assetAnimationClip, 0);

                if (basicAnimationsMetadata.Where(
                    x => x.Value.animationName.Equals(assetAnimationClip.name)).Count() > 0)
                {
                    float speedDedicatedToThisAnimation = basicAnimationsMetadata.Where(
                    x => x.Value.animationName.Equals(assetAnimationClip.name)).First().Value.playingSpeed;
                    animatorState.speed = speedDedicatedToThisAnimation;
                }
            }

            List<AdditionalAnimationClipInfo> additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime =
                DerivedAnimationsHelper.GetProgrammaticallyDerivedAndConstructedAdditionalAnimationClips
                    (animatedModelResourcesPath,
                        additionalDerivedAnimationsConstructingRecipes,
                        animatorTransform, animatorBonesTransforms);

            foreach (AdditionalAnimationClipInfo additionalAnimClipInfo in additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime)
            {
                AssetDatabase.CreateAsset(
                    additionalAnimClipInfo.animationClip,
                    "Assets/Resources/" + additionalDerivedAnimationClipsResourceAssetsDirectoryPath
                    + additionalAnimClipInfo.animationClip.name + ".anim");
            }
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            foreach (AdditionalAnimationClipInfo additionalAnimClipInfo in additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime)
            {
                string additionalAnimName = additionalAnimClipInfo.animationClip.name;
                int additionalAnimLayer = additionalAnimClipInfo.animationClipLayerIndex;

                string animClipResourcesAssetPath =
                    additionalDerivedAnimationClipsResourceAssetsDirectoryPath
                    + additionalAnimClipInfo.animationClip.name;

                AnimationClip animationClipAsLoadedFromAssets =
                    ((AnimationClip)Resources.Load(animClipResourcesAssetPath, typeof(AnimationClip)));

                AnimatorState additionalAnimatorState = animatorController.AddMotion(
                    animationClipAsLoadedFromAssets, additionalAnimLayer);

                additionalAnimatorState.speed = additionalAnimClipInfo.playingSpeed;
                animatorController.SetStateEffectiveMotion(
                    additionalAnimatorState, animationClipAsLoadedFromAssets);
            }
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
#endif
        }
    }
}
