using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.HUD.ScoreCounting;
using UnityEngine;

public class ScheduledScoring
{
    public float scoreStepInTick;
    public int fixedUpdatesLeft;
    public int id;

    public ScheduledScoring(int id, float scoreStepInTick, int fixedUpdatesLeft)
    {
        this.id = id;
        this.scoreStepInTick = scoreStepInTick;
        this.fixedUpdatesLeft = fixedUpdatesLeft;
    }
}

public class DrawPlayerScoreCounter : GameplayOnlyMonoBehaviour
{
    public Texture2D playerScoreCounterTexture;
    private ScoreCounter scoreCounter;

    private void Awake()
    {
        ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator =
            FindObjectOfType<ScoreAccumulatorDeccumulator>();
        this.scoreCounter = new ScoreCounter(scoreAccumulatorDeccumulator);
        scoreAccumulatorDeccumulator.SetScoreCounter(this.scoreCounter);
    }

    protected override void GameplayOnGUI()
    {
        this.scoreCounter.Draw(
            70, 70,
            0,
            0,
            this.playerScoreCounterTexture
            );
    }
}
