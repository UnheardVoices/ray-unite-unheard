﻿using UnityEngine;

namespace Assets.Scripts.HUD
{
    public class DrawTestHUDTextureCoordinates : MonoBehaviour
    {
        private Rect texCoordXRect;
        private Rect texCoordYRect;

        private Rect texCoordWidthRect;
        private Rect texCoordHeightRect;

        private Rect testRect;

        public Texture2D texture;

        public float srcWidth;
        public float srcHeight;
        public float srcX;
        public float srcY;

        private void Start()
        {
            this.testRect = new Rect(
            Screen.width / 2,
            Screen.height / 2,
            200,
            200
            );

            float center = Screen.width / 2.0f;
            //rect = new Rect(center - 200, 200, 400, 250);

            this.texCoordXRect = new Rect(center - 200, 125, 400, 30);
            this.texCoordYRect = new Rect(center - 200, 160, 400, 30);

            this.texCoordWidthRect = new Rect(center - 200, 190, 400, 30);
            this.texCoordHeightRect = new Rect(center - 200, 230, 400, 30);
        }

        private void OnGUI()
        {
            this.srcWidth = GUI.HorizontalSlider(this.texCoordWidthRect, this.srcWidth, -1f, 1f);
            this.srcHeight = GUI.HorizontalSlider(this.texCoordHeightRect, this.srcHeight, -1f, 1f);

            this.srcX = GUI.HorizontalSlider(this.texCoordXRect, this.srcX, 0.0f, 1f);
            this.srcY = GUI.HorizontalSlider(this.texCoordYRect, this.srcY, 0.0f, 1f);

            GUI.DrawTextureWithTexCoords(this.testRect, this.texture,
                new Rect(this.srcX, this.srcY, this.srcWidth, this.srcHeight));
        }
    }
}
