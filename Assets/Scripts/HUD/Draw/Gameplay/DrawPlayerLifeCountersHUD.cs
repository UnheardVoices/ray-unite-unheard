using Assets.Scripts.Engine.Behaviours;
using UnityEngine;

public class DrawPlayerLifeCountersHUD : GameplayOnlyMonoBehaviour
{
    public Texture2D playerCountersTexture;

    private Rect rightSideHealthBarWithSuperpowerMeterRect;
    private Rect rightSideHealthFilledBarRect;

    private Rect leftSideHealthFilledBarRect;


    private Rect lifeBarMiddleRect;
    private Rect lifeBarFilledMiddleRect;

    private Rect leftEndLifeBarRect;

    protected override void BehaviourStart()
    {
        this.rightSideHealthBarWithSuperpowerMeterRect = new Rect(
            Screen.width - 131 - 20 - 20 - 20 - 20,
            Screen.height - 172 - 41 - 20 - 20,
            131,
            172);

        this.rightSideHealthFilledBarRect = new Rect(
            Screen.width - 131 + 65 - 20 - 10 - 20 - 20,
            Screen.height - 41 - 37 - 11.8f - 10 - 20,
            20,
            40);

        this.lifeBarMiddleRect = new Rect(
            Screen.width - 131 - 20 - 1 - 20 - 20 - 20,
            Screen.height - 41 - 37 - 11.8f - 10 - 20, // 37 - 172 - 134, where 134 height of powermeter
            1,
            39);

        this.lifeBarFilledMiddleRect = new Rect(
            Screen.width - 131 - 20 - 1 - 20 - 20 - 20,
            Screen.height - 41 - 37 - 11.8f - 10 - 20, // 37 - 172 - 134, where 134 height of powermeter
            1,
            39);

        this.leftEndLifeBarRect = new Rect(
            Screen.width - 131 - 10 - 1 - 5 - 10 - 20 - 20 - 20,
            Screen.height - 41 - 37 - 11.8f - 10 - 20,  // 37 - 172 - 134, where 134 height of powermeter
            20,
            40);

        this.leftSideHealthFilledBarRect = new Rect(
            Screen.width - 131 - 10 - 1 - 5 - 10 - 20 - 20 - 20,
            Screen.height - 41 - 37 - 11.8f - 10 - 20,  // 37 - 172 - 134, where 134 height of powermeter
            20,
            40);
    }

    protected override void GameplayOnGUI()
    {
        // draw right side of filled health bar
        GUI.DrawTextureWithTexCoords(this.rightSideHealthFilledBarRect, this.playerCountersTexture,
            new Rect(0.25f, 0f, 0.05154639f, 0.07731958f));

        int healthBarLength = 1000;
        // draw beginning from right for filled health bar
        for (int i = 0; i < 600; i++)
        {
            // draw middle of life bar repeatedly
            GUI.DrawTextureWithTexCoords(
                new Rect(this.rightSideHealthFilledBarRect.x - i, this.lifeBarFilledMiddleRect.y,
                this.lifeBarFilledMiddleRect.width, this.lifeBarFilledMiddleRect.height), this.playerCountersTexture,
               new Rect(0.1701031f, 0.01546392f, 0.06958763f, -0.00773196f));
        }

        // draw right side of health bar with superpower meter
        GUI.DrawTextureWithTexCoords(this.rightSideHealthBarWithSuperpowerMeterRect, this.playerCountersTexture,
            new Rect(0.7293814f, 0.04381443f, 0.2603093f, 0.3350515f));


        for (int i = 0; i < healthBarLength; i++)
        {
            //GUI.DrawTextureWithTexCoords(
            //    new Rect(lifeBarFilledMiddleRect.x - i, lifeBarFilledMiddleRect.y,
            //    lifeBarFilledMiddleRect.width, lifeBarFilledMiddleRect.height), playerCountersTexture,
            //   new Rect(0.1701031f, 0.01546392f, 0.06958763f, -0.00773196f));

            // draw middle of life bar repeatedly
            GUI.DrawTextureWithTexCoords(
                new Rect(this.lifeBarMiddleRect.x - i, this.lifeBarMiddleRect.y,
                this.lifeBarMiddleRect.width, this.lifeBarMiddleRect.height), this.playerCountersTexture,
               new Rect(0.3685567f, 0.04381443f, 0f, 0.07474227f));
        }

        //// draw left side of filled health bar
        //GUI.DrawTextureWithTexCoords(
        //    new Rect(
        //        Screen.width - 131 - 10 - (1 * (healthBarLength + 1)) - leftSideHealthFilledBarRect.width - 20,
        //        leftSideHealthFilledBarRect.y,
        //        leftSideHealthFilledBarRect.width,
        //        leftSideHealthFilledBarRect.height),
        //        playerCountersTexture,
        //    new Rect(0.1030928f, 0.007731959f, 0.06701031f, 0.07216495f));

        // draw left end of life bar
        GUI.DrawTextureWithTexCoords(
            new Rect(
                Screen.width - 131 - 10 - (1 * (healthBarLength + 1)) - this.leftEndLifeBarRect.width - 20 - 20 - 20,
                this.leftEndLifeBarRect.y,
                this.leftEndLifeBarRect.width,
                this.leftEndLifeBarRect.height),
                this.playerCountersTexture,
           new Rect(0.927835f, 0.04123711f, -0.05154638f, 0.07731958f));
    }
}
