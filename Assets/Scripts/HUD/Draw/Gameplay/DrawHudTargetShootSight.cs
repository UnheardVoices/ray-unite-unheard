﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.HUD.Targetting;
using UnityEngine;

namespace Assets.Scripts.HUD
{
    public class DrawHudTargetShootSight : GameplayOnlyMonoBehaviour
    {
        public Texture2D targetShootSightCircle;
        public Texture2D targetShootSightRightArrow;
        public Texture2D targetShootSightLeftArrow;
        public Texture2D targetShootSightStraightArrow;

        private Vector3 targetShootSightScreenPosition;

        private bool isShootSightVisible = false;

        private float targetShootSightWidthPixels = 150;
        private float targetShootSightHeightPixels = 150;

        private float circleSizeFluctuationStep = 0.1f;
        private float currentCircleSizeFluctuation = 0f;

        private ShootSightDirection shootSightDirection;

        private bool enabledDrawing = true;

        public void SetEnabledDrawing(bool enabled)
        {
            this.enabledDrawing = enabled;
        }

        public void SetIsShootSightVisible(bool isShootSightVisible)
        {
            this.isShootSightVisible = isShootSightVisible;
        }

        public void SetShootSightScreenCoordinates(
            Vector3 targetShootSightScreenPosition)
        {
            this.targetShootSightScreenPosition = targetShootSightScreenPosition;
        }

        protected override void GameplayFixedUpdate()
        {
            if (this.isShootSightVisible)
            {
                this.currentCircleSizeFluctuation += this.circleSizeFluctuationStep;
            }
        }

        protected override void GameplayOnGUI()
        {
            if (this.isShootSightVisible && this.enabledDrawing)
            {
                float circleSizeMultiplier = (Mathf.Sin(this.currentCircleSizeFluctuation) / 5.0f) + 0.8f;
                float currentCircleWidth = this.targetShootSightWidthPixels * circleSizeMultiplier;
                float currentCircleHeight = this.targetShootSightHeightPixels * circleSizeMultiplier;

                Rect shootSightCirclePosition =
                    new Rect(
                        this.targetShootSightScreenPosition.x - (currentCircleWidth / 2.0f),
                        this.targetShootSightScreenPosition.y - (currentCircleHeight / 2.0f),
                        currentCircleWidth,
                        currentCircleHeight
                        );



                Rect shootArrowPositionUpToBottom =
                    new Rect(
                        this.targetShootSightScreenPosition.x - (this.targetShootSightWidthPixels / 2.0f),
                        this.targetShootSightScreenPosition.y - (this.targetShootSightHeightPixels / 2.0f),
                        this.targetShootSightWidthPixels,
                        this.targetShootSightHeightPixels
                        );

                Rect shootArrowPositionBottomToUp =
                    new Rect(
                        this.targetShootSightScreenPosition.x - (this.targetShootSightWidthPixels / 2.0f),
                        this.targetShootSightScreenPosition.y - (this.targetShootSightHeightPixels / 2.0f) + this.targetShootSightHeightPixels,
                        this.targetShootSightWidthPixels,
                        -this.targetShootSightHeightPixels
                        );


                    GUI.DrawTexture(shootSightCirclePosition, this.targetShootSightCircle);

                    if (this.shootSightDirection == ShootSightDirection.UP_ARROW_STRAIGHT)
                    {
                        GUI.DrawTexture(shootArrowPositionUpToBottom, this.targetShootSightStraightArrow);
                    }
                    else if (this.shootSightDirection == ShootSightDirection.UP_ARROW_RIGHT)
                    {
                        GUI.DrawTexture(shootArrowPositionUpToBottom, this.targetShootSightRightArrow);
                    }
                    else if (this.shootSightDirection == ShootSightDirection.UP_ARROW_LEFT)
                    {
                        GUI.DrawTexture(shootArrowPositionUpToBottom, this.targetShootSightLeftArrow);
                    }



                    else if (this.shootSightDirection == ShootSightDirection.DOWN_ARROW_STRAIGHT)
                    {
                        GUI.DrawTexture(shootArrowPositionBottomToUp, this.targetShootSightStraightArrow);
                    }
                    else if (this.shootSightDirection == ShootSightDirection.DOWN_ARROW_RIGHT)
                    {
                        GUI.DrawTexture(shootArrowPositionBottomToUp, this.targetShootSightRightArrow);
                    }
                    else if (this.shootSightDirection == ShootSightDirection.DOWN_ARROW_LEFT)
                    {
                        GUI.DrawTexture(shootArrowPositionBottomToUp, this.targetShootSightLeftArrow);
                    }
                             
            }
        }

        public void SetShootingDirection(ShootSightDirection shootSightDirection)
        {
            this.shootSightDirection = shootSightDirection;
        }
    }
}
