﻿using Assets.Scripts.HUD.ScoreCounting.Counters;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ScoreCounter : ScoreCounterBase
    {
        private ComboScoreCounter comboScoreCounter;

        public ScoreCounter(ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator) :
            base(new ScoreCounterHelper(), 50, 250, 0.5f, 5, scoreAccumulatorDeccumulator)
        {
            this.comboScoreCounter = new ComboScoreCounter(scoreAccumulatorDeccumulator);
            this.scoreCounterHelper.SetScoreCounter(this);
        }

        public override void Draw(float x, float y, float width, float height, Texture2D playerScoreCounterTexture)
        {
            this.comboScoreCounter.Draw(x, y, width, height, playerScoreCounterTexture);
            base.Draw(x, y, width, height, playerScoreCounterTexture);
        }

        public void AccumulateComboScore(float accumulation)
        {
            this.comboScoreCounter.AccumulateScore(accumulation);
        }

        public void DeccumulateComboScore(float deccumulation)
        {
            this.comboScoreCounter.DeccumulateScore(deccumulation);
        }

        public bool IsComboOngoing()
        {
            return this.comboScoreCounter.IsComboOngoing();
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            this.comboScoreCounter.FixedUpdate();
        }
    }
}
