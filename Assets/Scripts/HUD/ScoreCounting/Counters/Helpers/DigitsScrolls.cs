﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting.Counters.Helpers
{
    public static class DigitScrollUtil
    {
        public static float GetScrollForDigitFromScoreAccumulation(
            int digitIndex, float scoreAccumulationStep)
        {
            return (scoreAccumulationStep / Mathf.Pow(10f, digitIndex)) % 10f;
        }
    }

    public static class DigitsScrollsHelper
    {
        public static void AccumulateScrollsWithScoreNumberStep(
            List<float> digitsScrollsFrom0To10,
            float scoreAccumulationStep
            )
        {
            for (int digitIndex = 0;
                digitIndex < digitsScrollsFrom0To10.Count;
                digitIndex++)
            {
                digitsScrollsFrom0To10[
                    digitsScrollsFrom0To10.Count - digitIndex - 1] +=
                        DigitScrollUtil.GetScrollForDigitFromScoreAccumulation(
                            digitsScrollsFrom0To10.Count - digitIndex - 1, scoreAccumulationStep);

                digitsScrollsFrom0To10[
                    digitsScrollsFrom0To10.Count - digitIndex - 1] =
                    digitsScrollsFrom0To10[
                        digitsScrollsFrom0To10.Count - digitIndex - 1] % 10f;
            }
        }

        public static void DecummulateScrollsWithScoreNumberStep(
            List<float> digitsScrollsFrom0To10,
            float scoreDeccumulationStep)
        {
            for (int digitIndex = 0;
                digitIndex < digitsScrollsFrom0To10.Count;
                digitIndex++)
            {
                digitsScrollsFrom0To10[
                    digitsScrollsFrom0To10.Count - digitIndex - 1] +=
                        -DigitScrollUtil.GetScrollForDigitFromScoreAccumulation(
                            digitsScrollsFrom0To10.Count - digitIndex - 1, scoreDeccumulationStep);

                digitsScrollsFrom0To10[
                    digitsScrollsFrom0To10.Count - digitIndex - 1] =
                    digitsScrollsFrom0To10[
                        digitsScrollsFrom0To10.Count - digitIndex - 1] < 0.1f ?
                        10f + digitsScrollsFrom0To10[
                        digitsScrollsFrom0To10.Count - digitIndex - 1] :
                    digitsScrollsFrom0To10[
                        digitsScrollsFrom0To10.Count - digitIndex - 1] % 10f;
            }
        }

        public static float
            GetDigitScrollNormalizationInTimeStep(
            float targetNumber,
            int digitIndex,
            float currentDigitScrollFrom0To10,
            float digitScrollOnBeginningOfCurrentCounterNormalization,
            int normalizationTimeMilliseconds)
        {
            float scrollToleranceEpsilon = 0.1f;

            //digitScrollOnBeginningOfCurrentCounterNormalization = 
            //    digitScrollOnBeginningOfCurrentCounterNormalization % 10f;

            float desiredDigitScroll = ((int)(Mathf.Round(targetNumber) / Mathf.Pow(10f, digitIndex))) % 10f;
            if (Mathf.Abs(desiredDigitScroll - currentDigitScrollFrom0To10) > scrollToleranceEpsilon)
            {
                // keep normalizing the digit - still not normalized
                if (desiredDigitScroll > digitScrollOnBeginningOfCurrentCounterNormalization)
                {
                    // we can slight the scroll forward and stop in some moment
                    int fixedUpdatesPerSecond = (int)(1f / Time.fixedDeltaTime);
                    int fixedUpdatesAmountToPerformNormalizationIn =
                        (int)((normalizationTimeMilliseconds / 1000f) * fixedUpdatesPerSecond);
                    float normalizationStepInOneFixedUpdate =
                        (desiredDigitScroll - digitScrollOnBeginningOfCurrentCounterNormalization) /
                            fixedUpdatesAmountToPerformNormalizationIn;


                    //normalizationStepInOneFixedUpdate = desiredDigitScroll - currentDigitScrollFrom0To10;

                    if (normalizationStepInOneFixedUpdate < 0f)
                    {
                        Debug.Log("LESS THAN ZERO A" + normalizationStepInOneFixedUpdate);
                    }
                    return normalizationStepInOneFixedUpdate;
                }
                {
                    // we need to roll over the counter for the digit and go from the beginning
                    int fixedUpdatesPerSecond = (int)(1f / Time.fixedDeltaTime);
                    int fixedUpdatesAmountToPerformNormalizationIn =
                        (int)((normalizationTimeMilliseconds / 1000f) * fixedUpdatesPerSecond);
                    float normalizationStepInOneFixedUpdate =
                        (desiredDigitScroll + 10f - digitScrollOnBeginningOfCurrentCounterNormalization)
                            / fixedUpdatesAmountToPerformNormalizationIn;


                    //normalizationStepInOneFixedUpdate = desiredDigitScroll + 10f - currentDigitScrollFrom0To10;
                    if (normalizationStepInOneFixedUpdate < 0f)
                    {
                        Debug.Log("LESS THAN ZERO B" + normalizationStepInOneFixedUpdate);
                    }
                    return normalizationStepInOneFixedUpdate;
                }
            }
            else
            {
                // do not normalize scroll anymore - this particular digit is roughly ok right now
                return 0f;
            }
        }

        public static void SetDigitsScrollsToReflectNumber
            (List<float> digitsScrollsFrom0To10, float number)
        {
            for (int digitIndex = 0;
                 digitIndex < digitsScrollsFrom0To10.Count;
                 digitIndex++)
            {
                int properDigitIndex = digitsScrollsFrom0To10.Count - digitIndex - 1;
                float desiredDigitScroll = ((int)(Mathf.Round(number) / Mathf.Pow(10f, properDigitIndex))) % 10f;
                digitsScrollsFrom0To10[properDigitIndex] = desiredDigitScroll;
            }
        }
    }
}
