﻿using UnityEngine;

namespace Assets.Scripts.Utils.Spawners
{
    public static class ExplosionSpawner
    {
        public static void SpawnExplosion(
            Vector3 position, GameObject explosionPrefab)
        {
            UnityEngine.Object.Instantiate(
               explosionPrefab, position, Quaternion.identity);
        }
    }
}
