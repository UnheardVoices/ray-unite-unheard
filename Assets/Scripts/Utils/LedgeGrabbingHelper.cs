﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class LedgeGrabbingHelper
    {
        public static Vector3 GetNearestPointOnEdgeFrom(
            Vector3 position,
            Vector3 edgePointA,
            Vector3 edgePointB)
        {
            Vector3 lineDirection = edgePointB - edgePointA;
            float lineLength = lineDirection.magnitude;
            lineDirection.Normalize();
            float projectLength = Mathf.Clamp(
                Vector3.Dot(position - edgePointA, lineDirection), 0f, lineLength);
            return edgePointA + lineDirection * projectLength;
        }
    }
}
