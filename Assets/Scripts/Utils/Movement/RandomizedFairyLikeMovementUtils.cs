﻿using Assets.Scripts.Common;
using System;
using UnityEngine;

namespace Assets.Scripts.Utils.Movement
{
    public static class RandomizedFairyLikeMovementUtils
    {
        public static Vector3 EvaluateVelocityForFairyLikeMovement(
            Vector3 currentVelocity,
            Vector3 translationToCurrentDestination,
            float movementDirectionRotationAngularSpeed,
            float maxVelocityValue
            )
        {
            Vector3 result = Vector3.RotateTowards(
                    currentVelocity, translationToCurrentDestination,
                    movementDirectionRotationAngularSpeed,
                    maxVelocityValue / 5f);
            result = Vector3.ClampMagnitude(result,
                maxVelocityValue);
            return result;
        }

        public static Tuple<Vector3, int> DrawNextTargetFlyingPosition(
            Vector3 originalPosition,
            Vector3 currentPosition,
            Vector3 currentTargetFlyingPosition,
            float initialTimeDrawingFlyingPositionSeconds,
            float targetFlyingPositionDrawingRepeatingRateSeconds,
            int phase,
            float timeProgression,
            float movementAreaRadiusVertical,
            float movementAreaRadiusHorizontal)
        {
            Func<Vector3> drawPosition = () =>
            {
                Vector3 randomizedPositionOffset;

                while (true)
                {
                    randomizedPositionOffset = new Vector3(
                    x: movementAreaRadiusHorizontal * UnityEngine.Random.Range(-1f, 1f),
                    y: movementAreaRadiusVertical * UnityEngine.Random.Range(-1f, 1f),
                    z: movementAreaRadiusHorizontal * UnityEngine.Random.Range(-1, 1f)
                    );

                    Vector3 fromCurrentPositionOffset =
                        (originalPosition + randomizedPositionOffset)
                        - currentPosition;

                    bool willHitTheWall = PhysicsRaycaster.Raycast(
                        currentPosition,
                        fromCurrentPositionOffset.normalized,
                        fromCurrentPositionOffset.magnitude * 1.6f,
                        Layers.generalEnvironmentLayersMask);
                    if (!willHitTheWall)
                    {
                        return originalPosition + randomizedPositionOffset;
                    }
                }
            };

            if (timeProgression >= initialTimeDrawingFlyingPositionSeconds
                && phase == 0
                && timeProgression < initialTimeDrawingFlyingPositionSeconds
                    + targetFlyingPositionDrawingRepeatingRateSeconds)
            {
                return Tuple.Create(drawPosition(), phase + 1);
            }
            else if (
              timeProgression >= initialTimeDrawingFlyingPositionSeconds
              && timeProgression > targetFlyingPositionDrawingRepeatingRateSeconds * phase
              && phase > 0)
            {
                return Tuple.Create(drawPosition(), phase + 1);
            }
            else
            {
                return Tuple.Create(currentTargetFlyingPosition, phase);
            }
        }
    }
}
