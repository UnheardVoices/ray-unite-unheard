﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class RecursiveMultipleChildrenTraversingHelper {
        public static void RecursiveFindChildrenWithNamePattern(
            Transform parent, string namePattern,
            in List<Transform> foundOnes,
            Action<Transform> forEachFoundAction)
        {
            foreach (Transform child in parent)
            {
                if (Regex.IsMatch(child.name, namePattern))
                {
                    foundOnes.Add(child);
                    if (forEachFoundAction != null)
                    {
                        forEachFoundAction.Invoke(child);
                    }
                }

                RecursiveFindChildrenWithNamePattern(
                   child, namePattern, foundOnes, forEachFoundAction);
            }
        }
    }

    public static class TransformHelper
    {
        public static List<Transform> RecursiveFindChildrenWithNamePattern(
            Transform parent, string namePattern,
            Action<Transform> forEachFoundAction = null)
        {
            List<Transform> result = new List<Transform>();
            RecursiveMultipleChildrenTraversingHelper
                .RecursiveFindChildrenWithNamePattern(
                    parent, namePattern, result, forEachFoundAction);
            return result;
        }

        public static List<Transform> 
            RecursiveFindChildrenWithName(
                Transform parent, string name, Action<Transform> forEachFoundAction = null)
        {
            return RecursiveFindChildrenWithNamePattern(
                parent, $@"^({name})$", forEachFoundAction);
        }

        public static Transform RecursiveFindChild(
            Transform parent, string childName)
        {
            foreach (Transform child in parent)
            {
                if (child.name.Equals(childName))
                {
                    return child;
                }
                else
                {
                    Transform found = RecursiveFindChild(child, childName);
                    if (found != null)
                    {
                        return found;
                    }
                }
            }
            return null;
        }
    }
}
