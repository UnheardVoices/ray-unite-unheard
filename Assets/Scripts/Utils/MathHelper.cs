﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class MathHelper
    {
        public static float SinusInRange(
            float rangeA, float rangeB, float angleRadians)
        {
            float range = rangeB - rangeA;
            return Mathf.Sin(angleRadians) * range + (range / 2f) + rangeA;
        }
    }
}
