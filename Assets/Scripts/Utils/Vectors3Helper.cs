﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class Vectors3Helper
    {
        public static bool VectorsListContainsVectorWithSameDirection(Vector3 vector, List<Vector3> resultWallsNormals)
        {
            return resultWallsNormals.Where(x => Vector3.Dot(x.normalized, vector.normalized) > 0.95f).Count() > 0;
        }

        public static bool AreVectorsSameDirection(Vector3 vectorA, Vector3 vectorB)
        {
            return Vector3.Dot(vectorA.normalized, vectorB.normalized) > 0.95f;
        }
    }
}
