﻿using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class WallCollisionHelper
    {
        public static Vector3 GetMovementVelocityConsideringSolidEnvironmentCollisions(
            GameObject gameObject, RayCollider rayCollider,
            Vector3 entityForwardDirectionVector, Vector3 velocity
            )
        {
            if (Vector3.Angle(Vector3.down, velocity) < 90f)
            {
                rayCollider.AlignOnTopOfTheGround(null, Vector3.zero);
            }

            if (Vector3.Angle(Vector3.up, velocity) < 90f)
            {
                velocity = rayCollider.AdjustVelocityIfIsHittingTheCeiling(velocity);
            }

            velocity = GetMovementVelocityConsideringWallCollisions(
                gameObject, rayCollider, entityForwardDirectionVector,
                velocity);

            return velocity;
        }

        public static Vector3 GetMovementVelocityConsideringWallCollisions(
            GameObject gameObject, RayCollider rayCollider, Vector3 entityForwardDirectionVector,
            Vector3 velocity, float? wallHitCheckingRadius = null,
            Vector3 entityPositionOffsetForDebug = new Vector3(),
            bool debugInitialWallHitCheck = false,
            Color? debugInitialWallHitCheckColor = null,
            bool debugAveragingWallCollisionNormal = false,
            Color? debugAveragingWallCollisionNormalColor = null,
            bool debugHittingWallInGivenDirectionAtCorner = false,
            Color? debugHittingWallInGivenDirectionAtCornerColor = null,
            bool debugResultVelocity = false,
            float debugVelocityLengthMultiplier = 1.0f,
            Color? debugResultVelocityColor = null,
            bool debugResultWallNormal = false,
            Color? debugResultWallNormalColor = null,
            bool debugEntityForwardDirectionVector = false,
            float debugEntityForwardDirectionLengthMultiplier = 1.0f,
            Color? debugEntityForwardDirectionVectorColor = null)
        {
            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                entityForwardDirectionVector.normalized * debugEntityForwardDirectionLengthMultiplier,
                debugEntityForwardDirectionVectorColor,
                debugEntityForwardDirectionVector);

            if (rayCollider.IsHittingTheWall(
                wallHitCheckingRadius,
                debugInitialWallHitCheck,
                debugInitialWallHitCheckColor))
            {
                Tuple<Vector3, List<RaycastHit>>
                    wallsNormalsInfoPlayerCollidesWith =
                        rayCollider.GetAveragedNormalFromAllTheWallsTheCollisionOccurs(
                            wallHitCheckingRadius,
                            debugAveragingWallCollisionNormal,
                            debugAveragingWallCollisionNormalColor);

                Vector3 flatVelocity = Vector3.ProjectOnPlane(velocity, Vector3.up).normalized;

                if (flatVelocity.magnitude < 0.01f)
                {
                    // if velocity magnitude even after normalization is not sufficient
                    // (in case no velocity but still..), then substitute
                    // it with entity forward direction for the sake of calculations

                    flatVelocity = entityForwardDirectionVector;
                }

                if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 0)
                {
                    // if not colliding with anything, just return the velocity
                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                       velocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);
                    return velocity;
                }
                else
                {
                    if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 1)
                    {
                        Vector3 wallNormal =
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized;

                        DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                            wallNormal, debugResultWallNormalColor, debugResultWallNormal);

                        // if colliding with only one wall, then its easy
                        // project player's velocity on wall's plane so we can glide along the wall nicely
                        if (Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) > 90
                            || Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) < -90)
                        {
                            Vector3 resultVelocity = Vector3.ProjectOnPlane(
                                velocity, Vector3.ProjectOnPlane(
                                // always flatten the wall normal to the Vector3.up plane
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized);

                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                resultVelocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);

                            return resultVelocity;
                        }
                        else
                        {
                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                velocity * debugVelocityLengthMultiplier, debugResultVelocityColor, debugResultVelocity);
                            return velocity;
                        }
                    }
                    else
                    {
                        // colliding with more than wall, just use the heuristic that we can always get 
                        // two walls' normals that are at the corner, as long as it does the job, who cares :)
                        Vector3 wall1Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized;
                        Vector3 wall2Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[1].normal, Vector3.up).normalized;

                        Vector3 averagedWallsNormal = wallsNormalsInfoPlayerCollidesWith.Item1;

                        //DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                        //    averagedWallsNormal, debugResultWallNormalColor, debugResultWallNormal);

                        float angleBetweenAveragedWallsNormalAndWall1Normal =
                            Vector3.Angle(wall1Normal, averagedWallsNormal);
                        float angleBetweenAveragedWallsNormalAndWall2Normal =
                            Vector3.Angle(wall2Normal, averagedWallsNormal);

                        if (Vector3.Angle(entityForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall1Normal
                            && Vector3.Angle(entityForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall2Normal)
                        {
                            // if player tries to run in the direction outwards the corner in the direction towards the walls,
                            // simply do not let him move any further in that direction :)
                            // let him glide along one of the walls in the corner that is 'right' for that
                            Vector3 flattenedVelocity =
                                Vector3.ProjectOnPlane(velocity, Vector3.up).normalized *
                                    velocity.magnitude;

                            Vector3 potentialFlattenedVelocityAlongWall1 =
                                Vector3.ProjectOnPlane(flattenedVelocity, wall1Normal);
                            Vector3 potentialFlattenedVelocityAlongWall2 =
                                Vector3.ProjectOnPlane(flattenedVelocity, wall2Normal);

                            Vector3 potentialVelocityAlongWall1 =
                                Vector3.ProjectOnPlane(velocity, wall1Normal);
                            Vector3 potentialVelocityAlongWall2 =
                                Vector3.ProjectOnPlane(velocity, wall2Normal);

                            bool encountersWallAlongsideWall1 =
                                rayCollider.HitsTheWallCheckedInStackedRays(
                                    potentialFlattenedVelocityAlongWall1.normalized,
                                    debugHittingWallInGivenDirectionAtCorner,
                                    debugHittingWallInGivenDirectionAtCornerColor);

                            bool encountersWallAlongsideWall2 =
                                rayCollider.HitsTheWallCheckedInStackedRays(
                                    potentialFlattenedVelocityAlongWall2.normalized,
                                    debugHittingWallInGivenDirectionAtCorner,
                                    debugHittingWallInGivenDirectionAtCornerColor);

                            if (encountersWallAlongsideWall1 || encountersWallAlongsideWall2)
                            {
                                // allow player to move only in the direction that does not lead to another wall
                                if (!encountersWallAlongsideWall1)
                                {
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        potentialVelocityAlongWall1 * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return potentialVelocityAlongWall1;
                                }
                                else if
                                  (!encountersWallAlongsideWall2)
                                {
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        potentialVelocityAlongWall2 * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return potentialVelocityAlongWall2;
                                }
                                else
                                {
                                    Vector3 resultVelocity = new Vector3(0, velocity.y, 0);
                                    DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                        resultVelocity * debugVelocityLengthMultiplier,
                                        debugResultVelocityColor, debugResultVelocity);
                                    return resultVelocity;
                                }
                            }
                            else
                            {
                                DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                    velocity * debugVelocityLengthMultiplier,
                                    debugResultVelocityColor, debugResultVelocity);
                                return velocity;
                            }
                        }
                        else
                        {
                            DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                                 velocity * debugVelocityLengthMultiplier,
                                 debugResultVelocityColor, debugResultVelocity);
                            return velocity;
                        }
                    }
                }
            }
            else
            {
                DebugHelper.DrawRay(gameObject.transform.position + entityPositionOffsetForDebug,
                     velocity * debugVelocityLengthMultiplier,
                     debugResultVelocityColor, debugResultVelocity);
                return velocity;
            }
        }

        public static bool IsGoingTowardsTheWall(
            Vector3 entityPosition, Vector3 translation,
                int raysCount = 10,
                float raysArcAngle = 180f,
                float rayCheckLength = 0.8f,
                bool debug = false)
        {
            //RaycastHit emptyHit = new RaycastHit();
            if (raysCount < 2)
            {
                throw new ArgumentException("Rays count in here cannot be less than 2!");
            }

            Vector3 flatTranslation = Vector3.ProjectOnPlane(translation, Vector3.up);

            float angleStep = raysArcAngle / (raysCount - 1);

            for (int i = 0; i < raysCount; i++)
            {
                float currentAngle = (-raysArcAngle / 2f) + (i * angleStep);
                Vector3 rayVector = Quaternion.AngleAxis(currentAngle, Vector3.up)
                    * flatTranslation.normalized;
                if (PhysicsRaycaster.Raycast(
                    entityPosition, rayVector, rayCheckLength, Layers.generalEnvironmentLayersMask,
                    debug: debug))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
