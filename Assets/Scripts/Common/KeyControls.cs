﻿using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class KeyControls
    {
        public static string strafingButton = "Fire2";
        public static string groundRollButton = "Fire3";

        public static KeyCode menuKeyKeyboard = KeyCode.Escape;

        public static KeyCode cameraLeftButtonKeyboard = KeyCode.Q;
        public static KeyCode cameraRightButtonKeyboard = KeyCode.E;
        public static KeyCode cameraUpButtonKeyboard = KeyCode.W;
        public static KeyCode cameraDownButtonKeyboard = KeyCode.R;

        public static KeyCode walkingButtonKeyboard = KeyCode.LeftShift;
        public static KeyCode menuOptionApplyKeyKeyboard = KeyCode.Return;

        public static KeyCode menuPreviousCategoryKeyKeyboard = KeyCode.LeftControl;
        public static KeyCode menuNextCategoryKeyKeyboard = KeyCode.RightControl;
    }
}
