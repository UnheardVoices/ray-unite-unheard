﻿namespace Assets.Scripts.Common
{
    public static class Tags
    {
        public static string ledgeGrabbableTag = "LedgeGrabbable";
        public static string ledgeColliderTag = "LedgeCollider";
        public static string wallClimbTag = "ClimbWall";
        public static string roofHangingTag = "HangRoof";
        public static string gemsTag = "Gems";
        public static string projectilesTag = "Projectiles";
        public static string lumForHangingTag = "LumForHanging";
        public static string pigPotCrystalsTag = "PigPotCrystals";
        public static string destructibleTag = "Destructible";

        public static string targetablePartTag = "TargetablePart";
        public static string targetableSwitchTag = "TargetableSwitch";
        public static string enemyTag = "Enemy";
    }
}
