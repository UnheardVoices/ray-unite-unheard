﻿using Assets.Scripts.LevelMechanics;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public enum PigPotCrystalsDrop
    {
        DROP_3_YELLOW_CRYSTALS,
        DROP_4_YELLOW_CRYSTALS
    }

    public static class PigPotCrystalsDropHelper
    {
        public static int GetJewelsAmountToDrop(PigPotCrystalsDrop pigPotCrystalsDropType)
        {
            return pigPotCrystalsDropType ==
                PigPotCrystalsDrop.DROP_3_YELLOW_CRYSTALS ? 3 : 4;
        }
    }

    public static class DropJewelsSpawner
    {
        public static void SpawnYellowJewelsInCircle(
            Vector3 spawnOriginPosition,
            float spawnRadius,
            int jewelsCount)
        {
            float dropFlyingHeight = 1f;

            float angleRotationStep = 360f / jewelsCount;
            float currentRotationAngle = 0f;

            for (int i = 0; i < jewelsCount; i++)
            {
                Vector3 spawnFlyingDirection =
                    Quaternion.AngleAxis(currentRotationAngle, Vector3.up)
                        * Vector3.forward;

                DropJewelsFactory.SpawnDropGoldJewel(
                    spawnOriginPosition,
                    dropFlyingHeight,
                    spawnRadius,
                    spawnFlyingDirection
                );

                currentRotationAngle += angleRotationStep;
            }
        }
    }

    public class PigPotHitHdlBeh : TargetHitHandlingBeh
    {
        private bool isBeingHit = false;
        private bool isToBeDestroyed = false;
        private Vector3 originalPosition;
        private float currentHitShakeProgress = 0f;

        private float shakingSpeed = 0.3f;
        private float breakHitStrengthThreshold = 3f;

        public PigPotCrystalsDrop pigPotDropType;

        protected override void BehaviourStart()
        {
            this.isBeingHit = false;
            this.isToBeDestroyed = false;
            this.originalPosition = this.transform.position;
            this.currentHitShakeProgress = 0f;
        }

        public override void OnTargetHit(
           HandProjectileBehaviour handProjectileBehaviour)
        {
            this.isBeingHit = true;
            this.isToBeDestroyed =
                this.isToBeDestroyed || (handProjectileBehaviour.chargePower >= this.breakHitStrengthThreshold);
        }

        protected override void GameplayFixedUpdate()
        {
            if (!this.isToBeDestroyed && this.isBeingHit)
            {
                this.transform.position = this.originalPosition + Vector3.up * Mathf.Sin(this.currentHitShakeProgress);

                this.currentHitShakeProgress += this.shakingSpeed;
                if (this.currentHitShakeProgress > 180f * Mathf.Deg2Rad)
                {
                    this.currentHitShakeProgress = 0f;
                    this.isBeingHit = false;
                    this.transform.position = this.originalPosition;
                }
                return;
            }
            else if (this.isToBeDestroyed)
            {
                DropJewelsSpawner.SpawnYellowJewelsInCircle(
                    this.transform.position,
                    1.5f,
                    PigPotCrystalsDropHelper.GetJewelsAmountToDrop(this.pigPotDropType));
                FindObjectOfType<TargettingAspect>().targettables.Remove(this.gameObject);
                Destroy(this.gameObject);
                return;
            }
        }
    }
}
