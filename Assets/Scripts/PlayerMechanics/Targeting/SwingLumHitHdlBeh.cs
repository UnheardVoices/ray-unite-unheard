﻿using Assets.Scripts.Animations;
using Assets.Scripts.GameMechanics.Entities.Player.Rules;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public static class GrabbingLumHandOrientationHelper
    {
        public static void OrientateHand(
            GameObject handObject,
            GameObject playerActorObject,
            GameObject lumForSwingingObject)
        {
            Transform animatedPlayerPartTransform =
                playerActorObject.GetComponentInChildren<Animator>().transform;
            Vector3 animatedPlayerPartForwardDirection =
                -animatedPlayerPartTransform.forward;

            Vector3 lumForSwingingForwardDirection =
                lumForSwingingObject.transform.right;

            float angle = Vector3.Angle(
                    animatedPlayerPartForwardDirection,
                    lumForSwingingForwardDirection);

            Vector3 lumForSwingingForwardDirectionNormalizedWithPlayerForward =
                angle > 90f ?
                    -lumForSwingingForwardDirection :
                    lumForSwingingForwardDirection;

            handObject.transform.forward =
                lumForSwingingForwardDirectionNormalizedWithPlayerForward;
        }
    }

    public class SwingLumHitHdlBeh : TargetHitHandlingBeh
    {
        public float minimalSwingingDeflectionAngleDegrees = 60f;
        public float maximalSwingingDeflectionAngleDegrees = 70f;
        public float maxSwingingRadius = 3f;

        public override void OnTargetHit(
            HandProjectileBehaviour handProjectileBehaviour)
        {
            PlayerMovementStateInfo playerMovementStateInfo =
                this.playerObject.GetComponent<PlayerMovementStateInfo>();
            playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LUM_SWINGING;
            RuleLumSwinging ruleLumSwinging = this.playerObject.GetComponent<RuleLumSwinging>();

            GameObject animatedPlayerPart =
                this.playerObject.GetComponentInChildren<AnimationController>().gameObject;

            PlayerMovementMetrics playerMovementMetrics = FindObjectOfType<PlayerMovementMetrics>();
            playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_STRAFING;

            GameObject handObject =
                HandShootingProjectilesFactory.SpawnStaticHandObjectForLumSwinging(
                this.transform.position + Vector3.down * 0.5f,
                new Vector3(1.5f, 1.5f, 1.5f));

            GameObject playerActorObject =
                FindObjectOfType<PlayerActorHandle>().gameObject;

            GrabbingLumHandOrientationHelper
                .OrientateHand(
                    handObject,
                    playerActorObject,
                    this.gameObject);

            float velocity = playerMovementStateInfo
                .currentRule.Equals(PlayerMovementRuleEnum.LEGACY_EMPTY_RULE) ?
                playerActorObject.GetComponent<PlayerController>()
                    .controllerContext.kinematics.velocity.magnitude
                : playerMovementStateInfo.movementVelocity.magnitude;

            playerActorObject.GetComponent<PlayerController>()
                .SetCurrentRule(new EmptyNewPlayerMovementRule(
                    playerActorObject.GetComponent<PlayerController>()));

            ruleLumSwinging.ClearRuleState(
                this.gameObject, this.playerObject.transform.position,
                handProjectileBehaviour.isRightHand, handObject, this.maxSwingingRadius,
                velocity, this.minimalSwingingDeflectionAngleDegrees,
                this.maximalSwingingDeflectionAngleDegrees);

            // remove player shot hand from the scene for the time of swinging on the lum
            // we will recreate it flying towards the player to regain it
            // when the player leaves the lum
            Destroy(handProjectileBehaviour.gameObject);
        }
    }
}
