﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public class TargetHitHandlingBeh : GameplayOnlyMonoBehaviour
    {
        protected GameObject playerObject;

        private void Awake()
        {
            this.playerObject = FindObjectOfType<PlayerMovementStateInfo>().gameObject;
        }

        public virtual void OnTargetHit(HandProjectileBehaviour handProjectileBehaviour) { }
    }
}
