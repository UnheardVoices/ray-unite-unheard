﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Helper;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleLumSwinging : PlayerMovementRule
    {
        private GameObject lumForSwinging;
        private bool caughtByRightHand = true;
        private float swingingRadius = 0f;

        private float swingingGravityAcceleration = 0.025f;
        private float swingingDirectionChangingVelocity = 0.2f;

        private Vector3 initialPlayerPositionForSwinging;

        private GameObject handObject;

        private float minimalSwingingDeflectionAngleDegrees;
        private float maximalSwingingDeflectionAngleDegrees;
        private float maxSwingingRadius;

        private float swingingRadiusCorrectionStep = 0.1f;

        private bool hasGoneThroughDirectBottomOfLum = false;
        private bool hasEndedSwingingRadiusCorrectionProcess = false;

        private bool isBehindDirectBottomOfLum = true;

        private Vector3 swingingForwardDirection;
        private Vector3 swingingRightDirection;

        private Vector3 swingingVelocityVector = new Vector3(0f, 0f, 0f);
        private bool initialVelocitySet = false;
        private float initialVelocityMagnitude = 0f;

        private bool isOutsideBottomOfLum = true;

        private float maxDeflectionAngle = 0f;
        private float initialDeflectionAngle = 0f;

        private bool maxDeflectionAngleEstablished = false;
        private bool previousIsSwingingForward;
        private bool previousIsSwingingForwardEstablished = false;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_LUM_SWINGING;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        public void ClearRuleState(
            GameObject lumForSwinging,
            Vector3 playerPosition,
            bool caughtByRightHand,
            GameObject handObject,
            float maxSwingingRadius,
            float initialVelocityMagnitude,
            float minimalSwingingDeflectionAngleDegrees,
            float maximalSwingingDeflectionAngleDegrees)
        {
            this.lumForSwinging = lumForSwinging;
            this.hasGoneThroughDirectBottomOfLum = false;
            // we only care about vector's direction, no matter the vector's turn
            this.caughtByRightHand = caughtByRightHand;

            this.initialVelocityMagnitude = initialVelocityMagnitude;
            this.initialVelocitySet = false;

            this.playerMovementStateInfo.movementVelocity = new Vector3(0f, 0f, 0f);
            this.swingingRadius = Vector3.Distance(playerPosition, lumForSwinging.transform.position);

            this.initialPlayerPositionForSwinging = playerPosition;
            this.handObject = handObject;

            this.swingingVelocityVector = new Vector3(0f, 0f, 0f);

            this.minimalSwingingDeflectionAngleDegrees = minimalSwingingDeflectionAngleDegrees;
            this.maximalSwingingDeflectionAngleDegrees = maximalSwingingDeflectionAngleDegrees;
            this.maxSwingingRadius = maxSwingingRadius;

            this.swingingForwardDirection =
                -Vector3.ProjectOnPlane(
                    (this.gameObject.transform.position - lumForSwinging.transform.position).normalized,
                    Vector3.up).normalized;

            this.initialDeflectionAngle = Vector3.Angle(
                Vector3.down, (playerPosition - lumForSwinging.transform.position).normalized);

            this.swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
            this.isBehindDirectBottomOfLum = true;
            this.isOutsideBottomOfLum = true;
            this.hasEndedSwingingRadiusCorrectionProcess = false;
            this.maxDeflectionAngleEstablished = false;

            this.previousIsSwingingForwardEstablished = false;
        }

        protected override void OnRuleEnter()
        {
            this.animationController.StopAnimatorUpdating();
            FindObjectOfType<CameraLumSwinging>().SetLumForSwinging(this.lumForSwinging);
            this.playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_LUM_SWINGING;
            this.playerController.playerParams
                .soundParams.lumSwingingSounds
                .lumCatchingSound.PlayOnceWithAudioSource(
                    this.playerController.controllerContext
                    .playerAudioHandles.lumSwingingAudioHandles.lumCatchingLeavingAudioSource
                );
        }

        protected override void OnRuleExit()
        {
            this.animationController.ResumeAnimatorUpdating();
            if (this.handObject)
            {
                Destroy(this.handObject);
            }
            this.playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_FOLLOW;
            this.playerController.playerParams
                .soundParams.lumSwingingSounds
                .lumLeavingSound.PlayOnceWithAudioSource(
                    this.playerController.controllerContext
                    .playerAudioHandles.lumSwingingAudioHandles.lumCatchingLeavingAudioSource
                );
        }

        protected override void OnRuleFixedUpdate()
        {
            this.swingingRadius = Mathf.Clamp(this.swingingRadius, 3f, 10000f);
            if (this.swingingRadius > this.maxSwingingRadius && !this.hasEndedSwingingRadiusCorrectionProcess)
            {
                this.swingingRadius -= this.swingingRadiusCorrectionStep;
                this.hasGoneThroughDirectBottomOfLum = false;
            }

            if (this.swingingRadius <= this.maxSwingingRadius && !this.hasEndedSwingingRadiusCorrectionProcess)
            {
                this.hasEndedSwingingRadiusCorrectionProcess = true;
            }

            this.transform.forward = this.playerMovementMetrics.forwardDirection;
            this.animatedPlayerPart.transform.forward = -this.swingingForwardDirection;
            this.playerMovementMetrics.intentionalMovingDirection =
                this.swingingForwardDirection.normalized;

            Vector3 velocityPlaneNormal = (this.gameObject.transform.position - this.lumForSwinging.transform.position).normalized;

            if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            if (this.playerMovementInput.GetJumpButton() &&
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                LegacyRuleLumSwingingToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleLumSwinging(
                       this.playerMovementMetrics,
                       this.playerMovementStateInfo,
                       this.playerShootingAspect,
                       this.animatedPlayerPart,
                       this.lumForSwinging,
                       this.caughtByRightHand,
                       this.playerController
                    );
                //playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_FOLLOW;
                //playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                //playerMovementStateInfo.isJumping = true;
                //playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                //playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

                //HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                //    playerShootingAspect, animatedPlayerPart.transform, lumForSwinging.transform.position, caughtByRightHand);

                return;
            }

            if (this.rayCollider.HasHitTheSolidObstacle())
            {
                LegacyRuleLumSwingingToNewAirTransitions.
                    EnterRuleAirOrGroundHittingObstacleFromRuleLumSwinging(
                       this.rayCollider,
                       this.playerShootingAspect,
                       this.animatedPlayerPart,
                       this.lumForSwinging,
                       this.playerMovementMetrics,
                       this.playerMovementStateInfo,
                       this.caughtByRightHand,
                       this.playerController
                    );

                //bool isTouchingTheGround = rayCollider.IsHittingGround();

                //playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_FOLLOW;
                //playerMovementStateInfo.currentRule =
                //    isTouchingTheGround ? PlayerMovementRuleEnum.RULE_GROUND : PlayerMovementRuleEnum.RULE_AIR;
                //playerMovementStateInfo.isJumping = false;
                //playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                //playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

                //HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                //    playerShootingAspect, animatedPlayerPart.transform, lumForSwinging.transform.position, caughtByRightHand);

                return;
            }

            this.transform.position = this.lumForSwinging.transform.position + (velocityPlaneNormal * this.swingingRadius);
            Vector3 currentVelocityWorldSpace =
                Vector3.ProjectOnPlane(
                    this.swingingVelocityVector,
                    velocityPlaneNormal).normalized *
                this.swingingVelocityVector.magnitude;

            if (!this.initialVelocitySet)
            {
                this.swingingVelocityVector = Vector3.ProjectOnPlane(Vector3.down, velocityPlaneNormal).normalized
                    * this.initialVelocityMagnitude;
                this.initialVelocitySet = true;
            }

            Vector3 gravitionalAcceleration = new Vector3(0f, -this.swingingGravityAcceleration, 0f);
            Vector3 projectedGravitionalAcceleration =
                Vector3.ProjectOnPlane(gravitionalAcceleration, velocityPlaneNormal);

            currentVelocityWorldSpace =
               currentVelocityWorldSpace + projectedGravitionalAcceleration;

            this.swingingVelocityVector = currentVelocityWorldSpace;

            Vector3 playerInputTranslationLocalSpace = this.playerMovementInput.GetTranslation();
            playerInputTranslationLocalSpace = Vector3.Project(playerInputTranslationLocalSpace, Vector3.right);
            Vector3 playerInputTranslationWorldSpace =
                this.transform.TransformDirection(playerInputTranslationLocalSpace);

            if (playerInputTranslationWorldSpace.magnitude > 0.1f
                //&& isBehindDirectBottomOfLum
                )
            {
                float swingingDirectionInterpolationStep = 0.03f;
                //float maxSwingingRotationAngleChangeSpeed = 5f;

                playerInputTranslationWorldSpace =
                    this.isBehindDirectBottomOfLum ? -playerInputTranslationWorldSpace :
                        playerInputTranslationWorldSpace;

                float angleToRotateBy = SwingingDirectionInterpolationHelper
                    .GetAngleToRotateInFlatDirection(velocityPlaneNormal, playerInputTranslationWorldSpace,
                        swingingDirectionInterpolationStep);

                velocityPlaneNormal = (Quaternion.AngleAxis(
                    angleToRotateBy, Vector3.up)
                    * velocityPlaneNormal).normalized;

                currentVelocityWorldSpace = (Quaternion.AngleAxis(
                    angleToRotateBy, Vector3.up)
                    * currentVelocityWorldSpace).normalized * currentVelocityWorldSpace.magnitude;

                this.swingingVelocityVector = (Quaternion.AngleAxis(
                    angleToRotateBy, Vector3.up)
                    * this.swingingVelocityVector).normalized * this.swingingVelocityVector.magnitude;
            }

            this.transform.position = this.lumForSwinging.transform.position +
                (velocityPlaneNormal.normalized * this.swingingRadius) + currentVelocityWorldSpace;

            //Debug.DrawRay(lumForSwinging.transform.position, swingingRightDirection * 5f);

            float bottomOfLumRangeAngle = 10f;

            float angleBetweenFlatVelocityPlaneNormalAndCurrentSwingingForwardDirection =
                Vector3.Angle(this.swingingForwardDirection, Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up));

            if (angleBetweenFlatVelocityPlaneNormalAndCurrentSwingingForwardDirection >= 89f)
            {
                this.swingingForwardDirection = -Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
                this.swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
            }
            else
            {
                this.swingingForwardDirection = Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
                this.swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
            }

            //if (isBehindDirectBottomOfLum)
            //{
            //    // is behind lum when swinging
            //    swingingForwardDirection = -Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
            //    //swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
            //}
            //else
            //{
            //    // is in front of lum when swinging
            //    swingingForwardDirection = Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
            //    //swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
            //}

            if (this.isOutsideBottomOfLum)
            {
                float angleToBottom = Vector3.SignedAngle(Vector3.down, velocityPlaneNormal, this.swingingRightDirection);

                if (Mathf.Abs(angleToBottom) <= bottomOfLumRangeAngle)
                {
                    // is qualified inside bottom of lum
                    if (angleToBottom >= 0)
                    {
                        this.isBehindDirectBottomOfLum = false;

                        //swingingForwardDirection = -Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
                        //swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;

                    }
                    else
                    {
                        this.isBehindDirectBottomOfLum = true;

                        //swingingForwardDirection = Vector3.ProjectOnPlane(velocityPlaneNormal, Vector3.up).normalized;
                        //swingingRightDirection = Vector3.Cross(this.swingingForwardDirection, Vector3.down).normalized;
                    }

                    float angleOfPlayerSwinging =
                        Vector3.Angle(this.swingingForwardDirection,
                            Vector3.ProjectOnPlane(this.swingingVelocityVector, Vector3.up).normalized);

                    if (!this.hasGoneThroughDirectBottomOfLum && angleOfPlayerSwinging < 89f)
                    {
                        float deflectionAngle =
                            LumSwingingHelper.CalculateDeflectionAngle(
                                this.swingingVelocityVector.magnitude,
                                this.swingingGravityAcceleration,
                                this.swingingRadius
                                );

                        if (deflectionAngle < this.minimalSwingingDeflectionAngleDegrees)
                        {
                            this.swingingVelocityVector =
                            (this.hasEndedSwingingRadiusCorrectionProcess ?
                                this.swingingForwardDirection.normalized : this.swingingVelocityVector.normalized) *
                            Mathf.Sqrt(2 * this.swingingGravityAcceleration * this.swingingRadius *
                                (1 - Mathf.Cos(Mathf.Deg2Rad * this.minimalSwingingDeflectionAngleDegrees)));
                            this.maxDeflectionAngle = this.minimalSwingingDeflectionAngleDegrees;
                        }
                        else if (deflectionAngle > this.maximalSwingingDeflectionAngleDegrees)
                        {
                            this.swingingVelocityVector =
                            (this.hasEndedSwingingRadiusCorrectionProcess ?
                                this.swingingForwardDirection.normalized : this.swingingVelocityVector.normalized) *
                            Mathf.Sqrt(2 * this.swingingGravityAcceleration * this.swingingRadius *
                                (1 - Mathf.Cos(Mathf.Deg2Rad * this.maximalSwingingDeflectionAngleDegrees)));
                            this.maxDeflectionAngle = this.maximalSwingingDeflectionAngleDegrees;
                        }
                        else
                        {
                            this.maxDeflectionAngle = deflectionAngle;
                        }
                        this.maxDeflectionAngleEstablished = true;

                        this.hasGoneThroughDirectBottomOfLum = true;
                    }

                    this.isOutsideBottomOfLum = false;
                }
            }
            else
            {
                float angleToBottom = Vector3.SignedAngle(Vector3.down, velocityPlaneNormal, this.swingingRightDirection);
                if (Mathf.Abs(angleToBottom) > bottomOfLumRangeAngle)
                {
                    this.isOutsideBottomOfLum = true;
                }
            }

            //if (!hasGoneThroughDirectBottomOfLum && 
            //    Vector3.Angle(velocityPlaneNormal, Vector3.down) < 5f)
            //{
            //    // .... TODO
            //    hasGoneThroughDirectBottomOfLum = true;
            //}

            if (!this.maxDeflectionAngleEstablished)
            {
                float animationTimeProgress =
                    (Vector3.Angle(Vector3.down, velocityPlaneNormal)
                    / this.initialDeflectionAngle) / 2f;

                this.animationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName(),
                    Mathf.Clamp(0.5f - animationTimeProgress, 0f, 0.5f),
                    Time.fixedDeltaTime);
            }
            else
            {
                float animationTimeProgress =
                    ((-Vector3.SignedAngle(Vector3.down, velocityPlaneNormal, this.swingingRightDirection) / this.maxDeflectionAngle)
                    + 1f) / 2f;

                bool isSwingingForward = Vector3.Angle(
                    Vector3.ProjectOnPlane(this.swingingVelocityVector, Vector3.up).normalized,
                    this.swingingForwardDirection) < 89f;

                if (this.previousIsSwingingForwardEstablished)
                {
                    if (!this.previousIsSwingingForward && isSwingingForward)
                    {
                        // from back to forward
                        this.playerController.playerParams
                            .soundParams.lumSwingingSounds
                            .swingingBackToFrontSound
                                .PlayOnceWithAudioSource(
                                    this.playerController.controllerContext
                                        .playerAudioHandles.lumSwingingAudioHandles
                                        .lumSwingingDirectionAudioSource);
                    } else if (this.previousIsSwingingForward && !isSwingingForward)
                    {
                        // from forward to back
                        this.playerController.playerParams
                            .soundParams.lumSwingingSounds
                            .swingingFrontToBackSound
                                .PlayOnceWithAudioSource(
                                    this.playerController.controllerContext
                                        .playerAudioHandles.lumSwingingAudioHandles
                                        .lumSwingingDirectionAudioSource);
                    }
                }

                this.previousIsSwingingForward = isSwingingForward;
                this.previousIsSwingingForwardEstablished = true;

                if (isSwingingForward)
                {
                    this.animationController.SetAnimationFrameInManualUpdating(
                        RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName(),
                        Mathf.Clamp01(animationTimeProgress),
                        Time.fixedDeltaTime);
                }
                else
                {
                    this.animationController.SetAnimationFrameInManualUpdating(
                        RaymanAnimations.LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName(),
                        Mathf.Clamp01(1f - animationTimeProgress),
                        Time.fixedDeltaTime);
                }
            }
        }

        //private void UpdateSwingingAnimationAccordingToSwingProgress(
        //    float swingProgress, bool swingingForward, float deltaTime)
        //{
        //    if (swingingForward)
        //    {
        //        animationController.SetAnimationFrameInManualUpdating(
        //            RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName(),
        //            swingProgress,
        //            deltaTime);
        //    } else
        //    {
        //        animationController.SetAnimationFrameInManualUpdating(
        //           RaymanAnimations.LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName(),
        //           swingProgress,
        //           deltaTime
        //           );
        //    }

        //}

        //private Tuple<float, bool> GetCurrentSwingTrajectoryProgressInfo(Vector3 velocityWorldSpace)
        //{
        //    float angle = Vector3.Angle(
        //        -animatedPlayerPart.transform.forward,
        //        Vector3.ProjectOnPlane(velocityWorldSpace, Vector3.up).normalized);


        //    bool isSwingingForward = angle < 90;


        //    // from the law of energy/momentum conservation... should roughly just work..
        //    Vector3 maxStartDirectionToPlayerInSwingingSequence = 
        //        (initialPlayerPositionForSwinging - lumForSwinging.transform.position).normalized;
        //    Vector3 mirroredMaxDirectionInSwinging = 
        //        Vector3.Reflect(-maxStartDirectionToPlayerInSwingingSequence, Vector3.down).normalized;

        //    Vector3 directionToPlayer = (transform.position - lumForSwinging.transform.position).normalized;

        //    float angularDistanceFromSwingStartExtremumPointToCurrentPosition =
        //        isSwingingForward ? 
        //        Vector3.Angle(maxStartDirectionToPlayerInSwingingSequence, directionToPlayer) : 
        //        Vector3.Angle(mirroredMaxDirectionInSwinging, directionToPlayer);

        //    float halfOverallAngularDistance = Vector3.Angle(Vector3.down, maxStartDirectionToPlayerInSwingingSequence);

        //    return Tuple.Create(
        //        angularDistanceFromSwingStartExtremumPointToCurrentPosition / (2f * halfOverallAngularDistance),
        //        isSwingingForward);

        //    //float angularDistanceFromSwingingMiddlePointToPlayer =
        //    //    Vector3.Angle(Vector3.down, directionToPlayer);

        //    //float angularDistanceFromSwingingMiddlePointToPlayerWithSign =
        //    //    Vector3.SignedAngle(Vector3.down, directionToPlayer, -animatedPlayerPart.transform.right);

        //    //float circularDistanceFromPlayerToMiddle = 
        //    //    2 * Mathf.PI * swingingRadius * (angularDistanceFromSwingingMiddlePointToPlayer / 360f);
        //}
    }
}
