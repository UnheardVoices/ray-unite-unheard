﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Engine.Input;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleLedgeGrabbing : PlayerMovementRule
    {
        private RaycastHit ledgeColliderHit;
        private bool canClimbTheLedge = false;
        private bool isAllowedToClimb = false;
        private Vector3 ledgeGrabbingLocalPosition;

        private Vector3 startingHangingPosition;
        private Vector3 endingClimbingPosition;

        protected PlayerInputHub playerInputHub;
        private CameraFollow cameraFollowRule;

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            this.playerInputHub = FindObjectOfType<PlayerInputHub>();
            this.cameraFollowRule = FindObjectOfType<CameraFollow>();
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        public void ClearRuleState(RaycastHit ledgeColliderHit)
        {
            this.ledgeColliderHit = ledgeColliderHit;
            this.canClimbTheLedge = false;
            this.isAllowedToClimb = false;

            LedgeColliderInfo ledgeColliderInfo =
                this.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;
            Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                this.transform.position,
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

            this.ledgeGrabbingLocalPosition = this.ledgeColliderHit.transform.InverseTransformPoint(nearestPointOnEdge);
        }

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
        }

        protected override void OnRuleExit()
        {
            this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
        }

        protected override void OnRuleEnter()
        {
            this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_LEDGE_GRAB;

            LedgeColliderInfo ledgeColliderInfo =
                this.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;
            //Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
            //    this.transform.position,
            //    ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
            //    ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

            this.animatedPlayerPart.transform.forward = grabNormal;
            this.playerMovementMetrics.intentionalMovingDirection =
                -grabNormal.normalized;
            this.transform.position = ledgeColliderInfo.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
            this.animatedPlayerPart.transform.position = 
                ledgeColliderInfo.transform.TransformPoint(this.ledgeGrabbingLocalPosition) + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;

            DelayAllowingPlayerToClimbMilliseconds(300);

            this.animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName(),
                priority: 5);

            this.playerController.playerParams.soundParams
                .ledgeGrabbingSounds.ledgeGrabbingSound
                .PlayOnceWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                        .ledgeGrabbingAudioHandles.ledgeGrabbingAudioSource);
        }

        private IEnumerator AllowPlayerToClimbAfterMilliseconds(int milliseconds)
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            this.isAllowedToClimb = true;
        }

        private void DelayAllowingPlayerToClimbMilliseconds(int milliseconds)
        {
            StartCoroutine(AllowPlayerToClimbAfterMilliseconds(milliseconds));
        }

        protected override void OnRuleFixedUpdate()
        {
            LedgeColliderInfo ledgeColliderInfoMov =
                this.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();

            Vector3 grabNormalMov = ledgeColliderInfoMov.normal;

            Vector3 nearestPointOnEdgeMov = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                this.transform.position,
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointALocal),
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointBLocal));

            this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
            // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;

            if (this.animationController.GetCurrentAnimationState().Equals(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName()) &&
                this.animationController.HasAnimationEnded())
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.HangingOnLedgeStateName());
                this.canClimbTheLedge = true;

                this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                return;
            }

            if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
            }

            LedgeColliderInfo ledgeColliderInfo =
                    this.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;

            if (this.playerMovementInput.GetJumpButton() && this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                LegacyRuleLedgeGrabbingToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleLedgeGrabbing(
                        this.transform,
                        this.rayCollider,
                        this.animatedPlayerPart,
                        this.animationController,
                        this.playerMovementStateInfo,
                        this.playerMovementMetrics,
                        ledgeColliderInfo,
                        grabNormal,
                        this.playerController
                    );
                this.playerController.playerParams.soundParams
                    .ledgeGrabbingSounds.ledgeLeavingJumpSound
                    .PlayOnceWithAudioSource(
                        this.playerController.controllerContext.playerAudioHandles
                            .ledgeGrabbingAudioHandles.ledgeGrabbingAudioSource);

                return;
            }

            if (this.canClimbTheLedge)
            {
                float axesMagnitude =
                Mathf.Clamp01(new Vector2(
                    this.playerInputHub.moveInputRaw.x, this.playerInputHub.moveInputRaw.y).magnitude);

                Vector3 translation =
                       this.transform.TransformDirection((this.playerInputHub.moveInputRaw.y * Vector3.forward).normalized +
                       (this.playerInputHub.moveInputRaw.x * Vector3.right).normalized).normalized
                       * axesMagnitude;

                this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;

                if (Vector3.Angle(translation, -grabNormal) < 80f && translation.magnitude > 0.1f && this.isAllowedToClimb)
                {
                    //if deciding to climb the ledge
                    this.canClimbTheLedge = false;

                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.ClimbingFromHangingOnLedgeStateName(),
                        priority: 5);
                    this.playerController.playerParams.soundParams
                        .ledgeGrabbingSounds.ledgeClimbingSound
                        .PlayOnceWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                                .ledgeGrabbingAudioHandles.ledgeGrabbingAudioSource);

                    Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                        this.transform.position,
                        ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointALocal),
                        ledgeColliderInfo.transform.TransformPoint(ledgeColliderInfo.edgePointBLocal));

                    this.startingHangingPosition = nearestPointOnEdge + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;
                    this.endingClimbingPosition = this.transform.position + ((-grabNormal.normalized) * 1.18f);

                    this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                    // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                    return;
                }
                else if (!this.playerMovementInput.GetJumpButton() && Vector3.Angle(translation, -grabNormal) > 100f &&
                    translation.magnitude > 0.1f)
                {
                    LegacyRuleLedgeGrabbingToNewAirTransitions.
                        EnterRuleAirFallingOffFromRuleLedgeGrabbing(
                            this.transform,
                            this.rayCollider,
                            this.animatedPlayerPart,
                            this.playerMovementStateInfo,
                            this.playerMovementMetrics,
                            ledgeColliderInfo,
                            grabNormal,
                            this.playerController
                        );
                    this.playerController.playerParams.soundParams
                        .ledgeGrabbingSounds.ledgeLeavingSoundDown
                        .PlayOnceWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                                .ledgeGrabbingAudioHandles.ledgeGrabbingAudioSource);
                    this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                    // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                    return;
                }
                this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                // otherwise ignore all Player's intentions, inputs etc for now :)
            }
            this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
            // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
        }

        protected override void OnRuleUpdate()
        {
            LedgeColliderInfo ledgeColliderInfoMov =
                this.ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();

            Vector3 grabNormalMov = ledgeColliderInfoMov.normal;

            Vector3 nearestPointOnEdgeMov = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                this.transform.position,
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointALocal),
                ledgeColliderInfoMov.transform.TransformPoint(ledgeColliderInfoMov.edgePointBLocal));

            this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);

            if (this.animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.ClimbingFromHangingOnLedgeStateName()))
            {
                this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);

                if (!this.animationController.HasAnimationEnded())
                {
                    this.startingHangingPosition = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                    this.endingClimbingPosition = nearestPointOnEdgeMov + ((-grabNormalMov.normalized) * 1.18f);
                    // still climbing, adjust Rayman's model position in-between
                    // since climbing animation has positioning offset...
                    float animationNormalizedTime = this.animationController.GetAnimationNormalizedTime();

                    this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                    this.animatedPlayerPart.transform.position =
                        Vector3.Lerp(
                            this.startingHangingPosition,
                            this.endingClimbingPosition,
                            animationNormalizedTime);
                    return;
                }
                else
                {
                    this.startingHangingPosition = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
                    this.endingClimbingPosition = this.transform.position + ((-grabNormalMov.normalized) * 1.18f);

                    //this.transform.position = this.endingClimbingPosition;
                    this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
                    this.animatedPlayerPart.transform.position = this.transform.position;
                    this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                    this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                    this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                    // climbing ended, move to GROUND rule with 'clear/canonical' state
                    return;
                }
            }
            this.transform.position = ledgeColliderInfoMov.transform.TransformPoint(this.ledgeGrabbingLocalPosition);
            // this.animatedPlayerPart.transform.position = nearestPointOnEdgeMov + Vector3.down * 1.78f + grabNormalMov.normalized * 0.82f;
        }
    }
}
