﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class GroundMovementHelper
    {
        public static Vector3 GetClippedTranslationConsideringWalkingCycles(
            Vector3 inputTranslation, float maxSpeed)
        {
            if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude < 0.3f)
            {
                return inputTranslation.normalized * (maxSpeed / 3f);
            }
            else if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude <= 0.8f)
            {
                return inputTranslation.normalized * (maxSpeed / 2f);
            }
            else if (inputTranslation.magnitude > 0.8f)
            {
                return inputTranslation.normalized * (maxSpeed);
            }
            else
            {
                return Vector3.zero;
            }
        }
    }

    public class RuleGround : GroundRulesBase
    {
        private float playerRunningSpeed = 0.21f;
        //private Vector3 movementVelocity = new Vector3(0.0f, 0.0f, 0.0f);

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_GROUND;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnRuleEnter()
        {
            this.playerMovementStateInfo.isHelicopter = false;
            //this.helicopterSound.Pause();
            this.playerMovementStateInfo.isJumping = false;
            this.playerMovementStateInfo.previousLocalGroundContactPointOffset = null;
        }

        protected override void OnRuleExit()
        {

        }

        protected override void OnRuleFixedUpdate()
        {
            #region Rule
            if (this.playerMovementInput.GetStrafingButton())
            {
                // exit ground rule right away and go into strafing ground rule
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                this.playerController.playerParams.soundParams
                    .strafingGroundSounds.enteringStrafingSound.PlayOnceWithAudioSource(
                        this.playerController.controllerContext.playerAudioHandles
                        .strafingGroundAudioHandles.enteringLeavingStrafingAudioSource
                    );
                return;
            }

            if (this.playerMovementInput.GetGroundRollButton() &&
                (!this.playerGroundRollingAspect.IsPerformingGroundRoll() && !this.playerGroundRollingAspect.HasEndedGroundRoll()))
            {
                this.playerGroundRollingAspect.StartPerformingForwardGroundRoll(
                    -this.animatedPlayerPart.transform.forward);
                this.playerController.playerParams.soundParams.groundSounds.rollSound
                    .PlayOnceWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.groundAudioHandles.rollAudioSource);
                return;
            }

            HandleShooting();
           
            this.playerMovementStateInfo.previousLocalGroundContactPointOffset =
                this.rayCollider.AlignOnTopOfTheGround(
                    this.playerMovementStateInfo.previousLocalGroundContactPointOffset,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity));

            Vector3 inputTranslation = this.playerMovementInput.GetTranslation();
            Vector3 translation = this.playerMovementInput.GetTranslation() * this.playerRunningSpeed;

            Vector3 translationDirectionInWorldSpace = 
                (Quaternion.AngleAxis(this.playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            this.playerMovementStateInfo.movementVelocity.y = 0;

            if (!this.playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                if (inputTranslation.magnitude < 0.1f)
                {
                    float movementSmoothingInterpolation = 0.6f;
                    this.playerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                    this.playerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
                }
                else
                {
                    float movementSmoothingInterpolation = 0.25f;
                    this.playerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                    this.playerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
                }

                if (inputTranslation.magnitude > 0.1f)
                {
                    this.transform.forward = this.playerMovementMetrics.forwardDirection;
                    this.animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
                    this.playerMovementMetrics.intentionalMovingDirection =
                        translationDirectionInWorldSpace.normalized;
                }
            }
            else
            {
                this.playerMovementStateInfo.movementVelocity =
                    this.transform.InverseTransformDirection(this.playerGroundRollingAspect
                        .GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            }

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    this.gameObject,
                    this.rayCollider,
                    this.playerMovementMetrics.intentionalMovingDirection.normalized,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            Vector3 nonInterpolatedInputTranslation = this.playerMovementInput.GetNonInterpolatedTranslation();

            string currentAnimationState = this.animationController.GetCurrentAnimationState();

            if (
                !RaymanAnimationsGroundHelper.IsUprisingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsLandingAnimation(currentAnimationState))
            {
                this.transform.Translate(this.playerMovementStateInfo.movementVelocity);
            }
            else if (
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState))
            {
                this.transform.Translate(this.playerMovementStateInfo.movementVelocity / 2f);
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
            {
                Vector3 fadingOutTranslation;

                if (this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()))
                {
                    fadingOutTranslation = (
                        -this.animatedPlayerPart.transform.forward).normalized * this.playerRunningSpeed * 0.25f *
                        (1f - this.animationController.GetAnimationNormalizedTime());

                    //transform.Translate(transform.InverseTransformDirection(
                    //    -animatedPlayerPart.transform.forward).normalized * playerRunningSpeed * 0.25f *
                    //    (1f - animationController.GetAnimationNormalizedTime()));
                }
                else
                {
                    fadingOutTranslation = (
                        -this.animatedPlayerPart.transform.forward).normalized * this.playerRunningSpeed * 0.6f *
                        (1f - this.animationController.GetAnimationNormalizedTime());

                    //transform.Translate(transform.InverseTransformDirection(
                    //    -animatedPlayerPart.transform.forward).normalized * playerRunningSpeed * 0.6f *
                    //    (1f - animationController.GetAnimationNormalizedTime()));
                }

                if (!WallCollisionHelper.IsGoingTowardsTheWall(
                    this.transform.position + Vector3.up, fadingOutTranslation))
                {
                    this.transform.Translate(this.transform.InverseTransformDirection(fadingOutTranslation));
                    //this.playerMovementStateInfo.previousLocalGroundContactPointOffset =
                    //    this.playerMovementStateInfo.previousLocalGroundContactPointOffset + ;
                }
            }

            if (this.playerMovementInput.GetJumpButton()
                && this.rayCollider.IsHittingGround()
                && this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState
                && !this.playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                LegacyRuleGroundToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleGround(
                        this.playerMovementStateInfo,
                        this.playerMovementMetrics,
                        this.playerController
                    );
                this.playerMovementStateInfo.isJumping = true;
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                return;
            }

            if (!this.rayCollider.IsHittingGround())
            {
                //playerGroundRollingAspect.BreakTheRollCycle();

                LegacyRuleGroundToNewAirTransitions.
                    EnterRuleAirFreeFallingFromRuleGround(
                        this.playerGroundRollingAspect,
                        this.playerMovementStateInfo,
                        this.playerMovementMetrics,
                        this.playerController
                    );
                //playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                return;
            }

            if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation

            if (this.enteringFromGroundStrafing)
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.StrafingExitIntoIdleGroundStateName());
                this.enteringFromGroundStrafing = false;
                return;
            }

            if (this.landingWithHelicopter)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    this.animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                }
                else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    this.animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName());
                }
                else
                {
                    this.animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                }


                this.landingWithHelicopter = false;
                this.landing = false;
                return;
            }
            else if (this.landing)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    this.animationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToWalkingCycle2StateName());
                }
                else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    this.animationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToGroundIdleStateName());
                }
                else
                {
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName());
                }


                this.landingWithHelicopter = false;
                this.landing = false;
                return;
            }



            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f
                && this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude > 0.8f &&
                this.animationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f &&
                this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.RunningAnimationStateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName());
                return;
            }
            else if (nonInterpolatedInputTranslation.magnitude <= 0.1f &&
                this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.WalkingCycle2StateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
                return;
            }

            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f &&

                (this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName())

                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(this.animationController.GetCurrentAnimationState())
                )
                && !this.animationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude > 0.8f &&
                (this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName())
                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(this.animationController.GetCurrentAnimationState()))
                && !this.animationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude < 0.1f &&
                (
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()
                ))
                && !this.animationController.HasAnimationEnded())
            {
                return;
            }

            if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude < 0.4f)
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.WalkingCycle1StateName());
            }
            else if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude <= 0.8f)
            {
                this.animationController.ChangeAnimationState(RaymanAnimations.WalkingCycle2StateName());
            }
            else if (inputTranslation.magnitude > 0.8f)
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.RunningAnimationStateName());
            }
            else if ((!RaymanAnimationsGroundHelper
                .IsLandingAnimation(this.animationController.GetCurrentAnimationState())
                && !this.animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingExitIntoIdleGroundStateName())) ||
                this.animationController.HasAnimationEnded())
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.IdleAnimationStateName());
            }
            #endregion
        }
    }
}
