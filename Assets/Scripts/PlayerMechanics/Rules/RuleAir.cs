﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleAir : PlayerMovementRule
    {
        private float playerInAirSpeed = 0.21f;
        private float playerJumpSpeed = 0.28f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_AIR;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnRuleEnter()
        {
            this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
        }

        protected override void OnRuleExit()
        {
            if (!this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                this.playerMovementStateInfo.isJumping = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                this.playerMovementStateInfo.isHelicopter = false;
                //this.helicopterSound.Pause();
            }
        }

        protected override void OnRuleFixedUpdate()
        {
            if (this.playerMovementInput.GetStrafingButton())
            {
                // exit air rule right away and go into strafing air rule
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                return;
            }

            HandleShooting();
            Vector3 inputTranslation = this.playerMovementInput.GetTranslation();
            Vector3 translation = this.playerMovementInput.GetTranslation() * this.playerInAirSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(this.playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
            }

            if (inputTranslation.magnitude > 0.1)
            {
                this.transform.forward = this.playerMovementMetrics.forwardDirection;
                this.animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
                this.playerMovementMetrics.intentionalMovingDirection =
                    translationDirectionInWorldSpace.normalized;
            }

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    this.gameObject,
                    this.rayCollider,
                    this.playerMovementMetrics.intentionalMovingDirection.normalized,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection(
                this.rayCollider.AdjustVelocityIfIsHittingTheCeiling(
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            if (this.playerMovementStateInfo.isJumping)
            {
                this.playerMovementStateInfo.isJumping = false;
                //this.jumpSound.Play();
                this.playerMovementStateInfo.movementVelocity.y = this.playerJumpSpeed;

                if (inputTranslation.magnitude > 0.7f)
                {
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName(),
                        priority: 3);
                }
                else
                {
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                        priority: 3);
                }
            }

            if (this.playerMovementStateInfo.movementVelocity.y > -PlayerMovementMetrics.limitFallSpeed)
            {
                if (!this.playerMovementStateInfo.isHelicopter)
                {
                    this.playerMovementStateInfo.movementVelocity.y -= PlayerMovementMetrics.gravityAcceleration;
                }
            }

            if (this.playerMovementStateInfo.isHelicopter)
            {
                this.playerMovementStateInfo.movementVelocity.y = -PlayerMovementMetrics.limitFallSpeedHelicopter;
            }

            this.transform.Translate(this.playerMovementStateInfo.movementVelocity);

            if (this.animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName()) &&
                  this.animationController.HasAnimationEnded())
            {
                if (this.playerMovementStateInfo.movementVelocity.y < 0.1f)
                {
                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateSpeed(), 2);

                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
                }
            }
            else if ((this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName())
                    && this.animationController.HasAnimationEnded())
               )
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.FallingFromJumpFromRunningAnimationStateName());
            }

            else
                if ((this.playerMovementStateInfo.movementVelocity.y < 0.1f &&
                    this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!this.playerMovementStateInfo.isHelicopter &&
                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&

                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if (
                this.playerMovementInput.GetJumpButton() &&
                    !this.playerMovementStateInfo.isHelicopter
                    && this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                this.playerMovementStateInfo.isHelicopter = true;
                //this.helicopterSound.Play();
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.animationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), priority: 4);
            }

            if (this.playerMovementStateInfo.isHelicopter
                    && !this.playerMovementInput.GetJumpButton() &&
                    this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.isHelicopter = false;
                //this.helicopterSound.Pause();
                this.animationController.ChangeAnimationState(
                   RaymanAnimations.FallingAnimationStateName());
            }

            if (this.animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  this.animationController.HasAnimationEnded())
            {
                this.animationController.ChangeAnimationState(
                   RaymanAnimations.HelicopterStateName());
            }

            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (this.rayCollider.IsHittingGround() && this.playerMovementStateInfo.movementVelocity.y <= 0)
            {
                // allow falling animation to be interrupted
                this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                GetComponent<RuleGround>().SetTransitionData(
                    landing: true,
                    withHelicopterFlag: this.playerMovementStateInfo.isHelicopter);
                return;
            }
            else if (this.rayCollider.IsCollidingWithLedgeGrabCollider(out ledgeGrabHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                this.playerMovementStateInfo.isHelicopter = false;
                //this.helicopterSound.Pause();
                GetComponent<RuleLedgeGrabbing>().ClearRuleState(ledgeGrabHit);
                return;
            }
            else if (this.rayCollider.IsCollidingWithWallClimbObject(
                -this.animatedPlayerPart.transform.forward,
                this.animatedPlayerPart.transform.up,
                -this.animatedPlayerPart.transform.right,
                out wallClimbHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                //this.helicopterSound.Pause();
                this.playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);
                return;
            }
            else if (this.rayCollider.IsHittingRoofHanging(out roofHangingHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                //this.helicopterSound.Pause();
                this.playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);
                return;
            }
            else if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }
    }
}
