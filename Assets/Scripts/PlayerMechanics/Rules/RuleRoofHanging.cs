﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleRoofHanging : PlayerMovementRule
    {
        private RaycastHit roofHangingHit;
        private float playerHangingMovingSpeed = 0.05f;

        private bool canControlPlayer = false;
        private bool isAlreadyMoving = false;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_ROOF_HANGING;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnRuleExit()
        {
            this.playerController.playerParams.soundParams
                .roofHangingSounds.roofLeavingFallingSound
                .PlayOnceWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .roofHangingAudioHandles.roofCatchingLeavingAudioSource);
            this.playerController.playerParams.soundParams
                .roofHangingSounds.underRoofMovementSound
                .StopPlayingWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .roofHangingAudioHandles.roofMovementAudioSource);
            this.isAlreadyMoving = false;
        }

        protected override void OnRuleEnter()
        {
            Vector3 roofHangingNormal = this.roofHangingHit.normal;
            this.transform.position = this.roofHangingHit.point + roofHangingNormal * 2.7f;

            this.animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.RoofHangingIdleStateName(),
                priority: 5);

            this.playerController.playerParams.soundParams
                .roofHangingSounds.roofCatchingSound
                .PlayOnceWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .roofHangingAudioHandles.roofCatchingLeavingAudioSource);

            EnablePlayerControlAfterMilliseconds(250);
        }

        protected override void OnRuleFixedUpdate()
        {
            if (this.canControlPlayer)
            {
                if (!this.playerMovementInput.GetJumpButton())
                {
                    this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                }

                Vector3 inputTranslation = this.playerMovementInput.GetTranslation();
                Vector3 translation = this.playerMovementInput.GetTranslation() * this.playerHangingMovingSpeed;

                if (this.playerMovementInput.GetJumpButton() && this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
                {
                    LegacyRuleRoofHangingToNewAirTransitions.
                        EnterRuleAirFallingOffFromRuleRoofHanging(
                            this.rayCollider,
                            this.animationController,
                            this.playerMovementStateInfo,
                            this.playerMovementMetrics,
                            this.playerController
                        );
                    return;
                }

                Vector3 translationInWorldSpace = this.transform.TransformDirection(translation);

                if (inputTranslation.magnitude > 0.1f && WillStillCollideWithRoofHangAndDoesNotCollideWithWall(
                    translationInWorldSpace))
                {
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingGoingForwardStateName(),
                        priority: 5);

                    this.animatedPlayerPart.transform.forward =
                        -Vector3.ProjectOnPlane(translationInWorldSpace, Vector3.up).normalized;
                    this.playerMovementMetrics.intentionalMovingDirection =
                        Vector3.ProjectOnPlane(translationInWorldSpace, Vector3.up).normalized;

                    this.transform.forward = this.playerMovementMetrics.forwardDirection;

                    this.transform.Translate(translation);

                    if (!this.isAlreadyMoving)
                    {
                        this.playerController.playerParams.soundParams
                            .roofHangingSounds.underRoofMovementSound
                            .PlayLoopedWithChangingModulationEachTimeWithAudioSource(
                                this.playerController,
                                this.playerController.controllerContext.playerAudioHandles
                                .roofHangingAudioHandles.roofMovementAudioSource);
                    }

                    this.isAlreadyMoving = true;
                }
                else
                {
                    this.playerController.playerParams.soundParams
                        .roofHangingSounds.underRoofMovementSound
                        .StopPlayingWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                            .roofHangingAudioHandles.roofMovementAudioSource);
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingIdleStateName(),
                        priority: 5);

                    this.isAlreadyMoving = false;
                }
            }
        }

        private bool WillStillCollideWithRoofHangAndDoesNotCollideWithWall(Vector3 translationInWorldSpace)
        {
            float wallCollisionCheckRayLength = 0.3f;

            bool willCollideWithWall =
                PhysicsRaycaster.Raycast(
                    this.animatedPlayerPart.transform.position + translationInWorldSpace,
                        -this.animatedPlayerPart.transform.forward, wallCollisionCheckRayLength,
                        Layers.generalEnvironmentLayersMask);

            float roofHangingCheckRayLength = 2f;
            float roofHangingCheckRayDepth = 3f;

            RaycastHit roofHangingCollideHit = new RaycastHit();

            bool willCollideWithRoofHanging = PhysicsRaycaster.Raycast(
                this.animatedPlayerPart.transform.position + translationInWorldSpace.normalized * roofHangingCheckRayLength,
                Vector3.up, out roofHangingCollideHit, roofHangingCheckRayDepth);

            willCollideWithRoofHanging = willCollideWithRoofHanging &&
                roofHangingCollideHit.collider.gameObject.tag.Equals(Tags.roofHangingTag);

            return willCollideWithRoofHanging && !willCollideWithWall;
        }

        private void EnablePlayerControlAfterMilliseconds(int milliseconds)
        {
            StartCoroutine(EnableCanControlPlayerFlagAfterTimePeriod(milliseconds));
        }

        private IEnumerator EnableCanControlPlayerFlagAfterTimePeriod(int milliseconds)
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            this.canControlPlayer = true;
        }

        public void ClearRuleState(RaycastHit roofHangingHit)
        {
            this.roofHangingHit = roofHangingHit;
            this.canControlPlayer = false;
            this.isAlreadyMoving = false;
        }
    }
}
