﻿using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules.Helper
{
    public static class SwingingDirectionInterpolationHelper
    {
        public static float GetAngleToRotateInFlatDirection(
            Vector3 swingingVelocityPlaneNormal,
            Vector3 playerInputTranslationWorldSpace,
            float swingingDirectionInterpolationStep)
        {
            Vector3 currentSwingingFlatDirection =
                Vector3.ProjectOnPlane(swingingVelocityPlaneNormal, Vector3.up).normalized;

            Vector3 newSwingingVelocityPlaneNormal =
                (swingingVelocityPlaneNormal +
                Vector3.ProjectOnPlane(playerInputTranslationWorldSpace, swingingVelocityPlaneNormal)).normalized;

            return Vector3.SignedAngle(swingingVelocityPlaneNormal, newSwingingVelocityPlaneNormal, Vector3.up) *
                swingingDirectionInterpolationStep;

            //float missingAngle = Vector3.SignedAngle(currentSwingingFlatDirection, playerDesiredFlatDirection, Vector3.up);
            //float angleToRotateBy = Mathf.Clamp(
            //    missingAngle * swingingDirectionInterpolationStep, 0f, maxRotationAngleStep);

            //return angleToRotateBy;
            //return Quaternion.AngleAxis(angleToRotateBy, Vector3.up) * swingingVelocityPlaneNormal;
        }
    }
}
