﻿using Assets.Scripts.Animations;
using Assets.Scripts.Collectibles;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.HUD.ScoreCounting;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class CurvedShootingDirectionHelper
    {
        public static Vector3 GetCurvedShootingDirection(
            Vector3 playerMovementDirection,
            Vector3 playerForward,
            Vector3 playerRight)
        {
            float angleRight = Vector3.Angle(playerMovementDirection.normalized, playerRight.normalized);
            float additionalAngle = 20f;

            if (angleRight < 80f)
            {
                // shooting right
                return Quaternion.AngleAxis(additionalAngle, Vector3.up) *
                    ((playerForward.normalized + playerRight.normalized) / 2.0f).normalized;
            }
            else
            {
                // shooting left
                return Quaternion.AngleAxis(-additionalAngle, Vector3.up) *
                    ((playerForward.normalized + (-playerRight.normalized)) / 2.0f).normalized;
            }
        }
    }

    public abstract class PlayerMovementRule : GameplayOnlyMonoBehaviour
    {
        protected PlayerMovementMetrics playerMovementMetrics;
        protected GameObject animatedPlayerPart;
        protected AnimationController animationController;
        protected RayCollider rayCollider;
        protected PlayerMovementStateInfo playerMovementStateInfo;

        protected TargettingAspect targettingAspect;

        protected ScoreAccumulatorDeccumulator scoreHunterHook;

        //protected AudioSource helicopterSound;
        //protected AudioSource jumpSound;

        protected PlayerShootingAspect playerShootingAspect;

        protected PlayerMovementInput playerMovementInput;

        protected PlayerController playerController;

        protected bool previousRuleEnabledState = false;

        protected virtual void OnRuleFixedUpdate() { }
        protected virtual void OnRuleUpdate() { }

        protected virtual void OnRuleEnter() { }
        protected virtual void OnRuleExit() { }
        protected abstract PlayerMovementRuleEnum GetPlayerMovementRuleEnum();

        public abstract void OnPortalTravel();

        protected void HandleGemsCollectiblesCollisions()
        {
            List<RaycastHit> gemsHits = this.rayCollider.GetAllCollectibleCrystalsThePlayerCollidesWith();
            foreach (RaycastHit gem in gemsHits)
            {
                gem.collider.gameObject.GetComponent<ICollectible>()
                    .InvokeCollectedBehaviour(this.animatedPlayerPart.transform);
                this.scoreHunterHook.ScorePointsInTime(10f, 1000);
            }
        }

        protected bool IsInStrafingKindRule()
        {
            return this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND);
        }

        protected void HandleShooting()
        {
            if (this.playerShootingAspect.HasHandsToShoot()
                && this.playerShootingAspect.CanTriggerShootingHand()
                && this.playerShootingAspect.IsPressingShootingButtonInstantShot())
            {
                if (!IsInStrafingKindRule())
                {
                    this.playerShootingAspect.ShootHand();
                }
                else
                {
                    if (!this.playerShootingAspect.IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                    this.transform.TransformDirection(
                                        this.playerMovementStateInfo.movementVelocity.normalized),
                                    Vector3.up),
                                -this.animatedPlayerPart.transform.forward.normalized,
                                -this.animatedPlayerPart.transform.right.normalized);
                        this.playerShootingAspect
                            .ShootHandFreelyInCurvedPath(
                                shootingDirection);
                    }
                    else
                    {
                        this.playerShootingAspect.ShootHand();
                    }
                }
                return;
            }
            else if (this.playerShootingAspect.HasHandsToShoot()
                && this.playerShootingAspect.CanTriggerShootingHand()
                && this.playerShootingAspect.IsPressingShootingButtonForCharging())
            {
                if (this.playerShootingAspect.HasJustStartedPressingShootingButtonForCharging())
                {
                    this.playerController.playerParams.soundParams
                        .shootingSounds.chargingFistSound.PlayLoopedWithoutChangingModulationWithAudioSource(
                            this.playerController,
                            this.playerController.controllerContext
                            .playerAudioHandles.shootingAudioHandles.chargingAudioSource);
                    this.playerShootingAspect.InitiateFistChargingCycleForAppropriateHand();
                    return;
                }

                this.playerController.playerParams.soundParams
                    .shootingSounds.chargingFistSound.AlterAudioSourcePitch(
                       this.playerController.controllerContext
                       .playerAudioHandles.shootingAudioHandles.chargingAudioSource,
                       1.5f - this.playerShootingAspect.GetCurrentChargingPowerNormalized()
                    );
                this.playerShootingAspect.KeepChargingCycle();
                return;
            }
            else if (this.playerShootingAspect.HasHandsToShoot()
                && this.playerShootingAspect.WantsToFinishChargingAndShoot())
            {
                if (!IsInStrafingKindRule())
                {
                    this.playerShootingAspect.FinishFistChargingCycleAndShootHand();
                }
                else
                {
                    if (!this.playerShootingAspect.IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                    this.transform.TransformDirection(
                                        this.playerMovementStateInfo.movementVelocity.normalized),
                                    Vector3.up),
                                -this.animatedPlayerPart.transform.forward.normalized,
                                -this.animatedPlayerPart.transform.right.normalized);
                        this.playerShootingAspect
                            .FinishFistChargingCycleAndShootHandFreelyInCurvedPath(
                                shootingDirection);
                    }
                    else
                    {
                        this.playerShootingAspect.FinishFistChargingCycleAndShootHand();
                    }
                }

                //playerShootingAspect.ShootHand();
                return;
            }
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            if (this.playerMovementStateInfo.currentRule.Equals(GetPlayerMovementRuleEnum()))
            {
                OnRuleUpdate();
            }
        }

        protected override void GameplayFixedUpdate()
        {
            bool currentRuleEnabledState = this.playerMovementStateInfo.currentRule.Equals(GetPlayerMovementRuleEnum());
            if (currentRuleEnabledState && !this.previousRuleEnabledState)
            {
                OnRuleEnter();
            }

            if (!currentRuleEnabledState && this.previousRuleEnabledState)
            {
                OnRuleExit();
            }

            HandleGemsCollectiblesCollisions();
            if (currentRuleEnabledState)
            {
                OnRuleFixedUpdate();
            }

            this.previousRuleEnabledState = currentRuleEnabledState;
        }

        protected void Awake()
        {
            this.playerMovementMetrics = FindObjectsOfType<PlayerMovementMetrics>()[0];
            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject;
            this.animationController = GetComponentInChildren<AnimationController>();
            this.rayCollider = GetComponent<RayCollider>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();

            //this.helicopterSound = GetComponents<AudioSource>()[0];
            //this.jumpSound = GetComponents<AudioSource>()[1];

            this.scoreHunterHook = FindObjectOfType<ScoreAccumulatorDeccumulator>();

            this.playerShootingAspect = GetComponent<PlayerShootingAspect>();
            this.targettingAspect = GetComponent<TargettingAspect>();

            this.playerMovementInput = FindObjectOfType<PlayerMovementInput>();

            this.playerController = FindObjectOfType<PlayerController>();
        }
    }
}
