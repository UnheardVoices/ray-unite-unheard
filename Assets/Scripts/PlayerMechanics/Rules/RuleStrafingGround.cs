﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleStrafingGround : GroundRulesBase, IStrafingRuleUtils
    {
        private float playerStrafingSpeed = 0.12f;

        public ShootSightDirection GetShootSightDirection()
        {
            Vector3 translation = this.playerMovementInput.GetTranslation();
            float angle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);
            if (translation.magnitude < 0.95f || (angle >= -10 && angle <= 10) || (angle >= 170 || angle <= -170))
            {
                // straight
                return ShootSightDirection.DOWN_ARROW_STRAIGHT;
            }
            else if (angle > 10)
            {
                // right
                return ShootSightDirection.DOWN_ARROW_RIGHT;
            }
            else
            {
                // left
                return ShootSightDirection.DOWN_ARROW_LEFT;
            }
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
        }

        protected override void OnRuleEnter()
        {
            this.animationController.ChangeAnimationState(
                RaymanAnimations.StrafingEnterFromIdleGroundStateName());
            this.playerMovementStateInfo.previousLocalGroundContactPointOffset = null;
        }

        protected override void OnRuleExit()
        {
            if (!this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                this.playerController.playerParams.soundParams
                    .strafingGroundSounds.leavingStrafingSound.PlayOnceWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.strafingGroundAudioHandles.enteringLeavingStrafingAudioSource
                    );
            }
        }

        protected override void OnRuleFixedUpdate()
        {
            #region Rule
            if (!this.playerMovementInput.GetStrafingButton() &&
                !this.playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                // exit strafing ground rule right away and go into regular ground rule
                GetComponent<RuleGround>().SetTransitionData(fromGroundStrafing: true);
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }

            HandleShooting();
            this.playerMovementStateInfo.previousLocalGroundContactPointOffset = 
                this.rayCollider.AlignOnTopOfTheGround(
                    this.playerMovementStateInfo.previousLocalGroundContactPointOffset,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity));

            Vector3 inputTranslation = this.playerMovementInput.GetTranslation();
            Vector3 translation = this.playerMovementInput.GetTranslation() * this.playerStrafingSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(this.playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            this.playerMovementStateInfo.movementVelocity.y = 0;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);

                this.playerMovementMetrics.intentionalMovingDirection =
                    Vector3.ProjectOnPlane(
                        this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity),
                            Vector3.up).normalized;
            }

            if (inputTranslation.magnitude > 0.1)
            {
                this.transform.forward = this.playerMovementMetrics.forwardDirection;

                if (this.playerMovementInput.GetGroundRollButton()
                && (!this.playerGroundRollingAspect.IsPerformingGroundRoll() && !this.playerGroundRollingAspect.HasEndedGroundRoll()))
                {
                    this.playerGroundRollingAspect.StartPerformingStrafingGroundRoll(
                        -this.animatedPlayerPart.transform.forward.normalized,
                        -this.animatedPlayerPart.transform.right.normalized,
                        translationDirectionInWorldSpace.normalized);
                    return;
                }
            }

            if (this.targettingAspect.currentTarget)
            {
                Vector3 targetLookingDirection =
                    this.targettingAspect.GetPlayerLookingDirectionTowardsTarget(
                        this.animatedPlayerPart.transform.position);
                this.animatedPlayerPart.transform.forward = -targetLookingDirection;
                //this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }
            else
            {
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }

            if (this.playerGroundRollingAspect.IsPerformingGroundForwardRoll())
            {
                this.playerMovementStateInfo.movementVelocity =
                    this.transform.InverseTransformDirection(
                        this.playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            }
            else if (this.playerGroundRollingAspect.IsPerformingGroundLeftOrRightRoll())
            {
                this.playerMovementStateInfo.movementVelocity =
                    this.transform.InverseTransformDirection(
                        this.playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentLeftRightRollCycle());
            }
            else if (this.playerGroundRollingAspect.IsPerformingGroundBackwardsRoll())
            {
                this.playerMovementStateInfo.movementVelocity =
                    this.transform.InverseTransformDirection(
                        this.playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentBackwardsRollCycle());
            }

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    this.gameObject,
                    this.rayCollider,
                    this.playerMovementMetrics.intentionalMovingDirection.normalized,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            this.transform.Translate(this.playerMovementStateInfo.movementVelocity);

            if (this.playerMovementInput.GetJumpButton()
                && this.rayCollider.IsHittingGround()
                && this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                this.playerMovementStateInfo.isJumping = true;
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                this.playerGroundRollingAspect.BreakTheRollCycle();
                //playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }

            if (!this.rayCollider.IsHittingGround())
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                //playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                this.playerGroundRollingAspect.BreakTheRollCycle();
                return;
            }

            if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation
            if (!this.playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                if (inputTranslation.magnitude > 0.1)
                {
                    float angleBetweenTranslationAndAnimatedPlayerPartForward =
                        Vector3.Angle(
                            -this.animatedPlayerPart.transform.forward.normalized, translationDirectionInWorldSpace);

                    float angleBetweenTranslationAndAnimatedPlayerPartRight =
                        Vector3.Angle(
                            -this.animatedPlayerPart.transform.right.normalized, translationDirectionInWorldSpace
                            );
                    if (angleBetweenTranslationAndAnimatedPlayerPartForward < 20f)
                    {
                        // going straight forward
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingForwardStateName());
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 80f)
                    {
                        // going forward left or forward right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going forward right
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardRightStateName());
                        }
                        else
                        {
                            // going forward left
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 90f)
                    {
                        // going left or right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going right
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingRightStateName());
                        }
                        else
                        {
                            // going left
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 160f)
                    {
                        // going backwards left or backwards right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going backwards right
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsRightStateName());
                        }
                        else
                        {
                            // going backwards left
                            this.animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsLeftStateName());
                        }
                    }
                    else
                    {
                        // going straight backwards
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingBackwardsStateName());
                    }

                    //animationController.ChangeAnimationState(
                    //    RaymanAnimations.StrafingIdleStateName());
                }
                else if (!this.animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingEnterFromIdleGroundStateName())
                    || this.animationController.HasAnimationEnded())
                {
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingIdleStateName());
                }
            }
            #endregion
        }

    }
}
