﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.PlayerMechanics.Collisions;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleWallClimbing : PlayerMovementRule
    {
        private RaycastHit wallClimbHit;
        private float playerClimbingSpeed = 0.05f;
        private bool isMovingAlready = false;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnRuleExit()
        {
            this.playerController.playerParams.soundParams
               .wallClimbingSounds.leavingWallJumpSound
               .PlayOnceWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .wallClimbingAudioHandles.wallGrabbingAudioSource);
            this.playerController.playerParams.soundParams
               .wallClimbingSounds.climbingUpSound
               .StopPlayingWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .wallClimbingAudioHandles.wallMovementAudioSource);
        }

        protected override void OnRuleEnter()
        {
            Vector3 wallClimbNormal = this.wallClimbHit.normal;

            this.transform.position = this.wallClimbHit.point - Vector3.up;
            //transform.forward = Vector3.up;
            this.transform.forward = -wallClimbNormal;

            this.animatedPlayerPart.transform.forward = wallClimbNormal;
            this.animatedPlayerPart.transform.position =
                this.animatedPlayerPart.transform.position + wallClimbNormal * 0.8f + Vector3.up * 0.5f;


            this.animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.WallClimbingIdleStateName(), priority: 5);

            this.playerController.playerParams.soundParams
               .wallClimbingSounds.grabbingWallSound
               .PlayOnceWithAudioSource(
                    this.playerController.controllerContext.playerAudioHandles
                    .wallClimbingAudioHandles.wallGrabbingAudioSource);
        }

        protected override void OnRuleFixedUpdate()
        {
            if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            Vector3 inputTranslation = this.playerMovementInput.GetNonInterpolatedTranslationUpRight();
            Vector3 translation = this.playerMovementInput.GetNonInterpolatedTranslationUpRight() * this.playerClimbingSpeed;

            Vector3 translationInWorldSpace = this.transform.TransformDirection(translation);

            //Debug.DrawRay(transform.position, transform.forward);

            if (this.playerMovementInput.GetJumpButton() &&
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                LegacyRuleWallClimbingToNewAirTransitions.
                    EnterRuleAirJumpingFromRuleWallClimbing(
                        this.playerMovementStateInfo,
                        this.transform,
                        this.animatedPlayerPart,
                        this.rayCollider,
                        this.animationController,
                        this.playerMovementMetrics,
                        this.playerController
                    );
                return;
            }

            if (inputTranslation.magnitude < 0.1f)
            {
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.WallClimbingIdleStateName(),
                    priority: 5);
                this.isMovingAlready = false;
                this.playerController.playerParams.soundParams
                    .wallClimbingSounds.climbingUpSound
                    .StopPlayingWithAudioSource(
                         this.playerController.controllerContext.playerAudioHandles
                            .wallClimbingAudioHandles.wallMovementAudioSource);
            }
            else
            {
                if (!this.isMovingAlready)
                {
                    this.playerController.playerParams.soundParams
                        .wallClimbingSounds.climbingUpSound
                        .PlayLoopedWithChangingModulationEachTimeWithAudioSource(
                            this.playerController,
                            this.playerController.controllerContext.playerAudioHandles
                                .wallClimbingAudioHandles.wallMovementAudioSource);
                }

                this.playerMovementMetrics.intentionalMovingDirection =
                    this.transform.TransformDirection(translation).normalized;

                if (Vector3.Angle(Vector3.up, translation) < 20f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going straight up
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingUpStateName(),
                        priority: 5);

                    this.transform.Translate(translation);
                    this.isMovingAlready = true;
                }
                else if (Vector3.Angle(Vector3.up, translation) < 80f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going diagonal up
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal up right
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpRightStateName(),
                            priority: 5);
                    }
                    else
                    {
                        // going diagonal up left
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpLeftStateName(),
                            priority: 5);
                    }
                    this.isMovingAlready = true;
                    this.transform.Translate(translation);
                }
                else if (Vector3.Angle(Vector3.up, translation) < 90f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going left or right
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going right
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingRightStateName(),
                            priority: 5);
                        this.isMovingAlready = true;
                    }
                    else
                    {
                        // going left
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingLeftStateName(),
                            priority: 5);
                        this.isMovingAlready = true;
                    }
                    this.transform.Translate(translation);
                }
                else if (Vector3.Angle(Vector3.up, translation) < 160f &&
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going diagonal down
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal down right
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownRightStateName(),
                            priority: 5);
                        this.isMovingAlready = true;
                    }
                    else
                    {
                        // going diagonal down left
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownLeftStateName(),
                            priority: 5);
                        this.isMovingAlready = true;
                    }

                    this.transform.Translate(translation);
                }
                else if (WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going straight down
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingDownStateName(),
                        priority: 5);
                    this.isMovingAlready = true;
                    this.transform.Translate(translation);
                }
                else
                {
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingIdleStateName(),
                        priority: 5);
                    this.playerController.playerParams.soundParams
                    .wallClimbingSounds.climbingUpSound
                    .StopPlayingWithAudioSource(
                         this.playerController.controllerContext.playerAudioHandles
                            .wallClimbingAudioHandles.wallMovementAudioSource);
                    this.isMovingAlready = false;
                }
            }
        }

        private bool WillStillCollideWithWallClimbAndDoesNotCollideWithWall(Vector3 translationInWorldSpace)
        {
            bool willCollideWithWall = false;

            Vector3 wallClimbNormal = this.wallClimbHit.normal;
            float wallClimbCheckRayDepth = 1.5f;

            RaycastHit wallClimbCollideHit = new RaycastHit();

            bool willCollideWithWallClimb =
                PlayerCollidingRules.IsCollidingWithWallClimbObject(
                    this.transform.position + translationInWorldSpace,
                    -this.animatedPlayerPart.transform.forward,
                    this.animatedPlayerPart.transform.up,
                    -this.animatedPlayerPart.transform.right,
                    out wallClimbCollideHit,
                    Vector3.up + this.animatedPlayerPart.transform.forward,
                    wallClimbCheckRayDepth,
                    1.7f);

            return willCollideWithWallClimb && !willCollideWithWall;
        }

        public void ClearRuleState(RaycastHit wallClimbHit)
        {
            this.wallClimbHit = wallClimbHit;
            this.isMovingAlready = false;
        }
    }
}
