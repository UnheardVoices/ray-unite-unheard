﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.PlayerMechanics.Aspects;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class RaymanAnimationsGroundHelper
    {
        public static bool IsUprisingWalkingRunningAnimation(string animationName)
        {
            return animationName.Equals(
               RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName())
               ||
               animationName.Equals(
                   RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
        }

        public static bool IsExitingWalkingRunningAnimation(string animationName)
        {
            return animationName.Equals(
                RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName())
                ||
                animationName.Equals(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
        }

        public static bool IsLandingAnimation(string animationName)
        {
            return
                animationName.Equals(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromRunningJumpAfterAirFlipToWalkingCycle2StateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromVerticalJumpToGroundIdleStateName())
                ||
                animationName.Equals(RaymanAnimations.LandingFromVerticalJumpToWalkingCycle2StateName())
                ;
        }
    }

    public abstract class GroundRulesBase : PlayerMovementRule
    {
        protected PlayerGroundRollingAspect playerGroundRollingAspect;

        protected bool landingWithHelicopter = false;
        protected bool landing = false;
        protected bool enteringFromGroundStrafing = false;

        public void SetTransitionData(
            bool landing = false, bool withHelicopterFlag = false, bool fromGroundStrafing = false)
        {
            this.landingWithHelicopter = withHelicopterFlag;
            this.landing = landing;
            this.enteringFromGroundStrafing = fromGroundStrafing;
        }

        protected void Awake()
        {
            base.Awake();
            this.playerGroundRollingAspect = GetComponent<PlayerGroundRollingAspect>();
            this.landingWithHelicopter = false;
        }
    }
}
