﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.Engine.Input;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules.Common
{
    public static class PlayerMovementVectorRotationWithLengthInterpolationHelper
    {
        public static Vector3 InterpolationRotationWithLength(
            Vector3 forward, Vector3 start, Vector3 end, float t)
        {
            float interpolatedLength = Mathf.Lerp(start.magnitude, end.magnitude, t);

            float angle = Vector3.SignedAngle(forward.normalized, end.normalized, Vector3.up);

            float angleFromForwardToStart = Vector3.SignedAngle(forward, start, Vector3.up);
            float angleBetweenStartAndEnd = Vector3.SignedAngle(start, end, Vector3.up);

            return Quaternion.AngleAxis(angleFromForwardToStart +
                (angleBetweenStartAndEnd * t), Vector3.up) * Vector3.forward * interpolatedLength;
        }
    }

    public class PlayerMovementInput : GameplayOnlyMonoBehaviour
    {
        public Vector3 translationFromUpdate;
        public float axesMagnitudeFromUpdate;
        public float horizontalAxisRawFromUpdate;

        public Vector3 currentTranslationFromUpdate;
        public Vector3 currentTranslationFromUpdateWithoutRotationInterpolation;

        private bool isActuallyControlledByPlayerInput = true;

        protected PlayerInputHub playerInputHub;

        //public void OrientatePlayerAccordingToPortals(Transform fromPortal, Transform toPortal)
        //{

        //}

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            this.playerInputHub = FindObjectOfType<PlayerInputHub>();
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            if (this.isActuallyControlledByPlayerInput)
            {
                this.axesMagnitudeFromUpdate =
                    Mathf.Clamp01(new Vector2(
                        this.playerInputHub.moveInputRaw.x, this.playerInputHub.moveInputRaw.y).magnitude);

                this.horizontalAxisRawFromUpdate = this.playerInputHub.moveInputRaw.x;

                this.translationFromUpdate = ((this.playerInputHub.moveInputRaw.y * Vector3.forward) +
                       (this.playerInputHub.moveInputRaw.x * Vector3.right)).normalized
                       * this.axesMagnitudeFromUpdate;

                if (this.playerInputHub.walkingButton)
                {
                    this.translationFromUpdate = this.translationFromUpdate / 2f;
                }
            }

            this.currentTranslationFromUpdate =
                PlayerMovementVectorRotationWithLengthInterpolationHelper.InterpolationRotationWithLength(
                    Vector3.forward, this.currentTranslationFromUpdate, this.translationFromUpdate,
                    //0.25f);
                    (0.25f * deltaTime) / (Time.fixedDeltaTime));

            this.currentTranslationFromUpdateWithoutRotationInterpolation =
                Vector3.Lerp(this.currentTranslationFromUpdateWithoutRotationInterpolation, this.translationFromUpdate,
                (0.25f * deltaTime) / (Time.fixedDeltaTime));
        }

        public bool GetGroundRollButton()
        {
            if (this.isActuallyControlledByPlayerInput)
            {
                return this.playerInputHub.groundRollButton;
            }
            else
            {
                return false;
            }
        }

        public bool GetStrafingButton()
        {
            if (this.isActuallyControlledByPlayerInput)
            {
                return this.playerInputHub.strafingButton;
            }
            else
            {
                return false;
            }
        }

        public bool GetFireButton()
        {
            if (this.isActuallyControlledByPlayerInput)
            {
                return this.playerInputHub.fireButton;
            }
            else
            {
                return false;
            }
        }

        public bool GetJumpButton()
        {
            if (this.isActuallyControlledByPlayerInput)
            {
                return this.playerInputHub.jumpButton;
            }
            else
            {
                return false;
            }
        }

        public Vector3 GetTranslationUpRightWithoutRotationInterpolation()
        {
            return Quaternion.AngleAxis(-90f, Vector3.right) *
                this.currentTranslationFromUpdateWithoutRotationInterpolation;
        }

        public Vector3 GetTranslationUpRight()
        {
            return Quaternion.AngleAxis(-90f, Vector3.right) * this.currentTranslationFromUpdate;
        }

        public float GetHorizontalAxisRaw()
        {
            return this.horizontalAxisRawFromUpdate;
        }

        public Vector3 GetNonInterpolatedTranslation()
        {
            return this.translationFromUpdate;
        }

        public Vector3 GetNonInterpolatedTranslationUpRight()
        {
            return Quaternion.AngleAxis(-90f, Vector3.right) * this.translationFromUpdate;
        }

        public Vector3 GetTranslation()
        {
            return this.currentTranslationFromUpdate;
        }

        public void DisablePlayerControl()
        {
            this.isActuallyControlledByPlayerInput = false;
        }

        public void EnablePlayerControl()
        {
            this.isActuallyControlledByPlayerInput = true;
        }
        public void SetMovementInput(
            Vector3 inputMovementTranslation)
        {
            this.axesMagnitudeFromUpdate = inputMovementTranslation.magnitude;

            //horizontalAxisRawFromUpdate = 

            this.translationFromUpdate = inputMovementTranslation.normalized * this.axesMagnitudeFromUpdate;



            //axesMagnitudeFromUpdate =
            //        Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            //horizontalAxisRawFromUpdate = Input.GetAxisRaw("Horizontal");

            //translationFromUpdate = ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
            //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
            //       * axesMagnitudeFromUpdate;

            //if (Input.GetKey(KeyControls.walkingButtonKeyboard))
            //{
            //    translationFromUpdate = translationFromUpdate / 2f;
            //}
        }
    }
}
