﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.GameMechanics.Entities.Player.Rules.Transitions.Legacy;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleStrafingAir : PlayerMovementRule, IStrafingRuleUtils
    {
        private float playerInAirStrafingSpeed = 0.16f;
        private float playerJumpSpeed = 0.28f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_STRAFING_AIR;
        }

        protected override void OnRuleEnter()
        {
            this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
        }

        public override void OnPortalTravel()
        {
            //throw new System.NotImplementedException();
        }

        protected override void OnRuleExit()
        {
            if (!this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_AIR))
            {
                this.playerMovementStateInfo.isJumping = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //this.helicopterSound.Pause();

                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
            }
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
            Vector3 translationInWorldSpace)
        {
            return Vector3.Angle(translationInWorldSpace.normalized, -this.animatedPlayerPart.transform.forward.normalized);
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
            Vector3 translationInWorldSpace)
        {
            return Vector3.Angle(translationInWorldSpace.normalized, -this.animatedPlayerPart.transform.right.normalized);
        }

        protected override void OnRuleFixedUpdate()
        {
            if (!this.playerMovementInput.GetStrafingButton())
            {
                // exit strafing air rule right away and go into regular air rule
                LegacyRuleStrafingAirToNewAirTransitions.
                    EnterRuleAirFromRuleStrafingAir(
                        this.playerMovementStateInfo,
                        this.playerMovementMetrics,
                        this.playerController
                    );
                //playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                //playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }

            HandleShooting();
            Vector3 inputTranslation = this.playerMovementInput.GetTranslation();
            Vector3 translation = this.playerMovementInput.GetTranslation() * this.playerInAirStrafingSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(this.playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                this.playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                this.playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(this.playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
                this.playerMovementMetrics.intentionalMovingDirection =
                    Vector3.ProjectOnPlane(
                        this.transform.TransformDirection(
                            this.playerMovementStateInfo.movementVelocity), Vector3.up).normalized;
            }

            if (inputTranslation.magnitude > 0.1)
            {
                this.transform.forward = this.playerMovementMetrics.forwardDirection;
                //animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
            }

            if (this.targettingAspect.currentTarget)
            {
                Vector3 targetLookingDirection =
                    this.targettingAspect.GetPlayerLookingDirectionTowardsTarget(
                        this.animatedPlayerPart.transform.position);
                //transform.forward = targetLookingDirection;
                this.animatedPlayerPart.transform.forward = -targetLookingDirection;
                //this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }
            else
            {
                this.playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection
                (PlayerWallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    this.gameObject,
                    this.rayCollider,
                    this.playerMovementMetrics.intentionalMovingDirection.normalized,
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            this.playerMovementStateInfo.movementVelocity = this.transform.InverseTransformDirection(
                this.rayCollider.AdjustVelocityIfIsHittingTheCeiling(
                    this.transform.TransformDirection(this.playerMovementStateInfo.movementVelocity)));

            if (this.playerMovementStateInfo.isJumping)
            {
                this.playerMovementStateInfo.isJumping = false;
                //this.jumpSound.Play();
                this.playerMovementStateInfo.movementVelocity.y = this.playerJumpSpeed;

                Vector3 flatVelocity = Vector3.ProjectOnPlane(this.playerMovementStateInfo.movementVelocity, Vector3.up);

                if (inputTranslation.magnitude > 0.1f)
                {
                    float angleBetweenPlayerForwardAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
                            translationDirectionInWorldSpace);

                    float angleBetweenPlayerRightAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
                            translationDirectionInWorldSpace
                            );

                    if (angleBetweenPlayerForwardAndTranslation < 45f)
                    {
                        // jumping forward
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName(),
                            priority: 3);
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.jumpAudioSource);
                    }
                    else if (angleBetweenPlayerForwardAndTranslation < 120f)
                    {
                        // jumping left or right
                        if (angleBetweenPlayerRightAndTranslation < 90f)
                        {
                            // jumping right
                            this.animationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName(),
                                priority: 3);
                            this.playerController.playerParams.soundParams
                                .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                                .PlayOnceWithAudioSource(
                                    this.playerController.controllerContext.playerAudioHandles
                                    .strafingAirAudioHandles.jumpAudioSource);
                        }
                        else
                        {
                            // jumping left
                            this.animationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName(),
                                priority: 3);
                            this.playerController.playerParams.soundParams
                                .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                                .PlayOnceWithAudioSource(
                                    this.playerController.controllerContext.playerAudioHandles
                                    .strafingAirAudioHandles.jumpAudioSource);
                        }
                    }
                    else
                    {
                        // jumping backwards
                        this.animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName(),
                            priority: 3);
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.jumpAudioSource);
                    }
                }
                else
                {
                    // jumping straight up
                    this.animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                        priority: 3);
                    this.playerController.playerParams.soundParams
                        .strafingAirSounds.jumpingSounds.jumpOffTheGroundSound
                        .PlayOnceWithAudioSource(
                            this.playerController.controllerContext.playerAudioHandles
                            .strafingAirAudioHandles.jumpAudioSource);
                }
            }

            if (this.playerMovementStateInfo.movementVelocity.y > -PlayerMovementMetrics.limitFallSpeed)
            {
                if (!this.playerMovementStateInfo.isHelicopter)
                {
                    this.playerMovementStateInfo.movementVelocity.y -= PlayerMovementMetrics.gravityAcceleration;
                }
            }

            if (this.playerMovementStateInfo.isHelicopter)
            {
                this.playerMovementStateInfo.movementVelocity.y = -PlayerMovementMetrics.limitFallSpeedHelicopter;
            }

            this.transform.Translate(this.playerMovementStateInfo.movementVelocity);

            if (
                (
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName())
                    ||
                    this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName())

                ) &&
                  this.animationController.HasAnimationEnded())
            {
                if (this.playerMovementStateInfo.movementVelocity.y < 0.1f)
                {
                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateSpeed(), 2);


                    // change into respective roll/extremum of the animation sequence in the air

                    if (this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName()))
                    {
                        // extremum when strafing forward
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName());
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airExtremumForwardStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                    }
                    else if (this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName()))
                    {
                        // backflip when strafing backwards
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName());
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipBackwardStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                    }
                    else if (this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName()))
                    {
                        // extremum (air flip?) when strafing left
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName());
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipLeftStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                    }
                    else
                    {
                        // extremum (air flip?) when strafing right
                        this.animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName());
                        this.playerController.playerParams.soundParams
                            .strafingAirSounds.jumpingSounds.airFlipRightStrafingJumpSound
                            .PlayOnceWithAudioSource(
                                this.playerController.controllerContext.playerAudioHandles
                                .strafingAirAudioHandles.airFlipAudioSource);
                    }
                }
            }
            else if (
                (
                    (
                        this.animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())

                       ||

                       this.animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                       ||

                       this.animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                       ||

                       this.animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName())
                    )
                    && this.animationController.HasAnimationEnded())
               )
            {

                // change into finishing falling animation appropriate for given strafing air jumping sequence

                if (this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName()))
                {
                    // falling from extremum forward
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName());
                }
                else if (this.animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName()))
                {
                    // falling from extremum backwards
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName());
                }
                else if (this.animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName()))
                {
                    // falling from extremum left
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName());
                }
                else
                {
                    // falling from extremum right
                    this.animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName());
                }
            }

            else
                if ((this.playerMovementStateInfo.movementVelocity.y < 0.1f &&
                    this.animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!this.playerMovementStateInfo.isHelicopter &&
                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&


                        (!this.playerMovementStateInfo.isHelicopter &&
                            !this.animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName()))

                            &&

                            (!this.playerMovementStateInfo.isHelicopter &&
                                !this.animationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName()))

                            &&

                            (!this.playerMovementStateInfo.isHelicopter &&
                                !this.animationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName()))


                                &&


                            (!this.playerMovementStateInfo.isHelicopter &&
                               !this.animationController.GetCurrentAnimationState().Equals(
                               RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName()))



                      &&


                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())

                    &&

                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                    &&

                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                    &&
                    !this.animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName()))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if (this.playerMovementInput.GetJumpButton() &&
                    !this.playerMovementStateInfo.isHelicopter
                    && this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                this.playerMovementStateInfo.isHelicopter = true;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.PlayLoopedWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );

                //this.helicopterSound.Play();
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.animationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), priority: 4);
            }

            if (this.playerMovementStateInfo.isHelicopter
                    && !this.playerMovementInput.GetJumpButton() &&
                    this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //this.helicopterSound.Pause();
                this.animationController.ChangeAnimationState(
                   RaymanAnimations.FallingAnimationStateName());
            }

            if (this.animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  this.animationController.HasAnimationEnded())
            {
                this.animationController.ChangeAnimationState(
                   RaymanAnimations.HelicopterStateName());
            }

            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (this.rayCollider.IsHittingGround() && this.playerMovementStateInfo.movementVelocity.y <= 0)
            {
                // allow falling animation to be interrupted
                this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                this.animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //this.helicopterSound.Pause();
                return;
            }
            else if (this.rayCollider.IsCollidingWithLedgeGrabCollider(out ledgeGrabHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                //this.helicopterSound.Pause();
                GetComponent<RuleLedgeGrabbing>().ClearRuleState(ledgeGrabHit);
                return;
            }
            else if (this.rayCollider.IsCollidingWithWallClimbObject(
                -this.animatedPlayerPart.transform.forward,
                this.animatedPlayerPart.transform.up,
                -this.animatedPlayerPart.transform.right,
                out wallClimbHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                //this.helicopterSound.Pause();
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);
                return;
            }
            else if (this.rayCollider.IsHittingRoofHanging(out roofHangingHit))
            {
                this.playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                //this.helicopterSound.Pause();
                this.playerMovementStateInfo.isHelicopter = false;
                this.playerController.playerParams.soundParams
                    .helicopterSounds.helicopterSound.StopPlayingWithAudioSource(
                        this.playerController.controllerContext
                            .playerAudioHandles.helicopterAudioHandles.helicopterAudioSource
                    );
                GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);
                return;
            }
            else if (!this.playerMovementInput.GetJumpButton())
            {
                this.playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }

        public ShootSightDirection GetShootSightDirection()
        {
            Vector3 translation = this.playerMovementInput.GetTranslation();
            float angle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);
            if (translation.magnitude < 0.95f || (angle >= -10 && angle <= 10) || (angle >= 170 || angle <= -170))
            {
                // straight
                return ShootSightDirection.DOWN_ARROW_STRAIGHT;
            }
            else if (angle > 10)
            {
                // right
                return ShootSightDirection.DOWN_ARROW_RIGHT;
            }
            else
            {
                // left
                return ShootSightDirection.DOWN_ARROW_LEFT;
            }
        }
    }
}
