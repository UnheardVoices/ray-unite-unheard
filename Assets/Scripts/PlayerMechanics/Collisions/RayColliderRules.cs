﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class RayColliderRules
    {
        public static bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) &&
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        public static bool IsStandingOnMovingPlatform(
            Transform transform, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundRaycastDistance = 1.2f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, Vector3.down).normalized;
            }
            else
            {
                intentTranslation = Vector3.down;
            }

            return PhysicsRaycaster.Raycast(transform.position + Vector3.up,
                (Vector3)intentTranslation, out hit, maxGroundRaycastDistance,
                Layers.movableEnvironmentLayerMask, debug: debug) &&
                IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingFlatGround(
            Transform transform, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundRaycastDistance = 1.2f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, Vector3.down).normalized;
            }
            else
            {
                intentTranslation = Vector3.down;
            }

            return PhysicsRaycaster.Raycast(transform.position + Vector3.up,
                (Vector3)intentTranslation, out hit, maxGroundRaycastDistance,
                Layers.generalEnvironmentLayersMask, debug: debug) &&
                IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingSlope(
            Transform transform, out RaycastHit hit,
            Vector3? intentTranslation = null,
            float maxGroundSlopeRaycastDistance = 1.5f, bool debug = false)
        {
            if (intentTranslation != null)
            {
                intentTranslation = Vector3.Project(
                    (Vector3)intentTranslation, Vector3.down).normalized;
            }
            else
            {
                intentTranslation = Vector3.down;
            }

            float angleThresholdDegrees = 1;
            if (PhysicsRaycaster.Raycast(
                transform.position + Vector3.up,
                (Vector3)intentTranslation, out hit,
                maxGroundSlopeRaycastDistance,
                Layers.generalEnvironmentLayersMask, debug: debug))
            {
                return (Vector3.Angle(Vector3.up, hit.normal) > angleThresholdDegrees) &&
                    IsLegitSolidGroundOrWallForCollision(hit);
            }
            return false;
        }

        public static bool IsHittingTheWall(
            Transform transform, out RaycastHit hit,
            float circleWallCollisionCheckingRadius = 1.1f,
            bool debug = false,
            Color? debugColor = null)
        {
            int layers = 10;
            int raysInLayer = 30;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                circleWallCollisionCheckingRadius,
                raysInLayer,
                out hit,
                Layers.generalEnvironmentLayersMask,
                debug,
                debugColor))
                {
                    if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static bool HitsTheWallCheckedInStackedRays(
            Vector3 originPosition, Vector3 raysDirection,
            out RaycastHit hit,
            float wallCollisionCheckingRadius = 0.8f,
            bool debug = false,
            Color? debugColor = null)
        {
            int layers = 10;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsRaycaster.Raycast(
                originPosition + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                raysDirection,
                out hit,
                wallCollisionCheckingRadius,
                Layers.generalEnvironmentLayersMask,
                debug: debug,
                debugColor: debugColor
                ))
                {
                    if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static bool HasHitTheSolidObstacle(
            Transform transform, out RaycastHit hit,
            float wallDistanceCheck = 1.1f,
            float slopeDistanceCheck = 1.5f,
            float flatGroundDistanceCheck = 1.2f)
        {
            return IsHittingTheWall(transform, out hit, wallDistanceCheck)
                || IsHittingSlope(transform, out hit, maxGroundSlopeRaycastDistance: slopeDistanceCheck)
                || IsHittingFlatGround(transform, out hit, maxGroundRaycastDistance: flatGroundDistanceCheck);
        }

        public static bool HasHitTheSolidObstacle(Transform transform)
        {
            RaycastHit hit = new RaycastHit();
            return HasHitTheSolidObstacle(transform, out hit);
        }
    }
}
