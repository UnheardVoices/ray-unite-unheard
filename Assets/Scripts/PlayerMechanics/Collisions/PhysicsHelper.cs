﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class PhysicsHelper
    {
        private static bool debug = false;

        public static bool CastRaysInCircleVertical(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out RaycastHit hit,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null
            )
        {
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask,
                    debug: debug,
                    debugColor: debugColor))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysCollidersInCircleVerticalGetUniqueColliders(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            List<RaycastHit> result = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                RaycastHit hit = new RaycastHit();
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask,
                    debug: debug) && endingColliderFilterCriteria(hit))
                {
                    result.Add(hit);
                }
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }

        public static bool CastRaysInCircleVerticalListPoints(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out List<RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask,
            bool debug = false,
            Color? debugColor = null
            )
        {
            hits = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                RaycastHit hitAtGivenMoment = new RaycastHit();
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                if (PhysicsRaycaster.Raycast(
                    origin,
                    rayVector,
                    out hitAtGivenMoment,
                    radius,
                    layerMask,
                    debug: debug,
                    debugColor: debugColor))
                {
                    hits.Add(hitAtGivenMoment);
                }
            }
            return hits.Count > 0;
        }

        public static bool CastRaysInCyllinderVertical(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            out RaycastHit hit,
            float layersDistanceInterval,
            int layers,
            int layerMask = Layers.defaultLayerMask
            )
        {
            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                    origin + Vector3.up * layersDistanceInterval * i,
                    Vector3.forward,
                    radius,
                    raysCountInLayer,
                    out hit,
                    layerMask))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysInCyllinderVerticalGetUniqueColliders(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            float layersDistanceInterval,
            int layers,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            List<RaycastHit> result = new List<RaycastHit>();
            for (int i = 0; i < layers; i++)
            {
                result.AddRange(
                    PhysicsHelper.CastRaysCollidersInCircleVerticalGetUniqueColliders(
                        origin + Vector3.up * layersDistanceInterval * i,
                        Vector3.forward,
                        radius,
                        raysCountInLayer,
                        endingColliderFilterCriteria,
                        layerMask
                    ));
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }

        //public static Vector3 GetAveragedCollisionNormalFromSphereCollision(
        //    Vector3 originPosition,
        //    int layersCount,
        //    float checkRadius,
        //    int raysPerLayer,
        //    Func<RaycastHit, bool> collisionValidityCheck
        //    )
        //{
        //    var hits = new List<RaycastHit>(); 
        //    if (layersCount < 2)
        //    {
        //        throw new ArgumentException(
        //            "Layers count for this sphere environmental collision cannot be less than 2!");
        //    }
        //    float angleLayerStep = 180f / layersCount;
        //    float layerAngleRotationStep = 360f / raysPerLayer;

        //    Vector3 collisionsNormalsSum = Vector3.zero;
        //    int collisionsNormalsTotal = 0;

        //    var collisionsNormalsSumInfo = HitsCollectingHelper.GatherHitsNormalsOnlySignificantlyDifferent(
        //       collisionsNormalsSum, originPosition, Vector3.up, checkRadius, collisionValidityCheck);
        //    if (collisionsNormalsSumInfo.Item1)
        //    {
        //        collisionsNormalsTotal++;
        //        collisionsNormalsSum = collisionsNormalsSumInfo.Item2;
        //    }

        //    for (int i = 0; i < layersCount - 2; i++)
        //    {
        //        float baseAngle = angleLayerStep * (i + 1);
        //        Vector3 baseVectorAtLayer = 
        //            Quaternion.AngleAxis(baseAngle, Vector3.forward) * Vector3.up;

        //        for (int j = 0; j < raysPerLayer; j++)
        //        {
        //            Vector3 rayVector = Quaternion.AngleAxis(layerAngleRotationStep * j, Vector3.up)
        //                * baseVectorAtLayer;

        //            //Debug.DrawRay(originPosition, rayVector * checkRadius);
        //            collisionsNormalsSumInfo = HitsCollectingHelper.GatherHitsNormalsOnlySignificantlyDifferent(
        //                collisionsNormalsSum, originPosition, rayVector, checkRadius, collisionValidityCheck);
        //            if (collisionsNormalsSumInfo.Item1)
        //            {
        //                collisionsNormalsTotal++;
        //                collisionsNormalsSum = collisionsNormalsSumInfo.Item2;
        //            }
        //        }
        //    }
        //    collisionsNormalsSumInfo = HitsCollectingHelper.GatherHitsNormalsOnlySignificantlyDifferent(
        //       collisionsNormalsSum, originPosition, Vector3.down, checkRadius, collisionValidityCheck);
        //    if (collisionsNormalsSumInfo.Item1)
        //    {
        //        collisionsNormalsTotal++;
        //        collisionsNormalsSum = collisionsNormalsSumInfo.Item2;
        //    }
        //    if (collisionsNormalsTotal > 0)
        //    {
        //        return (collisionsNormalsSum / collisionsNormalsTotal).normalized;
        //    } else
        //    {
        //        return Vector3.zero;
        //    }
        //    //return hits.Select(x => x.normal).Aggregate(
        //    //    Vector3.zero, (acc, v) => acc + v) / hits.Count;
        //}

        public static bool CastRaysInCyllinderVerticalListPoints(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            out List<RaycastHit> hits,
            float layersDistanceInterval,
            int layers,
            int layerMask = Layers.defaultLayerMask
            )
        {
            hits = new List<RaycastHit>();
            for (int i = 0; i < layers; i++)
            {
                List<RaycastHit> hitsAtGivenMoment = new List<RaycastHit>();
                if (PhysicsHelper.CastRaysInCircleVerticalListPoints(
                    origin + Vector3.up * layersDistanceInterval * i,
                    Vector3.forward,
                    radius,
                    raysCountInLayer,
                    out hitsAtGivenMoment,
                    layerMask))
                {
                    hits.AddRange(hitsAtGivenMoment);
                }
            }
            return hits.Count > 0;
        }
    }
}
