﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class HitsCollectingHelper
    {
        public static Tuple<bool, Vector3> GatherHitsNormalsOnlySignificantlyDifferent(
            Vector3 currentCollisionsNormalsSum, Vector3 originPosition,
            Vector3 raycastDirection, float raycastLength,
            Func<RaycastHit, bool> collisionValidityCheck)
        {
            RaycastHit testingRaycastHit = new RaycastHit();
            bool wasHit = false;
            if (PhysicsRaycaster.Raycast(originPosition,
                raycastDirection, out testingRaycastHit,
                raycastLength))
            {
                Vector3 collisionNormal = testingRaycastHit.normal;
                if (currentCollisionsNormalsSum.magnitude < 0.01f ||
                    Vector3.Angle(collisionNormal, currentCollisionsNormalsSum) > 1.5f)
                {
                    if (collisionValidityCheck(testingRaycastHit))
                    {
                        wasHit = true;
                        currentCollisionsNormalsSum += collisionNormal;
                    }
                }
            }
            return Tuple.Create(wasHit, currentCollisionsNormalsSum);
        }
    }
}
