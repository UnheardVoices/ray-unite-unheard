﻿using Assets.Scripts.Common;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class PlayerCollidingRules
    {
        private static bool IsWallClimbHit(RaycastHit hit)
        {
            return hit.collider.gameObject.tag.Equals(Tags.wallClimbTag);
        }

        public static bool IsCollidingWithWallClimbObject(
            Vector3 playerPosition,
            Vector3 playerForwardDirection,
            Vector3 playerUpDirection,
            Vector3 playerRightDirection,
            out RaycastHit wallClimbHit,
            Vector3 originPositionOffset = new Vector3(),
            float checkDepth = 1.0f,
            float raysDistributionDistance = 1.7f)
        {
            //Vector3 originPosition = playerPosition + Vector3.up;
            Vector3 originPosition = playerPosition + originPositionOffset;
            RaycastHit upperRightHit = new RaycastHit();
            RaycastHit upperLeftHit = new RaycastHit();
            RaycastHit bottomRightHit = new RaycastHit();
            RaycastHit bottomLeftHit = new RaycastHit();

            bool upperRight = PhysicsRaycaster.Raycast(originPosition +
                (playerRightDirection + playerUpDirection).normalized * raysDistributionDistance,
                playerForwardDirection, out upperRightHit, checkDepth, Layers.wallClimbablesLayerMask);

            bool upperLeft = PhysicsRaycaster.Raycast(originPosition -
                (playerRightDirection + playerUpDirection).normalized * raysDistributionDistance,
                playerForwardDirection, out upperLeftHit, checkDepth, Layers.wallClimbablesLayerMask);

            bool bottomLeft = PhysicsRaycaster.Raycast(originPosition -
                (playerRightDirection - playerUpDirection).normalized * raysDistributionDistance,
                playerForwardDirection, out bottomLeftHit, checkDepth, Layers.wallClimbablesLayerMask);

            bool bottomRight = PhysicsRaycaster.Raycast(originPosition +
                (playerRightDirection - playerUpDirection).normalized * raysDistributionDistance,
                playerForwardDirection, out bottomRightHit, checkDepth, Layers.wallClimbablesLayerMask);

            if (upperRight && upperLeft && bottomLeft && bottomRight &&
                IsWallClimbHit(upperRightHit) && IsWallClimbHit(upperLeftHit)
                && IsWallClimbHit(bottomLeftHit) && IsWallClimbHit(bottomRightHit))
            {
                PhysicsRaycaster.Raycast(originPosition, playerForwardDirection,
                    out wallClimbHit, checkDepth, Layers.wallClimbablesLayerMask);
                return true;
            }
            else
            {
                wallClimbHit = new RaycastHit();
                return false;
            }
        }
    }
}
