﻿using Assets.Scripts.Collectibles;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public class RayCollider : MonoBehaviour
    {
        private static float maxGroundRaycastDistance = 1.2f;
        private static float maxGroundSlopeRaycastDistance = 1.5f;

        private float circleWallCollisionCheckingRadius = 0.8f;

        private RaycastHit emptyRaycastHit = new RaycastHit();

        private bool debug = true;
        private bool isLedgeGrabColliderCollisionDetectionEnabled = true;
        private bool isWallClimbingCollisionDetectionEnabled = true;
        private bool isRoofHangingCollisionDetectionEnabled = true;

        private void Awake()
        {

        }

        public Vector3? AlignOnTopOfTheGround(
            Vector3? previousLocalGroundContactPointOffset, Vector3 velocity)
        {
            if (IsHittingGround(out RaycastHit hit, checkForMovingPlatforms: true))
            {
                if (hit.collider.gameObject.layer == Layers.movableEnvironmentLayerIndex)
                {
                    var collidedObject = hit.collider.gameObject;

                    Vector3 positionToKeep = 
                        (previousLocalGroundContactPointOffset != null ? 
                            (collidedObject.transform.position + (Vector3)previousLocalGroundContactPointOffset)
                            : hit.point) + velocity;

                    Vector3 alignedPosition =
                        new Vector3(
                            positionToKeep.x,
                            hit.point.y,
                            positionToKeep.z);

                    this.transform.position = alignedPosition;
                    return alignedPosition - collidedObject.transform.position;
                } else
                {
                    this.transform.position = new Vector3(this.transform.position.x, hit.point.y, this.transform.position.z);
                    return null;
                }                
            }
            return null;
        }

        public bool IsHittingGround()
        {
            return IsHittingGround(out this.emptyRaycastHit);
        }

        public bool HasHitTheSolidObstacle()
        {
            return RayColliderRules.HasHitTheSolidObstacle(this.transform);
        }

        private bool IsHittingGround(out RaycastHit hit, bool checkForMovingPlatforms = false)
        {
            return IsHittingFlatGround(out hit) || IsHittingSlope(out hit) || (checkForMovingPlatforms && IsStandingOnMovingPlatform(out hit));
        }

        public bool IsHittingFlatGround()
        {
            return IsHittingFlatGround(out this.emptyRaycastHit);
        }

        public bool IsStandingOnMovingPlatform(out RaycastHit hit)
        {
            return RayColliderRules.IsStandingOnMovingPlatform(this.transform, out hit, null, maxGroundRaycastDistance * 2f);
        }

        public bool IsHittingFlatGround(out RaycastHit hit)
        {
            return RayColliderRules.IsHittingFlatGround(this.transform, out hit, null, maxGroundRaycastDistance);
        }

        private bool IsHittingSlope(out RaycastHit hit)
        {
            return RayColliderRules.IsHittingSlope(this.transform, out hit, null, maxGroundSlopeRaycastDistance);
        }

        public Tuple<Vector3, List<RaycastHit>> GetAveragedNormalFromAllTheWallsTheCollisionOccurs(
            float? wallHitCheckingRadius = null,
            bool debug = false,
            Color? debugColor = null
            )
        {
            int layers = 10;
            int raysInLayer = 60;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            List<RaycastHit> hits = new List<RaycastHit>();
            List<Vector3> resultWallsNormals = new List<Vector3>();

            List<RaycastHit> resultHits = new List<RaycastHit>();

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVerticalListPoints(
                this.transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                wallHitCheckingRadius != null ? (float)wallHitCheckingRadius :
                    this.circleWallCollisionCheckingRadius * 1.1f,
                raysInLayer,
                out hits,
                Layers.generalEnvironmentLayersMask,
                debug,
                debugColor))
                {
                    for (int j = 0; j < hits.Count; j++)
                    {
                        RaycastHit hit = hits[j];
                        if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5)
                        {
                            hit.normal =
                                new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                            if (!Vectors3Helper.VectorsListContainsVectorWithSameDirection(hit.normal, resultWallsNormals))
                            {
                                resultWallsNormals.Add(hit.normal);
                                resultHits.Add(hit);
                            }
                        }
                    }
                }
            }
            if (resultWallsNormals.Count > 0)
            {
                Vector3 resultNormal =
                new Vector3(
                    resultWallsNormals.Average(x => x.x),
                    resultWallsNormals.Average(x => x.y),
                    resultWallsNormals.Average(x => x.z));
                return Tuple.Create(resultNormal, resultHits);
            }
            else
            {
                Debug.LogError("For some reason no normals to average in here, potentially clipping through wall...");
                return Tuple.Create(Vector3.zero, new List<RaycastHit>());
            }
        }

        public bool IsHittingTheWall(
            float? wallHitCheckingRadius = null,
            bool debug = false,
            Color? debugColor = null)
        {
            return IsHittingTheWall(out this.emptyRaycastHit, wallHitCheckingRadius, debug, debugColor);
        }

        public bool IsHittingRoofHanging(out RaycastHit roofHangingHit)
        {
            if (this.isRoofHangingCollisionDetectionEnabled)
            {
                float ceilingRoofHangingCheckRayLength = 2.0f;
                if (PhysicsRaycaster.Raycast(this.transform.position + Vector3.up, Vector3.up,
                    out roofHangingHit, ceilingRoofHangingCheckRayLength))
                {
                    if (roofHangingHit.collider.gameObject.tag.Equals(Tags.roofHangingTag))
                    {
                        roofHangingHit.normal = Vector3.Project(roofHangingHit.normal, Vector3.up).normalized;
                        return true;
                    }
                }
            }
            roofHangingHit = new RaycastHit();
            return false;
        }

        public Vector3 AdjustVelocityIfIsHittingTheCeiling(Vector3 velocityInWorldSpace)
        {
            //if (isRoofHangingCollisionDetectionEnabled)
            //{
            RaycastHit ceilingHit = new RaycastHit();
            float ceilingCollisionCheckRayLength = 2.0f;

            Vector3 velocityInVerticalUpDirection = Vector3.ProjectOnPlane(
                velocityInWorldSpace, Vector3.forward);

            if (Vector3.Angle(velocityInVerticalUpDirection, Vector3.up) > 90f)
            {
                velocityInVerticalUpDirection = Vector3.zero;
            }

            if (PhysicsRaycaster.Raycast(this.transform.position + Vector3.up, velocityInVerticalUpDirection,
                out ceilingHit, ceilingCollisionCheckRayLength, Layers.generalEnvironmentLayersMask) &&
                IsLegitSolidGroundOrWallForCollision(ceilingHit))
            {
                return Vector3.ProjectOnPlane(velocityInWorldSpace, ceilingHit.normal);
            }
            //}            
            return velocityInWorldSpace;
        }

        public bool IsHittingTheWall(out RaycastHit hit,
            float? wallHitCheckingRadius = null, bool debug = false, Color? debugColor = null)
        {
            return RayColliderRules.IsHittingTheWall(this.transform, out hit,
                wallHitCheckingRadius != null ? (float)wallHitCheckingRadius :
                this.circleWallCollisionCheckingRadius, debug, debugColor);
        }

        public bool HitsTheWallCheckedInStackedRays(
            Vector3 raysDirection, bool debug = false, Color? debugColor = null)
        {
            Vector3 originPosition = this.gameObject.transform.position;
            return RayColliderRules.HitsTheWallCheckedInStackedRays(
                originPosition, raysDirection, out this.emptyRaycastHit,
                debug: debug, debugColor: debugColor);
        }

        public bool IsCollidingWithWallClimbObject(
            Vector3 playerForwardDirection,
            Vector3 playerUpDirection,
            Vector3 playerRightDirection,
            out RaycastHit wallClimbHit)
        {
            if (this.isWallClimbingCollisionDetectionEnabled)
            {
                //return PlayerCollidingRules.IsCollidingWithWallClimbObject(
                //    transform.position,
                //    playerForwardDirection,
                //    playerUpDirection,
                //    playerRightDirection,
                //    out wallClimbHit,
                //    Vector3.up);
                int layers = 10;
                int raysInLayer = 15;
                float layersIntervalHeight = 0.1f;
                float startLayersOffset = 0.6f;

                RaycastHit initialWallClimbHit = new RaycastHit();

                for (int i = 0; i < layers; i++)
                {
                    if (PhysicsHelper.CastRaysInCircleVertical(
                    this.transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                    Vector3.forward,
                    this.circleWallCollisionCheckingRadius,
                    raysInLayer,
                    out initialWallClimbHit,
                    Layers.wallClimbablesLayerMask))
                    {
                        //if (wallClimbHit.collider.gameObject.tag.Equals(Tags.wallClimbTag))
                        //{
                        //    return true;
                        //}

                        Vector3 rightDirection = Vector3.Cross(
                            initialWallClimbHit.normal, playerUpDirection);

                        return PlayerCollidingRules.IsCollidingWithWallClimbObject(
                            this.transform.position,
                            -initialWallClimbHit.normal,
                            playerUpDirection,
                            rightDirection,
                            out wallClimbHit,
                            Vector3.up);
                    }
                }
            }
            wallClimbHit = new RaycastHit();
            return false;
        }

        public List<RaycastHit> GetAllCollectibleCrystalsThePlayerCollidesWith()
        {
            float startLayersOffset = 0f;
            int layers = 5;
            int raysInLayer = 15;
            float layersIntervalHeight = 0.3f;
            float circleGemsCollisionCheckingRadius = 2f;

            Func<RaycastHit, bool> colliderEndingFilterCriteria = (hit) => hit.collider.CompareTag(Tags.gemsTag)
                && hit.collider.gameObject.GetComponent<ICollectible>().IsCanBeCollected();

            return PhysicsHelper
                .CastRaysInCyllinderVerticalGetUniqueColliders(
                    this.transform.position + Vector3.up * startLayersOffset,
                    circleGemsCollisionCheckingRadius,
                    raysInLayer,
                    layersIntervalHeight,
                    layers,
                    colliderEndingFilterCriteria,
                    Layers.collectiblesLayerMask
                    );
        }

        public bool IsCollidingWithLedgeGrabCollider(out RaycastHit ledgeGrabHit)
        {
            if (this.isLedgeGrabColliderCollisionDetectionEnabled)
            {
                float startLayersOffset = 1.6f;
                int layers = 5;
                int raysInLayer = 10;
                float layersIntervalHeight = 0.3f;
                float circleLedgeGrabCollisionCheckingRadius = 2f;
                float ledgeGrabAirHeightThreshold = 1.5f;

                if (CheckHeight() > ledgeGrabAirHeightThreshold && PhysicsHelper
                    .CastRaysInCyllinderVertical(
                        this.transform.position + Vector3.up * startLayersOffset,
                        circleLedgeGrabCollisionCheckingRadius,
                        raysInLayer,
                        out ledgeGrabHit,
                        layersIntervalHeight,
                        layers,
                        Layers.ledgeCollidersLayerMask
                        ))
                {
                    if (ledgeGrabHit.collider.CompareTag(Tags.ledgeColliderTag))
                    {
                        ledgeGrabHit.normal = ledgeGrabHit.collider.gameObject.GetComponent<LedgeColliderInfo>().normal;
                        return true;
                    }
                }
            }
            ledgeGrabHit = new RaycastHit();
            return false;
        }

        public float CheckHeight()
        {
            float heightCheckMaxDistance = 1000f;
            RaycastHit heightCheckRaycastHit = new RaycastHit();
            if (PhysicsRaycaster.Raycast(this.transform.position, Vector3.down, out heightCheckRaycastHit, heightCheckMaxDistance,
                Layers.generalEnvironmentLayersMask)
                && IsLegitSolidGroundOrWallForCollision(heightCheckRaycastHit))
            {
                return heightCheckRaycastHit.distance;
            }
            else
            {
                return heightCheckMaxDistance;
            }
        }

        private bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) &&
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        private IEnumerator DisableLedgeGrabColliderCollisionDetectionForTimePeriod(int milliseconds)
        {
            this.isLedgeGrabColliderCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            this.isLedgeGrabColliderCollisionDetectionEnabled = true;
        }

        private IEnumerator DisableWallClimbCollisionDetectionForTimePeriod(int milliseconds)
        {
            this.isWallClimbingCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            this.isWallClimbingCollisionDetectionEnabled = true;
        }

        private IEnumerator DisableRoofHangingCollisionDetectionForTimePeriod(int milliseconds)
        {
            this.isRoofHangingCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            this.isRoofHangingCollisionDetectionEnabled = true;
        }

        public void DisableLedgeGrabColliderDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableLedgeGrabColliderCollisionDetectionForTimePeriod(milliseconds));
        }

        public void DisableWallClimbCollisionDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableWallClimbCollisionDetectionForTimePeriod(milliseconds));
        }

        public void DisableRoofHangingCollisionDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableRoofHangingCollisionDetectionForTimePeriod(milliseconds));
        }
    }
}
