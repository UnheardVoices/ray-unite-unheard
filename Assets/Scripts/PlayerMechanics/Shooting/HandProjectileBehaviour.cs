﻿using Assets.Scripts.Common;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.PlayerMechanics.Targeting;
using System;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Shooting
{
    public static class MathParabola
    {
        public static Vector3 Parabola(Vector3 start, Vector3 end, float height, float t)
        {
            t = Mathf.Clamp01(t);

            Func<float, float> f = x => -4 * height * x * x + 4 * height * x;

            Vector3 mid = Vector3.Lerp(start, end, t);

            return new Vector3(mid.x, f(t) + Mathf.Lerp(start.y, end.y, t), mid.z);
        }

        public static float GetParabolaArcLength(Vector3 start, Vector3 end, float height)
        {
            // https://www.vcalc.com/wiki/vCalc/Parabola+-+arc+length
            float b = Vector3.Distance(start, end);
            float a = height;

            float b2 = b * b;
            float a2 = a * a;
            return (0.5f * Mathf.Sqrt(b2 + 16 * a2)) + (b2 / (8 * a)) * Mathf.Log(((4 * a) + Mathf.Sqrt(b2 + 16 * a2)) / b);
        }
    }

    public static class PointRotationHelper
    {
        public static Vector3 RotatePointAroundAxis(Vector3 point, float angle, Vector3 axisOrigin, Vector3 axis)
        {
            Vector3 translatedPoint = point - axisOrigin;

            Quaternion q = Quaternion.AngleAxis(angle, axis.normalized);
            return (q * translatedPoint) + axisOrigin; //Note: q must be first (point * q wouldn't compile)
        }
    }

    public static class TrajectoryHelper
    {
        public static Vector3 RotateTowardsVector
            (Vector3 initialVector,
             Vector3 rotationBiasVector,
             float rotationProgress)
        {
            bool isPlusWhenAddingRotation =
                Vector3.SignedAngle(initialVector, rotationBiasVector, Vector3.up)
                    >= 0f;

            return Quaternion.AngleAxis(
                isPlusWhenAddingRotation ? (rotationProgress * 180f)
                : -(rotationProgress * 180f), Vector3.up) * initialVector;
        }

        public static Vector3 RotateTowardsVectorWithTarget
            (Vector3 currentFlyingDirection,
            Vector3 startProjectilePosition,
            Vector3 rotationBiasVector,
            Vector3 currentProjectilePosition,
            Vector3 targetPosition,
            float trajectoryProgression)
        {
            float trajectoryParabolaHeight = Vector3.Distance(startProjectilePosition, targetPosition);
            Vector3 pointOnParabola = MathParabola.Parabola(
                startProjectilePosition, targetPosition, trajectoryParabolaHeight, trajectoryProgression);
            Vector3 parabolaPointRotationAxis = (targetPosition - startProjectilePosition).normalized;

            bool isPlusWhenAddingRotation =
                Vector3.SignedAngle(currentFlyingDirection, rotationBiasVector, Vector3.up)
                    >= 0f;

            //Vector3 rotatedParabolaPoint = Quaternion.AngleAxis(90f, parabolaPointRotationAxis) * pointOnParabola;
            Vector3 rotatedParabolaPoint = PointRotationHelper.RotatePointAroundAxis(
                pointOnParabola, isPlusWhenAddingRotation ? 90f : -90f,
                startProjectilePosition,
                parabolaPointRotationAxis);

            //Vector3 rotatedParabolaPoint = pointOnParabola;
            return rotatedParabolaPoint;
        }

        public static float GetParabolaTrajectoryProgressionStep(
            float speed, Vector3 startPosition, Vector3 endPosition)
        {
            float trajectoryParabolaHeight = Vector3.Distance(startPosition, endPosition);
            float parabolaArcLength = MathParabola
                .GetParabolaArcLength(startPosition, endPosition, trajectoryParabolaHeight);

            float parts = parabolaArcLength / speed;
            return 1f / parts;
        }
    }

    public static class HandProjectileConstants
    {
        public const float MAX_CHARGE_POWER = 5f;
    }

    public class HandProjectileBehaviour : GameplayOnlyMonoBehaviour
    {
        public Vector3 flyingDirection;
        public Vector3 initialPlayerRightVector;
        public Transform animatedPlayerPartTransform;
        public Vector3 currentCurvedPathFlyingDirection;

        public Vector3 startPosition;

        public GameObject target;

        public bool isFlyingInCurvedPath = false;

        private float flyingSpeed = 1.5f;
        private float returnSpeed = 4f;

        private float trajectoryRotationProgress = 0.0f;
        private float trajectoryRotationProgressingSpeed = 0.03f;
        private float trajectoryRotationMaxProgressingSpeed = 0.09f;

        private float flyingStartTime;
        private float returnToPlayerTimeThresholdStartSeconds = 0.5f;

        private float proximityRadiusToReturnHandToPlayerEventually = 6f;
        private float proximityRadiusForHittingTheTarget = 1f;

        private bool isFlyingAway = true;
        public bool hasToGoBackImmediately = false;

        public float chargePower = 0f;
        public float maxChargePower = HandProjectileConstants.MAX_CHARGE_POWER;

        public PlayerShootingAspect playerShootingAspect;
        public bool isRightHand;
        public Vector3 trajectoryForward;
        public Vector3 trajectoryUp;

        private Vector3 previousPositionOnTrajectory = new Vector3();

        protected override void BehaviourStart()
        {
            this.flyingStartTime = Time.time;
            this.isFlyingAway = true;
        }

        protected override void GameplayFixedUpdate()
        {
            Debug.DrawLine(this.transform.position, this.transform.position + this.trajectoryUp * 5f, Color.yellow);
            Debug.DrawLine(this.transform.position, this.transform.position + this.trajectoryForward * 5f, Color.red);

            if ((this.hasToGoBackImmediately && this.isFlyingAway)
                || (this.isFlyingAway && HasHitTheSolidObstacle(
                        wallDistanceCheck: 0.25f,
                        slopeDistanceCheck: 0.25f,
                        flatGroundDistanceCheck: 0.25f
                    )))
            {
                this.isFlyingAway = false;
                return;
            }

            float currentTime = Time.time;
            if (currentTime - this.flyingStartTime > this.returnToPlayerTimeThresholdStartSeconds
                && this.isFlyingAway && !this.target)
            {
                this.isFlyingAway = false;
                GetComponent<TrailRenderer>().enabled = false;
                return;
            }
            else if (this.isFlyingAway && this.target && HasHitTheTarget())
            {
                this.isFlyingAway = false;
                GetComponent<TrailRenderer>().enabled = false;

                this.target.GetComponent<TargetHitHandlingBeh>()?.OnTargetHit(this);
                return;
            }

            if (this.isFlyingAway)
            {
                if (!this.isFlyingInCurvedPath)
                {
                    if (this.target)
                    {
                        this.flyingDirection = (this.target.transform.position - this.transform.position).normalized;
                    }

                    this.transform.position = this.transform.position + (this.flyingDirection.normalized * this.flyingSpeed);
                    this.transform.forward = this.flyingDirection.normalized;
                    this.trajectoryForward = this.flyingDirection.normalized;
                    this.trajectoryUp = Quaternion.AngleAxis(Vector3.Angle(
                        this.flyingDirection.normalized,
                        Vector3.ProjectOnPlane(this.flyingDirection.normalized, Vector3.up).normalized
                        ), Vector3.right) * Vector3.up;
                }
                else
                {
                    //if (target)
                    //{
                    //    flyingDirection = (target.transform.position - transform.position).normalized;
                    //}

                    Vector3 translation = new Vector3();
                    Vector3 positionOnTrajectory = new Vector3();

                    if (this.target == null)
                    {
                        translation = TrajectoryHelper.RotateTowardsVector
                            (this.currentCurvedPathFlyingDirection.normalized, this.flyingDirection.normalized,
                            this.trajectoryRotationProgress).normalized;
                    }
                    else
                    {
                        if ((this.target.transform.position - this.transform.position).magnitude > 0.0001f)
                        {
                            this.flyingDirection = (this.target.transform.position - this.transform.position).normalized;
                        }
                        this.trajectoryRotationProgressingSpeed =
                            0.85f *
                            TrajectoryHelper
                                .GetParabolaTrajectoryProgressionStep(
                                    this.flyingSpeed, this.startPosition, this.target.transform.position);

                        this.previousPositionOnTrajectory = 
                            TrajectoryHelper.RotateTowardsVectorWithTarget
                                (this.currentCurvedPathFlyingDirection.normalized,
                                this.startPosition,
                                this.flyingDirection.normalized,
                                this.transform.position,
                                this.target.transform.position,
                                this.trajectoryRotationProgress - 0.01f);

                        positionOnTrajectory = TrajectoryHelper.RotateTowardsVectorWithTarget
                            (this.currentCurvedPathFlyingDirection.normalized,
                            this.startPosition,
                            this.flyingDirection.normalized,
                            this.transform.position,
                            this.target.transform.position,
                            this.trajectoryRotationProgress);
                        //currentCurvedPathFlyingDirection = translation.normalized;
                    }

                    //Vector3 translation = initialCurvedPathFlyingDirection;

                    if (this.target == null)
                    {
                        this.transform.position = this.transform.position + translation.normalized * this.flyingSpeed;
                        this.trajectoryForward = translation.normalized;
                        this.trajectoryUp = Quaternion.AngleAxis(Vector3.Angle(
                            translation.normalized,
                            Vector3.ProjectOnPlane(translation.normalized, Vector3.up).normalized
                        ), Vector3.right) * Vector3.up;
                    }
                    else
                    {
                        this.transform.position = positionOnTrajectory;
                        this.trajectoryForward = (positionOnTrajectory - this.previousPositionOnTrajectory).normalized;
                        this.trajectoryUp = Quaternion.AngleAxis(Vector3.Angle(
                            this.trajectoryForward.normalized,
                            Vector3.ProjectOnPlane(this.trajectoryForward.normalized, Vector3.up).normalized
                        ), Vector3.right) * Vector3.up;

                    }
                    this.transform.forward = translation.normalized;
                    this.trajectoryRotationProgress += this.trajectoryRotationProgressingSpeed;
                }
                return;
            }
            else
            {
                GoTowardsPlayer();

                if ((this.transform.position -
                    (this.animatedPlayerPartTransform.transform.position + Vector3.up)).magnitude <
                    this.proximityRadiusToReturnHandToPlayerEventually)
                {
                    if (this.isRightHand)
                    {
                        this.playerShootingAspect.RegainRightHand();
                    }
                    else
                    {
                        this.playerShootingAspect.RegainLeftHand();
                    }
                    //enabled = false;
                    //GetComponentInChildren<MeshRenderer>().enabled = false;
                    Destroy(this.gameObject);
                    return;
                }

                return;
            }
        }

        private bool HasHitTheSolidObstacle(
            float wallDistanceCheck = 1.1f,
            float slopeDistanceCheck = 1.5f,
            float flatGroundDistanceCheck = 1.2f
        )
        {
            RaycastHit hit = new RaycastHit();
            return RayColliderRules.HasHitTheSolidObstacle(
                    this.transform, out hit,
                    wallDistanceCheck: wallDistanceCheck,
                    slopeDistanceCheck: slopeDistanceCheck,
                    flatGroundDistanceCheck: flatGroundDistanceCheck) &&
                !hit.transform.tag.Equals(Tags.pigPotCrystalsTag) &&
                !hit.transform.tag.Equals(Tags.targetablePartTag) &&
                !hit.transform.tag.Equals(Tags.targetableSwitchTag) &&
                !hit.transform.tag.Equals(Tags.enemyTag);
        }

        private bool HasHitTheTarget()
        {
            return (Vector3.Distance(this.transform.position, this.target.transform.position)
                <= this.proximityRadiusForHittingTheTarget) || 
                (this.isFlyingInCurvedPath && this.target != null && 
                    this.trajectoryRotationProgress > 0.97f);
        }

        private void GoTowardsPlayer()
        {
            this.transform.LookAt(this.animatedPlayerPartTransform.position + Vector3.up);
            this.transform.Translate(Vector3.forward * this.returnSpeed);
        }
    }
}
