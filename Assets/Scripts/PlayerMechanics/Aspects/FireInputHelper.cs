﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.PlayerMechanics.Rules.Common;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public class FireInputHelper : GameplayOnlyMonoBehaviour
    {
        private float instantShotFireMaxTimePressingThreshold = 0.05f;
        private float lastFirePressingTime = 0;
        private float currentFirePressingTime = 0;

        private bool checkedForFirePressingCycleFinish = false;
        private bool checkedForChargingFirePressingCycleStart = false;
        private bool finishedFirePressingCycle = true;

        private PlayerMovementInput playerMovementInput;

        public float GetInstantShotFireMaxTimePressingThreshold()
        {
            return this.instantShotFireMaxTimePressingThreshold;
        }

        private void Awake()
        {
            this.playerMovementInput = FindObjectOfType<PlayerMovementInput>();
        }

        protected override void GameplayUpdate(float deltaTime)
        {
            if (IsPressingFire())
            {
                this.currentFirePressingTime += deltaTime;
                this.finishedFirePressingCycle = false;
            }
            else if (!this.finishedFirePressingCycle)
            {
                this.lastFirePressingTime = this.currentFirePressingTime;
                this.currentFirePressingTime = 0f;
                this.finishedFirePressingCycle = true;
                this.checkedForFirePressingCycleFinish = false;
                this.checkedForChargingFirePressingCycleStart = false;
            }
        }

        public bool IsPressingFire()
        {
            return this.playerMovementInput.GetFireButton();
        }

        public float LastFirePressingSumTime()
        {
            return this.lastFirePressingTime;
        }

        public float CurrentFirePressingSumTime()
        {
            return this.currentFirePressingTime;
        }

        public bool HasJustFinishedPressingFire()
        {
            if (this.finishedFirePressingCycle && !this.checkedForFirePressingCycleFinish)
            {
                this.checkedForFirePressingCycleFinish = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasJustStartedPressingFireForCharging()
        {
            if (!this.finishedFirePressingCycle && !this.checkedForChargingFirePressingCycleStart)
            {
                this.checkedForChargingFirePressingCycleStart = true;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
