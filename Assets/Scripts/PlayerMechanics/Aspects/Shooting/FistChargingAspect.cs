﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class FistChargingAnimationHelper
    {
        private static Vector3 rightChargingHandOriginOffset = Vector3.right;
        private static Vector3 leftChargingHandOriginOffset = -Vector3.right;

        private static float handSpinningRadiusAroundOrigin = 0.5f;
        private static float handSpinningAngularDegreesSpeed = 40.0f;

        public static void SpinRightFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(rightChargingHandOriginOffset));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(rightChargingHandOriginOffset);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, 0.7f, handSpinningAngularDegreesSpeed);
        }

        public static void SpinLeftFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(leftChargingHandOriginOffset));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(leftChargingHandOriginOffset);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, 0.7f, handSpinningAngularDegreesSpeed);
        }

        public static Transform
            InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(
            Transform animatedPlayerPart)
        {
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingRightHandStateName(), 1);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            //handObj.GetComponent<TrailRenderer>().time = 10f;
            handObj.transform.position = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(Vector3.right));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static Transform
            InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(
            Transform animatedPlayerPart)
        {
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingLeftHandStateName(), 2);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            //handObj.GetComponent<TrailRenderer>().time = 10f;
            handObj.transform.position = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(-Vector3.right));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static void DestroyRespectiveFistChargingPrefab(
            Transform currentChargingFistPrefabTransform,
            bool isChargingRightHandCurrently)
        {
            if (currentChargingFistPrefabTransform && currentChargingFistPrefabTransform.gameObject)
            {
                GameObject.Destroy(currentChargingFistPrefabTransform.gameObject);
            }
        }
    }

    public class FistChargingAspect : MonoBehaviour
    {
        private float currentChargePower = 1.0f;
        private float maxChargePower = HandProjectileConstants.MAX_CHARGE_POWER;
        private float lastKeepChargingCycleTime;

        public bool isChargingRightHandCurrently = false;
        private bool isInValidChargingCycle = false;

        private Transform animatedPlayerPart;
        private Transform currentChargingFistPrefabTransform;

        private float fistChargingSpeed = 0.1f;

        public float powerNormalized {
            get {
                return this.currentChargePower / this.maxChargePower;
            }
        }

        private void Start()
        {
            this.lastKeepChargingCycleTime = Time.time;
            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject.transform;
        }

        public void KeepChargingCycle()
        {
            this.lastKeepChargingCycleTime = Time.time;
            if (this.isChargingRightHandCurrently)
            {
                FistChargingAnimationHelper
                    .SpinRightFistChargingPrefab(this.animatedPlayerPart, this.currentChargingFistPrefabTransform);
            }
            else
            {
                FistChargingAnimationHelper.SpinLeftFistChargingPrefab(
                    this.animatedPlayerPart, this.currentChargingFistPrefabTransform);
            }
            this.currentChargePower += this.fistChargingSpeed;
            if (this.currentChargePower > this.maxChargePower)
            {
                this.currentChargePower = this.maxChargePower;
            }
        }

        public void InitiateFistChargingCycleForHand(bool isRightHand)
        {
            this.isChargingRightHandCurrently = isRightHand;
            this.isInValidChargingCycle = true;
            if (this.isChargingRightHandCurrently)
            {
                this.currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(this.animatedPlayerPart);
            }
            else
            {
                this.currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(this.animatedPlayerPart);
            }
        }

        public (float, float) FinishChargingCycleAndGetEventualChargePowerAndMaxPower()
        {
            float result = this.currentChargePower;
            this.currentChargePower = 1f;
            this.isInValidChargingCycle = false;

            FistChargingAnimationHelper
              .DestroyRespectiveFistChargingPrefab(
                 this.currentChargingFistPrefabTransform, this.isChargingRightHandCurrently);
            return (result, this.maxChargePower);
        }

        public bool IsChargingRightHandCurrently()
        {
            return this.isChargingRightHandCurrently && IsInValidCycleCurrently();
        }

        private bool IsInValidCycleCurrently()
        {
            return this.isInValidChargingCycle;
        }

        public bool IsChargingLeftHandCurrently()
        {
            return !this.isChargingRightHandCurrently && IsInValidCycleCurrently();
        }
    }
}
