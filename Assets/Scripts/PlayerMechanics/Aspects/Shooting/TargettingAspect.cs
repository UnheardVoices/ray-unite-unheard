﻿using Assets.Scripts.Animations;
using Assets.Scripts.Common;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.GameMechanics.Entities.NewEntity.Aspects;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.HUD;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Camera;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class TargettablesHelper
    {
        public static List<GameObject>
            GetTargettableGameObjectsInTheScene()
        {
            return
                GameObject.FindGameObjectsWithTag(Tags.lumForHangingTag).ToList()
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.pigPotCrystalsTag).ToList())
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.targetableSwitchTag).ToList())
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.enemyTag).ToList()).ToList();
        }

        public static GameObject
            GetClosestValidTarget(
                GameObject playerObject,
                Vector3 playerLookingDirection,
                List<GameObject> targettables)
        {
            Transform targetMinimalDistance = null;
            float minimalDistance = Mathf.Infinity;
            float minimalAngle = Mathf.Infinity;
             
            Vector3 currentPosition = playerObject.transform.position;
            List<Tuple<GameObject, Vector3>> potentialTargets = new List<Tuple<GameObject, Vector3>>();
            foreach (GameObject targetObject in targettables)
            {
                float minimalDistanceTargetThresholdConsideration = 18f;
                Vector3 distanceDirection = (targetObject.transform.position - playerObject.transform.position);

                TargetableProperties targetableProperties = 
                    targetObject.GetComponent<TargetableProperties>();

                if (targetableProperties)
                {
                    minimalDistanceTargetThresholdConsideration = targetableProperties.maxTargetingDistance;
                }

                float distance =
                    Vector3.Distance(
                        targetObject.transform.position, currentPosition);

                Vector3 directionToTarget =
                    (targetObject.transform.position - playerObject.transform.position).normalized;

                float angleFromPlayerLookingDirection =
                    Vector3.Angle(directionToTarget, playerLookingDirection);

                if (
                    distance < minimalDistanceTargetThresholdConsideration
                    && (Vector3.Angle(
                            Vector3.ProjectOnPlane(directionToTarget, Vector3.up).normalized,
                                playerLookingDirection) < 80f ||
                        Vector3.Angle(Vector3.up, directionToTarget.normalized) < 5f)
                    )
                {
                    potentialTargets.Add(Tuple.Create(targetObject, directionToTarget));
                    targetMinimalDistance = targetObject.transform;
                    minimalAngle = angleFromPlayerLookingDirection;
                    minimalDistance = distance;
                }
            }

            return potentialTargets.OrderBy(x =>
            Vector3.Angle(
                playerLookingDirection,
                Vector3.ProjectOnPlane(x.Item2, Vector3.up)))
                .OrderBy(x => Mathf.Abs(x.Item1.transform.position.y - playerObject.transform.position.y))
                .FirstOrDefault()?.Item1;
        }

        public static bool IsTooFarToTargetAnything(
            GameObject playerObject,
            GameObject currentTarget,
            Vector3 playerLookingDirection,
            List<GameObject> targettables)
        {
            if (currentTarget)
            {
                var targetableProperties = 
                    currentTarget.GetComponent<TargetableProperties>();

                float minimalDistanceTargetThresholdConsideration = 18f;

                if (targetableProperties)
                {
                    minimalDistanceTargetThresholdConsideration = targetableProperties.maxTargetingDistance;
                }
                return Vector3.Distance(currentTarget.transform.position,
                    playerObject.transform.position) >= minimalDistanceTargetThresholdConsideration;
            }
            else
            {
                return GetClosestValidTarget(
                    playerObject,
                    playerLookingDirection,
                    targettables) == null;
            }
        }
    }

    public static class TargetShootSightScreenPositioningHelper
    {
        public static Vector3 GetScreenPositionForTarget(
            UnityEngine.Camera playerCamera, GameObject target)
        {
            Vector3 projectedPos = 
                playerCamera.GetComponent<CameraRuleBase>()
                    .ProjectPositionPotentiallyLookingThroughThePortal(target.transform.position);

            Vector3 guiPosition = playerCamera.WorldToScreenPoint(projectedPos);
            guiPosition.y = Screen.height - guiPosition.y;

            return guiPosition;
        }
    }

    public class TargettingAspect : GameplayOnlyMonoBehaviour
    {
        private GameObject playerObject;
        private GameObject animatedPlayerPart;
        private UnityEngine.Camera playerCamera;
        private PlayerMovementStateInfo playerMovementStateInfo;
        private List<PortalRenderingBehaviour> portals;

        public GameObject currentTarget;
        public Vector3 currentTargetProjectedPosition
        {
            get
            {
                return PlayerPortalsHelper.GetClosestGameObjectPositionConsideringPortalsFrom(
                    this.playerCamera.transform.position, this.currentTarget.transform.position,
                    this.portals).Item1;
            }
        }

        private DrawHudTargetShootSight hudTargetShootSightDrawing;

        public List<GameObject> targettables = new List<GameObject>();

        private IStrafingRuleUtils groundStrafingRule;
        private IStrafingRuleUtils airStrafingRule;

        private void Awake()
        {
            this.playerObject = this.gameObject;
            this.playerCamera = FindObjectOfType<CameraActorHandle>().GetComponent<UnityEngine.Camera>();
            this.hudTargetShootSightDrawing = FindObjectOfType<DrawHudTargetShootSight>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();
            this.portals = FindObjectsOfType<PortalRenderingBehaviour>().ToList();

            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject;

            this.groundStrafingRule = GetComponent<RuleStrafingGround>();
            this.airStrafingRule = GetComponent<RuleStrafingAir>();
        }

        public Vector3 GetPlayerLookingDirectionTowardsTarget(Vector3 playerPosition)
        {
            if (this.currentTarget)
            {
                return Vector3.ProjectOnPlane(
                    this.currentTarget.transform.position - playerPosition,
                    Vector3.up).normalized;
            }
            else
            {
                return new Vector3();
            }
        }

        protected override void BehaviourStart()
        {
            this.targettables = TargettablesHelper.GetTargettableGameObjectsInTheScene();
        }

        private Tuple<IStrafingRuleUtils, bool> GetCurrentStrafingRule()
        {
            if (this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND))
            {
                return Tuple.Create(this.groundStrafingRule, true);
            }
            else if (this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                return Tuple.Create(this.airStrafingRule, true);
            }
            else
            {
                return Tuple.Create<IStrafingRuleUtils, bool>(null, false);
            }
        }

        protected override void GameplayFixedUpdate()
        {
            if (IsAllowedRuleCurrentlyForTargeting())
            {
                if (this.currentTarget == null || GetCurrentStrafingRule().Item2 == false)
                {
                    //Debug.Log("Changing target " + currentTarget);
                    this.currentTarget = TargettablesHelper.GetClosestValidTarget(
                        this.playerObject, -this.animatedPlayerPart.transform.forward, this.targettables);
                }

                if (TargettablesHelper.IsTooFarToTargetAnything(
                    this.playerObject, this.currentTarget, -this.animatedPlayerPart.transform.forward,
                    this.targettables))
                {
                    this.currentTarget = null;
                }

                if (this.currentTarget)
                {
                    TargetableProperties targetableProperties =
                        this.currentTarget.GetComponent<TargetableProperties>();

                    bool drawShootSight = true;

                    if (targetableProperties)
                    {
                        drawShootSight = targetableProperties.drawShootSight;
                    }

                    // this.hudTargetShootSightDrawing = FindObjectOfType<DrawHudTargetShootSight>();
                    this.hudTargetShootSightDrawing
                        .SetShootSightScreenCoordinates(
                            TargetShootSightScreenPositioningHelper
                                .GetScreenPositionForTarget(this.playerCamera, this.currentTarget)
                    );

                    Tuple<IStrafingRuleUtils, bool> currentStrafingRuleInfo = GetCurrentStrafingRule();

                    if (currentStrafingRuleInfo.Item2 == true)
                    {
                        this.hudTargetShootSightDrawing.SetShootingDirection(
                            currentStrafingRuleInfo.Item1.GetShootSightDirection());
                    }
                    else
                    {
                        this.hudTargetShootSightDrawing.SetShootingDirection(ShootSightDirection.DOWN_ARROW_STRAIGHT);
                    }

                    this.hudTargetShootSightDrawing.SetIsShootSightVisible(
                        this.currentTarget.GetComponentInChildren<Renderer>().isVisible && drawShootSight);
                }
                else
                {
                    this.hudTargetShootSightDrawing.SetIsShootSightVisible(false);
                }
            }
            else
            {
                this.hudTargetShootSightDrawing.SetIsShootSightVisible(false);
            }
        }

        private bool IsAllowedRuleCurrentlyForTargeting()
        {
            return this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_GROUND) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_AIR) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR) ||
                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND) ||

                this.playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.LEGACY_EMPTY_RULE);
        }

        public bool IsTargetVisible()
        {
            return this.currentTarget ?
                this.currentTarget.GetComponentInChildren<Renderer>().isVisible : false;
        }
    }
}
