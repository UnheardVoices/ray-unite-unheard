﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.NewPlayerMechanics;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Shooting;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public static class HandShootingProjectilesFactory
    {
        public static void ShootHandProjectile(
            float chargePower,
            float maxChargePower,
            Transform animatedPlayerPartTransform,
            PlayerShootingAspect playerShootingAspect,
            bool isRightHand,
            Vector3 startPosition, Vector3 direction,
            GameObject target)
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = startPosition;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);

            HandProjectileBehaviour handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.chargePower = chargePower;
            handProjectileBehaviour.maxChargePower = maxChargePower;
            handProjectileBehaviour.flyingDirection = direction;
            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.initialPlayerRightVector =
                -animatedPlayerPartTransform.transform.right.normalized;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.startPosition = startPosition;
            handProjectileBehaviour.target = target;
        }

        public static void ShootHandProjectileFreelyInCurvedPath(
            float chargePower,
            float maxChargePower,
            Transform animatedPlayerPartTransform,
            PlayerShootingAspect playerShootingAspect,
            bool isRightHand,
            Vector3 startPosition, Vector3 direction,
            Vector3 shootingInitialCurvedPathDirection,
            GameObject target
            )
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = startPosition;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);

            HandProjectileBehaviour handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.chargePower = chargePower;
            handProjectileBehaviour.maxChargePower = maxChargePower;
            handProjectileBehaviour.flyingDirection = direction;
            handProjectileBehaviour.isFlyingInCurvedPath = true;
            handProjectileBehaviour.currentCurvedPathFlyingDirection = shootingInitialCurvedPathDirection;
            handProjectileBehaviour.initialPlayerRightVector =
                -animatedPlayerPartTransform.transform.right.normalized;
            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.startPosition = startPosition;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.target = target;
        }

        public static void SpawnHandProjectileGoingBackToPlayer(
            PlayerShootingAspect playerShootingAspect,
            Transform animatedPlayerPartTransform, Vector3 position, bool isRightHand)
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = position;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);
            HandProjectileBehaviour handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.hasToGoBackImmediately = true;
            handProjectileBehaviour.GetComponent<TrailRenderer>().enabled = false;
        }

        public static GameObject SpawnStaticHandObjectForLumSwinging(
            Vector3 position, Vector3 localScale)
        {
            GameObject handObject = GameObject.Instantiate(
                GameObject.Find("RIGHT_HAND_OBJ_PURPLE_LUM"));
            handObject.transform.position = position;
            //handObject.transform.rotation = rotation;
            handObject.transform.localScale = localScale;

            //handObject.GetComponent<TrailRenderer>().enabled = false;
            return handObject;
        }
    }

    public class PlayerShootingAspect : GameplayOnlyMonoBehaviour
    {
        private bool canPlayerTriggerShootingHand = false;
        private AnimationController animationController;
        private PlayerController playerController;
        private PlayerMovementStateInfo playerMovementStateInfo;
        private GameObject animatedPlayerPart;

        private TargettingAspect targettingAspect;

        private bool hasLeftHand = true;
        private bool hasRightHand = true;

        private int leftHandAnimationLayer = 2;
        private int rightHandAnimationLayer = 1;

        private FireInputHelper fireInputHelper;
        private FistChargingAspect fistChargingAspect;

        public bool HasHandsToShoot()
        {
            return this.hasLeftHand || this.hasRightHand;
        }

        public float GetCurrentChargingPowerNormalized()
        {
            return this.fistChargingAspect.powerNormalized;
        }

        public bool IsGoingForwardOrBackwardsForShooting()
        {
            float angle = Vector3.Angle(-this.animatedPlayerPart.transform.forward,
                this.transform.TransformDirection(
                    Vector3.ProjectOnPlane(this.playerMovementStateInfo.movementVelocity.normalized,
                    Vector3.up)).normalized);

            return ((angle < 20f) || (angle > 160f))
                ||
                Vector3.ProjectOnPlane(this.playerMovementStateInfo.movementVelocity, Vector3.up).magnitude < 0.1f;
        }

        public bool CanTriggerShootingHand()
        {
            return this.canPlayerTriggerShootingHand;
        }

        public bool IsPressingShootingButtonInstantShot()
        {
            return !this.fireInputHelper.IsPressingFire() &&
                this.fireInputHelper.LastFirePressingSumTime()
                    <= this.fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool WantsToFinishChargingAndShoot()
        {
            return this.fireInputHelper.HasJustFinishedPressingFire()
                && this.fireInputHelper.LastFirePressingSumTime() >
                this.fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool IsPressingShootingButtonForCharging()
        {
            return this.fireInputHelper.IsPressingFire() &&
                this.fireInputHelper.CurrentFirePressingSumTime() >
                this.fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool HasJustStartedPressingShootingButtonForCharging()
        {
            return this.fireInputHelper.HasJustStartedPressingFireForCharging();
        }

        public void InitiateFistChargingCycleForAppropriateHand()
        {
            this.fistChargingAspect.InitiateFistChargingCycleForHand(isRightHand: this.hasRightHand);
        }

        public void FinishFistChargingCycleAndShootHand()
        {
            (float chargePower, float maxChargePower) =
                this.fistChargingAspect
                    .FinishChargingCycleAndGetEventualChargePowerAndMaxPower();
            this.playerController.playerParams.soundParams
                .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                    this.playerController.controllerContext
                        .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                    );
            // determine the hand explicitly?
            ShootHand(chargePower, maxChargePower,
                this.fistChargingAspect.isChargingRightHandCurrently == true); // ?
        }

        public void FinishFistChargingCycleAndShootHandFreelyInCurvedPath(Vector3 shootingInitialDirection)
        {
            (float chargePower, float maxChargePower) =
                this.fistChargingAspect
                    .FinishChargingCycleAndGetEventualChargePowerAndMaxPower();
            this.playerController.playerParams.soundParams
                .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                    this.playerController.controllerContext
                        .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                );
            ShootHandFreelyInCurvedPath(
                 shootingInitialDirection, chargePower,
                 maxChargePower,
                 this.fistChargingAspect.isChargingRightHandCurrently == true);
        }

        public void KeepChargingCycle()
        {
            this.fistChargingAspect.KeepChargingCycle();
        }

        public void RegainRightHand()
        {
            this.hasRightHand = true;
        }

        public void RegainLeftHand()
        {
            this.hasLeftHand = true;
        }

        public void ShootHandFreelyInCurvedPath(
            Vector3 shootingInitialDirection, float chargePower = 1.0f,
            float maxChargePower = HandProjectileConstants.MAX_CHARGE_POWER,
            bool? shootRightHand = null)
        {
            this.canPlayerTriggerShootingHand = false;

            if (this.hasRightHand && (shootRightHand == null || shootRightHand == true))
            {
                this.hasRightHand = false;
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingRightHandStateName(), layerIndex: 1);
                ShootHandProjectileFreelyInCurvedPath(
                    chargePower, maxChargePower, true, shootingInitialDirection);
            }
            else if (this.hasLeftHand && (shootRightHand == null || shootRightHand == false))
            {
                this.hasLeftHand = false;
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingLeftHandStateName(), layerIndex: 2);
                ShootHandProjectileFreelyInCurvedPath(
                    chargePower, maxChargePower, false, shootingInitialDirection);
            }

            this.playerController.playerParams.soundParams
                .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                    this.playerController.controllerContext
                        .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                    );
        }

        public void ShootHand(
            float chargePower = 1.0f,
            float maxChargePower = HandProjectileConstants.MAX_CHARGE_POWER,
            bool? shootRightHand = null)
        {
            this.canPlayerTriggerShootingHand = false;

            if (this.hasRightHand && (shootRightHand == null || shootRightHand == true))
            {
                this.hasRightHand = false;
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingRightHandStateName(), layerIndex: 1);
                ShootHandProjectile(chargePower, maxChargePower, true);
            }
            else if (this.hasLeftHand && (shootRightHand == null || shootRightHand == false))
            {
                this.hasLeftHand = false;
                this.animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingLeftHandStateName(), layerIndex: 2);
                ShootHandProjectile(chargePower, maxChargePower, false);
            }

            this.playerController.playerParams.soundParams
                .shootingSounds.chargingFistSound.StopPlayingWithAudioSource(
                    this.playerController.controllerContext
                        .playerAudioHandles.shootingAudioHandles.chargingAudioSource
                    );
        }

        private void ShootHandProjectileFreelyInCurvedPath(
            float chargePower, float maxChargePower,
            bool isRightHand, Vector3 shootingInitialDirection)
        {
            HandShootingProjectilesFactory.ShootHandProjectileFreelyInCurvedPath(
                chargePower,
                maxChargePower,
                this.animatedPlayerPart.transform,
                this,
                isRightHand,
                this.animatedPlayerPart.transform.position + Vector3.up,
                -this.animatedPlayerPart.transform.forward,
                shootingInitialDirection,
                this.targettingAspect.IsTargetVisible() ? this.targettingAspect.currentTarget : null
                );
        }

        private void ShootHandProjectile(
            float chargePower, float maxChargePower, bool isRightHand)
        {
            HandShootingProjectilesFactory.ShootHandProjectile(
                chargePower,
                maxChargePower,
                this.animatedPlayerPart.transform,
                this,
                isRightHand,
                this.animatedPlayerPart.transform.position + Vector3.up,
                -this.animatedPlayerPart.transform.forward,
                this.targettingAspect.IsTargetVisible() ? this.targettingAspect.currentTarget : null
                );
        }

        private void Awake()
        {
            this.animationController = GetComponentInChildren<AnimationController>();
            this.animatedPlayerPart = this.animationController.gameObject;

            this.fireInputHelper = GetComponent<FireInputHelper>();
            this.fistChargingAspect = GetComponent<FistChargingAspect>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();

            this.targettingAspect = GetComponent<TargettingAspect>();
            this.playerController = GetComponent<PlayerController>();
        }

        protected override void GameplayFixedUpdate()
        {
            if (!IsPressingShootingButtonInstantShot())
            {
                this.canPlayerTriggerShootingHand = true;
            }

            if (this.hasLeftHand &&
                !this.animationController.GetCurrentAnimationState(this.leftHandAnimationLayer)
                    .Equals(RaymanAnimations.NonOverridingAnimationLayer2StateName())
                && !this.fistChargingAspect.IsChargingLeftHandCurrently())
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.NonOverridingAnimationLayer2StateName(), this.leftHandAnimationLayer);
            }

            if (this.hasRightHand &&
                !this.animationController.GetCurrentAnimationState(this.rightHandAnimationLayer)
                    .Equals(RaymanAnimations.NonOverridingAnimationLayer1StateName())
                 && !this.fistChargingAspect.IsChargingRightHandCurrently())
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.NonOverridingAnimationLayer1StateName(), this.rightHandAnimationLayer);
            }

            if (!this.hasLeftHand &&
                this.animationController.GetCurrentAnimationState(this.leftHandAnimationLayer)
                    .Equals(RaymanAnimations.ExtraAnimShootingLeftHandStateName()) &&
                this.animationController.HasAnimationEnded(this.leftHandAnimationLayer))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.ExtraAnimMissingLeftHandStateName(), this.leftHandAnimationLayer);
            }


            if (!this.hasRightHand &&
                this.animationController.GetCurrentAnimationState(this.rightHandAnimationLayer)
                    .Equals(RaymanAnimations.ExtraAnimShootingRightHandStateName()) &&
                this.animationController.HasAnimationEnded(this.rightHandAnimationLayer))
            {
                this.animationController.ChangeAnimationState(
                    RaymanAnimations.ExtraAnimMissingRightHandStateName(), this.rightHandAnimationLayer);
            }
        }
    }
}
