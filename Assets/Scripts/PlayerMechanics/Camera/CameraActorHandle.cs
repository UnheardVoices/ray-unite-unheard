﻿using Assets.Scripts.GameMechanics.Entities.Portal;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraActorHandle : MonoBehaviour
    {
        private CameraFollow cameraFollowRule;
        PortalRenderingBehaviour[] portals;


        private void Awake()
        {
            this.cameraFollowRule = GetComponent<CameraFollow>();
            this.portals = FindObjectsOfType<PortalRenderingBehaviour>();
        }

        public void DisableGameplayCameraRules()
        {
            this.cameraFollowRule.DisableRule();
        }

        public void SetCameraPosition(Vector3 position)
        {
            this.transform.position = position;
        }

        public void EnableGameplayCameraRules()
        {
            this.cameraFollowRule.EnableRule();
        }

        public void LookAt(Vector3 targetPosition)
        {
            this.transform.LookAt(targetPosition);
        }

        void OnPreCull()
        {

            //for (int i = 0; i < portals.Length; i++)
            //{
            //    portals[i].PrePortalRender();
            //}
            //for (int i = 0; i < this.portals.Length; i++)
            //{
            //    this.portals[i].Render();
            //}

            //for (int i = 0; i < portals.Length; i++)
            //{
            //    portals[i].PostPortalRender();
            //}

        }
    }
}
