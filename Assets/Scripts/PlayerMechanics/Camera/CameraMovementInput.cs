﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.Engine.Input;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraMovementInput : GameplayOnlyMonoBehaviour
    {
        public Vector2 GetTranslation()
        {
            return this.currentTranslationFromUpdate;
        }

        private Vector2 currentTranslationFromUpdate = new Vector2();

        protected PlayerInputHub playerInputHub;

        protected override void BehaviourStart()
        {
            base.BehaviourStart();
            this.playerInputHub = FindObjectOfType<PlayerInputHub>();
        }

        protected override void GameplayFixedUpdate()
        {
            this.currentTranslationFromUpdate =
                Vector2.Lerp(this.currentTranslationFromUpdate,
                    Vector3.ClampMagnitude(this.playerInputHub.lookInputRaw, 1f),
                        0.02f);
        }
    }
}
