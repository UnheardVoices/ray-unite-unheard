﻿using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public class CameraTargetPlayerBackStrafe : CameraRuleBase
    {
        protected TargettingAspect playerTargettingAspect;

        protected new void Awake()
        {
            base.Awake();
            this.playerTargettingAspect = this.playerObject.GetComponent<TargettingAspect>();
        }

        protected override CameraRuleEnum GetCameraRuleEnum()
        {
            return CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
        }

        protected override void OnRuleFixedUpdate()
        {
            HandleCameraTooFarAway();

            if (this.playerTargettingAspect
                ?.currentTarget?.transform?.position != null)
            {
                //Vector3 targetPosition = this.playerTargettingAspect
                //    .currentTarget.transform.position;
                //Vector3 targetPosition = this.playerTargettingAspect.currentTargetProjectedPosition;

                //Vector3 lookDirection = (targetPosition - this.transform.position).normalized;

                Vector3 lookDirection =
                    PlayerPortalsHelper.GetProjectedDirectionThroughPortalIfCloserThanDirect(
                        this.transform.position, this.playerTargettingAspect.currentTarget.transform.position,
                        (this.playerTargettingAspect.currentTarget.transform.position - this.transform.position).normalized,
                        this.portals
                        );

                UpdatePlayerMovementMetrics(Vector3.ProjectOnPlane(lookDirection, Vector3.up).normalized);
                this.transform.forward = Vector3.Lerp(this.transform.forward, lookDirection, this.smoothSpeed);

                Vector3 playerLastPosition;
                playerLastPosition =
                    this.playerPositionToLookAt + Vector3.up +
                    (-this.playerMovementMetrics.forwardDirection.normalized) * this.distanceFromObject;
                playerLastPosition.y = this.playerPositionToLookAt.y + this.distanceFromObject / 7.5f;

                Vector3 newDesiredCameraPosition = Vector3.Lerp(this.transform.position, playerLastPosition, this.smoothSpeed);
                Vector3 cameraTranslation = newDesiredCameraPosition - this.transform.position;

                cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                    this.gameObject, this.rayCollider, cameraTranslation.normalized, cameraTranslation);
                this.transform.position = this.transform.position + cameraTranslation;
            }
            else
            {
                return;
            }
        }
    }
}
