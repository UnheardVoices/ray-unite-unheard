﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public class CameraStrafing : CameraRuleBase
    {
        protected override CameraRuleEnum GetCameraRuleEnum()
        {
            return CameraRuleEnum.CAMERA_STRAFING;
        }

        protected override void OnRuleFixedUpdate()
        {
            HandleCameraTooFarAway();

            Vector3 lookDirection = ((this.playerPositionToLookAt - this.transform.position).normalized + Vector3.up * 0.05f).normalized;
            //UpdatePlayerMovementMetrics(new Vector3(lookDirection.x, 0, lookDirection.z));
            this.transform.forward = Vector3.Lerp(this.transform.forward, lookDirection, this.smoothSpeed);

            Vector3 playerLastPosition;
            playerLastPosition =
                this.playerPositionToLookAt + Vector3.up +
                (-this.playerForwardDirectionProjected.normalized) * this.distanceFromObject;
                //(-this.playerMovementMetrics.forwardDirection.normalized) * this.distanceFromObject;
            playerLastPosition.y = this.playerPositionToLookAt.y + this.distanceFromObject / 2.5f;

            Vector3 newDesiredCameraPosition = Vector3.Lerp(this.transform.position, playerLastPosition, this.smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - this.transform.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                this.gameObject, this.rayCollider, cameraTranslation.normalized, cameraTranslation);
            this.transform.position = this.transform.position + cameraTranslation;
        }
    }
}
