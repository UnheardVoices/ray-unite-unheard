﻿namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public enum CameraRuleEnum
    {
        CAMERA_FOLLOW,
        CAMERA_STRAFING,
        CAMERA_TARGET_PLAYER_BACK_STRAFING,
        CAMERA_LUM_SWINGING,
        CAMERA_LEDGE_GRAB
    }
}
