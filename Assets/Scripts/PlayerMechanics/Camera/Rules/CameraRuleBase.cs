﻿using Assets.Scripts.Engine.Behaviours;
using Assets.Scripts.GameMechanics.Entities.Portal;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public static class PlayerPortalsHelper
    {
        public static (Vector3, PortalRenderingBehaviour) GetClosestGameObjectPositionConsideringPortalsFrom(
            Vector3 positionFrom, Vector3 positionTo, List<PortalRenderingBehaviour> portals)
        {
            float directDistance = Vector3.Distance(positionFrom, positionTo);
            var distanceCandidate = directDistance;
            (Vector3, PortalRenderingBehaviour) resultCandidate = (positionTo, null);
            foreach (var portal in portals)
            {
                (Vector3 projectedPositionByPortal, float distanceViaPortal) =
                    portal.GetProjectedPositionAndDistanceThrough(positionFrom, positionTo);
                if (distanceViaPortal < distanceCandidate)
                {
                    distanceCandidate = distanceViaPortal;
                    resultCandidate = (projectedPositionByPortal, portal);
                }
            }
            return resultCandidate;
        }

        public static Vector3 GetProjectedDirectionThroughPortalIfCloserThanDirect(
            Vector3 positionFrom, Vector3 positionTo, Vector3 directDirection, List<PortalRenderingBehaviour> portals)
        {
            var closestPortalContext = 
                GetClosestGameObjectPositionConsideringPortalsFrom(
                    positionFrom, positionTo, portals);
            if (closestPortalContext.Item2 != null)
            {
                var helperObject = new GameObject();
                var portal = closestPortalContext.Item2;
                var linkedPortal = closestPortalContext.Item2.GetComponent<PortalController>()
                    .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>();
                var derivedForward = linkedPortal.portalDerivedForward;

                var oldPortalForward = portal.transform.forward;
                var oldLinkedPortalForward = linkedPortal.transform.forward;

                //portal.transform.forward = -portal.portalDerivedForward;
                //linkedPortal.transform.forward = -derivedForward;

                var result = Vector3.ProjectOnPlane(
                    (positionTo - linkedPortal.transform.position).normalized, Vector3.up).normalized;

                return Vector3.ProjectOnPlane(
                    linkedPortal.transform.TransformDirection(portal.transform.InverseTransformDirection(directDirection)),
                    Vector3.up).normalized;

                //portal.transform.forward = oldPortalForward;
                //linkedPortal.transform.forward = oldLinkedPortalForward;

                // return result;

                //return Vector3.Project(directDirection, closestPortalContext.Item2.GetComponent<PortalController>()
                //        .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>().portalDerivedForward).normalized;
                //helperObject.transform.forward = closestPortalContext.Item2.GetComponent<PortalController>()
                //        .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>().portalDerivedForward;
                ////helperObject.transform.up = closestPortalContext.Item2.GetComponent<PortalController>()
                ////        .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>().transform.up;

                //var result = Vector3.ProjectOnPlane(closestPortalContext.Item2.GetComponent<PortalController>()
                //        .portalParams.linkedPortal.transform.TransformDirection(
                //            helperObject.transform.InverseTransformDirection(directDirection)), Vector3.up);
                //Object.Destroy(helperObject);
                // return result;
                //closestPortalContext.Item2.GetComponent<PortalController>()
                //    .portalParams.linkedPortal.transform.TransformDirection(
                //        closestPortalContext.Item2.transform.InverseTransformDirection(directDirection));
            }
            return Vector3.ProjectOnPlane(directDirection, Vector3.up).normalized;
        }
    }

    public abstract class CameraRuleBase : GameplayOnlyMonoBehaviour
    {
        public Transform playerObject;

        public float distanceFromObject = 18f;
        public float smoothSpeed = 2f;

        protected PlayerMovementMetrics playerMovementMetrics;
        protected CameraMovementInput cameraMovementInput;

        protected bool previousRuleEnabledState = false;

        protected bool isRuleEnabled = true;

        protected RayCollider rayCollider;

        public List<PortalRenderingBehaviour> portals = new List<PortalRenderingBehaviour>();

        protected Vector3 playerForwardDirectionProjected
        {
            get
            {
                return PlayerPortalsHelper
                    .GetProjectedDirectionThroughPortalIfCloserThanDirect(
                        this.transform.position, this.playerPositionToLookAt,
                        (this.playerPositionToLookAt - this.transform.position).normalized, this.portals
                    );
            }
        }

        protected float segmentedDistanceToPlayerProjectedPosition
        {
            get
            {
                var potentialPortalInfo = PlayerPortalsHelper
                    .GetClosestGameObjectPositionConsideringPortalsFrom(
                    this.transform.position, this.playerObject.position, this.portals);
                if (potentialPortalInfo.Item2 == null)
                {
                    return Vector3.Distance(this.transform.position, this.playerPositionToLookAt);
                } else
                {
                    return Vector3.Distance(
                        this.transform.position, potentialPortalInfo.Item2.GetPortalCollider().ClosestPoint(this.transform.position))
                        +
                        Vector3.Distance(
                            potentialPortalInfo.Item2.portalController.portalParams
                            .linkedPortal.controllerContext.renderingBehaviour
                            .GetPortalCollider().ClosestPoint(this.playerObject.transform.position),
                            this.playerObject.transform.position
                        );
                }
            }
        }

        protected Vector3 playerPositionToLookAt { 
            get {
                return 
                    PlayerPortalsHelper
                        .GetClosestGameObjectPositionConsideringPortalsFrom(
                            this.transform.position, this.playerObject.position, this.portals).Item1;
            }
        }

        public void DisableRule()
        {
            this.isRuleEnabled = false;
        }

        public void EnableRule()
        {
            this.isRuleEnabled = true;
        }

        protected void HandleCameraTooFarAway()
        {
            if (this.segmentedDistanceToPlayerProjectedPosition > this.distanceFromObject * 2f)
            {
                this.transform.position = this.playerObject.transform.position;
            }
        }

        protected virtual void OnRuleEnter() { }

        protected virtual void Awake()
        {
            this.playerMovementMetrics = FindObjectsOfType<PlayerMovementMetrics>()[0];
            this.cameraMovementInput = FindObjectOfType<CameraMovementInput>();
            this.rayCollider = GetComponent<RayCollider>();
            this.portals = FindObjectsOfType<PortalRenderingBehaviour>().ToList();
        }

        public Vector3 ProjectPositionPotentiallyLookingThroughThePortal(Vector3 positionWorldSpace)
        {
            var info = PlayerPortalsHelper.GetClosestGameObjectPositionConsideringPortalsFrom(
                this.transform.position,
                positionWorldSpace,
                this.portals);

            return info.Item1;
        }

        protected override void GameplayFixedUpdate()
        {
            bool currentRuleEnabledState = this.playerMovementMetrics.cameraRule.Equals(GetCameraRuleEnum());
            if (currentRuleEnabledState && !this.previousRuleEnabledState)
            {
                OnRuleEnter();
            }

            if (this.isRuleEnabled && this.playerMovementMetrics.cameraRule.Equals(GetCameraRuleEnum()))
            {
                OnRuleFixedUpdate();
            }

            this.previousRuleEnabledState = currentRuleEnabledState;
        }

        protected abstract CameraRuleEnum GetCameraRuleEnum();

        protected abstract void OnRuleFixedUpdate();

        protected bool IsLookingThroughPortal()
        {
            return PlayerPortalsHelper.GetClosestGameObjectPositionConsideringPortalsFrom(
                this.transform.position,
                this.playerObject.transform.position,
                this.portals).Item2 != null;
        }

        protected void UpdatePlayerMovementMetrics(Vector3 forwardVector)
        {
            // var refinedForwardVector = forwardVector;
            var refinedForwardVector =
                PlayerPortalsHelper.GetProjectedDirectionThroughPortalIfCloserThanDirect(
                    this.transform.position,
                    this.playerObject.transform.position, forwardVector, this.portals);

            //if (!IsLookingThroughPortal())
            //{
                this.playerMovementMetrics.UpdateForwardDirectionAndRightDirection(
                    refinedForwardVector,
                    Vector3.SignedAngle(Vector3.forward, refinedForwardVector, Vector3.up));
            //}
        }
    }
}
