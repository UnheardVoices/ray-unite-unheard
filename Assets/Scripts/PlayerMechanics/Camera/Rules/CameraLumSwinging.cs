﻿using Assets.Scripts.Animations;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public class CameraLumSwinging : CameraRuleBase
    {
        protected GameObject animatedPlayerPart;
        protected GameObject lumForSwinging;

        protected override CameraRuleEnum GetCameraRuleEnum()
        {
            return CameraRuleEnum.CAMERA_LUM_SWINGING;
        }

        protected override void Awake()
        {
            base.Awake();
            this.animatedPlayerPart = this.playerObject.GetComponentInChildren<AnimationController>().gameObject;
        }

        public void SetLumForSwinging(GameObject lumForSwinging)
        {
            this.lumForSwinging = lumForSwinging;
        }

        protected override void OnRuleFixedUpdate()
        {
            HandleCameraTooFarAway();

            Vector3 lookDirection = (this.playerPositionToLookAt - this.transform.position).normalized;
            UpdatePlayerMovementMetrics(Vector3.ProjectOnPlane(lookDirection, Vector3.up).normalized);
            this.transform.forward = Vector3.Lerp(this.transform.forward, lookDirection, this.smoothSpeed);

            Vector3 playerLastPosition;
            playerLastPosition =
                this.playerPositionToLookAt + Vector3.up +
                (this.animatedPlayerPart.transform.forward) * this.distanceFromObject;
            playerLastPosition.y = this.playerPositionToLookAt.y + this.distanceFromObject / 2.5f;

            Vector3 newDesiredCameraPosition = Vector3.Lerp(this.transform.position, playerLastPosition, this.smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - this.transform.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                this.gameObject, this.rayCollider, cameraTranslation.normalized, cameraTranslation);
            this.transform.position = this.transform.position + cameraTranslation;
            //transform.position = Vector3.Lerp(transform.position, playerLastPosition, smoothSpeed);
        }
    }
}
