using Assets.Scripts.Engine.MainMenuOverlay.Controls.Controls.Camera;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts
{
    public class CameraLedgeGrab : CameraRuleBase
    {
        [HideInInspector] public bool blockCameraFollow;

        private float cameraControlSpeed = 3f;
        private float smoothSpeedCameraRotation = 0f;

        private float cameraMaxHeightOffset = 3f;
        private float cameraMinHeightOffset = -3f;

        private float cameraHeightOffset = 0f;

        private float cameraHeightOffsetChangingStep = 0.15f;

        private float lastHorizontalDistanceToPlayer = 0f;

        protected override CameraRuleEnum GetCameraRuleEnum()
        {
            return CameraRuleEnum.CAMERA_LEDGE_GRAB;
        }

        protected override void OnRuleEnter()
        {
            this.cameraHeightOffset = 0f;
            this.lastHorizontalDistanceToPlayer = Vector3.ProjectOnPlane(
                this.playerPositionToLookAt - this.transform.position, Vector3.up
                ).magnitude;
        }

        protected override void OnRuleFixedUpdate()
        {
            //HandleCameraTooFarAway();

            if (this.blockCameraFollow)
            {
                return;
            }

            Vector2 translation = this.cameraMovementInput.GetTranslation();

            Vector2 horizontalTranslation = new Vector2(translation.x, 0f);
            Vector2 verticalTranslation = new Vector2(0f, translation.y);

            //Vector2 verticalTranslation = Vector3.Project(translation, Vector3.up);
            //Vector2 horizontalTranslation = Vector3.Project(translation, Vector3.right);

            float cameraRotationSpeedMultiplyingFactor =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.CAMERA_ROTATION_SENSITIVITY_PLAYER_PREFS_KEY,
                    CameraSettingsUtilsInfo.DEFAULT_CAMERA_ROTATION_SENSITIVITY) /
                (float)CameraSettingsUtilsInfo.DEFAULT_CAMERA_ROTATION_SENSITIVITY;

            bool invertHorizontalAxis =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.INVERT_CAMERA_HORIZONTAL_AXIS_PLAYER_PREFS_KEY, 0)
                    == 1;
            bool invertVerticalAxis =
                PlayerPrefs.GetInt(
                    CameraSettingsUtilsInfo.INVERT_CAMERA_VERTICAL_AXIS_PLAYER_PREFS_KEY, 0)
                    == 1; ;

            Vector3 playerInputCameraTranslation;

            Vector3 horizontalTranslationPart =
                    (this.transform.right * horizontalTranslation.magnitude);

            if (invertHorizontalAxis)
            {
                horizontalTranslationPart = -horizontalTranslationPart;
            }

            if (Vector3.Angle(Vector3.right, horizontalTranslation) < 90f)
            {
                playerInputCameraTranslation =
                    horizontalTranslationPart;
            }
            else
            {
                playerInputCameraTranslation =
                    -horizontalTranslationPart;
            }

            float verticalTranslationPart =
                verticalTranslation.magnitude
                        * this.cameraHeightOffsetChangingStep *
                        cameraRotationSpeedMultiplyingFactor;

            bool verticalAngleCriteria = invertVerticalAxis ?
                Vector3.Angle(Vector3.up, verticalTranslation) >= 90f
                :
                Vector3.Angle(Vector3.up, verticalTranslation) < 90f;

            if (verticalAngleCriteria && verticalTranslation.magnitude > 0.01f
                && this.lastHorizontalDistanceToPlayer > 5f)
            {
                // go up
                Vector3 directionToCamera = (this.transform.position - this.playerPositionToLookAt).normalized;
                if (this.cameraHeightOffset < this.cameraMaxHeightOffset)
                {
                    //playerInputCameraTranslation += (transform.up * verticalTranslation.magnitude);
                    this.cameraHeightOffset += verticalTranslationPart;
                }
            }
            else if (verticalTranslation.magnitude > 0.01f)
            {
                // go down
                Vector3 directionToCamera = (this.transform.position - this.playerPositionToLookAt).normalized;
                if (this.cameraHeightOffset > this.cameraMinHeightOffset)
                {
                    //playerInputCameraTranslation += -(transform.up * verticalTranslation.magnitude)
                    //    * cameraControlSpeed;
                    this.cameraHeightOffset -= verticalTranslationPart;
                }
            }

            if (this.lastHorizontalDistanceToPlayer <= 8f && this.cameraHeightOffset > 0f)
            {
                this.cameraHeightOffset -= this.cameraHeightOffsetChangingStep / 2f;
            }

            playerInputCameraTranslation *= this.cameraControlSpeed * cameraRotationSpeedMultiplyingFactor;

            if (playerInputCameraTranslation.magnitude > 0.01f)
            {
                this.smoothSpeedCameraRotation = Mathf.Lerp(this.smoothSpeedCameraRotation, 1f, 0.01f);
            }
            else
            {
                this.smoothSpeedCameraRotation = Mathf.Lerp(this.smoothSpeedCameraRotation, this.smoothSpeed, 0.01f);
            }

            Vector3 lookDirection = ((this.playerPositionToLookAt - this.transform.position)
                .normalized + Vector3.up * 0.05f).normalized;
            UpdatePlayerMovementMetrics(new Vector3(lookDirection.x, 0, lookDirection.z));
            this.transform.forward = Vector3.Lerp(this.transform.forward, lookDirection,
                playerInputCameraTranslation.magnitude > 0.01f ? this.smoothSpeedCameraRotation : this.smoothSpeed);

            Vector3 playerLastPosition;
            playerLastPosition = this.playerPositionToLookAt + playerInputCameraTranslation - (lookDirection.normalized * this.distanceFromObject);
            playerLastPosition.y = this.playerPositionToLookAt.y + this.distanceFromObject / 2.5f + this.cameraHeightOffset;

            Vector3 newDesiredCameraPosition = Vector3.Lerp(this.transform.position, playerLastPosition, this.smoothSpeed);
            Vector3 cameraTranslation = newDesiredCameraPosition - this.transform.position;

            cameraTranslation = WallCollisionHelper.GetMovementVelocityConsideringSolidEnvironmentCollisions(
                this.gameObject, this.rayCollider, cameraTranslation.normalized, cameraTranslation);
            this.transform.position = this.transform.position + cameraTranslation;

            this.lastHorizontalDistanceToPlayer = Vector3.ProjectOnPlane(
                this.playerPositionToLookAt - this.transform.position, Vector3.up
                ).magnitude;
        }
    }
}