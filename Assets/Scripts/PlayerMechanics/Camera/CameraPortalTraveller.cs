﻿using Assets.Scripts.Common;
using Assets.Scripts.GameMechanics.Entities.NewPlayer.Physics.Utils;
using Assets.Scripts.GameMechanics.Entities.Portal;
using Assets.Scripts.GameMechanics.Portals;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraPortalTraveller : PortalTraveller
    {
        //protected override bool CheckPortalCollision(out RaycastHit portalThresholdHit)
        //{
        //    portalThresholdHit = new RaycastHit();
        //    return false;
        //}

        //protected override bool CheckPortalCollision(out RaycastHit portalThresholdHit)
        //{
        //    return Raycaster.CylinderRaycasterSingle(
        //           this.transform.position, Vector3.forward,
        //            0.5f, 1f, 1, 100, out portalThresholdHit, Layers.portalsLayerMask, debug: true, debugColor: Color.yellow);
        //}

        public override void Teleport(Transform fromPortal, Transform toPortal, Vector3 position, Quaternion rotation)
        {
            //fromPortal.GetComponent<PortalRenderingBehaviour>().ProtectScreenFromClipping(position);
            toPortal.GetComponent<PortalRenderingBehaviour>().ProtectScreenFromClipping(position);
            base.Teleport(fromPortal, toPortal, position, rotation);
            //toPortal.GetComponent<PortalRenderingBehaviour>().ProtectScreenFromClipping(position);
        }

        protected override void OnPortalTravel(Transform fromPortal, Transform toPortal)
        {
            RedoForwardDirection(
                fromPortal.GetComponent<PortalRenderingBehaviour>(),
                toPortal.GetComponent<PortalRenderingBehaviour>());            
        }

        public void RedoForwardDirection(PortalRenderingBehaviour fromPortal, PortalRenderingBehaviour toPortal)
        {
            //var fromPortalRend = fromPortal.GetComponent<PortalRenderingBehaviour>();
            //var toPortalRend = toPortal.GetComponent<PortalRenderingBehaviour>();

            //var forward = PlayerPortalsHelper.GetProjectedDirectionThroughPortalIfCloserThanDirect(
            //    this.transform.position, FindObjectOfType<PlayerActorHandle>().transform.position,
            //    FindObjectOfType<PlayerActorHandle>().transform.position - this.transform.position,
            //    GetComponent<CameraRuleBase>().portals);

            //FindObjectOfType<PlayerController>().O

            //this.transform.forward =
            //    toPortalRend.TransformDirection(fromPortalRend.InverseTransformDirection(this.transform.forward));

            var linkedEntrancePortal = fromPortal.GetComponent<PortalController>()
                .portalParams.linkedPortal.GetComponent<PortalRenderingBehaviour>();

            var forward = Vector3.ProjectOnPlane(
                (FindObjectOfType<PlayerActorHandle>().transform.position - this.transform.position).normalized, Vector3.up).normalized;

            //var forward = Vector3.ProjectOnPlane(linkedEntrancePortal.transform.TransformDirection(portal.transform.InverseTransformDirection(
            //    FindObjectOfType<PlayerActorHandle>().transform.position - this.transform.position).normalized).normalized, Vector3.up).normalized;

            FindObjectOfType<PlayerMovementMetrics>().UpdateForwardDirectionAndRightDirection(
                forward,
                Vector3.SignedAngle(Vector3.forward, forward, Vector3.up));

            FindObjectOfType<PlayerMovementMetrics>().intentionalMovingDirection = forward;           
        }
    }
}
