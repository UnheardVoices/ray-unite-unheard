﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public class ShootingLeftHandAnimationConstructingRecipeImpl : IAnimConstrRecipe
    {
#if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe(
            Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip)
        {
            AnimationClip result = new AnimationClip
            {
                name = newAnimationClipName,
                legacy = false
            };

            BonesLocationRotationScaleCurves rightHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetRightHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip);

            BonesLocationRotationScaleCurves leftHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetLeftHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip);

            rightHandAnimationCurves = rightHandAnimationCurves.RemoveKeyframesAfterKeyframeNumber(9);
            leftHandAnimationCurves = leftHandAnimationCurves.RemoveKeyframesAfterKeyframeNumber(9);

            string pathPrefix = AnimationUtility.CalculateTransformPath(
                animatorBonesTransforms[1], animatorTransform).Replace(animatorBonesTransforms[1].name, "");

            Tuple<
                BonesLocationRotationScaleCurves,
                BonesLocationRotationScaleCurves
                > mirroredHandsCurves = RaymanAnimationClipsCurvesHelper
                    .MirrorHandsCurvesTransformsWithEachOther(
                        pathPrefix, leftHandAnimationCurves, rightHandAnimationCurves);

            AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(mirroredHandsCurves.Item1, result);
            //AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(mirroredHandsCurves.Item2, result);

            return result;
        }
#endif
    }
}
