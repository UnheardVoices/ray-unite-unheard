﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public class ShootingRightHandAnimationConstructingRecipeImpl : IAnimConstrRecipe
    {
#if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe(
            Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip)
        {
            AnimationClip result = new AnimationClip
            {
                name = newAnimationClipName
            };

            BonesLocationRotationScaleCurves rightHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetRightHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip);

            BonesLocationRotationScaleCurves leftHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetLeftHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip);

            rightHandAnimationCurves = rightHandAnimationCurves.RemoveKeyframesAfterKeyframeNumber(9);
            leftHandAnimationCurves = leftHandAnimationCurves.RemoveKeyframesAfterKeyframeNumber(9);

            AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(rightHandAnimationCurves, result);
            //AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(leftHandAnimationCurves, result);

            return result;
        }
#endif
    }
}
