﻿using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model
{
    public class AdditionalAnimationClipInfo
    {
        public int animationClipLayerIndex;
        public AnimationClip animationClip;
        public float playingSpeed;

        public AdditionalAnimationClipInfo(
            int animationClipLayerIndex,
            float playingSpeed,
            AnimationClip animationClip)
        {
            this.animationClipLayerIndex = animationClipLayerIndex;
            this.playingSpeed = playingSpeed;
            this.animationClip = animationClip;
        }
    }
}
