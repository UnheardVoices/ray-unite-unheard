﻿namespace Assets.Scripts.Animations.Models
{
    public struct AnimationInfo
    {
        public string animationName;
        public bool isLooped;
        public float playingSpeed;

        public AnimationInfo(string animationName, bool isLooped, float playingSpeed)
        {
            this.animationName = animationName;
            this.isLooped = isLooped;
            this.playingSpeed = playingSpeed;
        }
    }
}
