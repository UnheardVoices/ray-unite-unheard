﻿using Assets.Scripts.Animations.Models.Rayman.Recipes;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model
{
    public class AnimationClipConstructingRecipe
    {
#if (UNITY_EDITOR)
        public string baseAnimationClipName;
        public IAnimConstrRecipe animationConstructionRecipeImplementation;
        public string newAnimationClipName;
        public int animationClipLayerIndex;
        public float animationSpeed;

        public AnimationClipConstructingRecipe(
            string baseAnimationClipName,
            IAnimConstrRecipe animationConstructionRecipeImplementation,
            string newAnimationClipName,
            float animationSpeed,
            int animationClipLayerIndex)
        {
            this.baseAnimationClipName = baseAnimationClipName;
            this.animationConstructionRecipeImplementation = animationConstructionRecipeImplementation;
            this.newAnimationClipName = newAnimationClipName;
            this.animationClipLayerIndex = animationClipLayerIndex;
            this.animationSpeed = animationSpeed;
        }


        public AdditionalAnimationClipInfo GetDerivedAnimationClipAccordingToRecipe
            (Transform animatorTransform, List<Transform> animatorBonesTransforms, AnimationClip baseAnimationClip)
        {
            AnimationClip newAnimationClip = this.animationConstructionRecipeImplementation
                .GetDerivedAnimationClipAccordingToRecipe(
                animatorTransform, animatorBonesTransforms, this.newAnimationClipName, baseAnimationClip);

            return new AdditionalAnimationClipInfo(
                this.animationClipLayerIndex, this.animationSpeed, newAnimationClip);
        }
#endif
    }
}
