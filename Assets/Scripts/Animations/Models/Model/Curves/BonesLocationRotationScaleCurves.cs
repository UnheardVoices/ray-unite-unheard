﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model.Curves
{
    public class BoneKeyframeTransformBuilder
    {
        private BoneKeyframeTransform result = new BoneKeyframeTransform();

        public BoneKeyframeTransformBuilder WithPosition(Vector3 position)
        {
            this.result.position = position;
            return this;
        }

        public BoneKeyframeTransformBuilder WithRotation(Quaternion rotation)
        {
            this.result.rotation = rotation;
            return this;
        }

        public BoneKeyframeTransformBuilder WithScale(Vector3 scale)
        {
            this.result.scale = scale;
            return this;
        }

        public BoneKeyframeTransform Build()
        {
            return this.result;
        }
    }

    public class BonesLocationRotationScaleCurves
    {
#if (UNITY_EDITOR)
        private Dictionary<string, BoneTransTimeline> bonesKeyframes
            = new Dictionary<string, BoneTransTimeline>();

        public BonesLocationRotationScaleCurves RemoveKeyframesAfterKeyframeNumber(
            int keyframeNumber)
        {
            //return this;
            BonesLocationRotationScaleCurves result = new BonesLocationRotationScaleCurves();
            foreach (KeyValuePair<string, BoneTransTimeline> timelineItem in this.bonesKeyframes)
            {
                BoneTransTimeline newBoneTransTimeline = new BoneTransTimeline
                {
                    keyframes =
                    timelineItem.Value.keyframes
                    .Where(x => x.Key <= keyframeNumber)
                    .ToDictionary(x => x.Key, x => x.Value)
                };
                result.bonesKeyframes.Add(
                    timelineItem.Key,
                    newBoneTransTimeline);
            }
            return result;
        }

        public static BonesLocationRotationScaleCurves
            WithBonesKeyframesTimelines(
                Dictionary<string, BoneTransTimeline> bonesCurves)
        {
            BonesLocationRotationScaleCurves result = new BonesLocationRotationScaleCurves
            {
                bonesKeyframes = bonesCurves
            };
            return result;
        }

        public List<
            Tuple<string, EditorCurveBinding, AnimationCurve>>
            GetUnityAnimationCurvesWithEditorCurveBindings()
        {
            List<Tuple<string, EditorCurveBinding, AnimationCurve>> result = new List<Tuple<string, EditorCurveBinding, AnimationCurve>>();
            int totalFrames = GetTotalFrames();
            foreach (KeyValuePair<string, BoneTransTimeline> boneTimelineItem in this.bonesKeyframes)
            {
                string path = boneTimelineItem.Key;

                foreach (Tuple<AnimationCurve, EditorCurveBinding> unityAnimationCurveWithBindingInfo in
                    boneTimelineItem.Value.GetUnityAnimationCurvesWithBindingInfos(path, totalFrames))
                {
                    EditorCurveBinding editorCurveBinding = unityAnimationCurveWithBindingInfo.Item2;
                    AnimationCurve animationCurve = unityAnimationCurveWithBindingInfo.Item1;

                    result.Add(
                        Tuple.Create(
                            path,
                            editorCurveBinding,
                            animationCurve
                        ));
                }
            }

            return result;
        }

        public int GetTotalFrames()
        {
            return this.bonesKeyframes.First().Value.GetFramesCount();
        }

        public BonesLocationRotationScaleCurves ReflectViaWorldVector3RightNormal()
        {
            BonesLocationRotationScaleCurves result = new BonesLocationRotationScaleCurves();
            foreach (KeyValuePair<string, BoneTransTimeline> boneTimelineItem in this.bonesKeyframes)
            {
                result.bonesKeyframes.Add(
                    boneTimelineItem.Key,
                    boneTimelineItem.Value.ReflectViaWorldVector3RightNormal());
            }

            return result;
        }

        public BonesLocationRotationScaleCurves MapPaths(Dictionary<string, string> pathsMappings)
        {
            BonesLocationRotationScaleCurves result = new BonesLocationRotationScaleCurves();
            foreach (KeyValuePair<string, BoneTransTimeline> boneTimelineItem in this.bonesKeyframes)
            {
                string mappedBonePath = pathsMappings[boneTimelineItem.Key];
                result.bonesKeyframes.Add(mappedBonePath, boneTimelineItem.Value);
            }
            return result;
        }

        public BonesLocationRotationScaleCurves ZeroOutScaleKeyframes()
        {
            BonesLocationRotationScaleCurves result = new BonesLocationRotationScaleCurves();
            foreach (KeyValuePair<string, BoneTransTimeline> boneTimelineItem in this.bonesKeyframes)
            {
                result.bonesKeyframes.Add(boneTimelineItem.Key,
                    boneTimelineItem.Value.ZeroOutScaleKeyframes());
            }
            return result;
        }
#endif
    }
}
