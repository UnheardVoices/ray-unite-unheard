﻿using Assets.Scripts.Engine.Behaviours;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations
{
    public class AnimationController : GameplayOnlyMonoBehaviour
    {
        private Animator animator;
        private bool animatorIsUpdating = true;

        private Dictionary<int, string>
            currentStatesOnLayers = new Dictionary<int, string>();

        private Dictionary<int, int>
            currentPrioritiesOnLayers = new Dictionary<int, int>();

        private float registeredUpdateDeltaTime = 0f;

        private void Awake()
        {
            this.animator = GetComponent<Animator>();

            this.currentStatesOnLayers.Add(0, "");
            this.currentStatesOnLayers.Add(1, "");
            this.currentStatesOnLayers.Add(2, "");

            this.currentPrioritiesOnLayers.Add(0, 0);
            this.currentPrioritiesOnLayers.Add(1, 0);
            this.currentPrioritiesOnLayers.Add(2, 0);

            this.animator.speed = 0f;
        }

        public void StopAnimatorUpdating()
        {
            this.animatorIsUpdating = false;
            this.animator.speed = 0f;
        }

        public void ResumeAnimatorUpdating()
        {
            this.animatorIsUpdating = true;
        }

        public void ManualGameplayFixedUpdate()
        {
            if (Time.fixedDeltaTime < this.registeredUpdateDeltaTime && this.animatorIsUpdating)
            {
                this.animator.speed = 1f;
                this.animator.Update(Time.fixedDeltaTime);
                this.animator.speed = 0f;
            }
        }

        public void ManualGameplayUpdate(float deltaTime)
        {
            this.registeredUpdateDeltaTime = deltaTime;

            if (this.registeredUpdateDeltaTime <= Time.fixedDeltaTime && this.animatorIsUpdating)
            {
                this.animator.speed = 1f;
                this.animator.Update(deltaTime);
                this.animator.speed = 0f;
            }

            List<int> layersIndexes = new List<int>(this.currentStatesOnLayers.Keys);
            foreach (int layerIndex in layersIndexes)
            {
                if (layerIndex > 0)
                {
                    AnimatorClipInfo[] clipsAtLayer = this.animator.GetCurrentAnimatorClipInfo(layerIndex);
                    if (clipsAtLayer.Length > 0)
                    {
                        this.currentStatesOnLayers[layerIndex] =
                            this.animator.GetCurrentAnimatorClipInfo(layerIndex)[0].clip.name;
                    }
                    else
                    {
                        this.currentStatesOnLayers[layerIndex] = "";
                    }
                }
            }
        }

        public string GetCurrentAnimationState(int layerIndex = 0)
        {
            return this.currentStatesOnLayers[layerIndex];
        }

        public bool HasAnimationEnded(int layerIndex = 0)
        {
            return this.animator.GetCurrentAnimatorStateInfo(layerIndex).normalizedTime > 1
                && !this.animator.IsInTransition(layerIndex);
        }

        public float GetAnimationNormalizedTime(int layerIndex = 0)
        {
            return this.animator.GetCurrentAnimatorStateInfo(layerIndex).normalizedTime;
        }

        public void ChangeAnimationStateWithPriority(string newState, int layerIndex = 0, int priority = 1)
        {
            //stop the same animation from interrupting itself unless it has now higher priority
            if (this.currentStatesOnLayers[layerIndex].Equals(newState) &&
                this.currentPrioritiesOnLayers[layerIndex] >= priority)
            {
                return;
            }

            if (priority >= this.currentPrioritiesOnLayers[layerIndex] || HasAnimationEnded(layerIndex))
            {
                //play the animation
                this.animator.Play(newState);

                //reassign the current state
                this.currentStatesOnLayers[layerIndex] = newState;
                this.currentPrioritiesOnLayers[layerIndex] = priority;
            }
        }

        public void ChangeAnimationState(string newState, int layerIndex = 0)
        {
            ChangeAnimationStateWithPriority(newState, layerIndex, 1);
        }

        public void ChangeAnimationPriorityIfItIsBeingPlayed(
            string stateName, int newPriority, int layerIndex = 0)
        {
            if (this.currentStatesOnLayers[layerIndex].Equals(stateName))
            {
                this.currentPrioritiesOnLayers[layerIndex] = newPriority;
            }
        }

        public void SetAnimationFrameInManualUpdating
            (string stateName, float normalizedTime, float deltaTime, int layerIndex = 0)
        {
            this.animator.speed = 1f;
            this.animator.Play(stateName, layerIndex, normalizedTime);
            this.animator.Update(deltaTime);
            this.animator.speed = 0f;
        }
    }
}
