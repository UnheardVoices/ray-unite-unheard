﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations
{
    public class AnimationClipHelper
    {
#if (UNITY_EDITOR)
        public static void AddLocationRotationScaleCurvesForAnimationClip
            (BonesLocationRotationScaleCurves locationRotationScaleCurves, AnimationClip animationClip)
        {
            foreach (Tuple<string, EditorCurveBinding, AnimationCurve> animationCurveWithBindingInfo in
                locationRotationScaleCurves.GetUnityAnimationCurvesWithEditorCurveBindings())
            {
                string path = animationCurveWithBindingInfo.Item1;
                EditorCurveBinding editorCurveBinding = animationCurveWithBindingInfo.Item2;
                AnimationCurve animationCurve = animationCurveWithBindingInfo.Item3;
                animationClip.SetCurve(
                    path,
                    editorCurveBinding.type,
                    editorCurveBinding.propertyName,
                    animationCurve
                    );
            }
        }
#endif
    }
}
