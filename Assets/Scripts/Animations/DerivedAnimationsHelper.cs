﻿using Assets.Scripts.Animations.Models.Model;
using Assets.Scripts.EditorOnly.AssetsCreation.Animator;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Animations
{
    public static class DerivedAnimationsHelper
    {
#if (UNITY_EDITOR)
        public static List<AdditionalAnimationClipInfo>
            GetProgrammaticallyDerivedAndConstructedAdditionalAnimationClips(
            string resourcesModelPath,
            List<AnimationClipConstructingRecipe> animationClipConstructingRecipes,
            Transform animatorTransform, List<Transform> animatorBonesTransforms)
        {
            List<AnimationClip> baseAnimationClips = AnimatorFactory.GetBaseAnimationClipsAsImportedFromModel(resourcesModelPath);

            List<AdditionalAnimationClipInfo> result = new List<AdditionalAnimationClipInfo>();

            foreach (AnimationClipConstructingRecipe animClipConstrRecipe in animationClipConstructingRecipes)
            {
                AnimationClip appropriateAnimationClip =
                    baseAnimationClips.Where(x => x.name.Equals(animClipConstrRecipe.baseAnimationClipName)).First();
                result.Add(
                    animClipConstrRecipe
                        .GetDerivedAnimationClipAccordingToRecipe(
                        animatorTransform, animatorBonesTransforms, appropriateAnimationClip));
            }

            return result;
        }
#endif
    }
}
