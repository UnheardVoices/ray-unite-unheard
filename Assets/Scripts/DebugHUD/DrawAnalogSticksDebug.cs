﻿using Assets.Scripts.Engine.Input;
using System;
using UnityEngine;

namespace Assets.Scripts.DebugHUD
{
    public static class AnalogCoordsHelper
    {
        public static Tuple<float, float> GetCoords(
            float originX, float originY,
            float areaRadius,
            float analogStickWidth, float analogStickHeight,
            float horizontalInputValue, float verticalInputValue)
        {
            //float centerX = originX + areaWidth / 2f;
            //float centerY = originY + areaHeight / 2f;

            float mappedXOffset = horizontalInputValue * (areaRadius);
            float mappedYOffset = -(verticalInputValue * (areaRadius));

            Vector2 offset = new Vector2(mappedXOffset, mappedYOffset);
            offset = Vector2.ClampMagnitude(offset, areaRadius);


            float x = (originX + offset.x) - analogStickWidth / 2f;
            float y = (originY + offset.y) - analogStickHeight / 2f;


            return Tuple.Create(x, y);
        }
    }

    public class DrawAnalogSticksDebug : MonoBehaviour
    {
        private float positionX;
        private float positionY;

        private float marginRight = 300;
        private float marginTop = 300;

        public Texture2D analogStickAreaTexture;
        public Texture2D circleTexture;

        //private float width = 100;
        //private float height = 100;

        private float analogStickWidth = 60f;
        private float analogStickHeight = 60f;

        private float areaRadius = 100f;
        private float areaCenterX;
        private float areaCenterY;
        private float horizontalInputValue;
        private float verticalInputValue;

        protected PlayerInputHub playerInputHub;

        private void Awake()
        {
            this.positionX = Screen.width - (2 * this.areaRadius) - this.marginRight;
            this.positionY = 50 + this.marginTop;

            this.areaCenterX = this.positionX + this.areaRadius;
            this.areaCenterY = this.positionY + this.areaRadius;
        }

        private void Start()
        {
            this.playerInputHub = FindObjectOfType<PlayerInputHub>();
        }

        private void Update()
        {
            this.areaCenterX = this.positionX + this.areaRadius;
            this.areaCenterY = this.positionY + this.areaRadius;

            this.horizontalInputValue = this.playerInputHub.moveInputRaw.x;
            this.verticalInputValue = this.playerInputHub.moveInputRaw.y;
        }

        private void OnGUI()
        {
            Tuple<float, float> analogStickCoords = AnalogCoordsHelper.GetCoords(
                this.areaCenterX, this.areaCenterY, this.areaRadius, this.analogStickWidth, this.analogStickHeight,
                this.horizontalInputValue, this.verticalInputValue);

            Rect analogStickAreaRect = new Rect(this.positionX, this.positionY, 2 * this.areaRadius, 2 * this.areaRadius);
            Rect analogStickRect = new Rect(analogStickCoords.Item1, analogStickCoords.Item2, this.analogStickWidth, this.analogStickHeight);
            GUI.DrawTexture(analogStickAreaRect, this.analogStickAreaTexture);
            GUI.DrawTexture(analogStickRect, this.circleTexture);
        }
    }
}
